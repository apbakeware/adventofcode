#include "pch.h"
#include <sstream>
#include <string>
#include <vector>
#include "gtest/gtest.h"
#include "../Solutions/Rectangle.h"

//namespace {

struct TestCaseParameters {
   std::string input_line;
   types::Rectangle expected;   
};

// https://adventofcode.com/2018/day/3
const std::vector<TestCaseParameters> TEST_CASES = {
   {"#1 @ 1,3: 4x4", types::Rectangle::from_origin_and_size(1, 3, 4, 4)},
   {"#2 @ 3,1: 4x4", types::Rectangle::from_origin_and_size(3, 1, 4, 4)},
   {"#3 @ 5,5: 2x2", types::Rectangle::from_origin_and_size(5, 5, 2, 2)}
};

//}

std::ostream& operator<<(std::ostream& str, const TestCaseParameters& obj) {
   str << "Input line: [" << obj.input_line << "] --> " << obj.expected;
   return str;
}

class RectangleInputTest : public testing::TestWithParam<TestCaseParameters> {

};

TEST(RectangleTest, ConstructFromOriginAndSize){
   const types::Rectangle expected(4, 5, 7, 9);
   auto const sut = types::Rectangle::from_origin_and_size(4, 5, 3, 4);

   EXPECT_EQ(expected, sut);
}
TEST(RectangleTest, Area){
   const types::Rectangle sut(0, 0, 4, 5);
   const int expected = 4 * 5;
   EXPECT_EQ(expected, sut.area());
}

TEST(RectangleTest, IntersectionWithNoIntersection){
   const types::Rectangle first(1, 1, 2, 2);
   const types::Rectangle second(3, 3, 4, 4);

   auto const sut = types::intersection(first, second);
   
   EXPECT_EQ(0, sut.area());
}

TEST(RectangleTest, IntersectionSameObjectIsTheObjectArea){
   const types::Rectangle obj(1, 1, 5, 5);

   auto const sut = types::intersection(obj, obj);

   EXPECT_EQ(16, sut.area());
}

TEST(RectangleTest, IntersectionWithPartialOverlap){
   const types::Rectangle r1(1, 1, 5, 5);
   const types::Rectangle r2(3, 3, 6, 6);
   const int expected_area = 4;

   auto const sut = types::intersection(r1, r2);

   std::cout << sut << "\n";

   EXPECT_EQ(expected_area, sut.area());
}

TEST(RectangleTest, Day03SampleData){
   std::string s1 = "#1 @ 1,3: 4x4";
   std::string s2 = "#2 @ 3,1: 4x4";
   std::string s3 = "#3 @ 5,5: 2x2";
   
   types::Rectangle r1;
   types::Rectangle r2;
   types::Rectangle r3;

   std::istringstream str(s1);
   str >> r1;

   std::istringstream str2(s2);
   str2 >> r2;

   std::istringstream str3(s3);
   str3 >> r3;

   EXPECT_TRUE(types::intersects(r1, r2));
   EXPECT_TRUE(types::intersects(r2, r1));
   EXPECT_FALSE(types::intersects(r1, r3));
   EXPECT_FALSE(types::intersects(r2, r3));
   EXPECT_FALSE(types::intersects(r3, r1));
   EXPECT_FALSE(types::intersects(r3, r2));

}

TEST_P(RectangleInputTest, ProvidedSampleData) {
   // Inside a test, access the test parameter with the GetParam() method
   // of the TestWithParam<T> class:
   
   auto params = GetParam();
   std::istringstream str(params.input_line);
   types::Rectangle sut;

   str >> sut;

  /* EXPECT_EQ(params.expected.id, sut.id);
   EXPECT_EQ(params.expected.origin_x, sut.origin_x);
   EXPECT_EQ(params.expected.origin_y, sut.origin_y);
   EXPECT_EQ(params.expected.width, sut.width);
   EXPECT_EQ(params.expected.height, sut.height);*/

   EXPECT_EQ(params.expected, sut);
   
}

// todo overlap testing

// https://github.com/google/googletest/issues/2065
INSTANTIATE_TEST_CASE_P(
   AocTestCases, 
   RectangleInputTest,
   testing::ValuesIn(TEST_CASES)
);

