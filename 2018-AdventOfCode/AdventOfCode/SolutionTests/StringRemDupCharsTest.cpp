#include "pch.h"
#include "gtest/gtest.h"
#include <string>

#include "../Solutions/Utils.h"


TEST(IsLowerThenUpperTest, TestCase_aA) {
   EXPECT_TRUE(utils::is_lower_then_upper_of_same('a', 'A'));
}

TEST(IsLowerThenUpperTest, TestCase_aa) {
   EXPECT_FALSE(utils::is_lower_then_upper_of_same('a', 'a'));
}

TEST(IsLowerThenUpperTest, TestCase_Aa) {
   EXPECT_FALSE(utils::is_lower_then_upper_of_same('A', 'a'));
}

TEST(IsLowerThenUpperTest, TestCase_AA) {
   EXPECT_FALSE(utils::is_lower_then_upper_of_same('A', 'A'));
}

TEST(IsLowerThenUpperTest, TestCase_aB) {
   EXPECT_FALSE(utils::is_lower_then_upper_of_same('a', 'B'));
}


TEST(StringRemDupChars, StringWithNoDups) {

   const std::string orig = "abcde";

   ASSERT_EQ(orig, utils::remove_dups_with_opposite_case(orig));
}

TEST(StringRemDupChars, StringWithConsecutiveSameCaseAreNotRemoved) {

   const std::string orig = "abccde";

   ASSERT_EQ(orig, utils::remove_dups_with_opposite_case(orig));
}

TEST(StringRemDupChars, StringWithConsecutiveDiffCaseAreRemovedFromBeg) {

   const std::string orig = "aABcdeF";
   const std::string EXP = "BcdeF";

   ASSERT_EQ(EXP, utils::remove_dups_with_opposite_case(orig));
}

TEST(StringRemDupChars, StringWithConsecutiveDiffCaseAreRemovedFromEnd) {

   const std::string orig = "aBcdeFf";
   const std::string EXP = "aBcde";

   ASSERT_EQ(EXP, utils::remove_dups_with_opposite_case(orig));
}

TEST(StringRemDupChars, StringWithConsecutiveDiffCaseAreRemovedFromMid) {

   const std::string orig = "aBcdDef";
   const std::string EXP = "aBcef";

   ASSERT_EQ(EXP, utils::remove_dups_with_opposite_case(orig));
}

TEST(StringRemDupChars, Day05P1TestData) {
   
   const std::string orig = "dabAcCaCBAcCcaDA";
   const std::string EXP = "dabCBAcaDA";

   ASSERT_EQ(EXP, utils::remove_dups_with_opposite_case(orig));
}