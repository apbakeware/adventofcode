#include "pch.h"

#include <vector>
#include <string>
#include "../Solutions/Utils.h"
#include "../Solutions/ChecksumRecord.h"


namespace {

struct TestCaseParameters {
   std::string str1;
   std::string str2;
   std::string exp;

   friend std::ostream& operator<<
      (std::ostream& str, const TestCaseParameters& obj) {
      str << obj.str1 << " | " << obj.str2 << " --> intersection: " << obj.exp;
      return str;
   }
};

// https://adventofcode.com/2018/day/2
const std::vector< TestCaseParameters> TEST_CASES = {
   {"abcde", "abcde", "abcde"},
   {"abcde", "axcye", "ace"},
   {"fghij", "fguij", "fgij"}
};

}

class LinearIntersectionTest : public testing::TestWithParam<TestCaseParameters> {
   // You can implement all the usual fixture class members here.
   // To access the test parameter, call GetParam() from class
   // TestWithParam<T>.
};


TEST_P(LinearIntersectionTest, ProvidedSampleData) {
   // Inside a test, access the test parameter with the GetParam() method
   // of the TestWithParam<T> class:

   std::string intersection;
   auto params = GetParam();
   auto result = utils::linear_intersection(
      params.str1.begin(),
      params.str1.end(),
      params.str2.begin(),
      std::back_inserter(intersection)
   );

   EXPECT_EQ(params.exp, intersection);
}

// https://github.com/google/googletest/issues/2065
INSTANTIATE_TEST_CASE_P(
   AocTestCases, 
   LinearIntersectionTest, 
   testing::ValuesIn(TEST_CASES)
);

