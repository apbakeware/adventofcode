#include "pch.h"
#include "gtest/gtest.h"
#include "../Solutions/Utils.h"
#include "../Solutions/ChecksumRecord.h"

#include <string>

namespace {

struct TestCaseParameters {
   std::string input;
   bool exp_has_double;
   bool exp_has_triple;

   friend std::ostream& operator<<
   (std::ostream& str, const TestCaseParameters& obj) {
      str << "Input [" << obj.input << "] -- exp_has_double: "
         << obj.exp_has_double << " exp_has_triple: " << obj.exp_has_triple;
      return str;
   }
};

// https://adventofcode.com/2018/day/2
const std::vector< TestCaseParameters> TEST_CASES = {
   {"abcdef", false, false},
   {"bababc", true, true},
   {"abbcde", true, false},
   {"abcccd", false, true},
   {"aabcdd", true, false},
   {"abcdee", true, false},
   {"ababab", false, true}
};

}

class AnalyzeCharacterCountTest : public testing::TestWithParam<TestCaseParameters> {
   // You can implement all the usual fixture class members here.
   // To access the test parameter, call GetParam() from class
   // TestWithParam<T>.
};


TEST_P(AnalyzeCharacterCountTest, ProvidedSampleData) {
   // Inside a test, access the test parameter with the GetParam() method
   // of the TestWithParam<T> class:

   auto params = GetParam();
   auto result = types::analyze_checksum(params.input);
   EXPECT_EQ(params.exp_has_double, result.has_double_character);
   EXPECT_EQ(params.exp_has_triple, result.has_triple_character); 
}

// https://github.com/google/googletest/issues/2065
INSTANTIATE_TEST_CASE_P(InstantiationName, AnalyzeCharacterCountTest, testing::ValuesIn(TEST_CASES));

