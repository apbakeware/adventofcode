#include "pch.h"
#include <iostream>
#include "ExpandableWorkspaceCutCalculator.h"
#include "Rectangle.h"

namespace{

const size_t INITIAL_CAPACITY = 1000;

}

namespace types{

ExpandableWorkspaceCutCalculator::
ExpandableWorkspaceCutCalculator() :
   currently_supported_x(0), currently_supported_y(0){

   workspace.reserve(INITIAL_CAPACITY);
   for(auto& row : workspace){
      row.reserve(INITIAL_CAPACITY);
   }
}

bool ExpandableWorkspaceCutCalculator::
workspace_encompasses_cut(const Rectangle& cutout) const{
   return currently_supported_x >= cutout.x2() && 
      currently_supported_y >= cutout.y2();
}

void ExpandableWorkspaceCutCalculator::
cutout(const Rectangle& cutout){
   if(!workspace_encompasses_cut(cutout)){
      enlarge_workspace_to_encompass(cutout);
   }

   for(auto row_idx = cutout.y1(); row_idx < cutout.y2(); ++row_idx){
      for(auto col_idx = cutout.x1(); col_idx < cutout.x2(); ++col_idx){
         workspace.at(row_idx).at(col_idx) += 1;
      }
   }
}

int ExpandableWorkspaceCutCalculator::
get_cutout_overlap_area() const{
   int count = 0;
   for(auto riter = workspace.begin(); riter != workspace.end(); ++riter){
      for(auto citer = riter->begin(); citer != riter->end(); ++citer){
         if(*citer > 1){
            ++count;
         }
      }
   }
   return count;
}

void ExpandableWorkspaceCutCalculator::
enlarge_workspace_to_encompass(const Rectangle& cutout){

   if(cutout.y2() > currently_supported_y){
      // 1 to allow direct indexing.
      currently_supported_y = 1 + cutout.y2();
      workspace.resize(currently_supported_y, 
         std::vector<char>(currently_supported_x + 1, 0));
   }

   if(cutout.x2() > currently_supported_x){
      currently_supported_x = 1 + cutout.x2();
      for(auto& row : workspace){
         row.resize(currently_supported_x, 0);
      }
   }

}

std::ostream& operator<<
(std::ostream& str, const ExpandableWorkspaceCutCalculator& obj){

   str << "  ";
   for(size_t x = 0; x < obj.workspace[0].size(); ++x){
      str << x;
   }
   str << "\n";

   for(size_t y = 0; y < obj.workspace.size(); ++y) {
      const auto& row = obj.workspace[y];
      str << y << "|";
      for(size_t x = 0; x < row.size(); ++x){
         str << int(row[x]);
      }
      str << "\n";
   }
   /*for(auto const& row : obj.workspace){
      for(auto const& val : row){
         str << int(val);
      }
      str << "\n";
   }*/
   return str;
}


}
