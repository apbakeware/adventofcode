#include "pch.h"
#include "Day03_Problem1.h"
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "Rectangle.h"
#include "ExpandableWorkspaceCutCalculator.h"

namespace {
ProblemFactoryRegisterer<Day03_Problem1> _problem("Day03_Problem1");
}

Day03_Problem1::~Day03_Problem1() {

}

std::string Day03_Problem1::name() const {
   return "Day03 - Problem 1";
}

ISolution* Day03_Problem1::execute(std::istream& data_set) {
   types::ExpandableWorkspaceCutCalculator calculator;
   auto input_data = load_data <types::Rectangle>(data_set);
   
   std::cout << "Loaded data elements: " << input_data.size() << std::endl;
   for(auto const& rect : input_data){
      calculator.cutout(rect);
   }

   return new SingleValueSolution<int>(calculator.get_cutout_overlap_area());
}