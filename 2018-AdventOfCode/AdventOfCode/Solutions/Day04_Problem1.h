#pragma once
#include "ISolution.h"


class Day04_Problem1 : public IProblem {
public:
   virtual ~Day04_Problem1();

   virtual std::string name() const;

   virtual ISolution* execute(std::istream& data_set) override;

};

