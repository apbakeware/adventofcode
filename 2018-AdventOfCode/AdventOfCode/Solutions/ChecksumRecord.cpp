#include "pch.h"
#include <algorithm>
#include <iterator>

#include "ChecksumRecord.h"
#include "Utils.h"

namespace types {

ChecksumRecord analyze_checksum(const std::string& str) {

   ChecksumRecord result = { false, false };
   std::string working = str;
   std::sort(working.begin(), working.end());

   auto iter_cur = working.begin();
   auto const END = working.end();
   while(iter_cur != END) {
      auto end_of_contig = utils::find_end_of_contiguous(iter_cur, END);
      auto contig_count = std::distance(iter_cur, end_of_contig);
      result.has_double_character |= contig_count == 2;
      result.has_triple_character |= contig_count == 3;
      iter_cur = end_of_contig;
   }
   return result;
}

}