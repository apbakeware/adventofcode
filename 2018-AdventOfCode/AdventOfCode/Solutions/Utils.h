#pragma once
#include <string>

namespace utils {


bool is_lower_then_upper_of_same(char a, char b);

bool is_upper_then_lower_of_same(char a, char b);

bool is_same_with_diff_case(char a, char b);

std::string remove_dups_with_opposite_case(const std::string& orig);

std::string remove_dups_with_opposite_cases_until_done(std::string orig);

template<typename Iter_T>
Iter_T find_end_of_contiguous(Iter_T iter, Iter_T end) {

   if(iter == end) { return end; }

   auto const val = *iter;
   while(iter != end) {
      ++iter;
      if(iter == end || *iter != val) {
         return iter;
      }
   }
   return iter;
}

// Assumes same size or 2nd is > than first.
template<class Iter_T, class OIter_T>
OIter_T linear_intersection(Iter_T start, Iter_T end, Iter_T start2, OIter_T out) {

   while(start != end) {
      if(*start == *start2) {
         out = *start;
      }
      ++start;
      ++start2;
   }

   return out;
}

}