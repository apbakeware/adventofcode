#include "pch.h"
#include <algorithm>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include "Rectangle.h"

template<class T>
T from_string(const std::string& str) {
   T val;
   std::istringstream s(str);
   s >> val;
   return val;
}


namespace types{
Rectangle Rectangle::from_origin_and_size
(int origin_x, int origin_y, int width, int height){
   
   return Rectangle(origin_x, 
      origin_y, 
      origin_x + width, 
      origin_y + height
   );
}

Rectangle::Rectangle() :
   m_x1(0), m_y1(0), m_x2(0), m_y2(0){
}

Rectangle::Rectangle(int x1, int y1, int x2, int y2) :
   m_x1(x1),
   m_y1(y1),
   m_x2(x2),
   m_y2(y2) {

   // todo validation of x2 >= x1 and y2 >= y1
}

int Rectangle::area() const{
   return std::abs(width() * height());
}

std::ostream& operator<<(std::ostream& str, const types::Rectangle& obj){
   str << "Rectangle {"
      << "(" << obj.m_x1 << "," << obj.m_y1 << ") ("
      << obj.m_x2 << "," << obj.m_y2 << ") -- "
      << " Size: " << obj.width() << "x" << obj.height() << "}";
   return str;
}

// Assumes a single line
std::istream& operator>>(std::istream& str, types::Rectangle& obj){
   const std::regex capture("#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)");
   std::smatch matches;
   std::string line;
   std::getline(str, line);
   if(std::regex_search(line, matches, capture)){
      //obj.id = from_string<int>(matches[1]);
      obj.m_x1 = from_string<int>(matches[2]);
      obj.m_y1 = from_string<int>(matches[3]);
      obj.m_x2 = obj.m_x1 + from_string<int>(matches[4]);
      obj.m_y2 = obj.m_y1 + from_string<int>(matches[5]);
   }

   return str;
}

bool operator==(const Rectangle& lhs, const Rectangle& rhs){
   if(&lhs == &rhs) {
      return true;
   }
   
   return 
      lhs.m_x1 == rhs.m_x1 &&
      lhs.m_y1 == rhs.m_y1 &&
      lhs.m_x2 == rhs.m_x2 &&
      lhs.m_y2 == rhs.m_y2;
}

// upper left is 0
Rectangle intersection(const Rectangle& r1, const Rectangle& r2){
   
   if(!intersects(r1, r2)){
      return Rectangle();
   }

   auto const x1 = std::max(r1.x1(), r2.x1());
   auto const x2 = std::min(r1.x2(), r2.x2());
   auto const y1 = std::max(r1.y1(), r2.y1());
   auto const y2 = std::min(r1.y2(), r2.y2());

   return Rectangle(x1, y1, x2, y2);
}

bool intersects(const Rectangle& r1, const Rectangle& r2){

   if(r1.x1() >= r2.x2() || r2.x1() >= r1.x2()){
      // One is to the right of the other
      return false;
   } else if(r1.y1() >= r2.y2() || r2.y1() >= r1.y2()){
      // One is above of the other
      return false;
   }

   return true;
}

}
