#pragma once
#include "ISolution.h"
#include "ProblemFactory.h"

class Day01_Problem1 : public IProblem {
public:

   virtual ~Day01_Problem1();

   virtual std::string name() const;

   virtual ISolution * execute(std::istream& data_set) override;
};
