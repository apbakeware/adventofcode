#include "pch.h"
#include <algorithm>

#include "Day02_Problem1.h"
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "Utils.h"
#include "ChecksumRecord.h"

namespace {
ProblemFactoryRegisterer<Day02_Problem1> _problem("Day02_Problem1");
}

Day02_Problem1::~Day02_Problem1() {

}

std::string Day02_Problem1::name() const {
   return "Day02 - Problem 1";
}

ISolution* Day02_Problem1::execute(std::istream& data_set) {
   auto input_data = load_data <std::string> (data_set);
   std::cout << "Loaded data elements: " << input_data.size() << std::endl;
   int double_count = 0;
   int triple_count = 0;

   for (auto const& str : input_data) {
      auto const record = types::analyze_checksum(str);
      if (record.has_double_character) {
         ++double_count;
      }
      if (record.has_triple_character) {
         ++triple_count;
      }
   }
   
   return new SingleValueSolution<int>(double_count * triple_count);
}