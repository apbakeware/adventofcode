#include "pch.h"
#include <algorithm>
#include <cmath>
#include <iostream>
#include <sstream>
#include <vector>
#include "Day06_Problem1.h"
#include "DataLoader.h"
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "Utils.h"

namespace {
ProblemFactoryRegisterer<Day06_Problem1> _problem("Day06_Problem1");

static const int INDETERMINIATE_ID = 0;
static int point2_auto_id = 1;
static int g_max_x = 0;
static int g_max_y = 0;

struct Point2 {
   int id;
   int x;
   int y;
};

std::istream& operator>>(std::istream& str, Point2& obj) {
   obj.id = ++point2_auto_id;
   str >> obj.x;
   str.ignore(5, ' ');
   str >> obj.y;

   g_max_x = std::max(g_max_x, obj.x);
   g_max_y = std::max(g_max_y, obj.y);

   return str;
}

int manhattan_distance(int x1, int y1, int x2, int y2) {
   return std::abs(x1 - x2) + std::abs(y1 - y2);
}

}

Day06_Problem1::~Day06_Problem1() {
}

std::string Day06_Problem1::name() const {
   return "Day06 - Problem 1";
}

ISolution* Day06_Problem1::execute(std::istream& data_set) {
   std::string TEST_DATA = "1, 1\n1, 6\n8, 3\n3, 4\n5, 5\n8, 9";
   std::istringstream test_str(TEST_DATA);
   //auto input_data = load_data<Point2>(data_set);
   auto input_data = load_data<Point2>(test_str);
   std::cout << "size: " << input_data.size() << std::endl;

   std::vector<std::vector<int> > grid(g_max_y);
   int mdist = 0;
   for(int y = 0; y < g_max_y; ++y) {
      grid[y].resize(g_max_x, INDETERMINIATE_ID);
      for(int x = 0; x < g_max_x; ++x) {
         bool tied_distance = false;
         for(const auto& point : input_data) {
            mdist = manhattan_distance(x, y, point.x, point.y);
            if(mdist < grid[y][x]) {
               grid[y][x] = mdist;
               tied_distance = false;
            } else if(mdist == grid[y][x]) {
               tied_distance = true;
            }
         }

         if(tied_distance) {
            grid[y][x] = INDETERMINIATE_ID;
         }

      }
   }


   return new SingleValueSolution<int>(-1);
}
