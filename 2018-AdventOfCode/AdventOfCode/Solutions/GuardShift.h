#pragma once
#include <bitset>
#include <memory>
#include <regex>
#include <vector>
#include "FromString.h"

namespace types {

typedef std::bitset<60> MinuteBits;

class GuardShiftRecord;

class GuardSleepSpan {
public:

   GuardSleepSpan(MinuteBits& sleep_bits, int minute_fell_asleep);

   // Update bitset with span.
   ~GuardSleepSpan();

   void set_minute_wakes_up(int minute);

   int get_minute_fell_asleep() const {
      return minute_fell_asleep;
   }

   int get_minute_wakes_up() const {
      return minute_wakes_up;
   }

private:
   MinuteBits& sleep_bits;
   int minute_fell_asleep;
   int minute_wakes_up;
};


class GuardShiftRecord {
public:

   explicit GuardShiftRecord(int guard_id);

   GuardSleepSpan* create_sleep_record(int minute_fell_asleep);

   int get_minutes_asleep() const;

   int get_guard_id() const {
      return guard_id;
   }

   bool is_asleep_at(size_t minute) const {
      return sleep_minutes[minute];
   }

   friend std::ostream& operator<<(std::ostream& str, const GuardShiftRecord& record);

private:
   int guard_id;
   std::bitset<60> sleep_minutes;
};

template<typename Iter_T>
std::vector<types::GuardShiftRecord> create_guard_sleep_schedule
(Iter_T begin, Iter_T end) {
   const std::regex capture(".*#(\\d+).*");
   const std::string ASLEEP = "falls asleep";
   const std::string AWAKE = "wakes up";
   std::smatch matches;

   std::vector<types::GuardShiftRecord> guard_shifts;
   std::unique_ptr<types::GuardSleepSpan> current_sleep;

   for(Iter_T iter = begin; iter != end; ++iter) {
      const auto& content = iter->content();

      if(std::regex_search(content, matches, capture)) {
         const int guard_id = utils::from_string<int>(matches[1]);
         guard_shifts.push_back(types::GuardShiftRecord(guard_id));

      } else if(content == ASLEEP) {
         //std::cout << "Guard: " << current_guard_id << " asleep\n";
         types::GuardShiftRecord& record = guard_shifts.back();
         current_sleep.reset(record.create_sleep_record(iter->minute()));

      } else if(content == AWAKE) {
         //std::cout << "Guard: " << current_guard_id << " awake\n";
         current_sleep->set_minute_wakes_up(iter->minute());

      } else {
         // error
      }
   }

   return guard_shifts;
}

}