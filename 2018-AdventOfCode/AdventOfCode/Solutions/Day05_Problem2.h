#pragma once
#include "ISolution.h"
class Day05_Problem2 : public IProblem {
public:

   virtual ~Day05_Problem2();

   virtual std::string name() const;

   virtual ISolution* execute(std::istream& data_set) override;
};

