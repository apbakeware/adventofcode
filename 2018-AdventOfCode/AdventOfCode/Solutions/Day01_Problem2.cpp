#include "pch.h"
#include <unordered_set>

#include "Day01_Problem2.h"
#include "DataLoader.h"
#include "SingleValueSolution.h"
#include "ProblemFactory.h"

namespace {

ProblemFactoryRegisterer<Day01_Problem2> _problem("Day01_Problem2");

// runs over a range until the predicate is satisifed or end of range. 
// true if predicate found, otherwise false.
template<
   class Insert_Iter_T,
   class Predicate
>
bool for_each_until(Insert_Iter_T first, Insert_Iter_T last, Predicate pred) {
   while (first != last) {
      if (pred(*first)) {
         return true;
      }
      ++first;
   }
   return false;
}


}

Day01_Problem2::~Day01_Problem2() {

}

std::string Day01_Problem2::name() const {
   return "Day01 - Problem 2";
}

ISolution* Day01_Problem2::execute(std::istream& data_set) {

   auto const data = load_data<int>(data_set);
   std::unordered_set<int> accumulated_values;
   int sum = 0;
   int loop_tries = 0;
   bool found = false;
   do {
      ++loop_tries;
      found = for_each_until(data.begin(),
         data.end(),
         [&](auto accum) -> bool {
            sum += accum;
            auto res = accumulated_values.insert(sum);
            return !(res.second);
         }
      );
   } while (!found);

   return new SingleValueSolution<int>(sum);
}