#pragma once

#include <string>

namespace types {

struct ChecksumRecord {
   bool has_double_character;
   bool has_triple_character;
};

ChecksumRecord analyze_checksum(const std::string& str);



}
