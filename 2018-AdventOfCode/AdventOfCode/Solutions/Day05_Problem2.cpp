#include "pch.h"
#include <algorithm>
#include "Day05_Problem2.h"
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "Utils.h"

namespace {
ProblemFactoryRegisterer<Day05_Problem2> _problem("Day05_Problem2");
}

Day05_Problem2::~Day05_Problem2() {
}

std::string Day05_Problem2::name() const {
   return "Day05 - Problem 2";
}

ISolution* Day05_Problem2::execute(std::istream& data_set) {
   std::string input_data;
   data_set >> input_data;

   auto smallest_polymer = input_data.size();
   std::string reduced;
   reduced.reserve(input_data.size());
   
   for(auto filter = 'a'; filter <= 'z'; ++filter) {
      reduced.clear();
      std::copy_if(
         input_data.begin(),
         input_data.end(),
         std::back_inserter(reduced),
         [=](const auto cur) {
            return std::tolower(cur) != filter;
         }
      );
      reduced = utils::remove_dups_with_opposite_case(reduced);
      smallest_polymer = std::min(smallest_polymer, reduced.size());
   }

   return new SingleValueSolution<int>(smallest_polymer);
}
