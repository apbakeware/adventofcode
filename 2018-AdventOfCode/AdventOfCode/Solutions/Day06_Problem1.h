#pragma once
#include "ISolution.h"

class Day06_Problem1 : public IProblem {
public:

   virtual ~Day06_Problem1();

   std::string name() const override;

   ISolution* execute(std::istream& data_set) override;
};

