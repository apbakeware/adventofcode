#pragma once
#include <iostream>
#include "ISolution.h"

template<class Value_T>
class SingleValueSolution : public ISolution {
public:

   explicit SingleValueSolution(Value_T value)
      : value(value) {
   }

   SingleValueSolution(const std::string& label, Value_T value)
      : label(label), value(value) {
   }

   virtual ~SingleValueSolution() {
   }

   virtual void write_result(std::ostream& outs) override {
      outs << label << " " << value;
   }

private:
   const std::string label;
   const Value_T value;
};