#include "pch.h"
#include <algorithm>
#include <unordered_map>
#include <memory>
#include "Day04_Problem1.h"
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "Timestamped.h"
#include "FromString.h"
#include "GuardShift.h"

namespace {
ProblemFactoryRegisterer<Day04_Problem1> _problem("Day04_Problem1"); 




int find_sleepiest_guard(const std::unordered_map<int, int>& sleep_by_guard) {
   int sleepiest_guard_id = 0;
   int most_sleep = 0;
   for(const auto guard_sleeps : sleep_by_guard) {
      if(guard_sleeps.second > most_sleep) {
         most_sleep = guard_sleeps.second;
         sleepiest_guard_id = guard_sleeps.first;
      }
   }
   return sleepiest_guard_id;
}

int find_sleepiest_minute
(const std::vector<types::GuardShiftRecord> & the_guards_shifts) {
   std::vector<int> counts(60, 0);
   for(const auto& rec : the_guards_shifts) {
      for(size_t min = 0; min < 60; ++min) {
         if(rec.is_asleep_at(min)) {
            ++counts[min];
         }
      }
   }

   int sleepiest_minute = 0;
   int cur_max = 0;
   for(size_t idx = 0; idx < counts.size(); ++idx) {
      if(counts[idx] > cur_max) {
         cur_max = counts[idx];
         sleepiest_minute = idx;
      }
   }

   return sleepiest_minute;
}

}








Day04_Problem1::~Day04_Problem1() {

}

std::string Day04_Problem1::name() const {
   return "Day04 - Problem 1";
}


ISolution* Day04_Problem1::execute(std::istream& data_set) {
   auto input_data = load_data<utils::Timestamped>(data_set);
   std::sort(input_data.begin(), input_data.end());


   auto guard_shift_records = types::create_guard_sleep_schedule
      (input_data.begin(), input_data.end());

   std::sort(
      guard_shift_records.begin(),
      guard_shift_records.end(),
      [](const auto& lhs, const auto& rhs) {
         return lhs.get_guard_id() < rhs.get_guard_id();
      });

   std::unordered_map<int, int> sleep_by_guard;
   std::for_each(
      guard_shift_records.begin(),
      guard_shift_records.end(),
      [&sleep_by_guard](const auto& record) {

         auto iter = sleep_by_guard.find(record.get_guard_id());

         if(iter == sleep_by_guard.end()) {
            sleep_by_guard.insert(std::make_pair(
               record.get_guard_id(),
               record.get_minutes_asleep()
            ));
         } else {
            (iter -> second) += record.get_minutes_asleep();
         }
      }
   );

   int sleepiest_guard_id = find_sleepiest_guard(sleep_by_guard);

   std::vector<types::GuardShiftRecord> the_guards_shifts;
   std::copy_if(
      guard_shift_records.begin(),
      guard_shift_records.end(),
      std::back_inserter(the_guards_shifts),
      [sleepiest_guard_id](const auto& rec) {
         return rec.get_guard_id() == sleepiest_guard_id;
      }
   );

   int sleepiest_minute = find_sleepiest_minute(the_guards_shifts);

   return new SingleValueSolution<int>
      (sleepiest_guard_id * sleepiest_minute);
}
