#include "pch.h"
#include <iterator>
#include "Utils.h"

namespace utils {

bool is_lower_then_upper_of_same(char a, char b) {
   static const char BITWISE_AND_TO_UPPER = 0x5F;
   return std::islower(a) &&
      std::isupper(b) &&
      (a & BITWISE_AND_TO_UPPER) == b;
}

bool is_upper_then_lower_of_same(char a, char b) {
   return is_lower_then_upper_of_same(b, a);
}

bool is_same_with_diff_case(char a, char b) {
   return is_lower_then_upper_of_same(a, b) ||
      is_upper_then_lower_of_same(a, b);
}

std::string remove_dups_with_opposite_case(const std::string& orig) {

   if(orig.size() < 2) {
      return orig;
   }

   std::string working = orig;
   const auto END = working.end();
   auto iter_l = working.begin();
   auto iter_r = iter_l + 1;

   while(std::distance(iter_l, working.end()) > 1) {
      if(is_same_with_diff_case(*iter_l, *iter_r)) {
         iter_l = working.erase(iter_l, iter_r + 1);
         if(iter_l == working.end()) {
            break;

         // Backup to see if we need to collapse
         } else if(iter_l != working.begin()) {
            --iter_l;
         }
      } else {
         iter_l = iter_r;
      }
      iter_r = iter_l + 1;
   }

   return working;
}

}