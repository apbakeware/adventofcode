#pragma once
#include <iosfwd>
#include <string>

namespace utils{


class Timestamped {
public:

   Timestamped();

   int year() const;
   int month() const;
   int day() const;
   int hour() const;
   int minute() const;
   std::string content() const;

   friend std::ostream& operator<<(std::ostream& str, const Timestamped &obj);

   // Parse in the format [1518-11-01 00:00]
   friend std::istream& operator>>(std::istream& str, Timestamped& obj); 

private:

   // validate();

   // TODO: Want to use std::tm and get_time but not working as expected.
   //struct std::tm timestamp;

   int m_year;
   int m_month;
   int m_day;
   int m_hour;
   int m_minute;

   std::string m_content;
};

bool operator<(const Timestamped& lhs, const Timestamped& rhs);

}

