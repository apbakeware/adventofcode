// AdventOfCode.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream>
#include "../Solutions/DataLoader.h"
#include "../Solutions/ISolution.h"
#include "../Solutions/ProblemFactory.h"

struct Problem_To_Solve {
   const std::string problem_name;
   const std::string data_file_name;
};

struct Solved_Problem {

   Solved_Problem() : duration(0) {

   }

   std::string name;
   std::chrono::microseconds duration;
   std::shared_ptr<ISolution> solution;

   static void write_header(std::ostream& str) {
      str << "\n\n"
         << " Duration       | Name                  | Result\n"
         << "----------------+-----------------------+--------------------\n";
   }

   friend std::ostream& operator<<(std::ostream& outs, const Solved_Problem& obj) {
      auto ms = obj.duration.count() / 1000000.0;
      outs 
         << std::setprecision(6) << std::fixed << std::setw(15) << std::setfill(' ')  << ms << "  "
         << std::setw(20) << std::setfill(' ') << std::left << obj.name << "   ";
      obj.solution->write_result(outs);
      outs << std::resetiosflags(std::ios::adjustfield);
      return outs;
   }
};

const std::vector<Problem_To_Solve> PROBLEMS_TO_SOLVE = {
   //{ "Day01_Problem1", "day01.txt"},
   //{ "Day01_Problem2", "day01.txt"},
   //{ "Day02_Problem1", "day02.txt"},
   //{ "Day02_Problem2", "day02.txt"},
   //{ "Day03_Problem1", "day03.txt"},
   //{ "Day03_Problem2", "day03.txt"},
   //{ "Day04_Problem1", "day04.txt"},
   //{ "Day04_Problem2", "day04.txt"},
   //{ "Day05_Problem1", "day05.txt"},
   //{ "Day05_Problem2", "day05.txt"},
   { "Day06_Problem1", "day06.txt"}
};

std::vector<Solved_Problem> solved_problems;

void solve_problem(const std::string& name, std::istream & input_stream) {

   std::unique_ptr<IProblem> problem(ProblemFactory::factory().build(name));
   if (problem) {
      std::cout << "Finding solution for " << problem->name() << "\n";

      Solved_Problem solved;
      solved.name = problem->name();   
      auto start_t = std::chrono::high_resolution_clock::now();
      solved.solution.reset(problem->execute(input_stream));
      auto end_t = std::chrono::high_resolution_clock::now();
      solved.duration = std::chrono::duration_cast<std::chrono::microseconds>
         (end_t - start_t);

      solved_problems.push_back(solved);
   } else {
      std::cout << "Failed to allocate problem" << std::endl;
   }
}

int main() {

   std::cout << "Registered Problems:\n";
   for (auto const& name : ProblemFactory::factory().all_keys()) {
      std::cout << "\t" << name << "\n";
   }

   for (auto const& problem : PROBLEMS_TO_SOLVE) {
      std::ifstream input_data_stream(problem.data_file_name);
      if (input_data_stream.is_open()) {
         solve_problem(problem.problem_name, input_data_stream);
      } else {
         std::cout << "Failed to open data file '" << problem.data_file_name
            << "', can't solve problem '" << problem.data_file_name << "'" 
            << "\n";
      }
   }

   Solved_Problem::write_header(std::cout);
   std::ostream_iterator<Solved_Problem> out_it(std::cout, "\n");
   std::copy(solved_problems.begin(), solved_problems.end(), out_it);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
