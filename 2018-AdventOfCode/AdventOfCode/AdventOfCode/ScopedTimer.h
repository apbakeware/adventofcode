#pragma once

#include <chrono>

class ScopedTimer {
public:
   ScopedTimer();
   ~ScopedTimer();

private:

   std::chrono::high_resolution_clock start_time;
};

