#pragma once

#include <chrono>
#include <iostream>

/**
 * Method to time the executaion of a callable entity.
 */
template<class Callable_T>
std::ostream& time_execution(Callable_T callable, std::ostream & outs = std::cout) {
   
   auto start_t = std::chrono::high_resolution_clock::now();
   callable();
   auto end_t = std::chrono::high_resolution_clock::now();
   auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>
      (end_t - start_t);

   outs << elapsed / 1000000.0 << " seconds";
   return outs;

}