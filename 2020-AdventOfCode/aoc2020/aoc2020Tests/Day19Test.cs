﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using Serilog;
using aoc2020.Problems;

namespace aoc2020.Problems.Tests
{


    public class Day19ConstraintTreeTest
    {
        private ConstraintTree sut;

        public Day19ConstraintTreeTest(ITestOutputHelper output)
        {
            Log.Logger = new Serilog.LoggerConfiguration()
                .WriteTo.TestOutput(output)
                .MinimumLevel.Debug()
                .CreateLogger();

            sut = new ConstraintTree();
        }

        [Fact]
        public void TestIndexCreatesNodeNotInTree()
        {
            int nodeId = 3;

            var node = sut[nodeId];
            Assert.Equal(1, sut.Count);
        }

        [Fact]
        public void TestIndexerReturnsSameObjectForId()
        {
            int nodeId = 3;

            var node = sut[nodeId];
            var node2 = sut[nodeId];
            Assert.Same(node, node2);
        }

        [Fact]
        public void TestBuildTreeWithSingleChildNode()
        {
            int nodeId = 3;
            int childNodeId = 9;

            var node = sut[nodeId];

            var seq = node.NewConstraintSequence();
            seq.Add(sut[childNodeId]);

            Assert.Equal(2, sut.Count);
            Assert.Equal(1, node.NumConstraintChains);
        }

        [Fact]
        public void TestBuildTreeWith2ChildSequences()
        {
            StringBuilder builder = new StringBuilder();

            int nodeId = 3;
            int childNodeId1 = 9;
            int childNodeId2 = 343;

            var node = sut[nodeId];

            var seq = node.NewConstraintSequence();
            seq.Add(sut[childNodeId1]);

            seq = node.NewConstraintSequence();
            seq.Add(sut[childNodeId2]);

            Assert.Equal(3, sut.Count);
            Assert.Equal(2, node.NumConstraintChains);
        }

        [Fact]
        public void TestPrint()
        {
            int nodeId = 3;
            int childNodeId1 = 9;
            int childNodeId2 = 343;

            var node = sut[nodeId];

            var seq = node.NewConstraintSequence();
            seq.Add(sut[childNodeId1]);

            seq = node.NewConstraintSequence();
            seq.Add(sut[childNodeId2]);

            StringBuilder builder = new StringBuilder().Append('\n');
            ConstraintTree.BuildTreeDisplay(builder, sut, 3);
            Log.Information(builder.ToString());
            Assert.True(false);
        }
    }

    public class Day19Test
    {
        Day19 sut;
        public Day19Test(ITestOutputHelper output)
        {
            Log.Logger = new Serilog.LoggerConfiguration()
                .WriteTo.TestOutput(output)
                .MinimumLevel.Debug()
                .CreateLogger();

            sut = new Day19();
        }

        [Fact]
        void TestSampleInputTreeParse()
        {
            StringBuilder input = new StringBuilder()
                .Append("0: 1 2\n")
                .Append("1: \"a\"\n")
                .Append("2: 1 3 | 3 1\n")
                .Append("3: \"b\"");

            MemoryStream memStream = new MemoryStream();
            memStream.Write(Encoding.ASCII.GetBytes(input.ToString()));
            memStream.Seek(0, 0);

            sut.ReadInput(new StreamReader(memStream));

            StringBuilder builder = new StringBuilder().Append('\n');
            ConstraintTree.BuildTreeDisplay(builder, sut.ConstraintTree, 0);
            Log.Information(builder.ToString());

            //Assert.True(false);
        }

        [Fact]
        void TestSampleInputTreeParse2()
        {
            StringBuilder input = new StringBuilder()
                .Append("0: 4 1 5\n")
                .Append("1: 2 3 | 3 2\n")
                .Append("2: 4 4 | 5 5\n")
                .Append("3: 4 5 | 5 4\n")
                .Append("4: \"a\"\n")
                .Append("5: \"b\"");

            MemoryStream memStream = new MemoryStream();
            memStream.Write(Encoding.ASCII.GetBytes(input.ToString()));
            memStream.Seek(0, 0);

            sut.ReadInput(new StreamReader(memStream));

            StringBuilder builder = new StringBuilder().Append('\n');
            ConstraintTree.BuildTreeDisplay(builder, sut.ConstraintTree, 0);
            Log.Information(builder.ToString());

            //Assert.True(false);
        }

        [Fact]
        void TestBuildValidsDay19Part1Sample1()
        {
            StringBuilder input = new StringBuilder()
                .Append("0: 1 2\n")
                .Append("1: \"a\"\n")
                .Append("2: 1 3 | 3 1\n")
                .Append("3: \"b\"");

            MemoryStream memStream = new MemoryStream();
            memStream.Write(Encoding.ASCII.GetBytes(input.ToString()));
            memStream.Seek(0, 0);

            sut.ReadInput(new StreamReader(memStream));

            StringBuilder builder = new StringBuilder();
            List<string> valids = new List<string>();
            ConstraintTree.BuildValidMessages(builder, valids, sut.ConstraintTree, 0);
            
            foreach(var valid in valids)
            {
                Log.Information("Code: {0}", valid);
            }

            Assert.True(false);
        }
    }
}
