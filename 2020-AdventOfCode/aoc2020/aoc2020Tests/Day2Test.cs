﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

using aoc2020.Problems;

namespace aoc2020.Tests
{
    public class Day2Test
    {
        [Theory]
        [InlineData("1-3 a: abcde", 1, 3, 'a', "abcde")]
        [InlineData("1-3 b: cdefg", 1, 3, 'b', "cdefg")]
        [InlineData("2-9 c: ccccccccc", 2, 9, 'c', "ccccccccc")]
        public void TestPasswordSpecCreate(string line, int min, int max, char letter, string password)
        {
            PasswordSpec sut = PasswordSpec.Create(line);

            Assert.Equal(min, sut.Min);
            Assert.Equal(max, sut.Max);
            Assert.Equal(letter, sut.Letter);
            Assert.Equal(password, sut.Password);
        }

        [Theory]
        [InlineData("1-3 a: abcde", true)]
        [InlineData("1-3 b: cdefg", false)]
        [InlineData("2-9 c: ccccccccc", true)]
        public void TestPasswordSpecVadlidatePolicy1(string line, bool isValid)
        {
            PasswordSpec sut = PasswordSpec.Create(line);
            Assert.Equal(isValid, sut.ValidatePolicy1());
        }

        [Theory]
        [InlineData("1-3 a: abcde", true)]
        [InlineData("1-3 b: cdefg", false)]
        [InlineData("2-9 c: ccccccccc", false)]
        public void TestPasswordSpecVadlidatePolicy2(string line, bool isValid)
        {
            PasswordSpec sut = PasswordSpec.Create(line);
            Assert.Equal(isValid, sut.ValidatePolicy2());
        }
    }
}
