﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Serilog;
using aoc2020.Problems;

namespace aoc2020.Problems.Tests
{
    public class Day13Test
    {
        public Day13Test(ITestOutputHelper output)
        {
            Log.Logger = new Serilog.LoggerConfiguration()
                .WriteTo.TestOutput(output)
                .CreateLogger();
        }

        [Theory]
        [InlineData(3417L, "17,x,13,19")]
        [InlineData(754018L, "67,7,59,61")]
        [InlineData(779210L, "67, x, 7, 59, 61")]
        [InlineData(1261476L, "67,7,x,59,61")]
        [InlineData(1202161486L, "1789, 37, 47, 1889")]
        public void TestDay13Part2Examples(long expected, string input)
        {
            string dontCareHeaderRow = "1\n";
            string inputString = dontCareHeaderRow + input;
            byte[] bytes = Encoding.ASCII.GetBytes(inputString);
            MemoryStream inputStream = new MemoryStream(bytes);

            var sut = new Day13();
            sut.ReadInput(new System.IO.StreamReader(inputStream));

            Assert.Equal(expected.ToString(), sut.SolvePart2().Solution());
        }
    }
}
