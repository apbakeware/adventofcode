﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Serilog;

using aoc2020.Problems;


namespace aoc2020.Problems.Tests
{
    public class Day12Test
    {
        public Day12Test(ITestOutputHelper output)
        {
            Log.Logger = new Serilog.LoggerConfiguration()
                .WriteTo.TestOutput(output)
                .CreateLogger();
        }

        [Fact]
        public void TestWaypointRotateLeft90()
        {
            (int X, int Y) waypoint = (10, 1);

            waypoint = Day12.RotateWaypointLeft(waypoint, 90);
            Assert.Equal((-1, 10), waypoint);

            waypoint = Day12.RotateWaypointLeft(waypoint, 90);
            Assert.Equal((-10, -1), waypoint);

            waypoint = Day12.RotateWaypointLeft(waypoint, 90);
            Assert.Equal((1, -10), waypoint);

            waypoint = Day12.RotateWaypointLeft(waypoint, 90);
            Assert.Equal((10, 1), waypoint);
        }

        [Fact]
        public void TestWaypointRotateLeft180()
        {
            (int X, int Y) waypoint = (10, 1);

            waypoint = Day12.RotateWaypointLeft(waypoint, 180);
            Assert.Equal((-10, -1), waypoint);

            waypoint = Day12.RotateWaypointLeft(waypoint, 180);
            Assert.Equal((10, 1), waypoint);
        }

        [Fact]
        public void TestWaypointRotateRight90()
        {
            (int X, int Y) waypoint = (10, 1);

            Log.Logger.Information("Rotation 1");
            waypoint = Day12.RotateWaypointRight(waypoint, 90);
            Assert.Equal((1, -10), waypoint);

            Log.Logger.Information("Rotation 2");
            waypoint = Day12.RotateWaypointRight(waypoint, 90);
            Assert.Equal((-10, -1), waypoint);

            Log.Logger.Information("Rotation 3");
            waypoint = Day12.RotateWaypointRight(waypoint, 90);
            Assert.Equal((-1, 10), waypoint);

            Log.Logger.Information("Rotation 4");
            waypoint = Day12.RotateWaypointRight(waypoint, 90);
            Assert.Equal((10, 1), waypoint);
        }
    }
}
