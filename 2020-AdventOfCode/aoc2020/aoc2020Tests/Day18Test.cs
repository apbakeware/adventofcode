﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using Serilog;
using aoc2020.Problems;

namespace aoc2020.Problems.Tests
{
    

    public class OperatorPrecedenceSolverTest
    {
        public OperatorPrecedenceSolverTest(ITestOutputHelper output)
        {
            Log.Logger = new Serilog.LoggerConfiguration()
                .WriteTo.TestOutput(output)
                .MinimumLevel.Debug()
                .CreateLogger();
        }

        [Fact]
        public void TestMultiplicationExtractor()
        {
            string input = "1 + 2 * 3 + 7 * 12";
            Match match = OperatorPrecedenceSolver.MultiplicationExtractor.Match(input);

            Assert.True(match.Success);
            Assert.Equal("2 * 3", match.Groups["expr"].Value);
            Assert.Equal("2", match.Groups["op1"].Value);
            Assert.Equal("3", match.Groups["op2"].Value);
        }

        [Fact]
        public void TestAdditionExtractor()
        {
            string input = "1 + 2 * 3 + 7 * 12";
            Match match = OperatorPrecedenceSolver.AdditionExtractor.Match(input);

            Assert.True(match.Success);
            Assert.Equal("1 + 2", match.Groups["expr"].Value);
            Assert.Equal("1", match.Groups["op1"].Value);
            Assert.Equal("2", match.Groups["op2"].Value);
        }

        [Fact]
        public void TestAdditionExtractor2()
        {
            string input = "9 * 1 * 3 * 2 + 3";
            Match match = OperatorPrecedenceSolver.AdditionExtractor.Match(input);

            Assert.True(match.Success);
            Assert.Equal("2 + 3", match.Groups["expr"].Value);
            Assert.Equal("2", match.Groups["op1"].Value);
            Assert.Equal("3", match.Groups["op2"].Value);
        }

        [Theory]
        [InlineData("1 + 2 * 3 + 7 * 12", true, "3 * 3 + 7 * 12")]
        [InlineData("9 * 1 * 3 * 2 + 3", true, "9 * 1 * 3 * 5")]
        [InlineData("1 * 3", false, "1 * 3")]
        public void TestSolveAndReplaceAddition(string input, bool callResult, string resulting)
        {
            string updated;
            bool result = OperatorPrecedenceSolver.SolveAndReplace(input, OperatorPrecedenceSolver.AdditionExtractor, (a, b) => a + b, out updated);

            Assert.Equal(callResult, result);
            Assert.Equal(resulting, updated);
        }


        [Theory]
        [InlineData("1 + 1", false, "")]
        [InlineData("5 + (8 * 3 + 9 + 3 * 4 * 3)", true, "(8 * 3 + 9 + 3 * 4 * 3)")]
        [InlineData("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", true, "(8 + 6 * 4)")]
        [InlineData("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", true, "(6 + 9 * 8 + 6)")]
        public void TestParenExtractor(string input, bool expMatchResult, string expExtraction)
        {
            Match match = OperatorPrecedenceSolver.ParenExtractor.Match(input);

            Assert.Equal(expMatchResult, match.Success);
            Assert.Equal(expExtraction, match.Groups["outer"].Value);
        }


        [Theory]
        [InlineData("1 + 1", 2)]
        [InlineData("1 + (2 * 3) + (4 * (5 + 6))", 51)]
        [InlineData("2 * 3 + (4 * 5)", 46)]
        [InlineData("5 + (8 * 3 + 9 + 3 * 4 * 3)", 1445)]
        [InlineData("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 669060)]
        [InlineData("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 23340)]
        public void TestDay18Part2(string input, int expected)
        {
            Assert.Equal(expected, OperatorPrecedenceSolver.Solve(input));
        }
    }
}
