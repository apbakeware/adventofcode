﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xunit;
using Xunit.Abstractions;
using Serilog;

namespace aoc2020Tests.Utils.Tests
{
    // todo: dijestra's train switching algormithm
    // with configurable operator precedence

    /// <summary>
    /// Static class containing methods to solve PostOrder equations (Reverse Polish Notation).
    /// </summary>
    public static class PostfixEquationSolver
    {

        /// <summary>
        /// Solve postfix equations.
        /// 
        /// No error checking on the string is performed.
        /// </summary>
        /// <param name="equation">String separated operators of postfix equations.</param>
        /// <exception cref="ArgumentException">Thrown if an error is encountered</exception>
        /// <returns></returns>
        static public int Solve(string equation)
        {
            int answer = 0;
            Stack<int> operands = new Stack<int>();
            var elements = equation.Split(" ");
            foreach (var element in elements)
            {
                if (int.TryParse(element, out int operand))
                {
                    operands.Push(operand);
                }
                else
                {
                    var rhsOperand = operands.Pop();
                    var lhsOperand = operands.Pop();

                    var result = element[0] switch
                    {
                        '+' => lhsOperand + rhsOperand,
                        '-' => lhsOperand - rhsOperand,
                        '*' => lhsOperand * rhsOperand,
                        '/' => lhsOperand / rhsOperand,
                        _ => throw new ArgumentException($"Uknown operator {element[0]}")
                    };

                    operands.Push(result);
                }
            }

            answer = operands.Pop();
            if (operands.Count > 0) { throw new ArgumentException($"Stack not empty at end of solver"); }
            return answer;
        }
    }

    public class PostfixEquationSolverTest
    {
        [Theory]
        [InlineData("2 4 +", 6)]
        [InlineData("4 2 +", 6)]
        public void AdditionTest(string equation, int expected)
        {
            Assert.Equal(expected, PostfixEquationSolver.Solve(equation));
        }

        [Theory]
        [InlineData("5 3 -", 2)]
        [InlineData("3 5 -", -2)]
        public void SubtractionTest(string equation, int expected)
        {
            Assert.Equal(expected, PostfixEquationSolver.Solve(equation));
        }

        [Theory]
        [InlineData("11 3 *", 33)]
        [InlineData("3 11 *", 33)]
        public void MultiplicationTest(string equation, int expected)
        {
            Assert.Equal(expected, PostfixEquationSolver.Solve(equation));
        }

        [Theory]
        [InlineData("54 9 /", 6)]
        [InlineData("9 54 /", 0)] // Integer division
        public void DivisionTest(string equation, int expected)
        {
            Assert.Equal(expected, PostfixEquationSolver.Solve(equation));
        }

        [Theory]
        [InlineData("-1 5 +", 4)]
        [InlineData("-1 -1 +", -2)] // Integer division
        public void NegativeValueTest(string equation, int expected)
        {
            Assert.Equal(expected, PostfixEquationSolver.Solve(equation));
        }

        // TODO: compmlex number test.
    }
}
