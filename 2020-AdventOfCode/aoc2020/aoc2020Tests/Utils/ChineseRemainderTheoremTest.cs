﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Serilog;

using aoc2020.Utils;

namespace aoc2020Tests.Utils.Tests
{
    public class ChineseRemainderTheoremTest
    {
        public ChineseRemainderTheoremTest(ITestOutputHelper output)
        {
            Log.Logger = new Serilog.LoggerConfiguration()
                .WriteTo.TestOutput(output)
                .CreateLogger();
        }


        // Test Cases from: https://www.youtube.com/watch?v=zIFehsBHB8o

        [Fact]
        public void TestCompute()
        {
            long[] remainders = { 3, 1, 6 };
            long[] mods = { 5, 7, 8  };
            long exp = 78;

            Assert.Equal(exp, ChineseRemainderTheorem.Compute(remainders, mods));
        }

        [Fact]
        public void TestCompute2()
        {
            //long[] remainders = { 0, 1,2,3 };
            //long[] mods = { 37, 47, 1889 };
            //long exp = 3417;

            //Assert.Equal(exp, ChineseRemainderTheorem.Compute(mods, remainders));
        }

        [Theory]
        [InlineData(1L, 56L, 5L)]
        [InlineData(3L, 40L, 7L)]
        [InlineData(3L, 35L, 8L)]
        public void TestComputeModuloInverse(long expected, long target, long mod)
        {
            Assert.Equal(expected, ChineseRemainderTheorem.ComputeModuloInverse(target, mod));
        }
    }
}
