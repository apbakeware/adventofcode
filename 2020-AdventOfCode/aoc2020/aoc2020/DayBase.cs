﻿using System.IO;
using System.Diagnostics;
using Serilog;
using Serilog.Context;

namespace aoc2020
{
    /// <summary>
    /// Solver for a single Advent Of Code day's problems. Implementations
    /// require input pasred by the ReadInput() method prior to the
    /// solving methods.
    /// 
    /// The SolvePartX() method computes the solution to the problem and
    /// returns a solution.
    /// </summary>
    public abstract class DayBase
    {
        /// <summary>
        /// The name of the problem. Ex "Day01".
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// The name of the part 1 of the day's problem.
        /// </summary>
        public string NamePart1 { get => Name + ":01"; }
        
        /// <summary>
        /// The name of the part 2 of the day's problem.
        /// </summary>
        public string NamePart2 { get => Name + ":02"; }

        private ILogger logger;

        /// <summary>
        /// Constructor initializing the Name attribute.
        /// </summary>
        /// <param name="name">Name of the problem.</param>
        public DayBase(string name)
        {
            Name = name;
            logger = Log.ForContext<DayBase>();
        }

        public void ReadInput(System.IO.StreamReader inputStream)
        {
            using (LogContext.PushProperty("Problem", Name))
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                var valuesRead = ReadInputImpl(inputStream);
                stopwatch.Stop();
                logger.Debug("ReadInput() -- parsed {count} values from input in {time}", 
                    valuesRead, stopwatch.Elapsed);
            }
        }

        public ISolution SolvePart1()
        {
            ISolution solution;
            using (LogContext.PushProperty("Problem", NamePart1))
            {
                solution = SolvePart1Impl();
            }
            return solution;
        }

        public ISolution SolvePart2()
        {
            ISolution solution;
            using (LogContext.PushProperty("Problem", NamePart2))
            {
                solution = SolvePart2Impl();
            }
            return solution;
        }

        // Returns number of values read
        protected abstract int ReadInputImpl(System.IO.StreamReader inputStream);
        protected abstract ISolution SolvePart1Impl();
        protected abstract ISolution SolvePart2Impl();

    }
}
