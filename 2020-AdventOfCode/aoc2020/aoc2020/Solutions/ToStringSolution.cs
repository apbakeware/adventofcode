﻿namespace aoc2020.Solutions
{
    public class ToStringSolution<T> : ISolution
    {
        public string ProblemName { get; }

        public T Value { get; }


        public ToStringSolution(string problemName, T value)
        {
            ProblemName = problemName;
            Value = value;
        }

        public string Solution()
        {
            return Value.ToString();
        }
    }
}
