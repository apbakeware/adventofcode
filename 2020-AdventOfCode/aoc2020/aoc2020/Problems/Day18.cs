﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{
    public class Day18 : DayBase
    {
        private ILogger logger;
        private List<string> equations;

        public Day18() : base("Day18")
        {
            logger = Log.ForContext<Day18>();
            equations = new List<string>();
        }


        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            while ((line = inputStream.ReadLine()) != null)
            {
                equations.Add(line.Replace(" ", string.Empty));
            }
            return equations.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            EquationSolver solver = new EquationSolver();
            long result = equations
                .Select(e => solver.Solve(e))
                .Sum();

            return new ToStringSolution<long>(NamePart1, result);
        }

        protected override ISolution SolvePart2Impl()
        {
            EquationSolver solver = new EquationSolver();
            long result = equations
                .Select(e => OperatorPrecedenceSolver.Solve(e))
                .Sum();
            return new ToStringSolution<long>(NamePart2, result);
        }

        private int ProcessEquation(string equation)
        {

            // TODO: not sure best approach, but find close parans..then backtrack to open ...run the equastion between the bracket
            // when not backaets run equastion lef to right.

            

            return 0;
        }

    }

    public class EquationSolver
    {
        string equation;
        int processingIndex;

        readonly Func<long, long, long> AddOp = (a, b) => (a + b);
        readonly Func<long, long, long> MultiplyOp = (a, b) => (a * b);

        public long Solve(string equation)
        {
            this.equation = equation.Replace(" ", string.Empty);
            processingIndex = 0;

            //Log.Information("Solving: {}", this.equation);
            return SolverMultiplyAddSamePrecedence();
        }

        private long SolverMultiplyAddSamePrecedence()
        {
            long val = 0;

            Func<long, long, long> operation = AddOp;

            while (processingIndex != (long)equation.Length && equation[processingIndex] != ')')
            {
                int numStrLen = 0;
                while (processingIndex < equation.Length && Char.IsDigit(equation[processingIndex]))
                {
                    numStrLen++;
                    processingIndex++;
                }

                if(numStrLen > 0)
                {
                    string numPart = equation.Substring(processingIndex - numStrLen, numStrLen);
                    long extracted = long.Parse(numPart);
                    //Log.Information("Extracted number: {0}", val);
                    val = operation(val, extracted);
                    if(processingIndex == equation.Length)
                    {
                        break;
                    }
                }

                switch(equation[processingIndex])
                {
                    case '+':
                        processingIndex++;
                        //Log.Information("Addition operator");
                        operation = AddOp;
                        break;

                    case '*':
                        processingIndex++;
                        //Log.Information("Muliplication operator");
                        operation = MultiplyOp;
                        break;

                    case '(':
                        processingIndex++;
                        val = operation(val, SolverMultiplyAddSamePrecedence());
                        //Log.Information("Open paren--pushing call");
                        break;
                }
            }

            processingIndex++;
            return val;
        }
    }

    public class OperatorPrecedenceSolver
    {
        public static readonly Regex ParenExtractor = new Regex(@".*(?<outer>[(](?<expr>.*?)[)]).*");
        public static readonly Regex MultiplicationExtractor = new Regex(@".*?(?<expr>(?<op1>\d+)\s*\*\s*(?<op2>\d+)).*"); // Add division too
        public static readonly Regex AdditionExtractor = new Regex(@".*?(?<expr>(?<op1>\d+)\s*\+\s*(?<op2>\d+)).*"); // Add sub too


        static public long Solve(string input)
        {
            string output;
            //Log.Debug("Solving: {0}", input);
            SolveAndReplace(input, out output);
            return long.Parse(output);
        }

        static public void SolveAndReplace(string input, out string output)
        {
            string inProcess = "";
            while (SolveAndReplaceParen(input, out inProcess))
            {
                input = inProcess;
            }

            while (OperatorPrecedenceSolver.SolveAndReplace(input, OperatorPrecedenceSolver.AdditionExtractor, (x, y) => x + y, out inProcess))
            {
                input = inProcess;
            }

            while (OperatorPrecedenceSolver.SolveAndReplace(input, OperatorPrecedenceSolver.MultiplicationExtractor, (x, y) => x * y, out inProcess))
            {
                input = inProcess;
            }

            //SolveAndReplace(input, out inProcess);
            output = inProcess;
        }

        static public bool SolveAndReplaceParen(string input, out string output)
        {
            bool result = false;
            Match match = ParenExtractor.Match(input);
            if (match.Success)
            {
                //Log.Debug("Found regex match paren expression: {0} {1}"
                //    , match.Groups["expr"], match.Index, match.Groups["expr"].Length);

                string inProcess = "";
                SolveAndReplace(match.Groups["expr"].Value, out inProcess);
                StringBuilder replacement = new StringBuilder(input);
                replacement.Remove(match.Groups["outer"].Index, match.Groups["outer"].Length);
                replacement.Insert(match.Groups["outer"].Index, inProcess);
                output = replacement.ToString();
                //Log.Debug("New Equation: {0}", output);
                result = true;
            }
            else
            {
                output = input;
            }

            return result;
        }

        static public bool SolveAndReplace(string input, Regex extractor, Func<long, long, long> operation, out string updated)
        {
            bool result = false;
            Match match = extractor.Match(input);
            if (match.Success)
            {
                //Log.Debug("Found regex match {0} [{1}] to [{2}]"
                //    , match.Groups["expr"], match.Index, match.Groups["expr"].Length);
                long value = operation(long.Parse(match.Groups["op1"].Value), long.Parse(match.Groups["op2"].Value));
                StringBuilder replacement = new StringBuilder(input);
                replacement.Remove(match.Groups["expr"].Index, match.Groups["expr"].Length);
                replacement.Insert(match.Groups["expr"].Index, value.ToString());
                updated = replacement.ToString();

                //Log.Debug("New Equation: {0}", updated);
                result = true;
            }
            else
            {
                updated = input;
            }

            return result;
        }
    }
}
