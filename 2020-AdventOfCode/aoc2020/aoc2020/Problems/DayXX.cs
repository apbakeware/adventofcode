﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{
    public class DayXX : DayBase
    {
        private ILogger logger;
        
        public DayXX() : base("DayXX")
        {
            logger = Log.ForContext<DayXX>();
        }


        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            while ((line = inputStream.ReadLine()) != null)
            {
                
            }
            return 0;
        }

        protected override ISolution SolvePart1Impl()
        {
            return new ToStringSolution<int>(NamePart1, -1);
        }

        protected override ISolution SolvePart2Impl()
        {
            return new ToStringSolution<int>(NamePart2, -1);
        }
    }
}
