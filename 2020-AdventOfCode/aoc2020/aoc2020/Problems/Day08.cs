using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{

    public class GameConsoleProgramInstruction
    {
        public string Instruction { get; set; }
        public int Value { get; }
        public int ExecutionCount { get; set; }

        public GameConsoleProgramInstruction(string instruction, int value)
        {
            Instruction = instruction;
            Value = value;
            ExecutionCount = 0;
        }

        public void Reset()
        {
            ExecutionCount = 0;
        }

    };

    public class GameConosleProgram
    {
        public int Accumulator { get; private set; }
        public List<GameConsoleProgramInstruction> Instructions { get; }
        private int ProgramIndex;

        public GameConosleProgram(List<GameConsoleProgramInstruction> program)
        {
            Instructions = program;
            Accumulator = 0;
            ProgramIndex = 0;
        }

        public void Reset()
        {
            Accumulator = 0;
            ProgramIndex = 0;
            foreach (var inst in Instructions)
            {
                inst.Reset();
            }
        }

        public bool Execute()
        {
            while (ProgramIndex < Instructions.Count && Instructions[ProgramIndex].ExecutionCount < 1 )
            {
                
                //Log.Information("Program index: {idx}", ProgramIndex);
                //Log.Information("Instruction: {@instr}", Instructions[ProgramIndex]);
                GameConsoleProgramInstruction instr = Instructions[ProgramIndex];
                instr.ExecutionCount++;

                switch (instr.Instruction)
                {
                    case "nop":
                        //Log.Information("Processing NOP");
                        ProgramIndex++;
                        break;

                    case "acc":
                        //Log.Information("Processing acc {0}", instr.Value);
                        Accumulator += instr.Value;
                        ProgramIndex++;
                        break;

                    case "jmp":
                        //Log.Information("Processing jmp {0}", instr.Value);
                        ProgramIndex += instr.Value;
                        break;

                    default:
                        Log.Warning("Unhandled instruction to execute {0}", Instructions[ProgramIndex].Instruction);
                        break;
                }
            }

            // Log.Information("Terminated...ProgramIndex: {0}", ProgramIndex);
            // Good termination condition
            return ProgramIndex == Instructions.Count;
        }
    }

    public class Day08 : DayBase
    {

        private ILogger logger;

        private Regex instructionParser = new Regex(@"^(?<instr>\w+)\s+(?<value>.*)$"); // (?<value>\+?\d+)  // + or - \d+ instead of all
        private List<GameConsoleProgramInstruction> instructions;

        public Day08() : base("Day08")
        {
            logger = Log.ForContext<Day08>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            instructions = new List<GameConsoleProgramInstruction>();
            while ((line = inputStream.ReadLine()) != null)
            {
                Match match = instructionParser.Match(line);
                if (match.Success)
                {
                    instructions.Add(new GameConsoleProgramInstruction
                        (match.Groups["instr"].Value, int.Parse(match.Groups["value"].Value)));
                }
                else
                {
                    logger.Warning("Instruction parse failed");
                }
            }
            return instructions.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            GameConosleProgram console = new GameConosleProgram(instructions);
            console.Execute();
            return new ToStringSolution<int>(NamePart1, console.Accumulator);
        }

        protected override ISolution SolvePart2Impl()
        {
            string origInst = "";
            GameConosleProgram console = new GameConosleProgram(instructions);
            foreach (var inst in instructions)
            {
                if(inst.Instruction == "acc") { continue;  }

                origInst = inst.Instruction;
                inst.Instruction = (inst.Instruction == "nop") ? "jmp" : "nop";
                console.Reset();
                if ( console.Execute() )
                {
                    break;
                }
                inst.Instruction = origInst;

            }

            return new ToStringSolution<int>(NamePart2, console.Accumulator);
        }

    }
}
