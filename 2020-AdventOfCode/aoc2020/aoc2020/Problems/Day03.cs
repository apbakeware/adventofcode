﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{
    public class Day03 : DayBase
    {
        private readonly char Tree = '#';

        private ILogger logger;

        List<string> rows;
        int numCols;


        public Day03() : base("Day03")
        {
            logger = Log.ForContext<Day03>();
            rows = new List<string>();
            numCols = 0;
        }


        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            rows.Clear();
            while ((line = inputStream.ReadLine()) != null)
            {
                numCols = line.Length;
                rows.Add(line);
            }
            return rows.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            const int slope_col = 3;
            int result = 0;
            int column = 0;

            foreach (string row in rows)
            {
                if (row[column] == Tree)
                {
                    result++;
                }
                column = (column + slope_col) % numCols;
            }
            return new ToStringSolution<int>(NamePart1, result);
        }

        protected override ISolution SolvePart2Impl()
        {
            int[] trees = { 0, 0, 0, 0, 0 };
            int[] slope_cols = { 1, 3, 5, 7, 1 };
            int[] slope_rows = { 1, 1, 1, 1, 2 };
            int[] curCols = { 0, 0, 0, 0, 0 };

            for (int rowIdx = 0; rowIdx < rows.Count; rowIdx++)
            {
                for (int colIdx = 0; colIdx < curCols.Length; colIdx++)
                {
                    if (rowIdx % slope_rows[colIdx] == 0)
                    {
                        trees[colIdx] += rows[rowIdx][curCols[colIdx]] == Tree ? 1 : 0;
                        curCols[colIdx] = (curCols[colIdx] + slope_cols[colIdx]) % numCols;
                    }
                }
            }

            return new ToStringSolution<int>(NamePart2, trees.Aggregate(1, (a, b) => a * b));
        }
    }
}
