﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{
    public class Day19 : DayBase
    {
        private ILogger logger;

        public ConstraintTree ConstraintTree {get; }

        public Day19() : base("Day19")
        {
            logger = Log.ForContext<Day19>();
            ConstraintTree = new ConstraintTree();
        }


        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            while ((line = inputStream.ReadLine()) != null)
            {
                var splits = line.Split(": ");
                int id = int.Parse(splits[0]);
                var node = ConstraintTree[id];

                // if it starts with a "
                if(splits[1].StartsWith("\""))
                {
                    logger.Information("Found leaf node letter");
                    var constraint = splits[1].Replace("\"", string.Empty);
                    node.Constraint = constraint;
                }
                else
                {
                    logger.Information("Found constraint node path");
                    var paths = splits[1].Trim().Split("|");
                    foreach(var path in paths)
                    {
                        var constraintSeq = node.NewConstraintSequence();
                        var nodeIds = path.Trim().Split(' ');
                        foreach (var nodeId in nodeIds)
                        {
                            logger.Information("Added to constraint sequence: {0}", nodeId);
                            int childId = int.Parse(nodeId);

                            constraintSeq.Add(ConstraintTree[childId]);
                        }
                    }
                }

            }
            return 0;
        }

        protected override ISolution SolvePart1Impl()
        {
            return new ToStringSolution<int>(NamePart1, -1);
        }

        protected override ISolution SolvePart2Impl()
        {
            return new ToStringSolution<int>(NamePart2, -1);
        }

    }
    // TODO: Either build up the tree with all possible paths after setting up the tree
    //       - OR -
    //       DFS with options each run throught

    // TODO: go with DFS for now, can build back up later with strings parallel to constraint children.

    // TODO: DO both....creat a dictionary of <long, built sequence> which has acceptable paths...?
    // if not cached then dfs...

    public class ConstraintNode
    {
        public int Id { get; }
        public int NumConstraintChains { get => ChildChains.Count; }
        public string Constraint { get; set; }

        // List of constraint sequesnces...one of which has to be satisified.
        public List<List<ConstraintNode>> ChildChains { get; private set; }

        public ConstraintNode(int id)
        {
            Id = id;
            ChildChains = new List<List<ConstraintNode>>();
            Constraint = string.Empty;
        }

        // build a new seeqcnes and return it. Add it to the list
        public List<ConstraintNode> NewConstraintSequence()
        {
            List<ConstraintNode> sequence = new List<ConstraintNode>();
            ChildChains.Add(sequence);
            return sequence;
        }

        public List<ConstraintNode> GetChildSequence(int index)
        {
            return ChildChains[index];
        }
    }

    public class ConstraintTree
    {

        public int Count { get => Tree.Count; }

        // get and if not found add 
        public ConstraintNode this[int i]
        {
            get
            {
                if (!Tree.TryGetValue(i, out ConstraintNode node))
                {
                    node = new ConstraintNode(i);
                    Tree.Add(i, node);
                }
                return node;
            }
        }

        public Dictionary<int, ConstraintNode> Tree { get; }

        public ConstraintTree()
        {
            Tree = new Dictionary<int, ConstraintNode>();
        }

        static public void BuildTreeDisplay(StringBuilder builder, ConstraintTree tree, int nodeId, int level = 0)
        {
            if (tree.Tree.ContainsKey(nodeId))
            {
                ConstraintNode node = tree.Tree[nodeId];
                string prefixStr = new StringBuilder().Append('\t', level).ToString();
                builder.Append($"{prefixStr}[{nodeId}] ({node.Constraint})\n");
                foreach (var row in node.ChildChains)
                {
                    builder.Append($"{prefixStr}   + ");
                    foreach (var child in row)
                    {
                        builder.Append(child.Id).Append(' ');
                    }
                    builder.Append('\n');
                }

                // display the children
                foreach (var row in node.ChildChains)
                {
                    foreach (var child in row)
                    {
                        BuildTreeDisplay(builder, tree, child.Id, level + 1);
                    }
                }
            }
        }

        // TODO: think after processing all the constraints in only the first level, 
        // then add it to the set of valids

        // not working
        static public void BuildValidMessages(StringBuilder builder, List<string> valids, ConstraintTree tree, int nodeId, int level = 0)
        {
            if (tree.Tree.ContainsKey(nodeId))
            {
                ConstraintNode node = tree.Tree[nodeId];
                if(node.Constraint != string.Empty)
                {
                    builder.Append(node.Constraint);
                }

                foreach (var row in node.ChildChains)
                {
                    int builderLen = builder.Length;
                    foreach (var child in row)
                    {
                        BuildValidMessages(builder, valids, tree, child.Id, level + 1);
                    }

                    if(level == 0)
                    {
                        valids.Add(builder.ToString());
                    }

                    builder.Length = builderLen;
                }
            }
        }

        static public void BuildValidMessages(ConstraintTree tree, int nodeId, string target, int index)
        {
            //if (tree.Tree.ContainsKey(nodeId))
            //{
            //    ConstraintNode node = tree.Tree[nodeId];
            //    if(node.Constraint == string.Empty)
            //    {

            //    }
            //    else
            //    {
            //        return 
            //    }
                
                
                
            //    if (node.Constraint != string.Empty)
            //    {
            //        builder.Append(node.Constraint);
            //    }

            //    foreach (var row in node.ChildChains)
            //    {
            //        int builderLen = builder.Length;
            //        foreach (var child in row)
            //        {
            //            BuildValidMessages(builder, valids, tree, child.Id, level + 1);
            //        }

            //        if (level == 0)
            //        {
            //            valids.Add(builder.ToString());
            //        }

            //        builder.Length = builderLen;
            //    }
            //}
        }
    }


}