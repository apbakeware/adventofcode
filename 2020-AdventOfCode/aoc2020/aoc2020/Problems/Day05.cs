﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{

    public class BinarySpacePartion
    {

        // todo: left char
        // assumes not first value
        static public int ComputePath(CharEnumerator path, int start, int span, char leftKey, char rightKey)
        {
            //Log.Debug("BinarySpacePartition: {start}:{span}"
            //    ,start, span);

            int partition = span / 2;

            if(path.MoveNext())
            {
                var choice = path.Current;
                //Log.Debug("  char: " + choice);
                if(choice == leftKey)
                {
                    return ComputePath(path, start, partition, leftKey, rightKey);
                }
                else if(choice == rightKey)
                {
                    return ComputePath(path, start + partition, partition, leftKey, rightKey);
                }
            }

            //Log.Information("   Value: {va}", start);
            return start;
        }

    }


    public class Day05 : DayBase
    {
        
        private ILogger logger;
        private List<string> records;

        public Day05() : base("Day05")
        {
            logger = Log.ForContext<Day05>();
            records = new List<string>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            while ((line = inputStream.ReadLine()) != null)
            {
                records.Add(line);
            }
            return records.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            int result = -1;

            int rowValue = 0;
            int seatValue = 0;
            int seatId = 0;
            foreach(var row in records)
            {
                rowValue = BinarySpacePartion.ComputePath(
                    row.Substring(0, 8).GetEnumerator(), 0, 128, 'F', 'B');
                seatValue = BinarySpacePartion.ComputePath(
                    row.Substring(7).GetEnumerator(), 0, 8, 'L', 'R');

                seatId = rowValue * 8 + seatValue;
                result = Math.Max(result, seatId);
            }

            return new ToStringSolution<int>(NamePart1, result);
        }

        protected override ISolution SolvePart2Impl()
        {
            int result = -1;

            int rowValue = 0;
            int seatValue = 0;
            int seatId = 0;
            List<int> seatIds = new List<int>();

            foreach (var row in records)
            {
                rowValue = BinarySpacePartion.ComputePath(
                    row.Substring(0, 8).GetEnumerator(), 0, 128, 'F', 'B');
                seatValue = BinarySpacePartion.ComputePath(
                    row.Substring(7).GetEnumerator(), 0, 8, 'L', 'R');

                seatId = rowValue * 8 + seatValue;

                seatIds.Add(seatId);
            }

            seatIds.Sort();

            result = Enumerable.Range(seatIds[0], seatIds[seatIds.Count - 1]).Except(seatIds).First();

            return new ToStringSolution<int>(NamePart2, result);
        }

    }
}
