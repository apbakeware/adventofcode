﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{

    public class PasswordSpec
    {
        public int Min { get; }
        public int Max { get; }
        public char Letter { get; }
        public string Password { get; }

        static private readonly Regex specRegExParser = new Regex(@"(?<min>\d+)-(?<max>\d+) (?<letter>\w): (?<password>\w+)");

        public static PasswordSpec Create(string specLine)
        {
            Match regMatch = specRegExParser.Match(specLine);
            if (!regMatch.Success)
            {
                throw new FormatException("Spec line does not meet expectations");
            }

            return new PasswordSpec(
                int.Parse(regMatch.Groups["min"].Value),
                int.Parse(regMatch.Groups["max"].Value),
                (regMatch.Groups["letter"].Value)[0],
                regMatch.Groups["password"].Value
            );
        }

        public bool ValidatePolicy1()
        {
            // LINQ
            int occurances = Password.Count(l => l == Letter);
            return occurances >= Min && occurances <= Max;
        }

        public bool ValidatePolicy2()
        {
            return (Password[Min - 1] == Letter) ^ (Password[Max - 1] == Letter);
        }

        public PasswordSpec(int min, int max, char letter, string passwd)
        {
            Min = min;
            Max = max;
            Letter = letter;
            Password = passwd;
        }
    }


    public class Day02 : DayBase
    {
        private ILogger logger;

        private List<PasswordSpec> specList;

        public Day02() : base("Day02")
        {
            logger = Log.ForContext<Day02>();
            specList = new List<PasswordSpec>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            specList.Clear();
            while ((line = inputStream.ReadLine()) != null)
            {
                specList.Add(PasswordSpec.Create(line));
            }
            return specList.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            return new ToStringSolution<int>(
                NamePart1, specList.Count(spec => spec.ValidatePolicy1())
            );
        }

        protected override ISolution SolvePart2Impl()
        {
            return new ToStringSolution<int>(
                NamePart2, specList.Count(spec => spec.ValidatePolicy2())
            );
        }
    }
}