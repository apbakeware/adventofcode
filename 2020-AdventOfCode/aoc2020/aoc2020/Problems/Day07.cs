﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{

    public class TreeNode
    {
        public string Name { get; }
        public List<TreeNode> Children { get; }

        public Dictionary<string, int> ContentCount;

        public TreeNode(string name)
        {
            Name = name;
            Children = new List<TreeNode>();
            ContentCount = new Dictionary<string, int>();
        }

        public void Add(int qty, TreeNode kid)
        {
            ContentCount.Add(kid.Name, qty);
            Children.Add(kid);
        }
    }

    public class Day07 : DayBase
    {

        private ILogger logger;

        Dictionary<string, TreeNode> Tree;
        Dictionary<string, bool> containsShineyGold;

        private readonly Regex contentSplitter = new Regex(@"(?<qty>\d+) (?<bag>.*) bag(s?)\.?");

        public Day07() : base("Day07")
        {
            logger = Log.ForContext<Day07>();
            Tree = new Dictionary<string, TreeNode>();
            containsShineyGold = new Dictionary<string, bool>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            while ((line = inputStream.ReadLine()) != null)
            {

                var container = line.Split(" bags contain ");
                var bag = container[0];
                TreeNode node = GetNodeFor(bag);
                var contentItems = container[1].Split(",");

                foreach (var content in contentItems)
                {
                    Match contentMatch = contentSplitter.Match(content);
                    if (contentMatch.Success)
                    {
                        var qty = int.Parse(contentMatch.Groups["qty"].Value);
                        var contained = contentMatch.Groups["bag"].Value;

                        TreeNode kid;
                        if (Tree.ContainsKey(contained))
                        {
                            kid = Tree[contained];
                        }
                        else
                        {
                            kid = new TreeNode(contained);
                            Tree.Add(contained, kid);
                        }

                        node.Add(qty, kid);
                    }
                }

            }
            return Tree.Count;
        }

        private TreeNode GetNodeFor(string bag)
        {
            TreeNode node;
            if (Tree.ContainsKey(bag))
            {
                node = Tree[bag];
            }
            else
            {
                node = new TreeNode(bag);
                Tree.Add(bag, node);
            }

            return node;
        }

        protected override ISolution SolvePart1Impl()
        {
            int result = 0;
            containsShineyGold = new Dictionary<string, bool>();
            foreach (var node in Tree.Values)
            {
                var contained = CheckIfEventuallyContainsShineyGold(node);
                if (contained)
                {
                    result += 1;
                }
            }

            return new ToStringSolution<int>(NamePart1, result);
        }

        protected override ISolution SolvePart2Impl()
        {
            TreeNode shinyGold = Tree["shiny gold"];
            int result = GetRequiredBagCount(shinyGold);

            return new ToStringSolution<int>(NamePart2, result);
        }

        private bool CheckIfEventuallyContainsShineyGold(TreeNode node, int recurse = 1)
        {
            if (containsShineyGold.ContainsKey(node.Name))
            {
                return containsShineyGold[node.Name];
            }

            foreach (var kid in node.Children)
            {
                if (kid.Name == "shiny gold")
                {
                    containsShineyGold[node.Name] = true;
                    return true;
                }
                else if (CheckIfEventuallyContainsShineyGold(kid, recurse + 1))
                {
                    return true;
                }
            }

            return false;
        }

        private int GetRequiredBagCount(TreeNode node)
        {
            int count = 0;
            foreach (var kid in node.Children)
            {
                count += node.ContentCount[kid.Name] + node.ContentCount[kid.Name] * GetRequiredBagCount(kid);
            }

            return count;
        }

    }
}
