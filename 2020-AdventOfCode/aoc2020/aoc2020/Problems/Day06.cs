﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{

    public class Day06 : DayBase
    {

        private ILogger logger;
        private List<string> records;

        public Day06() : base("Day06")
        {
            logger = Log.ForContext<Day06>();
            records = new List<string>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line = "";
            records.Add(line);
            while ((line = inputStream.ReadLine()) != null)
            {
                if (line == "")
                {
                    line = new string("");
                    records.Add(line);
                }
                else
                {
                    if (records[records.Count - 1].Length > 0)
                    {
                        records[records.Count - 1] += "|"; 
                    }
                    records[records.Count - 1] += line;

                }
            }
            return records.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            int result = 0;
            string divider = "|";
            foreach(var record in records)
            {
                result += record.Except(divider).Distinct().Count();
            }

            return new ToStringSolution<int>(NamePart1, result);
        }

        protected override ISolution SolvePart2Impl()
        {
            int result = 0;

            string divider = "|";
            foreach (var record in records)
            {
                var groupSize =  record.Split(divider).Count();
                var countByChar = record.Replace(divider, "")
                    .GroupBy(c => c)
                    .Select(c => new { Char = c.Key, Count = c.Count() })
                    .Count(record => record.Count == groupSize);

                result += countByChar;
            }

            return new ToStringSolution<int>(NamePart2, result);
        }

    }
}
