﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;
using aoc2020.Utils;

namespace aoc2020.Problems
{

    public class Day15 : DayBase
    {

        private ILogger logger;
        private List<int> values;


        public Day15() : base("Day15")
        {
            logger = Log.ForContext<Day15>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            values = new List<int>();
            while ((line = inputStream.ReadLine()) != null)
            {
                foreach(var val in line.Split(','))
                {
                    values.Add(int.Parse(val));
                }
            }
            return values.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            int lastSpoken = PlayTheGame(2020);

            return new ToStringSolution<int>(NamePart1, lastSpoken);
        }

        protected override ISolution SolvePart2Impl()
        {
            int lastSpoken = PlayTheGame(30000000);

            return new ToStringSolution<int>(NamePart2, lastSpoken);
        }

        private int PlayTheGame(int targetTurn)
        {
            Dictionary<int, int> gameTurns = new Dictionary<int, int>();
            int turn = 1;
            int lastSpoken = 0;

            for (int idx = 0; idx < values.Count - 1; idx++)
            {
                lastSpoken = values[idx];
                gameTurns[lastSpoken] = turn;
                turn++;
            }

            lastSpoken = values[values.Count - 1];
            for (; turn < targetTurn; turn++)
            {
                //logger.Information("Speaking: {0}  Turn: {1}", lastSpoken, turn);
                var stored = lastSpoken;
                if (gameTurns.ContainsKey(lastSpoken))
                {
                    //logger.Information(" Was spoken on turn {0}", gameTurns[lastSpoken]);
                    lastSpoken = turn - gameTurns[lastSpoken];
                }
                else
                {
                    //logger.Information(" Was not spoken");
                    lastSpoken = 0;
                }
                //logger.Information("  --- Speaking {0}", lastSpoken);
                gameTurns[stored] = turn;
            }

            return lastSpoken;
        }

        

 
    }
}
