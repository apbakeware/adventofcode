﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{
    public class Day01 : DayBase
    {
        private ILogger logger;
        private List<int> values;
        private HashSet<int> valuesHash;

        public Day01() : base("Day01")
        {
            logger = Log.ForContext<Day01>();
            values = new List<int>();
            valuesHash = new HashSet<int>();
        }


        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            values.Clear();
            valuesHash.Clear();
            while ((line = inputStream.ReadLine()) != null)
            {
                int value = int.Parse(line);
                values.Add(value);
                valuesHash.Add(value);
            }
            return values.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            return new ToStringSolution<int>(NamePart1, FindSumPairAndMultiply(2020));
        }

        protected override ISolution SolvePart2Impl()
        {
            return new ToStringSolution<int>(NamePart2, FindSumTripleAndMultiply(2020));
        }

        private int FindSumPairAndMultiply(int sum)
        {
            foreach(int val in values)
            {
                int requiredValue = sum - val;
                if( valuesHash.Contains(requiredValue) )
                {
                    logger.Debug("Found sum pair {sum} : {v1} x {v2} = {mul}"
                        , sum, val, requiredValue, val*requiredValue);
                    return val * requiredValue;
                }
            }

            // TODO: throw
            return -1;
        }

        private int FindSumTripleAndMultiply(int sum)
        {
            foreach (int val in values)
            {
                int requiredValue = sum - val;
                int pairSum = FindSumPairAndMultiply(requiredValue);
                if( pairSum > -1)
                {
                    logger.Debug("Found sum triplet {mul}"
                        , val * pairSum);
                    return val * pairSum;
                }
            }

            // TODO: throw
            return -1;
        }

        
    }
}
