﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;

namespace aoc2020.Problems
{

    public class Day10 : DayBase
    {

        private ILogger logger;
        List<int> adapters;

        public Day10() : base("Day10")
        {
            logger = Log.ForContext<Day10>();
            adapters = new List<int>();    
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            adapters.Add(0);
            while ((line = inputStream.ReadLine()) != null)
            {
                adapters.Add(int.Parse(line));
            }
            adapters.Sort();
            adapters.Add(adapters[adapters.Count - 1] + 3); // Add your device
            return adapters.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            int prev = 0;
            (int one, int three) counts = (0, 0);
            foreach(var adapter in adapters)
            {
                var diff = adapter - prev;
                if(diff == 1) { counts.one++; }
                else if( diff == 3) { counts.three++; }
                prev = adapter;
            }
            return new ToStringSolution<int>(NamePart1, counts.one * counts.three);
        }


        // Path counting algorithm ... with help from Noah, thanks!
        // "in any path counting problem, the number of paths to reach a point is 
        // equal to the sum of the number of paths to all the points that can reach it"
        protected override ISolution SolvePart2Impl()
        {

            int targetJolts = adapters.Max();
            Dictionary<long, long> pathsFromAdapterValue = new Dictionary<long, long>();
            foreach (var adapter in adapters)
            {
                var sum = pathsFromAdapterValue
                    .Where(i => i.Key >= adapter - 3 && i.Key <= adapter - 1)
                    .Select(i => i.Value)
                    .Sum();

                pathsFromAdapterValue[adapter] = sum == 0 ? 1 : sum;
                //logger.Information("Checking {0}: paths to {1}", adapter, pathsFromAdapterValue[adapter]);
            }

            return new ToStringSolution<double>(NamePart2, pathsFromAdapterValue[targetJolts]);
        }

    }
}
