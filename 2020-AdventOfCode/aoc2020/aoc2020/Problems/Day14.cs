﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;
using aoc2020.Utils;

namespace aoc2020.Problems
{

    public class Day14 : DayBase
    {

        private ILogger logger;
        private List<string> instructions;
        private Regex maskRegex = new Regex(@"^mask = (?<mask>[1X0]+)$");
        private Regex memRegex = new Regex(@"^mem\[(?<addr>\d+)\] = (?<val>\d+)$");

        public Day14() : base("Day14")
        {
            logger = Log.ForContext<Day14>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            instructions = new List<string>();
            string line;
            while ((line = inputStream.ReadLine()) != null)
            {
                instructions.Add(line);
            }
            return instructions.Count;
        }

        protected override ISolution SolvePart1Impl()
        {
            const int numBits = 36;
            ulong mask36 = Convert.ToUInt64("111111111111111111111111111111111111", 2);
            string currentMask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
            Dictionary<int, ulong> memory = new Dictionary<int, ulong>();
            Match regexMatch;

            foreach (var instruction in instructions)
            {
                regexMatch = maskRegex.Match(instruction);
                if(regexMatch.Success)
                {
                    string tmpMask = regexMatch.Groups["mask"].Value;
                    char[] ary = tmpMask.ToArray();
                    Array.Reverse(ary);
                    currentMask = new string(ary);
                    
                }
                else
                {
                    regexMatch = memRegex.Match(instruction);
                    {
                        if(regexMatch.Success)
                        {
                            int addr = int.Parse(regexMatch.Groups["addr"].Value);
                            ulong value = ulong.Parse(regexMatch.Groups["val"].Value);
                            string binaryStringVal = Convert.ToString((long)value, 2);

                            if(!memory.ContainsKey(addr))
                            {
                                memory[addr] = 0x0;
                            }

                            for(int idx = 0;  idx < numBits; idx++)
                            {
                                ulong testBit = 0x1UL << idx;
                                switch (currentMask[idx])
                                {
                                    case 'X':
                                        if((value & testBit) > 0)
                                        {
                                            memory[addr] |= testBit;
                                        }
                                        else
                                        {
                                            memory[addr] &= ~testBit;
                                        }
                                    
                                        break;

                                    case '1':
                                        memory[addr] |= testBit;
                                        break;

                                    case '0':
                                        memory[addr] &= ~testBit;
                                        break;
                                }
                                //logger.Information("{1}] Memory Now: {0}", Convert.ToString((int)memory[addr], 2), idx);
                            }

                            memory[addr] &= mask36;
                            //logger.Information("[Updated memory to: {0}", Convert.ToString((int)memory[addr], 2));
                        }
                        else
                        {
                            logger.Error("Unknown line parse");
                        }
                    }
                }
            }

            ulong result = memory.Aggregate(0UL, (i, j) => i + j.Value);
            return new ToStringSolution<ulong>(NamePart1, result);
        }


        protected override ISolution SolvePart2Impl()
        {
            Dictionary<string, string> memory = new Dictionary<string, string>();

            string currentMask = "";
            Match regexMatch;

            foreach (var instruction in instructions)
            {
                regexMatch = maskRegex.Match(instruction);
                if (regexMatch.Success)
                {
                    currentMask = regexMatch.Groups["mask"].Value;


                }
                else
                {
                    regexMatch = memRegex.Match(instruction);
                    {
                        if (regexMatch.Success)
                        {
                            int addr = int.Parse(regexMatch.Groups["addr"].Value);
                            string value = regexMatch.Groups["val"].Value;
                            string[] memoryLocations = CreateAddrsFromMask(currentMask, addr);
                            foreach(var location in memoryLocations)
                            {
                                memory[location] = value;
                            }
                        }
                        else
                        {
                            logger.Error("Unknown line parse");
                        }
                    }
                }
            }

            ulong result = memory.Aggregate(0UL, (i, j) => i + ulong.Parse(j.Value));

            return new ToStringSolution<ulong>(NamePart2, result);
        }

        public static string[] CreateAddrsFromMask(string mask, int addr)
        {
            int xCount = mask.Count(c => c == 'X');
            int size = 1 << xCount;
            int skip = size;            
            StringBuilder[] stringBuilders = new StringBuilder[size];
            string addrString = String.Format("{0,36}", Convert.ToString(addr, 2))
                .Replace(' ', '0');

            for(int idx = 0; idx < stringBuilders.Length; idx++)
            {
                stringBuilders[idx] = new StringBuilder();
            }

            for(int idx = 0; idx < mask.Length; idx++)
            {
                char maskChar = mask[idx];
                char newChar = '0';
                switch(maskChar)
                {
                    case '0':
                        foreach(var builder in stringBuilders)
                        {
                            builder.Append(addrString[idx]);
                        }
                        break;

                    case '1':
                        foreach (var builder in stringBuilders)
                        {
                            builder.Append("1");
                        }
                        break;

                    case 'X':
                        skip /= 2;
                        //Log.Information("Skip {0}", skip);
                        for(int jdx = 0; jdx < stringBuilders.Length; jdx++)
                        {
                            if(jdx % skip == 0)
                            {
                                
                                newChar = (newChar == '0') ? '1' : '0';
                                //Log.Information("Setting newchar to {0}", newChar);
                            }
                            //Log.Information("idx: {0}  char: {1}", jdx, newChar);
                            stringBuilders[jdx].Append(newChar);
                            //Log.Information("Str : {0}", stringBuilders[jdx].ToString());
                        }
                        
                        break;

                }
            }

            return stringBuilders.Select(x => x.ToString()).ToArray();
        }
    }
}
