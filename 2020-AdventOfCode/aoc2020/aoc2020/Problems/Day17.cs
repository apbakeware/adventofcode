﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Serilog;
using Serilog.Context;
using aoc2020.Solutions;
using aoc2020.Utils;

namespace aoc2020.Problems
{
    // Value object IE state doesnt chagne, you get a new object
    public readonly struct XYZ
    {
        public readonly int X { get; }
        public readonly int Y { get; }
        public readonly int Z { get; }

        public XYZ(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static XYZ operator+(XYZ lhs, XYZ rhs) => 
            new XYZ(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z);

        public static XYZ operator -(XYZ lhs, XYZ rhs) =>
            new XYZ(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z - rhs.Z);

        public static XYZ operator +(XYZ lhs, int val) =>
            new XYZ(lhs.X + val, lhs.Y + val, lhs.Z + val);

        public static XYZ operator -(XYZ lhs, int val) =>
            new XYZ(lhs.X - val, lhs.Y - val, lhs.Z - val);
    }

    public readonly struct XYZW
    {
        public readonly int X { get; }
        public readonly int Y { get; }
        public readonly int Z { get; }

        public readonly int W { get; }

        public XYZW(int x, int y, int z, int w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }

        public static XYZW operator +(XYZW lhs, XYZW rhs) =>
            new XYZW(lhs.X + rhs.X, lhs.Y + rhs.Y, lhs.Z + rhs.Z, lhs.W + rhs.W);

        public static XYZW operator -(XYZW lhs, XYZW rhs) =>
            new XYZW(lhs.X - rhs.X, lhs.Y - rhs.Y, lhs.Z - rhs.Z, lhs.W - rhs.W);

        public static XYZW operator +(XYZW lhs, int val) =>
            new XYZW(lhs.X + val, lhs.Y + val, lhs.Z + val, lhs.W + val);

        public static XYZW operator -(XYZW lhs, int val) =>
            new XYZW(lhs.X - val, lhs.Y - val, lhs.Z - val, lhs.W - val);
    }

    public class Day17 : DayBase
    {

        static private XYZ[] directionalOffsets3 = CreateOffsets3();
        static private XYZW[] directionalOffsets4 = CreateOffsets4();

        private ILogger logger;
        //readonly Regex constrainRegex = new Regex(@"^(?<key>.+): (?<low1>\d+)-(?<high1>\d+) or (?<low2>\d+)-(?<high2>\d+)$");
        // Create offsetters.

        private Dictionary<XYZ, bool> cubeStatuses;
        private Dictionary<XYZW, bool> cubeStatuses4;
        private XYZ gridUpperLeft; // based on input reference
        private XYZ gridLowerRight;
        private XYZW gridUpperLeft4; // based on input reference
        private XYZW gridLowerRight4;

        public Day17() : base("Day17")
        {
            logger = Log.ForContext<Day17>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            string line;
            int row = 0;
            int col = 0;
            int z = 0;
            cubeStatuses = new Dictionary<XYZ, bool>();
            cubeStatuses4 = new Dictionary<XYZW, bool>();
            gridUpperLeft = new XYZ(col, row, z);
            gridLowerRight4 = new XYZW(col, row, z, z);
            while ((line = inputStream.ReadLine()) != null)
            {
                col = 0;
                foreach(char c in line)
                {
                    XYZ key = new XYZ(col, row, z);
                    bool active = c == '#';
                    cubeStatuses.Add(key, active);
                    cubeStatuses4.Add(new XYZW(col, row, z, z), active);
                    col++;
                }
                row++;
            }
            gridLowerRight = new XYZ(col - 1, row - 1, z);
            gridLowerRight4 = new XYZW(col - 1, row - 1, z, z);
            return cubeStatuses.Count;
        }

        // https://en.wikipedia.org/wiki/Cellular_evolutionary_algorithm
        // https://www.reddit.com/r/adventofcode/comments/kf2x4l/2020_day_17_helping_to_understand_the_sample/
        protected override ISolution SolvePart1Impl()
        {
            return new ToStringSolution<int>(NamePart1, -1);
            const int targetPhases = 6;
            Dictionary<XYZ, bool> nextPhase = new Dictionary<XYZ, bool>();
            XYZ phaseUL = gridUpperLeft - 1;
            XYZ phaseLR = gridLowerRight + 1;

            for (int phase = 0; phase < targetPhases; phase++)
            {
                for (int z = phaseUL.Z; z <= phaseLR.Z; z++) 
                {
                    for(int y = phaseUL.Y; y <= phaseLR.Y; y++)
                    {
                        for (int x = phaseUL.X; x <= phaseLR.X; x++)
                        {
                            XYZ loc = new XYZ(x, y, z);
                            bool isActive = GetStatusAndMark(loc, cubeStatuses);
                            int neighborActiveCount = CountActiveNeighbors(loc, cubeStatuses);
                            bool newState = false;

                            if(isActive && neighborActiveCount == 2 || neighborActiveCount == 3)
                            {
                                newState = true;
                            }
                            else if(!isActive && neighborActiveCount == 3)
                            {
                                newState = true;
                            }

                            nextPhase[loc] = newState;
                        }
                    }
                }

                phaseUL -= 1;
                phaseLR += 1;
                cubeStatuses = nextPhase;
                nextPhase = new Dictionary<XYZ, bool>();  // THIS WAS KEY BUG>>> references!!!!
            }

            int activeCount = cubeStatuses.Where(x => x.Value == true).Count();

            return new ToStringSolution<int>(NamePart1, activeCount);
        }

        protected override ISolution SolvePart2Impl()
        {
            // TODO: need to reset the data input after part1
            return new ToStringSolution<int>(NamePart1, -1);

            const int targetPhases = 1;
            Dictionary<XYZW, bool> nextPhase = new Dictionary<XYZW, bool>();
            XYZW phaseUL4 = gridUpperLeft4 - 1;
            XYZW phaseLR4 = gridLowerRight4 + 1;

            for (int phase = 0; phase < targetPhases; phase++)
            {
                Console.WriteLine($"Phase: {phase}");

                for (int w = phaseUL4.W; w <= phaseLR4.W; w++)
                {
                    for (int z = phaseUL4.Z; z <= phaseLR4.Z; z++)
                    {
                        for (int y = phaseUL4.Y; y <= phaseLR4.Y; y++)
                        {
                            for (int x = phaseUL4.X; x <= phaseLR4.X; x++)
                            {
                                XYZW loc = new XYZW(x, y, z, w);
                                bool isActive = GetStatusAndMark(loc, cubeStatuses4);
                                int neighborActiveCount = CountActiveNeighbors4(loc, cubeStatuses4);
                                bool newState = false;

                                if (isActive && neighborActiveCount == 2 || neighborActiveCount == 3)
                                {
                                    newState = true;
                                }
                                else if (!isActive && neighborActiveCount == 3)
                                {
                                    newState = true;
                                }

                                nextPhase[loc] = newState;
                            }
                        }
                    }
                }

                phaseUL4 -= 1;
                phaseLR4 += 1;
                cubeStatuses4 = nextPhase;
                nextPhase = new Dictionary<XYZW, bool>();  // THIS WAS KEY BUG>>> references!!!!
            }

            int activeCount = cubeStatuses4.Where(x => x.Value == true).Count();
            return new ToStringSolution<int>(NamePart2, activeCount);
        }

        static private XYZ[] CreateOffsets3()
        {
            List<XYZ> offsets = new List<XYZ>();
            for(int x = -1; x <= 1; x++)
            {
                for(int y = -1; y <= 1; y++)
                {
                    for(int z = -1; z <= 1; z++)
                    {
                        if (x == 0 && y == 0 && z == 0) continue;
                        offsets.Add(new XYZ(x, y, z));
                    }
                }
            }
            return offsets.ToArray();
        }

        static private XYZW[] CreateOffsets4()
        {
            List<XYZW> offsets = new List<XYZW>();
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    for (int z = -1; z <= 1; z++)
                    {
                        for (int w = -1; w <= 1; w++)
                        {
                            if (x == 0 && y == 0 && z == 0) continue;
                            offsets.Add(new XYZW(x, y, z,w));
                        }
                    }
                }
            }
            return offsets.ToArray();
        }

        //static bool GetStatusAndMark<KeyT>(KeyT location, Dictionary<KeyT, bool> grid)
        //{
        //    // AVOID DOUBLE LOOKUP?
        //    if(!grid.ContainsKey(location))
        //    {
        //        grid[location] = false;
        //        return false;
        //    }

        //    return grid[location];
        //}

        static bool GetStatusAndMark<KeyT>(KeyT location, Dictionary<KeyT, bool> grid)
        {
            // AVOID DOUBLE LOOKUP?
            if (!grid.ContainsKey(location))
            {
                grid[location] = false;
                return false;
            }

            return grid[location];
        }

        private int CountActiveNeighbors(XYZ location, Dictionary<XYZ, bool> grid)
        {
            int count = 0;
            foreach(var offset in directionalOffsets3)
            {
                var testLocation = location + offset;
                if(grid.ContainsKey(testLocation) && grid[testLocation])
                {
                    count++;
                }
            }
            return count;
        }

        private int CountActiveNeighbors4(XYZW location, Dictionary<XYZW, bool> grid)
        {
            int count = 0;
            foreach (var offset in directionalOffsets4)
            {
                var testLocation = location + offset;
                if (grid.ContainsKey(testLocation) && grid[testLocation])
                {
                    count++;
                }
            }
            return count;
        }
    }
}
