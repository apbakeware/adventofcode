﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Serilog;
using Serilog.Context;
using System.Linq;

using aoc2020.Solutions;
using aoc2020.Utils;

namespace aoc2020.Problems
{

    // Create all permutations (8) borders for each image. Put in a dictionary
    // cand count the occurances.  All are 1 or 2 confirming a border will
    // have 0 or 1 matching borders

    public class ImageGrid
    {
        public long TileNumber { get; }

        public string[] Borders { get; private set; }
        public string[] BordersFlippedV { get; private set; }
        public string[] BordersFlippedH { get; private set; }

        public string[] BorderPermutations
        {
            get
            {
                return new string[]
                {
                    Borders[0], Borders[1], Borders[2], Borders[3],
                    Borders[0].Reverse(), Borders[1].Reverse(), Borders[2].Reverse(), Borders[3].Reverse()
                };
            }
        }

        public int TopIndex { get; private set; }

        public int RightIndex { get => (TopIndex + 1) % 4; }

        public int BottomIndex { get => (TopIndex + 2) % 4; }
        public int LeftIndex { get => (TopIndex + 3) % 4; }

        public List<string> CommonBorders { get; }

        private List<string> ImageContent { get; }

        public ImageGrid(int tileNumber, List<string> imageContent)
        {
            TileNumber = tileNumber;
            this.ImageContent = imageContent;
            Borders = new string[] { string.Empty, string.Empty, string.Empty, string.Empty };
            BordersFlippedV = new string[] { string.Empty, string.Empty, string.Empty, string.Empty };
            BordersFlippedH = new string[] { string.Empty, string.Empty, string.Empty, string.Empty };
            CommonBorders = new List<string>();
            TopIndex = 0;
            ExtractBorders();
        }
        
        public void RotateRight()
        {
            TopIndex = (TopIndex - 1 + 4) % 4;
        }

        public void RotateLeft()
        {
            TopIndex = (TopIndex + 1 + 4) % 4;
        }

        //      0 
        //   >>>>>>>
        //   v     v
        // 3 v     v 1
        //   v     v
        //   >>>>>>>
        //      2
        private void ExtractBorders()
        {
            StringBuilder leftBuilder = new StringBuilder();
            StringBuilder rightBuilder = new StringBuilder();

            int rightCol = ImageContent[0].Length - 1;

            foreach(var row in ImageContent)
            {
                leftBuilder.Append(row[0]);
                rightBuilder.Append(row[rightCol]);
            }

            Borders[0] = ImageContent[0];
            Borders[1] = rightBuilder.ToString();
            Borders[2] = ImageContent[ImageContent.Count - 1];
            Borders[3] = leftBuilder.ToString();

            BordersFlippedV[0] = Borders[2];
            BordersFlippedV[1] = Borders[1].Reverse();
            BordersFlippedV[2] = Borders[0].Reverse();
            BordersFlippedV[3] = Borders[3];

            BordersFlippedH[0] = Borders[0].Reverse();
            BordersFlippedH[1] = Borders[3];
            BordersFlippedH[2] = Borders[2].Reverse();
            BordersFlippedH[3] = Borders[1];
        }
    }

    public class ImageAssembler
    {
        public List<ImageGrid> ImageTiles { get; }

        /// <summary>
        /// empty list if no corners are found
        /// </summary>
        public List<ImageGrid> CornerTiles { get; }
        public List<ImageGrid> Edges { get; }
        public List<ImageGrid> Middles { get; }

        private readonly ILogger logger;

        private Dictionary<string, int> commonBordersCount;
        private Dictionary<string, List<ImageGrid>> commonBordersImages;

        public ImageAssembler(List<ImageGrid> imageTiles)
        {
            ImageTiles = imageTiles;
            CornerTiles = new List<ImageGrid>();
            Edges = new List<ImageGrid>();
            Middles = new List<ImageGrid>();
            logger = Log.ForContext<ImageAssembler>();
            commonBordersCount = new Dictionary<string, int>();
            commonBordersImages = new Dictionary<string, List<ImageGrid>>();
        }

        public void AssembleIntoGroups()
        {
            InitializeAssembly();
            CategorizeEachPiece();
        }

        private void InitializeAssembly()
        {
            commonBordersCount.Clear();
            commonBordersImages.Clear();
            CornerTiles.Clear();
            Edges.Clear();
            Middles.Clear();
            foreach (var image in ImageTiles)
            {
                foreach (var border in image.BorderPermutations)
                {
                    if (commonBordersCount.ContainsKey(border))
                    {
                        commonBordersImages[border].Add(image);
                        commonBordersCount[border]++;
                    }
                    else
                    {
                        List<ImageGrid> imageList = new List<ImageGrid>() { image };
                        commonBordersImages[border] = imageList;
                        commonBordersCount[border] = 1;
                        
                    }
                }
            }
        }

        /// <summary>
        ///  Works assuming that there is only 1 valid assembly and that images with shared borders
        ///  only fit in one spot.
        ///  Images which have 2 shared borders are coners. Images with 3 are "middle" edgess. Images
        ///  with 4 common borders are inner pieces.
        /// </summary>
        private void CategorizeEachPiece()
        {
            foreach (var image in ImageTiles)
            {
                int commonBorderCount = 0;
                for (int idx = 0; idx < 4; idx++)
                {
                    if(commonBordersCount[image.Borders[idx]] > 1)
                    {
                        commonBorderCount++;
                        image.CommonBorders.Add(image.Borders[idx]);
                    }
                }

                switch(commonBorderCount)
                {
                    case 2:
                        CornerTiles.Add(image);
                        break;

                    case 3:
                        Edges.Add(image);
                        break;

                    case 4:
                        Middles.Add(image);
                        break;

                    default:
                        throw new ApplicationException("Unexpected number of common sides");
                }
            }
        }

        public void AssembleFinalImage()
        {

            // two times for top and bottomw row.
            // start with corner and one of the common edges
            // find the "middle" edge (upt to next corner) with thhe common edge
            // find corder with common edge

            // Build other rows

            // organize the rows.

            ImageGrid topLeft = CornerTiles[0];
            bool found = false;
            int sideLength = 3;
            int numEdgesPiecesPerSide = sideLength - 2;
            List<List<ImageGrid>> assembleImages = new List<List<ImageGrid>>();

            // Assemble top cornder row.
            List<ImageGrid> row = new List<ImageGrid>();
            
            row.Append(CornerTiles[0]);
            for(int cnt = 0; cnt < numEdgesPiecesPerSide; cnt++)
            {

            }
        }

        public void ScanForSeaMonsters()
        {


        }

    }
    public class Day20 : DayBase
    {
        private ILogger logger;

        List<ImageGrid> ImageTiles;

        private readonly Regex tileRegex = new Regex(@"Tile (?<tileNo>\d+):");

        public Day20() : base("Day20Test")
        {
            logger = Log.ForContext<Day19>();
            ImageTiles = new List<ImageGrid>();
        }

        protected override int ReadInputImpl(System.IO.StreamReader inputStream)
        {
            int tileNumber = -1;
            List<string> imageRows = new List<string>();
            ImageTiles = new List<ImageGrid>();
            Match match;
            string line;
            while ((line = inputStream.ReadLine()) != null)
            {
                match = tileRegex.Match(line);
                if (match.Success)
                {
                    tileNumber = int.Parse(match.Groups["tileNo"].Value);
                    imageRows = new List<string>();
                }
                else if (line == string.Empty)
                {
                    ImageTiles.Add(new ImageGrid(tileNumber, imageRows));
                }
                else
                {
                    imageRows.Add(line);
                }
            }

            ImageGrid image = new ImageGrid(tileNumber, imageRows);
            ImageTiles.Add(image);

            return 0;
        }

        protected override ISolution SolvePart1Impl()
        {
            ImageAssembler assembler = new ImageAssembler(ImageTiles);
            assembler.AssembleIntoGroups();
            long result = assembler.CornerTiles.Aggregate(1L, (a, b) => a * b.TileNumber);

            return new ToStringSolution<long>(NamePart1, result);
        }

        protected override ISolution SolvePart2Impl()
        {
            ImageAssembler assembler = new ImageAssembler(ImageTiles);
            assembler.AssembleIntoGroups();
            assembler.AssembleFinalImage();
            return new ToStringSolution<int>(NamePart2, -1);
        }
    }
}
