﻿namespace aoc2020
{
    /// <summary>
    /// Solution to a Advent of Code problem.
    /// </summary>
    public interface ISolution
    {
        /// <summary>
        /// The name of the problem this solution is for.
        /// </summary>
        public string ProblemName { get; }

        /// <summary>
        /// The solution of the problem.
        /// </summary>
        /// <returns>String representation of the solution to the problem.</returns>
        public string Solution();
    }
}
