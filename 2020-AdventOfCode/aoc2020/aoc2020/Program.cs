﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace aoc2020
{

    class TimedSolution
    {
        public ISolution Solution { get; }
        public TimeSpan ExecutionTime {get;}
        

        public TimedSolution(ISolution solution, TimeSpan executionTime)
        {
            Solution = solution;
            ExecutionTime = executionTime;
        }

        public string ToTableRowString() 
        {
            return String.Format("| {0,10} | {1,-10} | {2,15} |",
                ExecutionTime, Solution.ProblemName, Solution.Solution());
        }
    }

    class TimeSolutionTable
    {
        public static void WriteTable(List<TimedSolution> solutions)
        {
            TimeSpan totalTime = new TimeSpan();
            Console.WriteLine(" SOLUTIONS ");
            Console.WriteLine("+==================+============+=================+");
            Console.WriteLine("| Time             | Problem    | Solution        |");
            Console.WriteLine("+------------------+------------+-----------------|");
            foreach (var soln in solutions)
            {
                Console.WriteLine(soln.ToTableRowString());
                totalTime += soln.ExecutionTime;
            }
            Console.WriteLine("+------------------+------------+-----------------+");
            Console.WriteLine(String.Format("| Total Time: {0,10}                    |", totalTime));
            Console.WriteLine("+==================+============+=================+");
        }
    }

    class ProblemSpec
    {
        public DayBase Problem { get; }
        public string InputFileName { get; }

        public ProblemSpec(DayBase problem, string inputFileName)
        {
            Problem = problem;
            InputFileName = inputFileName;
        }

        public void ReadProblemInput()
        {
            Problem.ReadInput(new System.IO.StreamReader(InputFileName));
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder();
            var config = BuildAppConfig(builder);

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();
            
            try
            {
                Console.SetWindowSize(170, 50);
                Banner();
            }
            catch(System.IO.IOException ex )
            {
                Log.Warning("Can't display banner: Why not? {0}", ex.Message);
            }

            Log.Information("Running Advent of Code 2020 solutions!");


            Stopwatch stopWatch = new Stopwatch();
            List<TimedSolution> solutions = new List<TimedSolution>();
            ProblemSpec[] problemSpecs = GetProblemSpecs();
            ISolution solution = null;
            foreach(var spec in problemSpecs)
            {
                spec.ReadProblemInput();
                stopWatch.Restart();
                solution = spec.Problem.SolvePart1();
                stopWatch.Stop();
                solutions.Add(new TimedSolution(solution, stopWatch.Elapsed));

                stopWatch.Restart();
                solution = spec.Problem.SolvePart2();
                stopWatch.Stop();
                solutions.Add(new TimedSolution(solution, stopWatch.Elapsed));
            }

            Console.WriteLine("\n\n");
            TimeSolutionTable.WriteTable(solutions);
        }

        static IConfigurationRoot BuildAppConfig(IConfigurationBuilder builder)
        {
            return builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                //.AddEnvironmentVariables()
                .Build();
        }

        static void Banner()
        {
            Console.WriteLine(@" ________  ________  ___      ___ _______   ________   _________        ________  ________      ________  ________  ________  _______      ");
            Console.WriteLine(@"|\   __  \|\   ___ \|\  \    /  /|\  ___ \ |\   ___  \|\___   ___\     |\   __  \|\  _____\    |\   ____\|\   __  \|\   ___ \|\  ___ \     ");
            Console.WriteLine(@"\ \  \|\  \ \  \_|\ \ \  \  /  / | \   __/|\ \  \\ \  \|___ \  \_|     \ \  \|\  \ \  \__/     \ \  \___|\ \  \|\  \ \  \_|\ \ \   __/|    ");
            Console.WriteLine(@" \ \   __  \ \  \ \\ \ \  \/  / / \ \  \_|/_\ \  \\ \  \   \ \  \       \ \  \\\  \ \   __\     \ \  \    \ \  \\\  \ \  \ \\ \ \  \_|/__  ");
            Console.WriteLine(@"  \ \  \ \  \ \  \_\\ \ \    / /   \ \  \_|\ \ \  \\ \  \   \ \  \       \ \  \\\  \ \  \_|      \ \  \____\ \  \\\  \ \  \_\\ \ \  \_|\ \");
            Console.WriteLine(@"   \ \__\ \__\ \_______\ \__/ /     \ \_______\ \__\\ \__\   \ \__\       \ \_______\ \__\        \ \_______\ \_______\ \_______\ \_______\");
            Console.WriteLine(@"    \|__|\|__|\|_______|\|__|/       \|_______|\|__| \|__|    \|__|        \|_______|\|__|         \|_______|\|_______|\|_______|\|_______|");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(@"                                                   _______  ________    _______  ________");
            Console.WriteLine(@"                                                  /  ___  \|\   __  \  /  ___  \|\   __  \");
            Console.WriteLine(@"                                                 /__/|_/  /\ \  \|\  \/__/|_/  /\ \  \|\  \");
            Console.WriteLine(@"                                                 |__|//  / /\ \  \\\  \__|//  / /\ \  \\\  \");
            Console.WriteLine(@"                                                     /  /_/__\ \  \\\  \  /  /_/__\ \  \\\  \");
            Console.WriteLine(@"                                                    |\________\ \_______\|\________\ \_______\");
            Console.WriteLine(@"                                                     \|_______|\|_______| \|_______|\|_______|");
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
        }

        static ProblemSpec[] GetProblemSpecs()
        {
            // TODO: get this from some non-static place
            ProblemSpec[] specs =
            {
                //new ProblemSpec( new Problems.Day01(), "Data\\Day01.txt"),
                //new ProblemSpec( new Problems.Day02(), "Data\\Day02.txt"),
                //new ProblemSpec( new Problems.Day03(), "Data\\Day03.txt"),
                //new ProblemSpec( new Problems.Day04(), "Data\\Day04.txt"),
                //new ProblemSpec( new Problems.Day05(), "Data\\Day05.txt"),
                //new ProblemSpec( new Problems.Day06(), "Data\\Day06.txt"),
                //new ProblemSpec( new Problems.Day07(), "Data\\Day07.txt"),
                //new ProblemSpec( new Problems.Day08(), "Data\\Day08.txt"),
                //new ProblemSpec( new Problems.Day09(), "Data\\Day09.txt"),
                //new ProblemSpec( new Problems.Day10(), "Data\\Day10.txt"),
                //new ProblemSpec( new Problems.Day11(), "Data\\Day11.txt"),
                //new ProblemSpec( new Problems.Day12(), "Data\\Day12.txt"),
                //new ProblemSpec( new Problems.Day13(), "Data\\Day13.txt"),
                //new ProblemSpec( new Problems.Day14(), "Data\\Day14.txt"),
                //new ProblemSpec( new Problems.Day15(), "Data\\Day15.txt"),
                //new ProblemSpec( new Problems.Day16(), "Data\\Day16.txt"),
                //new ProblemSpec( new Problems.Day17(), "Data\\Day17.txt"),
                //new ProblemSpec( new Problems.Day18(), "Data\\Day18.txt"),
                //new ProblemSpec( new Problems.Day19(), "Data\\Day19.txt"),
                new ProblemSpec( new Problems.Day20(), "Data\\Day20.txt"),
            };

            return specs;
        }
    }
}
