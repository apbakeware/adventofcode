﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Serilog;

namespace aoc2020.Utils
{

    /// <summary>
    /// Solver employing the ChineseRemainderTheorem (CRT)
    /// learned during Advent of Code 2020, Day 13.
    /// 
    /// References:
    /// <list type="bullet">
    /// <item>https://en.wikipedia.org/wiki/Chinese_remainder_theorem#:~:text=The%20Chinese%20remainder%20theorem%20is,similar%20computations%20on%20small%20integers</item>
    /// <item>https://www.youtube.com/watch?v=ru7mWZJlRQg</item>
    /// <item>https://www.youtube.com/watch?v=zIFehsBHB8o</item>
    /// </list>
    ///   
    /// </summary>
    public static class ChineseRemainderTheorem
    {
        /// <summary>
        /// Compute the CRT and return the answer.
        /// </summary>
        /// 
        /// <example>
        /// To solve 
        ///  x === 2 ( mod 3 ) # Remainder 2 when modded by 3
        ///  x === 2 ( mod 4 ) # Remainder 2 when modded by 3
        ///  x === 1 ( mod 5 ) # Remainder 2 when modded by 3
        /// 
        /// Compute( {3, 4, 4}, {2, 2, 1} );
        /// 
        /// </example>
        /// 
        /// <param name="remainders">Array of all the remainders in the problem.</param>
        /// <param name="mods">Array of all the modulo values in the problem.</param>
        public static long Compute(long[] remainders, long[] mods)
        {
            // todo: check set of mods have greatest common denominator of 1;

            


            long sum = 0;
            long N = mods.Aggregate(1L, (i, j) => i * j);
            long[] Ni = mods.Select(i => N / i).ToArray();
            long[] Xi = new long[Ni.Length];

            for (int idx = 0; idx < Ni.Length; idx++)
            {
                Xi[idx] = ComputeModuloInverse(Ni[idx], mods[idx]);
                sum += remainders[idx] * Ni[idx] * Xi[idx];
            }

            Log.Information("Remainders: {0}", remainders);
            Log.Information("Mods      : {0}", mods);
            Log.Information("N         : {0}", N);
            Log.Information("Ni        : {0}", Ni);
            Log.Information("Xi        : {0}", Xi);
            Log.Information("Xum BiNiXi: {0}", sum);
            return sum % N;
        }

        /// <summary>
        /// Computes the modulo inverse of a value given the remainder and the modulo.
        /// 
        /// x such that Val*x = Remainder (mod ModVal)
        /// </summary>
        /// <example>
        /// 56x === 1 (mod 5)
        /// </example>
        /// <param name="target">The value to get the module inverse of</param>
        /// <param name="modVal">The value to modulo by</param>
        /// <returns>The module inverse.</returns>
        public static long ComputeModuloInverse(long target, long modVal)
        {
            long xVal = 1;
            long targetFactor = target % modVal;
            while( ((targetFactor * xVal) % modVal) != 1L )
            {
                xVal++;
            }
            return xVal;
        }
    }
}
