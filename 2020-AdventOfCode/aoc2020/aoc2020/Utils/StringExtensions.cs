﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aoc2020.Utils
{
    public static class StringExtensions
    {
        /// <summary>
        /// Create and return a new string with the characters
        /// of the original string reversed.
        /// </summary>
        /// <param name="str">String to reverse</param>
        /// <returns>Reversed string</returns>
        public static string Reverse(this String str)
        {
            var ary = str.ToArray();
            Array.Reverse(ary);
            return new string(ary);
        }
    }
}
