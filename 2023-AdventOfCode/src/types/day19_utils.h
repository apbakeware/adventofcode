#ifndef __DAY19_UTILS_H__
#define __DAY19_UTILS_H__

#include <functional>
#include <iosfwd>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

namespace Day19_Utils {

class Rule_Result {
 public:
  static Rule_Result make_accept_part_result();
  static Rule_Result make_reject_part_result();
  static Rule_Result make_workflow_redirection_result(const std::string& dest);
  static Rule_Result make_indeterminate_result();

  Rule_Result();

  bool is_accept() const;
  bool is_reject() const;
  bool is_workflow_redirection() const;
  bool is_indeterminate() const;  // predicate failed, continue process

  std::string get_resulting_workflow() const;

 private:
  explicit Rule_Result(const std::string result);
  std::string m_result;
};

struct Part_Spec {
  int x_factor;
  int m_factor;
  int a_factor;
  int s_factor;
};

// TODO: advanced thought is to have the rule just
// be a std::function<> returning a result and taking
// a rule spec
class Rule {
 public:
  /**
   * Method for retrieving the correct field from the part spec.
   */
  typedef std::function<int(const Part_Spec& part_spec)> Field_Accessor;

  /**
   * Method for assessing the conditional criteria of the rule
   * against a part specification value.
   */
  typedef std::function<bool(int)> Predicate;

  Rule(Rule::Field_Accessor part_spec_field_accessor,
       Rule::Predicate rule_prediate, const Rule_Result& successful_result,
       const Rule_Result& failure_result);

  Rule_Result evaluate(const Part_Spec& part_spec) const;

  bool has_accept_condition() const;

 private:
  Field_Accessor m_field_accessor;
  Predicate m_predicate;
  Rule_Result m_successful_result;
  Rule_Result m_failure_result;
};

struct Workflow {
  std::string name;
  std::vector<Rule> rules;
};

class Day19_Workflow_Runner {
 public:
  struct Part_Workflow_Result {
    std::vector<std::string> workflow_path;
    bool was_accepted;
  };

  // Accepted, Rejected
  int execute();

  Part_Workflow_Result execute_single_part(const Part_Spec& part);

  friend std::istream& operator>>(std::istream& instr,
                                  Day19_Workflow_Runner& obj);

 private:
  Workflow initial_workflow;
  std::unordered_map<std::string, Workflow> workflow;
  std::vector<Part_Spec> parts;
};

std::istream& operator>>(std::istream& instr, Day19_Workflow_Runner& obj);

Part_Spec parse_part_spec(const std::string& str);

Rule parse_rule(const std::string& str);

Workflow parse_workflow(const std::string& str);

Rule_Result execute_workflow(Workflow& workflow, const Part_Spec& part);

bool has_accept_condition_path(const Workflow& workflow);

}  // namespace Day19_Utils

#endif /* __DAY19_UTILS_H__ */