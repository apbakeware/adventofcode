#ifndef __DAY20_UTILS_H__
#define __DAY20_UTILS_H__

#include <memory>
#include <queue>
#include <string>
#include <unordered_map>
#include <vector>

namespace Day20_Utils {

enum class Pulse {
  LOW,
  HIGH,
};

class Pulse_Generator;

/**
 * Pulse for a module to process.
 */
struct Pulse_Event {
  std::string originating_module;
  Pulse pulse_value;
};

/**
 * Structure of pulses generated by a module handling
 * a pulse event.
 */
struct Pulse_Command {
  std::string originating_module;
  std::string destination_module;
  Pulse pulse_value;
};

typedef std::queue<Pulse_Command> Pulse_Command_Q;

class Pulse_Generator {
 public:
  Pulse_Generator();

  Pulse_Generator(const std::string& name);

  virtual void add_input_module(const std::string& name);
  void add_output_module(const std::string& name);

  virtual bool handle_pulse(const Pulse_Event& event,
                            Pulse_Command_Q& in_out_q) = 0;

  const std::string name() const { return m_name; }

 protected:
  void queue_pulse(Pulse pulse, Pulse_Command_Q& in_out_q);

  std::vector<std::string> m_connected_modules;

 private:
  std::string m_name;
};

class Broadcast_Pulse_Generator : public Pulse_Generator {
 public:
  Broadcast_Pulse_Generator();

  virtual bool handle_pulse(const Pulse_Event& event,
                            Pulse_Command_Q& in_out_q) override;

 private:
};

/**
 * Flip-flop modules (prefix %) are either on or off; they are initially off. If
 * a flip-flop module receives a high pulse, it is ignored and nothing happens.
 * However, if a flip-flop module receives a low pulse, it flips between on and
 * off. If it was off, it turns on and sends a high pulse. If it was on, it
 * turns off and sends a low pulse.
 */
class Flip_Flop_Pulse_Generator : public Pulse_Generator {
 public:
  Flip_Flop_Pulse_Generator(const std::string& name);

  virtual bool handle_pulse(const Pulse_Event& event,
                            Pulse_Command_Q& in_out_q) override;

 private:
  bool m_state;
};

/**
 * Conjunction modules (prefix &) remember the type of the most recent pulse
 * received from each of their connected input modules; they initially default
 * to remembering a low pulse for each input. When a pulse is received, the
 * conjunction module first updates its memory for that input. Then, if it
 * remembers high pulses for all inputs, it sends a low pulse; otherwise, it
 * sends a high pulse.
 */
class Conjunction_Pulse_Generator : public Pulse_Generator {
 public:
  Conjunction_Pulse_Generator(const std::string& name);

  virtual bool handle_pulse(const Pulse_Event& event,
                            Pulse_Command_Q& in_out_q) override;

  virtual void add_input_module(const std::string& name) override;

 private:
  // indexed by input module name
  std::unordered_map<std::string, Pulse> m_input_connection_states;
};

/**
 * Receives pulses, but does not generate more.
 */
class Termination_Pulse_Generator : public Pulse_Generator {
 public:
  Termination_Pulse_Generator(const std::string& name);

  virtual bool handle_pulse(const Pulse_Event& event,
                            Pulse_Command_Q& in_out_q) override;
};

class Pulse_Generator_Runner {
 public:
  Pulse_Generator_Runner();

  Pulse_Generator_Runner& add_generator(Pulse_Generator& generator);

  // Throw if broadcase not set
  std::pair<int, int> press_button();

  bool press_button_looking_for_rx_low_pulse();

 private:
  Pulse_Generator* m_broadcaster;
  std::unordered_map<std::string, Pulse_Generator*> m_generator_map;
};

struct In_Out_Pair {
  const std::string source;
  const std::string sink;
};

typedef std::vector<In_Out_Pair> In_Out_Pairs;

// return the pointer and add to the in/out pairs
Pulse_Generator* build_generator(const std::string& line,
                                 In_Out_Pairs& in_out_pairs);

std::vector<Pulse_Generator*> build_generators(
    const std::vector<std::string>& lines);

}  // namespace Day20_Utils

#endif /* __DAY20_UTILS_H__ */