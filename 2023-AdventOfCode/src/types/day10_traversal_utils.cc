#include "day10_traversal_utils.h"

bool Day10_Traversal_Utils::can_move_up(char src, char dst) {
  const bool valid_src = src == '|' || src == 'L' || src == 'J' || src == 'S';
  if (valid_src) {
    return dst == '|' || dst == '7' || dst == 'F' || dst == 'S';
  }

  return false;
}

bool Day10_Traversal_Utils::can_move_right(char src, char dst) {
  const bool valid_src = src == '-' || src == 'L' || src == 'F' || src == 'S';
  if (valid_src) {
    return dst == '-' || dst == '7' || dst == 'J' || dst == 'S';
  }

  return false;
}

bool Day10_Traversal_Utils::can_move_down(char src, char dst) {
  const bool valid_src = src == '|' || src == '7' || src == 'F' || src == 'S';
  if (valid_src) {
    return dst == '|' || dst == 'J' || dst == 'L' || dst == 'S';
  }

  return false;
}

bool Day10_Traversal_Utils::can_move_left(char src, char dst) {
  const bool valid_src = src == '-' || src == 'J' || src == '7' || src == 'S';
  if (valid_src) {
    return dst == '-' || dst == 'F' || dst == 'L' || dst == 'S';
  }

  return false;
}