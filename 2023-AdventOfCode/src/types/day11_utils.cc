#include "day11_utils.h"

#include "utils/grid/grid.h"
#include "utils/grid/grid_distance.h"
#include "utils/grid/grid_utils.h"

namespace Day11_Utils {

Galaxy_Location_Type::Galaxy_Location_Type() : row(0UL), col(0UL) {}

Galaxy_Location_Type::Galaxy_Location_Type(const Grid_Location& loc)
    : row(static_cast<Galaxy_Value_Type>(loc.row)),
      col(static_cast<Galaxy_Value_Type>(loc.col)) {
  // TODO: check if in has negative coordintes and throw
}

Galaxy_Location_Type::Galaxy_Location_Type(Galaxy_Value_Type in_row,
                                           Galaxy_Value_Type in_col)
    : row(in_row), col(in_col) {}

bool operator<(const Galaxy_Location_Type& lhs,
               const Galaxy_Location_Type& rhs) {
  return std::tie(lhs.row, lhs.col) < std::tie(rhs.row, rhs.col);
}

bool operator==(const Galaxy_Location_Type& lhs,
                const Galaxy_Location_Type& rhs) {
  return std::tie(lhs.row, lhs.col) == std::tie(rhs.row, rhs.col);
}

bool operator!=(const Galaxy_Location_Type& lhs,
                const Galaxy_Location_Type& rhs) {
  return std::tie(lhs.row, lhs.col) != std::tie(rhs.row, rhs.col);
}

std::istream& operator>>(std::istream& istr, Galaxy_Location_Type record) {
  istr >> record.row >> record.col;
  return istr;
}

Galaxy_Value_Type galaxy_distance(const Galaxy_Location_Type& orig,
                                  const Galaxy_Location_Type& dest) {
  Galaxy_Value_Type dist = 0;

  dist += (orig.row > dest.row) ? orig.row - dest.row : dest.row - orig.row;
  dist += (orig.col > dest.col) ? orig.col - dest.col : dest.col - orig.col;

  return dist;
}

Galaxy_Grid create_galaxy_grid(std::vector<std::string>::const_iterator start,
                               std::vector<std::string>::const_iterator end) {
  return create_char_grid_from_string_rows(start, end);
}

std::vector<Galaxy_Value_Type> get_rows_without_galaxies(
    const Fixed_Sized_Grid<char>& grid) {
  std::vector<Galaxy_Value_Type> row_numbers;

  for (ssize_t row = 0; row < grid.number_of_rows(); ++row) {
    const Grid_Location cell{row, 0};
    if (grid.at(cell) != GALAXY) {
      int dist_to_galaxy = distance_right_to(grid, cell, has_galaxy_pred);
      if (dist_to_galaxy == -1) {
        row_numbers.push_back(row);
      }
    }
  }

  return row_numbers;
}

std::vector<Galaxy_Value_Type> get_cols_without_galaxies(
    const Fixed_Sized_Grid<char>& grid) {
  std::vector<Galaxy_Value_Type> col_numbers;

  for (ssize_t col = 0; col < grid.number_of_cols(); ++col) {
    const Grid_Location cell{0, col};
    if (grid.at(cell) != GALAXY) {
      int dist_to_galaxy = distance_up_to(grid, cell, has_galaxy_pred);
      if (dist_to_galaxy == -1) {
        col_numbers.push_back(col);
      }
    }
  }

  return col_numbers;
}

std::vector<Galaxy_Location_Type> get_galaxy_locations(
    const Fixed_Sized_Grid<char>& grid) {
  std::vector<Galaxy_Location_Type> galaxy_locations;

  auto op = [&](const auto& loc, char value) {
    if (value == GALAXY) {
      galaxy_locations.push_back(Galaxy_Location_Type(loc));
    }
  };

  grid.for_each_cell(op);

  return galaxy_locations;
}

void do_galaxy_expansion(std::vector<Galaxy_Location_Type>& galaxy_locations,
                         const std::vector<Galaxy_Value_Type>& rows_to_expand,
                         const std::vector<Galaxy_Value_Type>& cols_to_expand,
                         Galaxy_Value_Type expansion) {
  std::sort(galaxy_locations.begin(), galaxy_locations.end(),
            [](const auto& first, const auto& second) {
              return first.row < second.row;
            });

  auto row_expansion_iter = rows_to_expand.rbegin();
  while (row_expansion_iter != rows_to_expand.rend()) {
    auto galaxy_iter = galaxy_locations.rbegin();
    auto galaxy_iter_2 = galaxy_iter;

    while (galaxy_iter_2->row > *row_expansion_iter) {
      ++galaxy_iter_2;
    }

    for (; galaxy_iter != galaxy_iter_2; ++galaxy_iter) {
      galaxy_iter->row += expansion;
    }

    ++row_expansion_iter;
  }

  std::sort(galaxy_locations.begin(), galaxy_locations.end(),
            [](const auto& first, const auto& second) {
              return first.col < second.col;
            });
  auto col_expansion_iter = cols_to_expand.rbegin();
  while (col_expansion_iter != cols_to_expand.rend()) {
    auto galaxy_iter = galaxy_locations.rbegin();
    auto galaxy_iter_2 = galaxy_iter;

    while (galaxy_iter_2->col > *col_expansion_iter) {
      ++galaxy_iter_2;
    }

    for (; galaxy_iter != galaxy_iter_2; ++galaxy_iter) {
      galaxy_iter->col += expansion;
    }

    ++col_expansion_iter;
  }
}

}  // namespace Day11_Utils