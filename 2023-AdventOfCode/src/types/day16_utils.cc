#include "day16_utils.h"

#include "../utils/grid/grid_location.h"
#include "spdlog/spdlog.h"

namespace Day16_Utils {

void Movement_Record::record_movement(Movement_Direction direction) {
  m_movement_record |= static_cast<uint32_t>(direction);
}

bool Movement_Record::has_recorded_movement(
    Movement_Direction direction) const {
  return m_movement_record & static_cast<uint32_t>(direction);
}

Beam_Grid_Type create_from_input_data(const std::vector<std::string>& input) {
  return create_char_grid_from_string_rows(input.begin(), input.end());
}

std::vector<Grid_Mover> do_movement_maintain_direction(
    const Beam_Grid_Type& grid, const Grid_Mover& current_location) {
  // SPDLOG_DEBUG("do_movement_maintain_direction()");

  if (current_location.direction == Movement_Direction::NONE ||
      current_location.in_motion == false) {
    return {{current_location.location, Movement_Direction::NONE, false}};
  }

  Grid_Mover movement_result;
  switch (current_location.direction) {
    case Movement_Direction::UP:
      // SPDLOG_DEBUG("  processing movement up");
      movement_result.location = Grid_Location::up(current_location.location);
      break;
    case Movement_Direction::RIGHT:
      // SPDLOG_DEBUG("  processing movement right");
      movement_result.location =
          Grid_Location::right(current_location.location);
      break;
    case Movement_Direction::DOWN:
      // SPDLOG_DEBUG("  processing movement down");
      movement_result.location = Grid_Location::down(current_location.location);
      break;
    case Movement_Direction::LEFT:
      // SPDLOG_DEBUG("  processing movement left");
      movement_result.location = Grid_Location::left(current_location.location);
      break;
    case Movement_Direction::NONE:
    default:
      SPDLOG_ERROR(
          "do_movement_maintain_direction() -- Unhandled Movement_Direction");
  }

  // SPDLOG_DEBUG("    checking on grid: {}", movement_result.location);
  if (is_on_grid(movement_result.location, grid)) {
    // SPDLOG_DEBUG("     result is on grid");
    movement_result.in_motion = true;
    movement_result.direction = current_location.direction;
  } else {
    // SPDLOG_DEBUG("     result is off grid");
    movement_result.location = current_location.location;
    movement_result.in_motion = false;
    movement_result.direction = Movement_Direction::NONE;
  }

  // SPDLOG_DEBUG(
  //     "  Movement Result -- Location: {}  Direction: {}  In Motion: {}",
  //     movement_result.location, (int)movement_result.direction,
  //     movement_result.in_motion);
  return {movement_result};
}

// return collection of next location as a result of moving onto the current
// location of type '-'.
// if the move ran off the grid, return the same location
// with movement false and no direction
std::vector<Grid_Mover> do_movement_horizontal_splitter(
    const Beam_Grid_Type& grid, const Grid_Mover& current_location) {
  // SPDLOG_DEBUG("do_movement_horizontal_splitter()");

  const Grid_Mover NO_MOVEMENT{current_location.location,
                               Movement_Direction::NONE, false};

  std::vector<Grid_Mover> movements;
  if (current_location.direction == Movement_Direction::RIGHT) {
    Grid_Location location = Grid_Location::right(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back({location, current_location.direction, true});
    }
  } else if (current_location.direction == Movement_Direction::LEFT) {
    Grid_Location location = Grid_Location::left(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back({location, current_location.direction, true});
    }
  } else if (current_location.direction == Movement_Direction::UP ||
             current_location.direction == Movement_Direction::DOWN) {
    // Split left
    const Grid_Mover left_split{Grid_Location::left(current_location.location),
                                Movement_Direction::LEFT, true};
    const Grid_Mover right_split{
        Grid_Location::right(current_location.location),
        Movement_Direction::RIGHT, true};

    if (is_on_grid(left_split.location, grid)) {
      movements.push_back(left_split);
    }

    if (is_on_grid(right_split.location, grid)) {
      movements.push_back(right_split);
    }
  }

  if (movements.empty()) {
    movements.push_back(NO_MOVEMENT);
  }

  return movements;
}

// return collection of next location as a result of moving onto the current
// location of type '|'.
// if the move ran off the grid, return the same location
// with movement false and no direction
std::vector<Grid_Mover> do_movement_vertical_splitter(
    const Beam_Grid_Type& grid, const Grid_Mover& current_location) {
  // SPDLOG_DEBUG("do_movement_vertical_splitter()");

  const Grid_Mover NO_MOVEMENT{current_location.location,
                               Movement_Direction::NONE, false};

  std::vector<Grid_Mover> movements;
  if (current_location.direction == Movement_Direction::UP) {
    Grid_Location location = Grid_Location::up(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back({location, current_location.direction, true});
    }
  } else if (current_location.direction == Movement_Direction::DOWN) {
    Grid_Location location = Grid_Location::down(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back({location, current_location.direction, true});
    }
  } else if (current_location.direction == Movement_Direction::LEFT ||
             current_location.direction == Movement_Direction::RIGHT) {
    // Split left
    const Grid_Mover up_split{Grid_Location::up(current_location.location),
                              Movement_Direction::UP, true};
    const Grid_Mover down_split{Grid_Location::down(current_location.location),
                                Movement_Direction::DOWN, true};

    if (is_on_grid(up_split.location, grid)) {
      movements.push_back(up_split);
    }

    if (is_on_grid(down_split.location, grid)) {
      movements.push_back(down_split);
    }
  }

  if (movements.empty()) {
    movements.push_back(NO_MOVEMENT);
  }

  return movements;
}

// '\'
std::vector<Grid_Mover> do_movement_fwd_slash_angle(
    const Beam_Grid_Type& grid, const Grid_Mover& current_location) {
  const Grid_Mover NO_MOVEMENT{current_location.location,
                               Movement_Direction::NONE, false};
  std::vector<Grid_Mover> movements;

  if (current_location.direction == Movement_Direction::UP) {
    Grid_Location location = Grid_Location::left(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back(
          {location, Day16_Utils::Movement_Direction::LEFT, true});
    }
  } else if (current_location.direction == Movement_Direction::RIGHT) {
    Grid_Location location = Grid_Location::down(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back(
          {location, Day16_Utils::Movement_Direction::DOWN, true});
    }
  } else if (current_location.direction == Movement_Direction::DOWN) {
    Grid_Location location = Grid_Location::right(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back(
          {location, Day16_Utils::Movement_Direction::RIGHT, true});
    }
  } else if (current_location.direction == Movement_Direction::LEFT) {
    Grid_Location location = Grid_Location::up(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back(
          {location, Day16_Utils::Movement_Direction::UP, true});
    }
  }

  if (movements.empty()) {
    movements.push_back(NO_MOVEMENT);
  }

  return movements;
}

// '/'
std::vector<Grid_Mover> do_movement_bck_slash_angle(
    const Beam_Grid_Type& grid, const Grid_Mover& current_location) {
  const Grid_Mover NO_MOVEMENT{current_location.location,
                               Movement_Direction::NONE, false};
  std::vector<Grid_Mover> movements;

  if (current_location.direction == Movement_Direction::UP) {
    Grid_Location location = Grid_Location::right(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back(
          {location, Day16_Utils::Movement_Direction::RIGHT, true});
    }
  } else if (current_location.direction == Movement_Direction::RIGHT) {
    Grid_Location location = Grid_Location::up(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back(
          {location, Day16_Utils::Movement_Direction::UP, true});
    }
  } else if (current_location.direction == Movement_Direction::DOWN) {
    Grid_Location location = Grid_Location::left(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back(
          {location, Day16_Utils::Movement_Direction::LEFT, true});
    }
  } else if (current_location.direction == Movement_Direction::LEFT) {
    Grid_Location location = Grid_Location::down(current_location.location);
    if (is_on_grid(location, grid)) {
      movements.push_back(
          {location, Day16_Utils::Movement_Direction::DOWN, true});
    }
  }

  if (movements.empty()) {
    movements.push_back(NO_MOVEMENT);
  }

  return movements;
}

// Return the number of energized sections  firing the beam. Each cell contains
// the direction of movement of the beam on the way out of the cell
int do_beam_walking(const Beam_Grid_Type& grid,
                    const Grid_Mover& starting_point,
                    Movement_Grid_Type& movement_grid) {
  std::set<Grid_Location> engergized_locations;
  std::queue<Grid_Mover> movement_queue;
  char value_at_movement = grid.at(starting_point.location);

  Beam_Grid_Type energized_grid(grid.number_of_rows(), grid.number_of_cols());
  energized_grid.for_each_cell(
      [&](const auto& loc, __attribute__((unused)) char val) {
        energized_grid.set(loc, '.');
      });

  // SPDLOG_DEBUG(
  //     "[Start of movement] -- Location: {}  Direction: {} Grid value: {}",
  //     starting_point.location, (int)starting_point.direction,
  //     value_at_movement);

  // std::cout << "Beam grid\n" << grid << "\n";

  movement_queue.push(starting_point);
  while (!movement_queue.empty()) {
    // std::cout << "In Prog Grid------------------------\n"
    //           << energized_grid << "=================\n";

    Grid_Mover curr_movement = movement_queue.front();
    movement_queue.pop();

    bool cell_movement_repeat =
        movement_grid.at(curr_movement.location)
            .has_recorded_movement(curr_movement.direction);

    if (curr_movement && !cell_movement_repeat) {
      auto record = movement_grid.at(curr_movement.location);
      record.record_movement(curr_movement.direction);
      movement_grid.set(curr_movement.location, record);

      energized_grid.set(curr_movement.location, '#');
      std::vector<Grid_Mover> movements;
      engergized_locations.insert(curr_movement.location);
      value_at_movement = grid.at(curr_movement.location);
      // SPDLOG_DEBUG(
      //     "[processing movement] -- Location: {}  Direction: {} Grid value:
      //     {}", curr_movement.location, (int)curr_movement.direction,
      //     curr_movement);

      if (value_at_movement == '.') {
        movements = do_movement_maintain_direction(grid, curr_movement);
      } else if (value_at_movement == '-') {
        movements = do_movement_horizontal_splitter(grid, curr_movement);
      } else if (value_at_movement == '|') {
        movements = do_movement_vertical_splitter(grid, curr_movement);
      } else if (value_at_movement == '\\') {
        movements = do_movement_fwd_slash_angle(grid, curr_movement);
      } else if (value_at_movement == '/') {
        movements = do_movement_bck_slash_angle(grid, curr_movement);
      } else {
        SPDLOG_ERROR("Unknown cell value '{}'", value_at_movement);
      }

      for (const auto& next_move : movements) {
        movement_queue.push(next_move);
      }
    } else {
      SPDLOG_DEBUG("Current movement has no motion, discarding");
    }
  }

  return engergized_locations.size();
}

}  // namespace Day16_Utils