#include "day21_utils.h"

#include "../utils/grid/grid_location.h"
#include "../utils/grid/grid_traversal.h"
#include "../utils/grid/grid_utils.h"
#include "spdlog/spdlog.h"

namespace Day21_Utils {

Step_Interation_Data build_iteration_grid(
    const std::vector<std::string> lines) {
  Map_Grid garden_grid =
      create_char_grid_from_string_rows(lines.begin(), lines.end());

  Distance_Grid dist_grid = Distance_Grid(garden_grid.number_of_rows(),
                                          garden_grid.number_of_cols(), 0);

  auto start = find_value(garden_grid, 'S');
  if (!start.second) {
    SPDLOG_ERROR("Failed to find starting value in garden grid");
  }

  Location_Queue loc_q;
  loc_q.push(start.first);

  return {garden_grid, dist_grid, loc_q};
}

Location_Queue walk_step(Step_Interation_Data& step_grid, int iteration) {
  Location_Queue steps_this_iteration;

  auto op = [&](const auto& location, const auto value) {
    if (!is_on_grid(location, step_grid.map_grid)) {
      return;
    }

    // SPDLOG_DEBUG("Checking {} garden: {}  distance: {}", location, value,
    //              step_grid.dist_grid.at(location));

    if (value != '#' && step_grid.dist_grid.at(location) == 0) {
      // SPDLOG_DEBUG("Found open location, adding it");
      step_grid.dist_grid.set(location, iteration);
      steps_this_iteration.push(location);
    }
  };

  while (!step_grid.location_q.empty()) {
    const auto location = step_grid.location_q.front();
    step_grid.location_q.pop();

    for_each_orthogonal(step_grid.map_grid, location, op);
  }

  return steps_this_iteration;
}

void walk_iterations(Step_Interation_Data& step_grid, int num_iterations) {
  for (int iteration = 1; iteration <= num_iterations; ++iteration) {
    Location_Queue stepped_to_locs = walk_step(step_grid, iteration);
    step_grid.location_q.swap(stepped_to_locs);
  }
}

unsigned int determine_possible_destination_at_step(Distance_Grid& step_grid,
                                                    int step) {
  unsigned int count = 0;
  int desired_mask = step & 0x1;  // even or odd

  auto count_op = [&](__attribute__((unused)) const auto& loc, auto val) {
    const int masked_val = val & 0x1;
    if (val > 0 && masked_val == desired_mask && val <= step) {
      ++count;
    }
  };

  step_grid.for_each_cell(count_op);
  return count;
}

}  // namespace Day21_Utils