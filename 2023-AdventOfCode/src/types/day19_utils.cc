#include "day19_utils.h"

#include <iostream>
#include <regex>

#include "framework/parse_exception.h"
#include "spdlog/spdlog.h"
#include "utils/string_utils.hpp"

namespace {

static const std::string ACCEPT = "ACCEPT";
static const std::string REJECT = "REJECT";
static const std::string INDETERMINATE =
    "";  // inconcludsive, evaluate other rules

int x_factor_accessor(const Day19_Utils::Part_Spec& spec) {
  return spec.x_factor;
};

int m_factor_accessor(const Day19_Utils::Part_Spec& spec) {
  return spec.m_factor;
};

int a_factor_accessor(const Day19_Utils::Part_Spec& spec) {
  return spec.a_factor;
};

int s_factor_accessor(const Day19_Utils::Part_Spec& spec) {
  return spec.s_factor;
};

int dont_care_accessor(__attribute__((unused))
                       const Day19_Utils::Part_Spec& spec) {
  return -1;
}

bool predicate_true(__attribute__((unused)) int value) { return true; }

}  // namespace

namespace Day19_Utils {

#pragma region "Rule_Result"

Rule_Result Rule_Result::make_accept_part_result() {
  return Rule_Result(ACCEPT);
}

Rule_Result Rule_Result::make_reject_part_result() {
  return Rule_Result(REJECT);
}

Rule_Result Rule_Result::make_workflow_redirection_result(
    const std::string& dest) {
  return Rule_Result(dest);
}

Rule_Result Rule_Result::make_indeterminate_result() {
  return Rule_Result(INDETERMINATE);
}

Rule_Result::Rule_Result() {}

bool Rule_Result::is_accept() const { return m_result == ACCEPT; }

bool Rule_Result::is_reject() const { return m_result == REJECT; }

bool Rule_Result::is_workflow_redirection() const {
  return !(is_accept() || is_reject() || is_indeterminate());
}

bool Rule_Result::is_indeterminate() const { return m_result == INDETERMINATE; }

std::string Rule_Result::get_resulting_workflow() const { return m_result; }

Rule_Result::Rule_Result(const std::string result) : m_result(result) {}

#pragma endregion

#pragma region "Rule"

Rule::Rule(Rule::Field_Accessor part_spec_field_accessor,
           Rule::Predicate rule_prediate, const Rule_Result& successful_result,
           const Rule_Result& failure_result)
    : m_field_accessor(part_spec_field_accessor),
      m_predicate(rule_prediate),
      m_successful_result(successful_result),
      m_failure_result(failure_result) {}

Rule_Result Rule::evaluate(const Part_Spec& part_spec) const {
  const auto val = m_field_accessor(part_spec);
  const bool pred = m_predicate(val);

  SPDLOG_DEBUG("Testing value: {}  result: {}", val, pred);
  return pred ? m_successful_result : m_failure_result;
}

bool Rule::has_accept_condition() const {
  return m_successful_result.is_accept();
}

#pragma endregion

#pragma region "Day19_Workflow_Runner"

int Day19_Workflow_Runner::execute() {
  int sum = 0;
  for (const auto& part : parts) {
    const auto& workflow_result = execute_single_part(part);
    if (workflow_result.was_accepted) {
      sum += part.x_factor + part.m_factor + part.a_factor + part.s_factor;
    }
  }

  return sum;
}

Day19_Workflow_Runner::Part_Workflow_Result
Day19_Workflow_Runner::execute_single_part(const Part_Spec& part) {
  SPDLOG_DEBUG("Executing Single Part [x: {}  m: {}  a: {}  s: {}] *********",
               part.x_factor, part.m_factor, part.a_factor, part.s_factor);

  Part_Workflow_Result workflow_result{{}, false};
  Rule_Result rule_result = execute_workflow(initial_workflow, part);
  workflow_result.workflow_path.push_back(initial_workflow.name);
  while (rule_result.is_workflow_redirection()) {
    SPDLOG_DEBUG("  Redirectiong to workflow: '{}', executing it",
                 rule_result.get_resulting_workflow());
    auto workflow_iter = workflow.find(rule_result.get_resulting_workflow());
    if (workflow_iter == workflow.end()) {
      SPDLOG_ERROR("====> Workflow not found!");
      assert(workflow_iter != workflow.end());
    }
    workflow_result.workflow_path.push_back(workflow_iter->first);
    rule_result = execute_workflow(workflow_iter->second, part);
  }

  workflow_result.was_accepted = rule_result.is_accept();
  return workflow_result;
}

std::istream& operator>>(std::istream& instr, Day19_Workflow_Runner& obj) {
  bool found_separater = false;
  std::string line;
  while (getline(instr, line)) {
    if (line.empty()) {
      found_separater = true;
    } else {
      if (found_separater) {
        obj.parts.push_back(parse_part_spec(line));
      } else {
        auto workflow = parse_workflow(line);
        obj.workflow.insert({workflow.name, workflow});
        if (workflow.name == "in") {
          obj.initial_workflow = workflow;
        }
      }
    }

    line.clear();
  }

  // Create A for accept node and R for reject node

  return instr;
}

#pragma endregion

// {x=787,m=2655,a=1222,s=2876}
Part_Spec parse_part_spec(const std::string& str) {
  static const std::regex REGEX{"^\\{x=(\\d+),m=(\\d+),a=(\\d+),s=(\\d+)\\}$"};
  std::smatch regex_match;
  if (!std::regex_search(str, regex_match, REGEX)) {
    const std::string msg =
        fmt::format("Failed to parse Part_Spec, regex failed. Line: '{}'", str);
    SPDLOG_DEBUG(msg);
    throw Parse_Exception(msg);
  }
  return {std::stoi(regex_match[1].str().c_str()),
          std::stoi(regex_match[2].str().c_str()),
          std::stoi(regex_match[3].str().c_str()),
          std::stoi(regex_match[4].str().c_str())};
}

Rule parse_rule(const std::string& str) {
  static const std::regex CONDITIONAL_RULE_REGEX{"^([xmas])(.)(\\d+):(\\w+)$"};
  static const std::regex DIRECT_REDIRECT_REGEX{"^(\\w+)$"};
  static const std::regex DIRECT_ACCEPT_REGEX{"^A$"};
  static const std::regex DIRECT_REJECT_REGEX{"^R$"};
  std::smatch regex_match;

  if (std::regex_search(str, regex_match, DIRECT_ACCEPT_REGEX)) {
    return Rule(dont_care_accessor, predicate_true,
                Rule_Result::make_accept_part_result(),
                Rule_Result::make_indeterminate_result());

  } else if (std::regex_search(str, regex_match, DIRECT_REJECT_REGEX)) {
    return Rule(dont_care_accessor, predicate_true,
                Rule_Result::make_reject_part_result(),
                Rule_Result::make_indeterminate_result());

  } else if (std::regex_search(str, regex_match, DIRECT_REDIRECT_REGEX)) {
    return Rule(
        dont_care_accessor, predicate_true,
        Rule_Result::make_workflow_redirection_result(regex_match[1].str()),
        Rule_Result::make_indeterminate_result());
  }

  if (!std::regex_search(str, regex_match, CONDITIONAL_RULE_REGEX)) {
    const std::string msg = fmt::format("Failed to parse Rule. Line: {}"), str;
    SPDLOG_DEBUG(msg);
    throw Parse_Exception(msg);
  }

  SPDLOG_DEBUG("Rule regex matched line: {}", str);
  int condition_value = std::stoi(regex_match[3].str().c_str());
  Rule::Field_Accessor field_accessor;
  Rule::Predicate predicate;

  auto field_identifier = regex_match[1].str().front();
  switch (field_identifier) {
    case 'x':
      field_accessor = x_factor_accessor;
      break;
    case 'm':
      field_accessor = m_factor_accessor;
      break;
    case 'a':
      field_accessor = a_factor_accessor;
      break;
    case 's':
      field_accessor = s_factor_accessor;
      break;
    default:
      SPDLOG_ERROR("Unhandled part specification identifier '{}'",
                   field_identifier);
  }

  auto conditional = regex_match[2].str().front();
  switch (conditional) {
    case '<':
      predicate =
          std::bind(std::less<int>(), std::placeholders::_1, condition_value);
      break;
    case '>':
      predicate = std::bind(std::greater<int>(), std::placeholders::_1,
                            condition_value);
      break;
    default:
      SPDLOG_ERROR("Unhanded condition '{}'", conditional);
  }

  const auto redirect_dest = regex_match[4].str();
  if (redirect_dest == "A") {
    return Rule(field_accessor, predicate,
                Rule_Result::make_accept_part_result(),
                Rule_Result::make_indeterminate_result());
  } else if (redirect_dest == "R") {
    return Rule(field_accessor, predicate,
                Rule_Result::make_reject_part_result(),
                Rule_Result::make_indeterminate_result());
  }

  return Rule(field_accessor, predicate,
              Rule_Result::make_workflow_redirection_result(redirect_dest),
              Rule_Result::make_indeterminate_result());
}

Workflow parse_workflow(const std::string& str) {
  static const std::regex WORKFLOW_REGEX{
      "^(\\w+)\\{(.*)\\}$"};  // {"^(\\w+)\{(.*)}$"};
  std::smatch regex_match;

  if (str.empty()) {
    const std::string msg =
        fmt::format("Failed to parse Workflow, empty line given");
    SPDLOG_DEBUG(msg);
    throw Parse_Exception(msg);
  }

  if (!std::regex_search(str, regex_match, WORKFLOW_REGEX)) {
    SPDLOG_ERROR("Workflow line '{}' did not match regex", str);
  }

  const auto& splits = split(regex_match[2].str(), ',');

  SPDLOG_DEBUG("value: {}  rules: {}  splits: {}", regex_match[1].str(),
               regex_match[2].str(), fmt::join(splits, " "));

  std::vector<Rule> rules;
  for (const auto& str : splits) {
    rules.push_back(parse_rule(str));
  }

  return Workflow{regex_match[1].str(), rules};
}

Rule_Result execute_workflow(Workflow& workflow, const Part_Spec& part) {
  Rule_Result result;
  SPDLOG_DEBUG("Executing workflow: {}", workflow.name);
  for (const auto& rule : workflow.rules) {
    result = rule.evaluate(part);
    if (!result.is_indeterminate()) {
      break;
    }
  }

  return result;
}

bool has_accept_condition_path(const Workflow& workflow) {
  return std::any_of(workflow.rules.begin(), workflow.rules.end(),
                     std::mem_fn(&Rule::has_accept_condition));
}

}  // Namespace Day19_Utils