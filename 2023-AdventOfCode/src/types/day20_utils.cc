#include "day20_utils.h"

#include <sstream>

#include "spdlog/spdlog.h"
#include "utils/string_utils.hpp"

namespace Day20_Utils {

#pragma region Pulse_Generator

Pulse_Generator::Pulse_Generator() : m_name() {}

Pulse_Generator::Pulse_Generator(const std::string& name) : m_name(name) {}

void Pulse_Generator::add_input_module(const std::string& name) {
  // TODO: No-op by default
}

void Pulse_Generator::add_output_module(const std::string& name) {
  SPDLOG_DEBUG("Added output module '{}' to generator '{}'", name, m_name);
  m_connected_modules.push_back(name);
}

void Pulse_Generator::queue_pulse(Pulse pulse, Pulse_Command_Q& in_out_q) {
  for (auto module : m_connected_modules) {
    SPDLOG_DEBUG(" Queued pulse: {} --{}--> {}", m_name, (int)pulse, module);
    in_out_q.push({m_name, module, pulse});
  }
}

#pragma endregion /* Pulse_Generator */

#pragma region Broadcast_Pulse_Generator

Broadcast_Pulse_Generator::Broadcast_Pulse_Generator()
    : Pulse_Generator("broadcaster") {}

bool Broadcast_Pulse_Generator::handle_pulse(const Pulse_Event& event,
                                             Pulse_Command_Q& in_out_q) {
  queue_pulse(event.pulse_value, in_out_q);
  return true;
}
#pragma endregion  // Broadcast_Pulse_Generator

#pragma region Flip_Flop_Pulse_Generator

Flip_Flop_Pulse_Generator::Flip_Flop_Pulse_Generator(const std::string& name)
    : Pulse_Generator(name), m_state(false) {}

bool Flip_Flop_Pulse_Generator::handle_pulse(const Pulse_Event& event,
                                             Pulse_Command_Q& in_out_q) {
  SPDLOG_DEBUG("  [{}] processing pulse type: {} [{}]", name(),
               int(event.pulse_value), "Flip_Flop_Pulse_Generator");

  if (event.pulse_value == Pulse::LOW) {
    m_state = !m_state;
    const Pulse generated = m_state ? Pulse::HIGH : Pulse::LOW;
    queue_pulse(generated, in_out_q);
  }
  return true;
}

#pragma endregion /* Flip_Flop_Pulse_Generator */

#pragma region Conjunction_Pulse_Generator

Conjunction_Pulse_Generator::Conjunction_Pulse_Generator(
    const std::string& name)
    : Pulse_Generator(name) {}

bool Conjunction_Pulse_Generator::handle_pulse(const Pulse_Event& event,
                                               Pulse_Command_Q& in_out_q) {
  SPDLOG_DEBUG("  [{}] processing pulse type: {} [{}]", name(),
               int(event.pulse_value), "Conjunction_Pulse_Generator");

  auto in_signal = m_input_connection_states.find(event.originating_module);
  if (in_signal == m_input_connection_states.end()) {
    SPDLOG_ERROR("Failed to find input signal '{}' in module '{}'",
                 event.originating_module, this->name());
  }

  in_signal->second = event.pulse_value;
  bool all_inputs_high = true;
  for (const auto& input : m_input_connection_states) {
    SPDLOG_DEBUG("    -- Input module [{}] value: {}", input.first,
                 (int)input.second);
    all_inputs_high &= input.second == Pulse::HIGH;
  }

  Pulse generated_pulse = all_inputs_high ? Pulse::LOW : Pulse::HIGH;
  SPDLOG_DEBUG("    -- all inputs AND'd {}  generated: {}", all_inputs_high,
               int(generated_pulse));

  queue_pulse(generated_pulse, in_out_q);
  return true;
}

void Conjunction_Pulse_Generator::add_input_module(const std::string& name) {
  SPDLOG_DEBUG("Adding input module '{}' to module '{}'", name, this->name());
  m_input_connection_states.insert({name, Pulse::LOW});
}

#pragma endregion /* Conjunction_Pulse_Generator */

#pragma region Termination_Pulse_Generator

Termination_Pulse_Generator::Termination_Pulse_Generator(
    const std::string& name)
    : Pulse_Generator(name) {}

bool Termination_Pulse_Generator::handle_pulse(const Pulse_Event& event,
                                               Pulse_Command_Q& in_out_q) {
  return true;
}

#pragma endregion /* Termination_Pulse_Generator */

#pragma region Pulse_Generator_Runner

Pulse_Generator_Runner::Pulse_Generator_Runner() : m_broadcaster(0x0) {}

Pulse_Generator_Runner& Pulse_Generator_Runner::add_generator(
    Pulse_Generator& generator) {
  SPDLOG_DEBUG("Adding generator: {}", generator.name());
  m_generator_map.insert({generator.name(), &generator});
  if (generator.name() == "broadcaster") {
    SPDLOG_DEBUG(" ** Storing broadcaster");
    m_broadcaster = &generator;
  }
  return *this;
}

std::pair<int, int> Pulse_Generator_Runner::press_button() {
  if (m_broadcaster == 0x0) {
    SPDLOG_ERROR("No 'broadcaster' generator set");
    return {0, 0};
  }

  // low, high
  std::pair<int, int> result{1, 0};
  Pulse_Command_Q pulse_q;
  m_broadcaster->handle_pulse({"button", Pulse::LOW}, pulse_q);
  while (!pulse_q.empty()) {
    auto generated_pulse = pulse_q.front();
    pulse_q.pop();

    auto next_generator =
        m_generator_map.find(generated_pulse.destination_module);
    if (next_generator == m_generator_map.end()) {
      SPDLOG_ERROR("Could not find generator '{}'",
                   generated_pulse.destination_module);
    }

    if (generated_pulse.pulse_value == Pulse::LOW) {
      ++result.first;
    } else {
      ++result.second;
    }

    const Pulse_Event event{generated_pulse.originating_module,
                            generated_pulse.pulse_value};
    next_generator->second->handle_pulse(event, pulse_q);
  }

  return result;
}

bool Pulse_Generator_Runner::press_button_looking_for_rx_low_pulse() {
  if (m_broadcaster == 0x0) {
    SPDLOG_ERROR("No 'broadcaster' generator set");
    return false;
  }

  Pulse_Command_Q pulse_q;
  m_broadcaster->handle_pulse({"button", Pulse::LOW}, pulse_q);
  while (!pulse_q.empty()) {
    auto generated_pulse = pulse_q.front();
    pulse_q.pop();

    auto next_generator =
        m_generator_map.find(generated_pulse.destination_module);
    if (next_generator == m_generator_map.end()) {
      SPDLOG_ERROR("Could not find generator '{}'",
                   generated_pulse.destination_module);
    }

    const Pulse_Event event{generated_pulse.originating_module,
                            generated_pulse.pulse_value};

    if (next_generator->second->name() == "rx" &&
        generated_pulse.pulse_value == Pulse::LOW) {
      return true;
    } else {
      next_generator->second->handle_pulse(event, pulse_q);
    }
  }

  return false;
}

#pragma endregion /* Pulse_Generator_Runner */

Pulse_Generator* build_generator(const std::string& line,
                                 In_Out_Pairs& in_out_pairs) {
  Pulse_Generator* generator = nullptr;

  std::istringstream instr(line);
  std::string identifier;
  std::string sep;
  std::string dest_module;

  instr >> identifier >> sep;
  SPDLOG_DEBUG("Parsed identifier: '{}'", identifier);

  // Allocate the type generator and trim identifier as needed
  if (identifier == "broadcaster") {
    generator = new Broadcast_Pulse_Generator();
    SPDLOG_DEBUG("Constructing Broadcast_Pulse_Generator");
  } else if (identifier.front() == '%') {
    identifier = identifier.substr(1);
    generator = new Flip_Flop_Pulse_Generator(identifier);
    SPDLOG_DEBUG("Constructing Flip_Flop_Pulse_Generator [{}]", identifier);
  } else if (identifier.front() == '&') {
    identifier = identifier.substr(1);
    generator = new Conjunction_Pulse_Generator(identifier);
    SPDLOG_DEBUG("Constructing Conjunction_Pulse_Generator [{}]", identifier);
  } else {
    SPDLOG_ERROR("Unexpected generator identifier '{}'", identifier);
  }

  while (getline(instr, dest_module, ',')) {
    trim(dest_module);
    SPDLOG_DEBUG("  output module: {}", dest_module);

    // TODO: can't have the leading prefix
    in_out_pairs.push_back({identifier, dest_module});
  }

  return generator;
}

std::vector<Pulse_Generator*> build_generators(
    const std::vector<std::string>& lines) {
  // Build all the generators

  std::unordered_map<std::string, Pulse_Generator*> generators;
  In_Out_Pairs io_mappings;

  SPDLOG_INFO("*** BUILDING GENERATORS");
  for (const auto& line : lines) {
    Pulse_Generator* gen_ptr = build_generator(line, io_mappings);
    generators[gen_ptr->name()] = gen_ptr;
  }

  SPDLOG_INFO("*** MAPPING INPUTS/OUTPUTS");
  for (const auto& gen_pair : generators) {
    SPDLOG_DEBUG("Built generator '{}'", gen_pair.first);
  }

  for (const auto& io_map : io_mappings) {
    SPDLOG_DEBUG("Searching for source: [{}]  and sink: [{}]", io_map.source,
                 io_map.sink);

    auto source_iter = generators.find(io_map.source);
    auto sink_iter = generators.find(io_map.sink);

    if (source_iter == generators.end()) {
      SPDLOG_ERROR("Failed to find source module: {}", io_map.source);
    }

    if (sink_iter == generators.end()) {
      SPDLOG_DEBUG(
          "Failed to find sink module: {}. Creating terminating module",
          io_map.sink);

      Pulse_Generator* gen_ptr = new Termination_Pulse_Generator(io_map.sink);
      generators[io_map.sink] = gen_ptr;
      sink_iter = generators.find(io_map.sink);
    }

    assert(source_iter != generators.end());
    assert(sink_iter != generators.end());

    source_iter->second->add_output_module(sink_iter->second->name());
    sink_iter->second->add_input_module(source_iter->second->name());
  }

  SPDLOG_DEBUG("Build generator count: {}", generators.size());

  // Add inputs and outputs

  // return the generators
  std::vector<Pulse_Generator*> gen_vec;
  for (const auto& gen : generators) {
    gen_vec.push_back(gen.second);
  }

  return gen_vec;
}

}  // namespace Day20_Utils