#include "day13_utils.h"

#include <algorithm>
#include <iterator>
#include <sstream>
#include <vector>

#include "../utils/string_utils.hpp"
#include "spdlog/spdlog.h"

namespace Day13_Utils {

/**
 * Find a common fold reflection index where each
 * of the strings have a fold reflection. If not
 * found, return -1 otherwise return the position
 * at which the reflection occurs.
 *
 * @algorithm Create a vector indxed by the location of fold reflections.
 * increment the indicies for each fold reflection of each
 * string and look for the index with the number of strings, or -1
 * if not found.
 *
 * Assumes ecah string in the collection is the same length.
 *
 */
std::vector<int> find_common_fold_reflection(
    const std::vector<std::string> &strings) {
  const ssize_t reflection_locations_size = strings.front().size() + 1;
  std::vector<int> reflection_locations(reflection_locations_size);
  std::vector<int> reflection_points;
  for (const auto &str : strings) {
    const auto &reflection_points = find_fold_reflection_points(str);
    for (auto idx : reflection_points) {
      reflection_locations[idx]++;
    }
  }

  // This is the part 2 issue...need cases within 1 mirror that has
  // multiple reflection points
  for (size_t idx = 0; idx < reflection_locations.size(); ++idx) {
    if (reflection_locations[idx] == strings.size()) {
      reflection_points.push_back(idx);
    }
  }
  return reflection_points;
}

std::vector<int> find_common_vertical_fold_reflection(const Char_Grid &grid) {
  std::vector<std::string> rows;
  for (auto row_num = 0; row_num < grid.number_of_rows(); ++row_num) {
    rows.push_back(grid.row(row_num));
  }

  return find_common_fold_reflection(rows);
}

// Reverse string because column is logicall bottom up
// (0 at bottom) and day scors top down
std::vector<int> find_common_horizontal_fold_reflection(const Char_Grid &grid) {
  std::vector<std::string> cols;
  for (auto col_num = 0; col_num < grid.number_of_cols(); ++col_num) {
    auto col_str = grid.col(col_num);
    std::reverse(col_str.begin(), col_str.end());
    cols.push_back(col_str);
  }

  return find_common_fold_reflection(cols);
}

int evaluate_part1_reflection_score(const Char_Grid &grid) {
  int score = 0;

  auto reflection_values = find_common_vertical_fold_reflection(grid);
  if (reflection_values.empty()) {
    reflection_values = find_common_horizontal_fold_reflection(grid);
    if (reflection_values.empty()) {
      SPDLOG_ERROR("UNKOWN ISSUE, NEITHER VERTICAL NOR HORIZONTAL");
    } else {
      score += 100 * reflection_values.front();
    }
  } else {
    score += reflection_values.front();
  }

  return score;
}

int evaluate_part2_reflection_score(Char_Grid &grid) {
  std::string original_lor;
  auto lines_of_reflection = find_common_horizontal_fold_reflection(grid);

  if (lines_of_reflection.empty()) {
    lines_of_reflection = find_common_vertical_fold_reflection(grid);
    if (lines_of_reflection.empty()) {
      SPDLOG_ERROR("No original line of reflection found");
    } else {
      original_lor = "C" + std::to_string(lines_of_reflection.front());
    }
  } else {
    original_lor = "R" + std::to_string(lines_of_reflection.front());
  }

  std::vector<int> smudge_lines_of_reflection;
  for (ssize_t row = 0; row < grid.number_of_rows(); ++row) {
    for (ssize_t col = 0; col < grid.number_of_cols(); ++col) {
      const Grid_Location loc{row, col};
      const auto value = grid.at(loc);
      const auto flip_value = value == '.' ? '#' : '.';

      grid.set(loc, flip_value);

      smudge_lines_of_reflection = find_common_horizontal_fold_reflection(grid);
      for (const int lor : smudge_lines_of_reflection) {
        const std::string smudge_lor = "R" + std::to_string(lor);
        if (smudge_lor != original_lor) {
          return lor * 100;
        }
      }

      smudge_lines_of_reflection = find_common_vertical_fold_reflection(grid);
      for (const int lor : smudge_lines_of_reflection) {
        const std::string smudge_lor = "C" + std::to_string(lor);
        if (smudge_lor != original_lor) {
          return lor;
        }
      }

      grid.set(loc, value);
    }
  }

  SPDLOG_ERROR("Did not find new line of reflection...original: {}",
               original_lor);
  std::cout << grid << "\n";

  // If nothing new, assume that this is an issue

  return -1;
}

}  // namespace Day13_Utils