#include "day18_utils.h"

#include <ios>
#include <sstream>
#include <utility>

#include "../utils/grid/grid_traversal.h"
#include "../utils/grid/grid_utils.h"

namespace Day18_Utils {

std::pair<char, int> decode_part2_color_string(const Dig_Instructions& obj) {
  std::pair<char, int> result;

  const std::string hex_num_string =
      obj.color_code.substr(1, obj.color_code.size() - 2);

  std::istringstream instr(hex_num_string);
  instr >> std::hex >> result.second;

  switch (obj.color_code.back()) {
    case '0':
      result.first = 'R';
      break;
    case '1':
      result.first = 'D';
      break;
    case '2':
      result.first = 'L';
      break;
    case '3':
      result.first = 'U';
      break;
  }

  return result;
}

std::istream& operator>>(std::istream& instr, Dig_Instructions& obj) {
  instr >> obj.direction >> obj.distance >> obj.color_code;
  if (obj.color_code.size() > 2) {
    obj.color_code = obj.color_code.substr(1, obj.color_code.size() - 2);
  } else {
    SPDLOG_ERROR("Color string too short to drop (): {}", obj.color_code);
  }
  return instr;
}

Dig_Bounds find_dig_bounds_using_decoded_color_code(
    const std::vector<Dig_Instructions>& instructions) {
  Dig_Bounds bounds{{0, 0}, {0, 0}, {0, 0}, {0, 0}};
  Grid_Location location{0, 0};

  for (const auto& inst : instructions) {
    const auto& decoded = decode_part2_color_string(inst);
    // std::cout << "Dir: " << decoded.first << "  Dist: " << decoded.second
    //           << "\n";
    // TODO: better way to wrap up in grid
    switch (decoded.first) {
      case 'U':
        location = Grid_Location::up(location, decoded.second);
        break;
      case 'R':
        location = Grid_Location::right(location, decoded.second);
        break;
      case 'D':
        location = Grid_Location::down(location, decoded.second);
        break;
      case 'L':
        location = Grid_Location::left(location, decoded.second);
        break;
    }
    std::cout << "  location: " << location << "\n";
    if (location.row >= bounds.upper_most_location.row) {
      // std::cout << "    updating uppermost\n";
      bounds.upper_most_location = location;
    }

    if (location.row <= bounds.lower_most_location.row) {
      // std::cout << "    updating lowermost\n";
      bounds.lower_most_location = location;
    }

    if (location.col <= bounds.lower_most_location.col) {
      // std::cout << "    updating leftmost\n";
      bounds.left_most_location = location;
    }

    if (location.col >= bounds.right_most_location.col) {
      // std::cout << "    updating rightmost\n";
      bounds.right_most_location = location;
    }
  }

  return bounds;
}

Dig_Site_Frame_Of_Reference create_dig_site_reference(
    const Dig_Bounds& bounds) {
  return {bounds.lower_most_location.row, bounds.left_most_location.col,
          bounds.upper_most_location.row - bounds.lower_most_location.row,
          bounds.right_most_location.col - bounds.left_most_location.col};
}

Dig_Site_Type create_dig_site(int grid_square_size) {
  return {{grid_square_size, grid_square_size, '.'},
          {grid_square_size / 2, grid_square_size / 2}};
}

Grid_Location dig_trench(Dig_Grid_Type& dig_grid,
                         const Dig_Instructions& instruction,
                         const Grid_Location& origin) {
  if (!is_on_grid(origin, dig_grid)) {
    SPDLOG_ERROR("Origin not on dig site!");
    // TODO: throw
  }

  // Plus one because the first charater in walk is the one its
  // already on
  const int step_distance = instruction.distance + 1;
  const char DIG_CHAR = '#';
  int step_count = 0;
  Grid_Location end;

  auto dig_handler = [&](const Grid_Location& loc,
                         __attribute__((unused)) char value) {
    dig_grid.set(loc, DIG_CHAR);
    end = loc;
  };

  auto stop_predicate = [&](const Grid_Location&, char value) -> bool {
    return step_count++ == step_distance;
  };

  switch (instruction.direction) {
    case 'U':
      walk_up_until(dig_grid, origin, dig_handler, stop_predicate);
      break;

    case 'R':
      walk_right_until(dig_grid, origin, dig_handler, stop_predicate);
      break;

    case 'D':
      walk_down_until(dig_grid, origin, dig_handler, stop_predicate);
      break;

    case 'L':
      walk_left_until(dig_grid, origin, dig_handler, stop_predicate);
      break;

    default:
      SPDLOG_ERROR("Instruction direction unknown '{}'", instruction.direction);
  }

  return end;
}

void dig_trench(Dig_Site_Type& dig_site,
                const std::vector<Dig_Instructions>& instructions,
                ssize_t origin_row_tanslation, ssize_t origin_col_translation) {
  Grid_Location loc{dig_site.origin.row + origin_row_tanslation,
                    dig_site.origin.col + origin_col_translation};
  for (const auto& instr : instructions) {
    loc = dig_trench(dig_site.dig_grid, instr, loc);
  }
}

/******************* Part 2 vertical trench approach ***********/

bool form_vertical_segment(const Grid_Location& first,
                           const Grid_Location& second) {
  return first.col == second.col;
}

std::vector<Line_Segment> find_vertical_trenches(
    const std::vector<Dig_Instructions>& instructions,
    std::function<std::pair<char, int>(Dig_Instructions)> move_decoder) {
  std::vector<Line_Segment> vsegments;
  Grid_Location location{0, 0};
  Grid_Location next_location;
  for (const auto& instruction : instructions) {
    const auto decoded_instr = move_decoder(instruction);
    switch (decoded_instr.first) {
      case 'U':
        next_location = Grid_Location::up(location, decoded_instr.second);
        break;

      case 'R':
        next_location = Grid_Location::right(location, decoded_instr.second);
        break;

      case 'D':
        next_location = Grid_Location::down(location, decoded_instr.second);
        break;

      case 'L':
        next_location = Grid_Location::left(location, decoded_instr.second);
        break;

      default:
        SPDLOG_ERROR("Instruction direction unknown '{}'",
                     instruction.direction);
    }

    if (form_vertical_segment(location, next_location)) {
      // TODO: make sure low to high order
      if (location.row < next_location.row) {
        vsegments.push_back({location, next_location});
      } else {
        vsegments.push_back({next_location, location});
      }
    }

    location = next_location;
  }

  return vsegments;
}

// Assumed to be vertical only, so just check i endpoint
void sort_trenches(std::vector<Line_Segment>& vert_trenches) {
  auto sorter = [](const auto& first, const auto& second) -> bool {
    if (first.start.row < second.start.row) return true;
    if (first.start.row > second.start.row) return false;
    return first.start.col < second.start.col;
  };

  std::sort(vert_trenches.begin(), vert_trenches.end(), sorter);
}

// Check if the last 2 segments can be combined into a joint segment.
// This occurs if the first and last movement for 1 continuous segment
void combine_close_loop(std::vector<Line_Segment>& vert_trenches) {
  auto last_segment = vert_trenches.back();
  auto loop_segment = vert_trenches.front();

  std::cout << "segment: [" << last_segment.start << ", " << last_segment.end
            << "]\n";

  std::cout << "segment: [" << loop_segment.start << ", " << loop_segment.end
            << "]\n";
}

// Assumes segment is vertical segment
bool vertical_segment_crosses_row(const Line_Segment& segment, ssize_t row) {
  return segment.start.row <= row && segment.end.row >= row;
}

}  // namespace Day18_Utils