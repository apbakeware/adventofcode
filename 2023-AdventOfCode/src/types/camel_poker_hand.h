#ifndef __CAMEL_POKER_HAND_H__
#define __CAMEL_POKER_HAND_H__

#include <iosfwd>
#include <string>

enum class Camel_Poker_Hand_Rank {
  FIVE_OF_A_KIND,
  FOUR_OF_A_KIND,
  FULL_HOUSE,
  THREE_OF_A_KIND,
  TWO_PAIR,
  ONE_PAIR,
  HIGH_CARD,
  UNKNOWN
};

struct Camel_Poker_Hand {
  std::string cards;
  int bid;
  Camel_Poker_Hand_Rank hand_rank;

  /**
   * Policy based computation of the rank of hand's cards.
   * Sets the internal hand_rank and returns it.
   *
   * Policy_t::rank_hand(const std::string & cards).
   */
  template <class Policy_T>
  Camel_Poker_Hand_Rank rank_hand() {
    hand_rank = Policy_T::rank_hand(cards);
    return hand_rank;
  }
};

std::istream& operator>>(std::istream& instr, Camel_Poker_Hand& obj);

struct Day07_Part_1_Rank_Policy {
  static Camel_Poker_Hand_Rank rank_hand(const std::string& cards);
};

// template with wild card policy
struct Day07_Part_2_Rank_Policy {
  static Camel_Poker_Hand_Rank rank_hand(const std::string& cards);
};

#endif /* __CAMEL_POKER_HAND_H__ */