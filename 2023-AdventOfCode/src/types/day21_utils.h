#ifndef __DAY21_UTILS_H__
#define __DAY21_UTILS_H__

#include <queue>

#include "../utils/grid/grid.h"
#include "../utils/grid/grid_location.h"

namespace Day21_Utils {

typedef Fixed_Sized_Grid<char> Map_Grid;
typedef Fixed_Sized_Grid<int> Distance_Grid;
typedef std::queue<Grid_Location> Location_Queue;

struct Step_Interation_Data {
  Map_Grid map_grid;
  Distance_Grid dist_grid;
  Location_Queue location_q;
};

Step_Interation_Data build_iteration_grid(const std::vector<std::string> lines);

/**
 * Walk the map from each of the starting points in 1 step. Update
 * only locations which haven't been visiting with the distance
 * to the cell.
 *
 * Return a queue of cells visited this iteration.
 */
Location_Queue walk_step(Step_Interation_Data& step_grid, int iteration);

void walk_iterations(Step_Interation_Data& step_grid, int num_iterations);

unsigned int determine_possible_destination_at_step(Distance_Grid& step_grid,
                                                    int step);

}  // namespace Day21_Utils

#endif /* __DAY21_UTILS_H__ */