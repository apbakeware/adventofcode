#include "day09_sensor_predictor.h"

#include <functional>

#include "spdlog/spdlog.h"
#include "utils/apply_adjacent_operation.h"

template <class Iter_T>
bool all_values_equal_to(
    Iter_T begin, Iter_T end,
    typename std::iterator_traits<Iter_T>::value_type value) {
  using Value_T = typename std::iterator_traits<Iter_T>::value_type;
  if (begin == end) return false;

  return std::all_of(
      begin, end,
      std::bind(std::equal_to<Value_T>(), std::placeholders::_1, value));
}

int Day09_Sensor_Predictor::predict_next_reading(
    const std::vector<int>& readings) {
  using Vector_Of_Vectors = std::vector<std::vector<int> >;

  Vector_Of_Vectors rows;
  std::vector<int> diffs;
  int value_diff;

  rows.push_back(readings);

  do {
    std::vector<int>& curr = rows.back();
    diffs.clear();
    apply_adjacent_operation(curr.begin(), curr.end(),
                             std::back_inserter(diffs), std::minus<int>());
    rows.push_back(diffs);
  } while (!all_values_equal_to(diffs.begin(), diffs.end(), 0));

  SPDLOG_DEBUG("Build differences");
  for (const auto& row : rows) {
    SPDLOG_DEBUG("Row: [{}]", fmt::join(row, " "));
  }

  int last_val = 0;
  auto iter = rows.rbegin();
  iter->push_back(last_val);
  ++iter;
  for (; iter != rows.rend(); ++iter) {
    last_val = iter->back() + last_val;
    iter->push_back(last_val);
  }

  SPDLOG_DEBUG("Extended");
  for (const auto& row : rows) {
    SPDLOG_DEBUG("Row: [{}]", fmt::join(row, " "));
  }

  return last_val;
}