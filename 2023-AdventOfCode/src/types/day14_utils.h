#ifndef __DAY14_UTILS_H__
#define __DAY14_UTILS_H__

#include "../utils/grid/grid.h"

namespace Day14_Utils {

static constexpr char OPEN = '.';
static constexpr char ROUND_ROCK = 'O';
static constexpr char CUBE_ROCK = '#';

// This is the entry point to solving the day. others are helpers
int tilt_and_evaluate_load(Fixed_Sized_Grid<char>& grid);

int load_after_spin_cycles(Fixed_Sized_Grid<char>& grid,
                           unsigned long int cycles);

void do_tilt_up(Fixed_Sized_Grid<char>& grid, size_t col);

void do_tilt_up(Fixed_Sized_Grid<char>& grid);

void do_spin_cycle(Fixed_Sized_Grid<char>& grid);

int compute_load(const Fixed_Sized_Grid<char>& grid);

}  // namespace Day14_Utils

#endif /* __DAY14_UTILS_H__ */