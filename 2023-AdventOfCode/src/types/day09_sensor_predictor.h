#ifndef __DAY09_SENSOR_PREDICTOR_H__
#define __DAY09_SENSOR_PREDICTOR_H__

#include <vector>

struct Day09_Sensor_Predictor {
  static int predict_next_reading(const std::vector<int>& readings);
};

#endif /* __DAY09_SENSOR_PREDICTOR_H__ */