#ifndef __DAY13_UTILS_H__
#define __DAY13_UTILS_H__

#include <vector>

#include "../utils/grid/grid.h"
#include "../utils/string_utils.hpp"

namespace Day13_Utils {

using Char_Grid = Fixed_Sized_Grid<char>;

std::vector<int> find_common_fold_reflection(const std::vector<std::string> &strings);

/**
 * Look for a vertical fold reflection shared by all rows in the grid.
 */
std::vector<int> find_common_vertical_fold_reflection(const Char_Grid &grid);

/**
 * Look for a vertical fold reflection shared by all rows in the grid.
 */
std::vector<int> find_common_horizontal_fold_reflection(const Char_Grid &grid);

int evaluate_part1_reflection_score(const Char_Grid &grid);

int evaluate_part2_reflection_score(Char_Grid &grid);

}  // namespace Day13_Utils

#endif /* __DAY13_UTILS_H__ */