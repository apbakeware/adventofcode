#ifndef __DAY10_TRAVERSAL_UTILS_H__
#define __DAY10_TRAVERSAL_UTILS_H__

#include <functional>
#include <iostream>

#include "../utils/grid/grid.h"
#include "../utils/grid/grid_flood_fill.h"
#include "../utils/grid/grid_utils.h"

struct Day10_Traversal_Utils {
  static bool is_pipe_cell(char cell) {
    static std::string PIPE_CHARS("F-7|JLS");
    return PIPE_CHARS.find(cell) != std::string::npos;
  }

  static bool can_move_up(char src, char dst);

  static bool can_move_right(char src, char dst);

  static bool can_move_down(char src, char dst);

  static bool can_move_left(char src, char dst);

  static void render_unicode_cell(char value) {
    if (value == '|') {
      std::cout << "\u2551";
    } else if (value == 'F') {
      std::cout << "\u2554";
    } else if (value == '7') {
      std::cout << "\u2557";
    } else if (value == '-') {
      std::cout << "\u2550";
    } else if (value == 'J') {
      std::cout << "\u255D";
    } else if (value == 'L') {
      std::cout << "\u255A";
    } else if (value == 'S') {
      std::cout << "\u2588";
    } else if (value == 'X') {
      std::cout << "\u2592";
    } else {
      std::cout << " ";
    }
  }

  template <class Grid_T>
  static void render_unicode_grid(const Grid_T& grid) {
    for (ssize_t row = 0; row < grid.number_of_rows(); ++row) {
      for (ssize_t col = 0; col < grid.number_of_cols(); ++col) {
        const Grid_Location loc{grid.number_of_rows() - 1 - row, col};
        const auto value = grid.at(loc);
        render_unicode_cell(value);
      }
      std::cout << "\n";
    }
  }

  /**
   * Try to move in the pipe. The next_op parameter describes how to get the
   * next directional cell while the next_op method checks if the
   * movement is legal. If so, updated the locations with the new
   * previous and current location and return true. If not, return false and the
   * locations are not updated.
   */

  template <class Grid_T>
  static bool try_pipe_move(
      const Grid_T& grid, Grid_Location& io_prev_loc, Grid_Location& io_loc,
      std::function<Grid_Location(Grid_Location, size_t)> next_op,
      std::function<bool(char, char)> move_predicate) {
    const Grid_Location next_loc = next_op(io_loc, 1);
    if (io_prev_loc == next_loc) return false;

    const bool did_move = is_on_grid(next_loc, grid) &&
                          move_predicate(grid.at(io_loc), grid.at(next_loc));

    if (did_move) {
      SPDLOG_DEBUG("Successful pipe move from {} to {}", io_loc, next_loc);
      io_prev_loc = io_loc;
      io_loc = next_loc;
    }
    // Make sure we didn't come from above
    return did_move;
  }

  /**
   * Travese through the pipe from the starting location until you arrive
   * back at the start. Inovke op() for each location along the pipe.
   *
   * Preconditions are they pipe is a loop an that in
   * each cell these are 2 connections, a way in and a way out.
   */
  template <class Grid_T, class Op>
  static void traverse_pipe_loop_to_start(const Grid_T& grid,
                                          const Grid_Location& start, Op op) {
    Grid_Location prev_loc = start;
    Grid_Location current_loc = start;

    auto move_up_test = [&]() -> bool {
      return Day10_Traversal_Utils::try_pipe_move<Grid_T>(
          grid, prev_loc, current_loc, &Grid_Location::up,
          &Day10_Traversal_Utils::can_move_up);
    };

    auto move_right_test = [&]() -> bool {
      return Day10_Traversal_Utils::try_pipe_move<Grid_T>(
          grid, prev_loc, current_loc, &Grid_Location::right,
          &Day10_Traversal_Utils::can_move_right);
    };

    auto move_down_test = [&]() -> bool {
      return Day10_Traversal_Utils::try_pipe_move<Grid_T>(
          grid, prev_loc, current_loc, &Grid_Location::down,
          &Day10_Traversal_Utils::can_move_down);
    };

    auto move_left_test = [&]() -> bool {
      return Day10_Traversal_Utils::try_pipe_move<Grid_T>(
          grid, prev_loc, current_loc, &Grid_Location::left,
          &Day10_Traversal_Utils::can_move_left);
    };

    do {
      const bool moved = move_up_test() || move_right_test() ||
                         move_down_test() || move_left_test();
      if (moved) {
        op(current_loc, grid.at(current_loc));
      } else {
        break;
      }
    } while (current_loc != start);
  }

  template <class Grid_T>
  static ssize_t determine_pipe_length(const Grid_T& grid,
                                       const Grid_Location& start) {
    ssize_t length = 0;
    auto op = [&](__attribute__((unused)) const auto& loc,
                  __attribute__((unused)) const auto unused) { ++length; };
    traverse_pipe_loop_to_start(grid, start, op);
    return length;
  }

  template <class Grid_T>
  static Grid_T isolate_pipe(const Grid_T& grid, const Grid_Location& start) {
    Grid_T pipe_isolated_grid(grid.number_of_rows(), grid.number_of_cols(),
                              ' ');
    auto op = [&](const auto& loc, const auto value) {
      pipe_isolated_grid.set(loc, value);
    };
    traverse_pipe_loop_to_start(grid, start, op);
    return pipe_isolated_grid;
  }

  /**
   * Find the location of the pipe which is left-most on the grid. Where
   * multiple pipe segments are in the same column, the most up location is
   * returned.
   */
  template <class Grid_T>
  static Grid_Location find_left_most_upper_location(
      const Grid_T& grid, const Grid_Location& start) {
    Grid_Location result = start;

    auto op = [&](const auto& loc, __attribute__((unused)) const auto unused) {
      if (loc.col < result.col) {
        result = loc;
      } else if (loc.col == result.col) {
        if (loc.row >= result.row) {
          result = loc;
        }
      }
    };

    traverse_pipe_loop_to_start(grid, start, op);
    return result;
  }

  /**
   * Traverse and find the pipe loop. While traversing the pipe
   * use a flood fill algorithm to find the number of cells
   * inscribed within the pipe loop. The grid is updated to reflect
   * the flooding.
   *
   * The grid passed in must be only the pipe and open cells.
   * This may be accomplished using teh isolate_pipe() method.
   *
   * Only | and -, and maybe S pipe elements have floodable
   * neighbors. The angle piples have pipes on the neighbor
   * where flooding would occur.
   */
  template <class Grid_T>
  static int flood_inscribed_cells(Grid_T& grid, const Grid_Location& start) {
    // TODO: pass or template these
    const char OPEN = ' ';
    const char FLOOD = 'X';

    Grid_Location prev_loc = start;
    Grid_Location current_loc = start;

    // staring location has to be F to be leftmost upper -- S masks F
    // 0 up, 1 right, 2 down, 3 left
    int direction_of_movement = 1;
    // ASSERT value at location is S or F

    int flood_size = 0;
    auto op = [&](const auto& loc, const auto value) {
      switch (value) {
        case 'F':
          direction_of_movement = 1;
          break;
        case '7':
          direction_of_movement = 2;
          break;
        case 'J':
          direction_of_movement = 3;
          break;
        case 'L':
          direction_of_movement = 0;
          break;
        case '-':
          if (direction_of_movement == 1) {
            flood_size +=
                grid_flood_fill(grid, Grid_Location::down(loc), OPEN, FLOOD);
          } else if (direction_of_movement == 3) {
            flood_size +=
                grid_flood_fill(grid, Grid_Location::up(loc), OPEN, FLOOD);
          } else {
            SPDLOG_ERROR("Unexpected direction of movement '{}' on cell '{}'",
                         direction_of_movement, value);
          }
          break;
        case '|':
          if (direction_of_movement == 0) {
            flood_size +=
                grid_flood_fill(grid, Grid_Location::right(loc), OPEN, FLOOD);
          } else if (direction_of_movement == 2) {
            flood_size +=
                grid_flood_fill(grid, Grid_Location::left(loc), OPEN, FLOOD);
          } else {
            SPDLOG_ERROR("Unexpected direction of movement '{}' on cell '{}'",
                         direction_of_movement, value);
          }
          break;
          // TODO: handle S
        default:
          SPDLOG_WARN("unhandled case '{}'", value);
          break;
      }
    };

    traverse_pipe_loop_to_start(grid, start, op);

    return flood_size;
  };

  // traverse_pipe_loop_to_start(grid, start, op);

  // auto move_up_test = [&]() -> bool {
  //   return Day10_Traversal_Utils::try_pipe_move<Grid_T>(
  //       grid, prev_loc, current_loc, &Grid_Location::up,
  //       &Day10_Traversal_Utils::can_move_up);
  // };

  // auto move_right_test = [&]() -> bool {
  //   return Day10_Traversal_Utils::try_pipe_move<Grid_T>(
  //       grid, prev_loc, current_loc, &Grid_Location::right,
  //       &Day10_Traversal_Utils::can_move_right);
  // };

  // auto move_down_test = [&]() -> bool {
  //   return Day10_Traversal_Utils::try_pipe_move<Grid_T>(
  //       grid, prev_loc, current_loc, &Grid_Location::down,
  //       &Day10_Traversal_Utils::can_move_down);
  // };

  // auto move_left_test = [&]() -> bool {
  //   return Day10_Traversal_Utils::try_pipe_move<Grid_T>(
  //       grid, prev_loc, current_loc, &Grid_Location::left,
  //       &Day10_Traversal_Utils::can_move_left);
  // };

  // do {
  //   counts.num_pipe_cells++;
  //   if (move_right_test()) {
  //     counts.num_flodded_cells += grid_flood_fill(
  //         grid, Grid_Location::down(current_loc), OPEN, FLOOD);
  //   } else if (move_down_test()) {
  //     counts.num_flodded_cells += grid_flood_fill(
  //         grid, Grid_Location::left(current_loc), OPEN, FLOOD);
  //   } else if (move_left_test()) {
  //     counts.num_flodded_cells +=
  //         grid_flood_fill(grid, Grid_Location::up(current_loc), OPEN,
  //         FLOOD);
  //   } else if (move_up_test()) {
  //     counts.num_flodded_cells += grid_flood_fill(
  //         grid, Grid_Location::right(current_loc), OPEN, FLOOD);
  //   }
  // } while (current_loc != start);

  // counts.num_non_flooded_cells =
  //     (grid.number_of_rows() * grid.number_of_cols()) -
  //     counts.num_flodded_cells - counts.num_pipe_cells;

  //   return counts;
  // }
};

#endif /* __DAY10_TRAVERSAL_UTILS_H__ */