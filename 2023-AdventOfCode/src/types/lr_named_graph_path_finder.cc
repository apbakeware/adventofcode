#include "lr_named_graph_path_finder.h"

#include "spdlog/spdlog.h"

Cyclic_Direction_List::Cyclic_Direction_List(const std::string& directions)
    : _directions(directions), _iter(_directions.begin()) {}

char Cyclic_Direction_List::next() {
  if (_iter == _directions.end()) {
    _iter = _directions.begin();
  }

  char next = *_iter;
  ++_iter;
  return next;
}

size_t Lr_Named_Graph_Path_Finder::distance_between(
    const Lr_Named_Graph_Path_Finder::LR_Named_Route_Graph_Type& graph,
    const std::string& start_node_label, const std::string& dest_node_label,
    Cyclic_Direction_List& directions) {
  size_t distance = 0;
  std::string current_node_label = start_node_label;

  // DETECT cycle by limiting distance to a large number
  SPDLOG_DEBUG("Start: {}  Destination: {}", start_node_label, dest_node_label);
  while (current_node_label != dest_node_label) {
    SPDLOG_DEBUG(" Current node label: {}", current_node_label);
    char direction = directions.next();
    const LR_Named_Route_Graph_Type::Routes_Node* node =
        graph.find(current_node_label);

    ++distance;
    if (node == nullptr) {
      // Log and better exception message
      SPDLOG_ERROR("Got null graph node, traversal failed");
      throw std::runtime_error("Could not traverse graph");
    }

    if (direction == 'L') {
      SPDLOG_DEBUG(" L -- at: 0");
      current_node_label = node->at(0);
    } else if (direction == 'R') {
      SPDLOG_DEBUG(" R -- at: 1");
      current_node_label = node->at(1);
    } else {
      throw std::runtime_error("Unexpected direction value");
    }

    if (distance > 1000000) {
      throw std::runtime_error("Max traversal stopping loop");
    }
  }

  return distance;
}

bool Lr_Named_Graph_Concurrent_Path_Finder::all_locations_end_with_z(
    const std::vector<std::string>& locations) {
  return std::all_of(locations.begin(), locations.end(),
                     [](const auto& name) { return name.back() == 'Z'; });
}

// Throws if graph cannot be traversed with directions
// TODO: make a predicate to test when all the locations are set

size_t Lr_Named_Graph_Concurrent_Path_Finder::distance_between(
    const Lr_Named_Graph_Concurrent_Path_Finder::LR_Named_Route_Graph_Type&
        graph,
    const std::vector<std::string>& starting_node_labels,
    Cyclic_Direction_List& directions) {
  size_t distance = 0;
  std::string current_node_label;
  std::vector<std::string> starting_locations(starting_node_labels);
  std::vector<std::string> ending_locations(starting_node_labels);

  while (!Lr_Named_Graph_Concurrent_Path_Finder::all_locations_end_with_z(
      ending_locations)) {
    char direction = directions.next();
    ++distance;
    starting_locations.swap(ending_locations);
    ending_locations.clear();

    // SPDLOG_ERROR("Starting locations: {}", fmt::join(starting_locations, "
    // "));

    for (const auto& name : starting_locations) {
      const auto node = graph.find(name);
      if (node == nullptr) {
        SPDLOG_ERROR("Got null graph node, traversal failed");
        throw std::runtime_error("Could not traverse graph");
      }

      if (direction == 'L') {
        ending_locations.push_back(node->at(0));
      } else if (direction == 'R') {
        ending_locations.push_back(node->at(1));
      } else {
        throw std::runtime_error("Unexpected direction value");
      }
    }

    if (distance % 5000 == 0) {
      SPDLOG_ERROR(" Hops: {}", distance);
    }

    // SPDLOG_ERROR("  Ending locations: {}", fmt::join(ending_locations, " "));
  }

  return distance;
}
