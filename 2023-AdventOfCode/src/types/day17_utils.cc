#include "day17_utils.h"

#include <array>
#include <numeric>

#include "spdlog/spdlog.h"
#include "utils/grid/grid_direction.h"
#include "utils/grid/grid_traversal.h"

namespace Day17_Utils {

// The starting point, the lava pool, is the top-left city block; the
// destination, the machine parts factory, is the bottom-right city block.
Origin_Destination_Location get_origin_and_destination_location(
    const Heat_Loss_Grid& grid) {
  // Error cases

  return {{grid.number_of_rows() - 1, 0}, {0, grid.number_of_cols() - 1}};
}

Visitation_Grid create_visitation_grid(const Heat_Loss_Grid& grid) {
  Visitation_Grid vgrid(grid.number_of_rows(), grid.number_of_cols(), false);

  return vgrid;
}

// Implement greedy traversal taking the least heat cost at each step.
// TODO: Is this sufficient? or do we need full traversal testing
Path create_least_cost_path(const Heat_Loss_Grid& heat_grid,
                            const Origin_Destination_Location& endpoints) {
  Path path;
  Visitation_Grid visits = create_visitation_grid(heat_grid);
  Grid_Location location = endpoints.origin;

  // TODO: move to a structure
  std::array<Orthogonal_Direction::Direction, 2> recent_moves = {
      Orthogonal_Direction::NONE, Orthogonal_Direction::NONE};

  SPDLOG_DEBUG("Grid:\n{}", heat_grid);
  SPDLOG_DEBUG("Start: {}  Dest: {}", endpoints.origin, endpoints.destination);

  path.push_back({location, heat_grid.at(location)});
  visits.set(location, true);

  const std::vector<Orthogonal_Direction::Direction> directions{
      Orthogonal_Direction::UP, Orthogonal_Direction::RIGHT,
      Orthogonal_Direction::DOWN, Orthogonal_Direction::LEFT};

  while (location != endpoints.destination) {
    SPDLOG_DEBUG("Current Location: {}", location);
    int least_value = std::numeric_limits<int>::max();
    Grid_Location move_location;
    Orthogonal_Direction::Direction move_direction;
    for (const auto direction : directions) {
      if (direction == recent_moves[0] && direction == recent_moves[1]) {
        SPDLOG_DEBUG("Capping to 3 squares in single direction");
        continue;
      }

      int check_value = heat_value_if_unvisited_direction(heat_grid, visits,
                                                          location, direction);

      if (Orthogonal_Direction::next_location(location, direction) ==
          endpoints.destination) {
        SPDLOG_DEBUG("  Found destination {}, taking it", direction);
        least_value = check_value;
        move_direction = direction;
        move_location =
            Orthogonal_Direction::next_location(location, direction);
        break;
      } else if (check_value < least_value) {
        least_value = check_value;
        move_direction = direction;
        move_location =
            Orthogonal_Direction::next_location(location, direction);

        SPDLOG_DEBUG("  Value {} @ {} is less", check_value, direction);
      }
    }

    // TODO: detect cannot move...
    recent_moves[1] = recent_moves[0];
    recent_moves[0] = move_direction;
    visits.set(move_location, true);
    path.push_back({move_location, least_value});
    location = move_location;
  }

  return path;
}

int path_heat_const(const Path& path) {
  return std::accumulate(
      path.begin(), path.end(), 0,
      [](int acc, const auto& node) { return acc + node.value; });
}

int heat_value_if_unvisited_direction(
    const Heat_Loss_Grid& grid, const Visitation_Grid& visits,
    const Grid_Location& location, Orthogonal_Direction::Direction direction) {
  Grid_Location next_location =
      Orthogonal_Direction::next_location(location, direction);

  if (!is_on_grid(next_location, grid)) {
    return UNACCEPTABLE_MOVEMENT;
  }

  if (visits.at(next_location)) {
    return UNACCEPTABLE_MOVEMENT;
  }

  return grid.at(next_location);
}

}  // namespace Day17_Utils