#ifndef __SOLVER_RESULT_H__
#define __SOLVER_RESULT_H__

#include <chrono>
#include <iosfwd>
#include <string>

struct Solver_Result {
   const std::string problem_label;
   const std::chrono::milliseconds data_lad_duration;
   const std::chrono::milliseconds part_1_solve_duration;
   const std::chrono::milliseconds part_2_solve_duration;
   const std::string part_1_solution;
   const std::string part_2_solution;
};

std::ostream &operator<<(std::ostream &ostr, const Solver_Result &record);

struct Expected_Solver_Result {
   const std::string problem_label;
   const std::chrono::milliseconds data_lad_duration;
   const std::chrono::milliseconds part_1_solve_duration;
   const std::chrono::milliseconds part_2_solve_duration;
   const std::string part_1_expected_solution;
   const std::string part_1_actual_solution;
   const std::string part_2_expected_solution;
   const std::string part_2_actual_solution;
};

std::ostream &operator<<(std::ostream &ostr, const Expected_Solver_Result &record);

#endif // __SOLVER_RESULT_H__