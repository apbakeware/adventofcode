#ifndef __LINE_STRING_H__
#define __LINE_STRING_H__

#include <iosfwd>
#include <string>

/**
 * Store a string which is parsed from a whole line
 * from an input stream.
 */
class Line_String {
 public:
  /**
   * Set the internal line value to a copy of the provided value.
   */
  void set(const std::string& instr) { _line = instr; };

  /**
   * Move the provided string into the internal line value.
   */
  void set(const std::string&& instr) { _line = instr; }

  /**
   * Get a constant reference to the internal line value.
   */
  const std::string& ref() const { return _line; }

  /**
   * Return a copy of the line value.
   */
  std::string get() const { return _line; }

 private:
  std::string _line;
};

std::istream& operator>>(std::istream& instr, Line_String& obj);

#endif /* __LINE_STRING_H__ */