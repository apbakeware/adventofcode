#include "parse_exception.h"

Parse_Exception::Parse_Exception() {}

Parse_Exception::Parse_Exception(const std::string& msg) : m_msg(msg) {}

const char* Parse_Exception::what() const throw() {
  std::string message = "Parse exception.";
  if (!m_msg.empty()) {
    message += " Message: ";
    message += m_msg;
  }

  return message.c_str();
}