#ifndef __I_RESULT_processor_H__
#define __I_RESULT_processor_H__

template<class Result_T>
class IResult_Processor {
public:

   using Result_Type = Result_T;

   virtual ~IResult_Processor() = default;

   virtual void process(const Result_Type & result ) = 0;

};

#endif // __I_RESULT_PROCESSOR_H__