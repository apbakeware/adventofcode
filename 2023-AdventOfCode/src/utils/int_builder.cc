#include "int_builder.h"

#include <cctype>

Int_Builder::Int_Builder() : m_val(0), m_num_digits(0), m_has_value(false) {}

// false if not a digit
// Can this be the << operator to act as a shift as well
bool Int_Builder::append(char digit) {
  if (!isdigit(digit)) return false;
  ++m_num_digits;
  m_has_value = true;
  m_val = m_val * 10 + (digit - '0');
  return true;
}