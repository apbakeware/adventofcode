#ifndef __APPLY_ADJACENT_OPERATION_H__
#define __APPLY_ADJACENT_OPERATION_H__

#include <iterator>

// Apply a binary operation to adjacent elements in a collecxtion
// and put the resulting value in the output collection.
// The resulting collecxtion will have 1 less element than the first.
// Ensure the destination collection is big enough or use an
// inserter iterator.

// called with op(next, current)
template <class In_Iter_T, class Out_Iter_T, class Binary_Op_T>
Out_Iter_T apply_adjacent_operation(In_Iter_T src_begin, In_Iter_T src_end,
                                    Out_Iter_T dst, Binary_Op_T op) {
  if (src_begin == src_end) return dst;

  In_Iter_T next_iter = std::next(src_begin, 1);
  while (next_iter != src_end) {
    *dst++ = op(*next_iter, *src_begin);
    ++src_begin;
    ++next_iter;
  }

  return dst;
}

#endif /* __APPLY_ADJACENT_OPERATION_H__ */