#ifndef __NAMED_ROUTE_GRAPH_H__
#define __NAMED_ROUTE_GRAPH_H__

#include <array>
#include <string>
#include <unordered_map>

/**
 * The Named_Route_Graph provides a bi-direction
 * graph where the edges between the nodes are
 * named. This allows for "directions" to traverse
 * the graph.
 *
 * This structure stores a fixed number of routes
 * between nodes. The Route Processing algorithm
 * is repsonsible for selecting the route.
 *
 * Routes are named by strings.
 */
template <size_t Max_Routes_T>
class Named_Route_Graph {
 public:
  static constexpr size_t Route_Capacity = Max_Routes_T;

  // Use strings to re-index the graph. Consider using
  // pointers but don't want to deal with iterators now
  struct Routes_Node {
    std::string& at(size_t index) { return _routes.at(index); }
    const std::string& at(size_t index) const { return _routes.at(index); }
    std::array<std::string, Route_Capacity> _routes;
  };

  bool has_node(const std::string& label) const {
    return _graph.count(label) != 0;
  }

  // Add a new node and return the routes structure which can be used
  // to add nodes
  // Don't store the pointer, subsecquent add_nodes() may invalidet the
  // pointer
  Routes_Node* add(const std::string& label) {
    typename Node_Set::value_type value{label, Routes_Node()};
    auto node = _graph.insert(value);
    auto ins_iter = node.first;
    return &(ins_iter->second);
  }

  // Null if not found?
  Routes_Node* find(const std::string& label) {
    auto node = _graph.find(label);
    Routes_Node* ptr = (node == _graph.end()) ? nullptr : &(node->second);
    return ptr;
  }

  const Routes_Node* find(const std::string& label) const {
    auto node = _graph.find(label);
    const Routes_Node* ptr = (node == _graph.end()) ? nullptr : &(node->second);
    return ptr;
  }

 private:
  using Node_Set = std::unordered_map<std::string, Routes_Node>;
  Node_Set _graph;
};

// Could rebuild to pointers at the graph, not just labels to speed up?

// Building doesnt ensure that all the route nodes are in the graph...need
// a validate graph method

#endif /* __NAMED_ROUTE_GRAPH_H__ */