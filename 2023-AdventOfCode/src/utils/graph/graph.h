#ifndef __GRAPH_H__
#define __GRAPH_H__

#include <algorithm>
#include <iosfwd>
#include <string>
#include <unordered_map>
#include <vector>
#include "spdlog/fmt/ostr.h"

// todo: make these templates of node type


struct Graph_Input_Record {
   std::string origin;
   std::string destination;
};

std::istream & operator>>(std::istream & instr, Graph_Input_Record & rec );

// Bi-directional
class Graph {
public:

   void add_connection(const std::string & orig, const std::string & dest);

   bool has_node(const std::string & node ) const {
      return graph.count(node);
   }

   size_t number_of_children(const std::string & node ) const {
      auto iter = graph.find(node);
      return (iter == graph.end()) ? 0 : (iter -> second).size();
   }

   // return iterators for the connections?

   template<class Operation_T>
   void for_each_connection(const std::string & node, Operation_T op) {

      if( graph.count(node) ) {
         const auto & connections = graph[node];
         std::for_each(
            connections.begin(),
            connections.end(),
            op
         );
      }
   }

   
   template<class Operation_T>
   void for_each_connection(const std::string & node, Operation_T op) const {

      if( graph.count(node) ) {
         const auto & connections = graph.find(node);
         if(connections != graph.end())
         {
            std::for_each(
               (connections->second).begin(),
               (connections->second).end(),
               op
            );
         }
      }
   }

   template<typename OStream>
   friend OStream &operator<<(OStream &os, const Graph & obj) {
      for(auto iter = obj.graph.begin(); iter != obj.graph.end(); ++iter) {
         os << "[" << iter -> first << "]: [";
         std::copy( (iter -> second).begin(),
            (iter -> second).end(),
            std::ostream_iterator<std::string>(os, "  ")
         );
         os << "]\n";
      }
      return os;
    }

private:
   using Connections = std::vector<std::string>;
   std::unordered_map<std::string, Connections> graph;
};


#endif // __GRAPH_H__