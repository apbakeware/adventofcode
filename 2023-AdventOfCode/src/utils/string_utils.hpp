#ifndef __STRING_UTILS_H__
#define __STRING_UTILS_H__

#include <algorithm>
#include <functional>
#include <string>
#include <vector>

template <class Str_Iter_T>
struct Str_Int_Extraction {
  operator bool() const { return extraction_begin != extraction_end; }

  int extracted;
  Str_Iter_T extraction_begin;
  Str_Iter_T extraction_end;
};

template <class Str_Iter_T>
Str_Iter_T find_first_digit(Str_Iter_T begin, Str_Iter_T end) {
  return std::find_if(begin, end,
                      [](const auto value) { return std::isdigit(value); });
}

template <class Str_Iter_T>
Str_Iter_T find_first_nondigit(Str_Iter_T begin, Str_Iter_T end) {
  return std::find_if(begin, end,
                      [](const auto value) { return !std::isdigit(value); });
}

template <class Str_Iter_T>
std::pair<Str_Iter_T, Str_Iter_T> find_first_digit_sequence(Str_Iter_T begin,
                                                            Str_Iter_T end) {
  auto dig_iter = find_first_digit(begin, end);
  return {dig_iter, find_first_nondigit(dig_iter, end)};
}

template <class Str_Iter_T>
Str_Int_Extraction<Str_Iter_T> extract_first_int_seqeuence_from_string(
    Str_Iter_T begin, Str_Iter_T end) {
  const auto& digit_seq = find_first_digit_sequence(begin, end);
  if (digit_seq.first != digit_seq.second) {
    const std::string extraction(digit_seq.first, digit_seq.second);
    return {std::stoi(extraction), digit_seq.first, digit_seq.second};
  }

  return {0, end, end};
}

template <class Str_Iter_T>
Str_Int_Extraction<Str_Iter_T> extract_last_int_seqeuence_from_string(
    Str_Iter_T begin, Str_Iter_T end) {
  auto extraction = find_first_digit_sequence(begin, end);
  while (extraction) {
    auto prev = extraction;
    extraction = find_first_digit_sequence(begin, end);
    if (!extraction) {
      extraction = std::move(prev);
    }
  }

  return extraction;
}

size_t find_last_occurance(const std::string& src, const std::string& target);

// TODO: structure to dtermine if is valid?
std::pair<size_t, size_t> find_first_and_last_occurance(
    const std::string& str, const std::string& srch);

/**
 * Find the positions in the string which if "folded"
 * there there is a perfect reflection over the
 * length of the shortest reflected string.
 *
 * The method is broken into two halves of the
 * string. Since only the smallest of the reflection and
 * original remaining string are tested, in order to
 * compare then correction, the std::equal() uses the
 * range of the shorter (reflection first half, original
 * remaining second half).
 *
 * Building the reflection needs to be done backwards. However
 * a vector is used with push_back for efficiency and its checked
 * against reverse iterators to make the reflection sequence correct.
 *
 * @param str String to detect fold reflections in.
 *
 * @return Collection of indicies where a fold reflection occurs.
 */
std::vector<ssize_t> find_fold_reflection_points(const std::string& str);

/**
 * Split a string on a delimiter into a collection of strings.
 */
std::vector<std::string> split(const std::string& str, char delim);

// trim from start (in place)
static inline void ltrim(std::string& s) {
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return !std::isspace(ch);
          }));
}

// trim from end (in place)
static inline void rtrim(std::string& s) {
  s.erase(std::find_if(s.rbegin(), s.rend(),
                       [](unsigned char ch) { return !std::isspace(ch); })
              .base(),
          s.end());
}

// trim from both ends (in place)
static inline void trim(std::string& s) {
  rtrim(s);
  ltrim(s);
}

// trim from start (copying)
static inline std::string ltrim_copy(std::string s) {
  ltrim(s);
  return s;
}

// trim from end (copying)
static inline std::string rtrim_copy(std::string s) {
  rtrim(s);
  return s;
}

// trim from both ends (copying)
static inline std::string trim_copy(std::string s) {
  trim(s);
  return s;
}

#endif  // __STRING_UTILS_H__