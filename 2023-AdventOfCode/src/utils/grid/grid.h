#ifndef __GRID_H__
#define __GRID_H__

#include <array>
#include <iostream>
#include <iterator>
#include <vector>

#include "grid_location.h"
#include "spdlog/fmt/ostr.h"
#include "spdlog/spdlog.h"

// Set on initialization
template <class Data_T>
class Fixed_Sized_Grid {
 public:
  typedef Data_T value_type;

  Fixed_Sized_Grid(int num_rows, int num_cols, Data_T fill_val = Data_T())
      : m_num_rows(num_rows), m_num_cols(num_cols) {
    grid.resize(num_rows);
    for (auto &row : grid) {
      row.resize(num_cols, fill_val);
    }
  }

  ssize_t number_of_cols() const { return static_cast<ssize_t>(m_num_cols); }

  ssize_t number_of_rows() const { return static_cast<ssize_t>(m_num_rows); }

  Data_T at(const Grid_Location &location) const {
    return grid.at(location.row).at(location.col);
  }

  Data_T &at_ref(const Grid_Location &location) const {
    return grid.at(location.row).at(location.col);
  }

  void set(const Grid_Location &location, const Data_T &data) {
    grid.at(location.row).at(location.col) = data;
  }

  // todo row() col() to return vector of Data_T (see char specialization)

  template <class Op>
  void for_each_cell(Op op) {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location));
      }
    }
  }

  template <class Op>
  void for_each_cell(Op op) const {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location));
      }
    }
  }

  /**
   * Write the contents of the grid to the output stream. This
   * is done in reverse row order. In this manner, the highest
   * indexed row is the first inserted line. This replicates
   * how it would be read in a file with a (0,0) in the
   * lower left corner.
   */
  template <typename OStream>
  friend OStream &operator<<(OStream &os,
                             const Fixed_Sized_Grid<Data_T> &grid) {
    for (auto row = grid.grid.rbegin(); row != grid.grid.rend(); ++row) {
      std::copy(row->begin(), row->end(),
                std::ostream_iterator<value_type>(os, ""));
      os << "\n";
    }

    return os;
  }

 private:
  using Row = std::vector<Data_T>;
  using Grid = std::vector<Row>;

  ssize_t m_num_rows;
  ssize_t m_num_cols;

  Grid grid;
};

template <>
class Fixed_Sized_Grid<char> {
 public:
  typedef char value_type;

  Fixed_Sized_Grid(int num_rows, int num_cols,
                   value_type fill_val = value_type())
      : m_num_rows(num_rows), m_num_cols(num_cols) {
    grid.resize(num_rows);
    for (auto &row : grid) {
      row.resize(num_cols, fill_val);
    }
  }

  ssize_t number_of_cols() const { return static_cast<ssize_t>(m_num_cols); }

  ssize_t number_of_rows() const { return static_cast<ssize_t>(m_num_rows); }

  value_type at(const Grid_Location &location) const {
    return grid.at(location.row).at(location.col);
  }

  void set(const Grid_Location &location, const value_type &data) {
    grid.at(location.row).at(location.col) = data;
  }

  /**
   * Return the characters in the specified row from col 0 to col X.
   */
  std::string row(ssize_t num) const {
    return std::string(grid.at(num).begin(), grid.at(num).end());
  }

  /**
   * Return the characters in the column from row 0 to row X.
   */
  std::string col(ssize_t num) const {
    std::string col;
    for (const auto &row : grid) {
      col += row.at(num);
    }
    return col;
  }

  template <class Op>
  void for_each_cell(Op op) {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location));
      }
    }
  }

  template <class Op>
  void for_each_cell(Op op) const {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location));
      }
    }
  }

  /**
   * Write the contents of the grid to the output stream. This
   * is done in reverse row order. In this manner, the highest
   * indexed row is the first inserted line. This replicates
   * how it would be read in a file with a (0,0) in the
   * lower left corner.
   */
  template <typename OStream>
  friend OStream &operator<<(OStream &os,
                             const Fixed_Sized_Grid<value_type> &grid) {
    for (auto row = grid.grid.rbegin(); row != grid.grid.rend(); ++row) {
      std::copy(row->begin(), row->end(),
                std::ostream_iterator<value_type>(os, ""));
      os << "\n";
    }

    return os;
  }

 private:
  using Row = std::vector<value_type>;
  using Grid = std::vector<Row>;

  ssize_t m_num_rows;
  ssize_t m_num_cols;

  Grid grid;
};

#endif  // __GRID_H__