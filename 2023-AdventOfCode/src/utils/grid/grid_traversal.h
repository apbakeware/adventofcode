#ifndef __GRID_TRAVERSAL_H__
#define __GRID_TRAVERSAL_H__
#include <functional>
#include <iostream>

#include "grid.h"
#include "grid_utils.h"
#include "spdlog/spdlog.h"

template <class Operation_T, class Grid_T>
void for_each_orthogonal(Grid_T& grid, const Grid_Location& location,
                         Operation_T op) {
  const std::vector<Grid_Location> surrounding = {
      Grid_Location::up(location), Grid_Location::right(location),
      Grid_Location::down(location), Grid_Location::left(location)};

  for_each_cell_location(grid, surrounding.begin(), surrounding.end(), op);
}

template <class Operation_T, class Grid_T>
void for_each_surrounding(Grid_T& grid, const Grid_Location& location,
                          Operation_T op) {
  const std::vector<Grid_Location> surrounding = {
      Grid_Location::left(Grid_Location::up(location)),
      Grid_Location::up(location),
      Grid_Location::right(Grid_Location::up(location)),
      Grid_Location::right(location),
      Grid_Location::right(Grid_Location::down(location)),
      Grid_Location::down(location),
      Grid_Location::left(Grid_Location::down(location)),
      Grid_Location::left(location)};

  for_each_cell_location(grid, surrounding.begin(), surrounding.end(), op);
}

/* #region walk_methods */

// TODO: these are incomplete. Walking methods should take a predicate
// with the grid location and value at each ste...just lick the for_each_
// methods

// Distance_to is satisifies these methods.

/**
 * Walk the grid from the starting location using the movement
 * function until the end of the grid has been reached. At each
 * step, the handler is called. The initial location is included
 * in the walk.
 *
 * @return The number of steps taken in the walk. -1 if the
 * original location is not on the grid.
 */
template <class Grid_T>
int walk(Grid_T& grid, Grid_Location location,
         std::function<Grid_Location(const Grid_Location&)> movement,
         std::function<void(const Grid_Location&, typename Grid_T::value_type)>
             handler) {
  if (!is_on_grid(location, grid)) return -1;

  int step_count = 0;
  while (is_on_grid(location, grid)) {
    handler(location, grid.at(location));
    location = movement(location);
    ++step_count;
  }

  return step_count;
}

template <class Grid_T>
int walk_up(
    Grid_T& grid, Grid_Location location,
    std::function<void(const Grid_Location&, typename Grid_T::value_type)>
        handler) {
  auto movement = [](const auto& loc) -> Grid_Location {
    return Grid_Location::up(loc);
  };

  return walk(grid, location, movement, handler);
}

template <class Grid_T>
int walk_right(
    Grid_T& grid, Grid_Location location,
    std::function<void(const Grid_Location&, typename Grid_T::value_type)>
        handler) {
  auto movement = [](const auto& loc) -> Grid_Location {
    return Grid_Location::right(loc);
  };

  return walk(grid, location, movement, handler);
}

template <class Grid_T>
int walk_down(
    Grid_T& grid, Grid_Location location,
    std::function<void(const Grid_Location&, typename Grid_T::value_type)>
        handler) {
  auto movement = [](const auto& loc) -> Grid_Location {
    return Grid_Location::down(loc);
  };

  return walk(grid, location, movement, handler);
}

template <class Grid_T>
int walk_left(
    Grid_T& grid, Grid_Location location,
    std::function<void(const Grid_Location&, typename Grid_T::value_type)>
        handler) {
  auto movement = [](const auto& loc) -> Grid_Location {
    return Grid_Location::left(loc);
  };

  return walk(grid, location, movement, handler);
}

/**
 * Walk the grid from the starting location using the movement
 * function until the end of the grid has been reached or the predicate
 * evalutes true. At each step, the handler is called. The handler is not
 * called for the cell where the predicate is true.
 *
 * @return The number of steps taken in the walk. -1 if the
 * original location is not on the grid.
 */
template <class Grid_T>
int walk_until(
    Grid_T& grid, Grid_Location location,
    std::function<Grid_Location(const Grid_Location&)> movement,
    std::function<void(const Grid_Location&, typename Grid_T::value_type)>
        handler,
    std::function<bool(const Grid_Location&, typename Grid_T::value_type)>
        stop_pred) {
  if (!is_on_grid(location, grid)) return -1;

  int step_count = 0;
  while (is_on_grid(location, grid)) {
    if (stop_pred(location, grid.at(location))) {
      break;
    }
    handler(location, grid.at(location));
    ++step_count;
    location = movement(location);
  }

  return step_count;
}

template <class Grid_T>
int walk_up_until(
    Grid_T& grid, Grid_Location location,
    std::function<void(const Grid_Location&, typename Grid_T::value_type)>
        handler,
    std::function<bool(const Grid_Location&, typename Grid_T::value_type)>
        stop_pred) {
  auto movement = [](const auto& loc) -> Grid_Location {
    return Grid_Location::up(loc);
  };

  return walk_until(grid, location, movement, handler, stop_pred);
}

template <class Grid_T>
int walk_right_until(
    Grid_T& grid, Grid_Location location,
    std::function<void(const Grid_Location&, typename Grid_T::value_type)>
        handler,
    std::function<bool(const Grid_Location&, typename Grid_T::value_type)>
        stop_pred) {
  auto movement = [](const auto& loc) -> Grid_Location {
    return Grid_Location::right(loc);
  };

  return walk_until(grid, location, movement, handler, stop_pred);
}

template <class Grid_T>
int walk_down_until(
    Grid_T& grid, Grid_Location location,
    std::function<void(const Grid_Location&, typename Grid_T::value_type)>
        handler,
    std::function<bool(const Grid_Location&, typename Grid_T::value_type)>
        stop_pred) {
  auto movement = [](const auto& loc) -> Grid_Location {
    return Grid_Location::down(loc);
  };

  return walk_until(grid, location, movement, handler, stop_pred);
}

template <class Grid_T>
int walk_left_until(
    Grid_T& grid, Grid_Location location,
    std::function<void(const Grid_Location&, typename Grid_T::value_type)>
        handler,
    std::function<bool(const Grid_Location&, typename Grid_T::value_type)>
        stop_pred) {
  auto movement = [](const auto& loc) -> Grid_Location {
    return Grid_Location::left(loc);
  };

  return walk_until(grid, location, movement, handler, stop_pred);
}

// https:  // stackoverflow.com/questions/11372855/stdbind-to-stdfunction
// https://stackoverflow.com/questions/68290666/stdbind-to-stdfunction-conversion-problem
// using std::placeholders::_2;
// std::function<Grid_Location(const Grid_Location&, ssize_t)> movement =
//     &Grid_Location::up;

// std::function<Grid_Location(const Grid_Location&)> movement1 =
//     std::bind(&Grid_Location::up, std::placeholders::_2, 1);
//      std::bind(&Grid_Location::up, _2, (ssize_t)1);

#endif /* __GRID_TRAVERSAL_H__ */