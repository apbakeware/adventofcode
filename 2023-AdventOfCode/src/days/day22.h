#ifndef __DAY22_H__
#define __DAY22_H__

#include <string>
#include <vector>

struct Day22Data {
   // TODO: Set data type
   typedef int Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day22Part1 : public Day22Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day22Part2 : public Day22Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day22Spec {
   using Data_Spec = Day22Data;
   using Part_1 = Day22Part1;
   using Part_2 = Day22Part2;
   static constexpr const char * day() { return "Day22"; }
   static constexpr const char * data_file_name() { return "data/day22.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day22.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * expected_sample_part_2_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * solved_part_1_result() { return "undefined"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __DAY22_H__
