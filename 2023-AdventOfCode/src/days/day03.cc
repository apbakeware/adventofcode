#include "day03.h"

#include <functional>
#include <numeric>
#include <set>

#include "../framework/solver_registry.h"
#include "../utils/grid/grid.h"
#include "../utils/grid/grid_traversal.h"
#include "../utils/grid/grid_utils.h"
#include "../utils/int_builder.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day03Spec>();
}

const bool _was_registered = _do_registration();

// typedef Fixed_Sized_Grid<char> Engine_Grid;

// TODO: move to grid_utils

typedef std::shared_ptr<Int_Builder> Int_Builder_Ptr;

struct Engine_Grid {
  Fixed_Sized_Grid<char> engine_grid;
  Fixed_Sized_Grid<Int_Builder_Ptr> part_num_grid;
  std::vector<Grid_Location> symbol_locations;
};

/**
 * Build the engine grid from the input data. Then loop each cell
 * in the grid. Build up an integer and record symbol locations
 * in the supplementary collections.
 */
Engine_Grid build_engine_grid(const Day03Data::Input_Data_Collection& data) {
  Int_Builder_Ptr int_builder = std::make_shared<Int_Builder>();

  Fixed_Sized_Grid<char> engine_grid =
      create_char_grid_from_string_rows(data.begin(), data.end());

  Fixed_Sized_Grid<Int_Builder_Ptr> part_num_grid =
      Fixed_Sized_Grid<Int_Builder_Ptr>(engine_grid.number_of_rows(),
                                        engine_grid.number_of_cols());

  std::vector<Grid_Location> symbol_locations;

  engine_grid.for_each_cell([&](const auto& location, char value) {
    if (!int_builder->append(value)) {
      if (value != '.') {
        symbol_locations.push_back(location);
      }

      if (int_builder->has_value()) {
        int_builder = std::make_shared<Int_Builder>();
      }
    } else {
      part_num_grid.set(location, int_builder);
    }
  });

  return {engine_grid, part_num_grid, symbol_locations};
}

}  // namespace

std::string Day03Part1::name() const { return "Day03-Part1"; }

/**
 * Algorithm: (Maybe be able to make more efficient with less passes through
 * grid)
 *  * Create a grid from the input.
 *  * Scan the grid for integers.
 *  * Scan the grid for symbol locations.
 *  * For each symbol location find the engine part number.
 *
 */
std::string Day03Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  int part_sum = 0;
  std::set<int> part_numbers;
  Engine_Grid engine = build_engine_grid(data);

  // For each symbol, look at the surrounding locations
  // in the Int_Builder grid to see if there is a
  // valid engine number. Only use the number once.
  for (const auto& loc : engine.symbol_locations) {
    // Only use a build onece if it touches the symbol in more than 1 area
    std::set<Int_Builder_Ptr> neighboring_builders;

    for_each_surrounding(
        engine.engine_grid, loc,
        [&](const auto& neighbor, __attribute__((unused)) const char val) {
          const auto& neighbor_part = engine.part_num_grid.at(neighbor);
          if (neighbor_part) {
            neighboring_builders.insert(neighbor_part);
          }
        });

    for (const auto& builder : neighboring_builders) {
      part_sum += builder->value();
    }
  }

  return std::to_string(part_sum);
}

std::string Day03Part2::name() const { return "Day03-Part2"; }

std::string Day03Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  int gear_ratio_sum = 0;
  std::set<int> part_numbers;
  Engine_Grid engine = build_engine_grid(data);

  // For each symbol, look at the surrounding locations
  // in the Int_Builder grid to see if there is a
  // valid engine number. Only use the number once.
  for (const auto& loc : engine.symbol_locations) {
    // Only use a build onece if it touches the symbol in more than 1 area
    std::set<Int_Builder_Ptr> neighboring_builders;

    for_each_surrounding(
        engine.engine_grid, loc,
        [&](const auto& neighbor, __attribute__((unused)) const char val) {
          const auto& neighbor_part = engine.part_num_grid.at(neighbor);
          if (neighbor_part) {
            neighboring_builders.insert(neighbor_part);
          }
        });

    if (engine.engine_grid.at(loc) == '*' && neighboring_builders.size() == 2) {
      int gear_ratio = 1;
      std::for_each(neighboring_builders.begin(), neighboring_builders.end(),
                    [&](const auto& obj) { gear_ratio *= obj->value(); });
      gear_ratio_sum += gear_ratio;
    }
  }

  return std::to_string(gear_ratio_sum);
}
