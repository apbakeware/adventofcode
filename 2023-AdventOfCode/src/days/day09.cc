#include "day09.h"

#include <sstream>

#include "../framework/data_loader.hpp"
#include "../framework/solver_registry.h"
#include "../types/day09_sensor_predictor.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day09Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day09Part1::name() const { return "Day09-Part1"; }

std::string Day09Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  int accumulated_prediction = 0;
  for (const auto& line : data) {
    std::istringstream instr(line.get());
    const std::vector<int>& readings = load_structured_data<int>(instr);

    accumulated_prediction +=
        Day09_Sensor_Predictor::predict_next_reading(readings);
  }

  return std::to_string(accumulated_prediction);
}

std::string Day09Part2::name() const { return "Day09-Part2"; }

std::string Day09Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());
  int accumulated_prediction = 0;
  for (const auto& line : data) {
    std::istringstream instr(line.get());
    std::vector<int> readings = load_structured_data<int>(instr);
    std::reverse(readings.begin(), readings.end());
    accumulated_prediction +=
        Day09_Sensor_Predictor::predict_next_reading(readings);
  }

  return std::to_string(accumulated_prediction);
}
