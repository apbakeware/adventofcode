#include "day11.h"

#include "../framework/solver_registry.h"
#include "../types/day11_utils.h"
#include "../utils/grid/grid_distance.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day11Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day11Part1::name() const { return "Day11-Part1"; }

std::string Day11Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  using namespace Day11_Utils;

  const auto& grid = create_galaxy_grid(data.begin(), data.end());
  const auto& rows_to_expand = get_rows_without_galaxies(grid);
  const auto& cols_to_expand = get_cols_without_galaxies(grid);
  auto galaxy_locations = get_galaxy_locations(grid);

  do_galaxy_expansion(galaxy_locations, rows_to_expand, cols_to_expand);

  const auto galaxy_pairs = create_pair_combinations(galaxy_locations);
  int distance_sum = 0;
  std::for_each(
      galaxy_pairs.begin(), galaxy_pairs.end(), [&](const auto& galaxy_pair) {
        distance_sum += galaxy_distance(galaxy_pair.first, galaxy_pair.second);
      });

  return std::to_string(distance_sum);
}

std::string Day11Part2::name() const { return "Day11-Part2"; }

std::string Day11Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  using namespace Day11_Utils;

  const Galaxy_Value_Type expansion_size = 1000000UL - 1UL;
  const auto& grid = create_galaxy_grid(data.begin(), data.end());
  const auto& rows_to_expand = get_rows_without_galaxies(grid);
  const auto& cols_to_expand = get_cols_without_galaxies(grid);
  auto galaxy_locations = get_galaxy_locations(grid);

  do_galaxy_expansion(galaxy_locations, rows_to_expand, cols_to_expand,
                      expansion_size);

  const auto galaxy_pairs = create_pair_combinations(galaxy_locations);
  Galaxy_Value_Type distance_sum = 0;
  std::for_each(
      galaxy_pairs.begin(), galaxy_pairs.end(), [&](const auto& galaxy_pair) {
        distance_sum += galaxy_distance(galaxy_pair.first, galaxy_pair.second);
      });

  return std::to_string(distance_sum);
}
