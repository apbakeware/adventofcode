#include "day18.h"

#include "../framework/solver_registry.h"
#include "../utils/grid/grid_flood_fill.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day18Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day18Part1::name() const { return "Day18-Part1"; }

std::string Day18Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());
  auto dig_site = Day18_Utils::create_dig_site(1000);  // guess at size
  Day18_Utils::dig_trench(dig_site, data);
  int filled = grid_flood_fill(dig_site.dig_grid, {0, 0}, '.', '*');
  int trench_area =
      dig_site.dig_grid.number_of_rows() * dig_site.dig_grid.number_of_cols() -
      filled;

  return std::to_string(trench_area);
}

std::string Day18Part2::name() const { return "Day18-Part2"; }

std::string Day18Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  // Grid flood fill wont work. Way to big. Thought aobut creating
  // frames, but try a different idea.

  // dig the trench and Create verital line sebments and order left to right /
  // top to bottom. Work from row 0 up and see what vertical line is left most.
  // The spread right and count the distance. That's in the area. Repeat
  // for the continuation of the row, then go up to next row.
  // odd number vertical line is start point, even is end of an interior
  // area

  // auto trenches = find_vertical_trenches(data);

  // Sort in ascening column order with low rows first
  // sort_trenches(vert_trenches);

  // Walk from lowest row to highest row
  // Find first trech intersetion
  // Find next
  //  Compute distance and add it to sum
  //  Find next trench pair in row

  return "Unimplemented";
}
