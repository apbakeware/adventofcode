#include "day15.h"

#include <numeric>

#include "../framework/solver_registry.h"
#include "../types/day15_utils.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day15Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day15Part1::name() const { return "Day15-Part1"; }

std::string Day15Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());
  int sum = std::accumulate(data.begin(), data.end(), 0,
                            [](const auto value, const auto& field) {
                              return value + Day15_Utils::hash(field.field);
                            });

  return std::to_string(sum);
}

std::string Day15Part2::name() const { return "Day15-Part2"; }

std::string Day15Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  std::array<Day15_Utils::Lens_Box, 256> lens_boxes;
  for (const auto& field : data) {
    const auto instr = Day15_Utils::parse_instruction(field.field);
    const int lens_index = Day15_Utils::hash(instr.first.label);

    if (instr.second == Day15_Utils::Operation::UPDATE) {
      lens_boxes.at(lens_index).update(instr.first);
    } else if (instr.second == Day15_Utils::Operation::REMOVE) {
      lens_boxes.at(lens_index).remove(instr.first);
    } else {
      SPDLOG_ERROR("Unexpected case for Day15_Utils::Operation");
    }
  }

  int total_focal_power = 0;
  for (size_t idx = 0; idx < lens_boxes.size(); ++idx) {
    total_focal_power += (idx + 1) * lens_boxes[idx].focal_power();
  }

  return std::to_string(total_focal_power);
}
