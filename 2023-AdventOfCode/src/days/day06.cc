#include "day06.h"

#include <algorithm>
#include <iostream>
#include <sstream>

#include "../framework/data_loader.hpp"
#include "../framework/solver_registry.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day06Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::istream& operator>>(std::istream& instr, Labeled_Vector& obj) {
  instr >> obj.label;
  obj.values = load_structured_data<unsigned long int>(instr);
  return instr;
}

Labeled_Vector from_line_string(const Line_String& lnstr) {
  std::istringstream istr(lnstr.ref());
  Labeled_Vector obj;
  istr >> obj;
  return obj;
}

bool exceeds_record(unsigned long long int hold_time,
                    unsigned long long int race_duration,
                    unsigned long long int record) {
  unsigned long int distance = hold_time * (race_duration - hold_time);
  return distance > record;
}

unsigned long int number_of_ways_record_is_beat(
    unsigned long int race_duration, unsigned long int record_distance) {
  unsigned long int result = 0;
  for (unsigned long int hold_time = 1; hold_time < race_duration;
       ++hold_time) {
    if (exceeds_record(hold_time, race_duration, record_distance)) {
      ++result;
    }
  }
  return result;
}

std::string Day06Part1::name() const { return "Day06-Part1"; }

std::string Day06Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  // https://stackoverflow.com/questions/56828562/unable-to-see-elements-of-stdvector-with-gcc-in-vs-code
  const Labeled_Vector& times = from_line_string(data[0]);
  const Labeled_Vector& distances = from_line_string(data[1]);

  // distance = num_held * (time - num_held);
  // I think only have to go half way based at looking on output

  // SPDLOG_ERROR("Times: {}", fmt::join(times.values, " "));
  unsigned long int distances_multiplied = 1;

  for (auto idx = 0; idx < times.values.size(); ++idx) {
    const unsigned long int race_time = times.values[idx];
    const unsigned long int record_distance = distances.values[idx];
    distances_multiplied *=
        number_of_ways_record_is_beat(race_time, record_distance);
  }

  return std::to_string(distances_multiplied);
}

std::string Day06Part2::name() const { return "Day06-Part2"; }

// Properties notied looking at input. Halfway pounsigned long int is max. each
// increases There has to be a mathy reason, but looks like it holds So only
// need to do half way and double and can binary search to see where the value
// first crosses the record and count until half way pounsigned long int.
std::string Day06Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  // TODO: use raw numbers, update input parser later
  const unsigned long int race_duration = 61709066;  // 71530;
  const unsigned long int record = 643118413621041;  // 940200;

  // Use binary search (L/R/midpoint) algorithm to test time values in the range
  // 1 race_duration
  unsigned long left = 1;
  unsigned long right = race_duration;
  unsigned long hold_time = 0;
  unsigned long distance = 0;
  while (left <= right) {
    hold_time = (left + right) / 2;
    distance = hold_time * (race_duration - hold_time);

    // SPDLOG_ERROR("hold time: {} distance: {}  record: {}  left: {}  right:
    // {}",
    //  hold_time, distance, record, left, right);
    if (distance < record) {
      left = hold_time + 1;
    } else if (distance > record) {
      right = hold_time - 1;
    }
  }

  // Number from the first hold time to the midpoint. And double it
  // but substract 1 if race_duration is event (no even mid point)
  int count = (race_duration / 2 - hold_time) * 2;
  if (race_duration % 2 == 0) {  // or use & 0x1
    --count;
  }

  return std::to_string(count);
}
