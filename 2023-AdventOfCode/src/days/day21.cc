#include "day21.h"

#include "../framework/solver_registry.h"
#include "../types/day21_utils.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day21Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day21Part1::name() const { return "Day21-Part1"; }

std::string Day21Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  const int ITERATIONS = 64;

  std::vector<std::string> lines(data.size());
  std::transform(data.begin(), data.end(), lines.begin(),
                 [](const auto& val) -> std::string { return val.get(); });

  auto sut = Day21_Utils::build_iteration_grid(lines);

  Day21_Utils::walk_iterations(sut, ITERATIONS);

  unsigned int count = Day21_Utils::determine_possible_destination_at_step(
      sut.dist_grid, ITERATIONS);

  return std::to_string(count);
}

std::string Day21Part2::name() const { return "Day21-Part2"; }

std::string Day21Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());
  return "Unimplemented";
}
