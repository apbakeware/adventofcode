#include "day13.h"

#include "../framework/solver_registry.h"
#include "../utils/grid/grid_utils.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day13Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day13Part1::name() const { return "Day13-Part1"; }

std::string Day13Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  std::vector<std::string> grid_lines;
  int score = 0;
  for (const auto& line : data) {
    if (line.ref().empty()) {
      auto grid = create_char_grid_from_string_rows(grid_lines.begin(),
                                                    grid_lines.end());

      score += Day13_Utils::evaluate_part1_reflection_score(grid);

      grid_lines.clear();
    } else {
      grid_lines.push_back(line.ref());
    }
  }

  auto grid =
      create_char_grid_from_string_rows(grid_lines.begin(), grid_lines.end());
  score += Day13_Utils::evaluate_part1_reflection_score(grid);

  return std::to_string(score);
}

std::string Day13Part2::name() const { return "Day13-Part2"; }

std::string Day13Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());
  std::vector<std::string> grid_lines;
  int score = 0;
  for (const auto& line : data) {
    if (line.ref().empty()) {
      auto grid = create_char_grid_from_string_rows(grid_lines.begin(),
                                                    grid_lines.end());

      int grid_score = Day13_Utils::evaluate_part2_reflection_score(grid);
      if (grid_score == -1) {
        std::cout << "Could not find new LOR\n" << grid << "\n";
        assert(false);
      }
      score += grid_score;
      grid_lines.clear();
    } else {
      grid_lines.push_back(line.ref());
    }
  }

  auto grid =
      create_char_grid_from_string_rows(grid_lines.begin(), grid_lines.end());
  score += Day13_Utils::evaluate_part2_reflection_score(grid);

  return std::to_string(score);
}
