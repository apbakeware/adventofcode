#ifndef __DAY18_H__
#define __DAY18_H__

#include <string>
#include <vector>

#include "../types/day18_utils.h"

struct Day18Data {
  typedef Day18_Utils::Dig_Instructions Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day18Part1 : public Day18Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day18Part2 : public Day18Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day18Spec {
  using Data_Spec = Day18Data;
  using Part_1 = Day18Part1;
  using Part_2 = Day18Part2;
  static constexpr const char* day() { return "Day18"; }
  static constexpr const char* data_file_name() { return "data/day18.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day18.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() { return "62"; }
  static constexpr const char* expected_sample_part_2_result() {
    return "UNIMPLEMENTED";
  }
  static constexpr const char* solved_part_1_result() { return "40131"; }
  static constexpr const char* solved_part_2_result() { return "undefined"; }
};

#endif  // __DAY18_H__
