#ifndef __DAY13_H__
#define __DAY13_H__

#include <string>
#include <vector>

#include "../framework/line_string.h"
#include "../types/day13_utils.h"
#include "../utils/grid/grid.h"

struct Day13Data {
  typedef Line_String Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day13Part1 : public Day13Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day13Part2 : public Day13Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day13Spec {
  using Data_Spec = Day13Data;
  using Part_1 = Day13Part1;
  using Part_2 = Day13Part2;
  static constexpr const char* day() { return "Day13"; }
  static constexpr const char* data_file_name() { return "data/day13.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day13.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() { return "405"; }
  static constexpr const char* expected_sample_part_2_result() { return "400"; }
  static constexpr const char* solved_part_1_result() { return "28895"; }
  static constexpr const char* solved_part_2_result() { return "31603"; }
};

#endif  // __DAY13_H__
