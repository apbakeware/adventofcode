#ifndef __DAY02_H__
#define __DAY02_H__

#include <string>
#include <vector>

#include "../framework/line_string.h"

struct Day02Data {
  typedef Line_String Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day02Part1 : public Day02Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day02Part2 : public Day02Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day02Spec {
  using Data_Spec = Day02Data;
  using Part_1 = Day02Part1;
  using Part_2 = Day02Part2;
  static constexpr const char* day() { return "Day02"; }
  static constexpr const char* data_file_name() { return "data/day02.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day02.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() { return "8"; }
  static constexpr const char* expected_sample_part_2_result() {
    return "2286";
  }
  static constexpr const char* solved_part_1_result() { return "1931"; }
  static constexpr const char* solved_part_2_result() { return "83105"; }
};

#endif  // __DAY02_H__
