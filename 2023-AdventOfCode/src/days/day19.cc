#include "day19.h"

#include <iostream>
#include <sstream>

#include "../framework/solver_registry.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day19Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day19Part1::name() const { return "Day19-Part1"; }

std::string Day19Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  // TODO: not sure why loading of the type directily (for size 1)
  // doesnt work, but come back to it maybe....
  std::stringstream sstr;
  for (const auto& line : data) {
    sstr << line.get() << "\n";
  }

  Day19_Utils::Day19_Workflow_Runner runner;
  sstr >> runner;

  int accepted_sum = runner.execute();
  return std::to_string(accepted_sum);
}

std::string Day19Part2::name() const { return "Day19-Part2"; }

std::string Day19Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  // TODO: I think for this I need to find all the paths
  // to the Accept nodes. Then build up all the conditionals in the
  // path to the A.

  // handle sample lnx which has 2 paths through which are accept, essentiall a
  // no-op

  return "Unimplemented";
}
