#ifndef __DAY01_H__
#define __DAY01_H__

#include <string>
#include <vector>

struct Day01Data {
  typedef std::string Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day01Part1 : public Day01Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day01Part2 : public Day01Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day01Spec {
  using Data_Spec = Day01Data;
  using Part_1 = Day01Part1;
  using Part_2 = Day01Part2;
  static constexpr const char* day() { return "Day01"; }
  static constexpr const char* data_file_name() { return "data/day01.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day01.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() { return "142"; }
  static constexpr const char* expected_sample_part_2_result() {
    return "UNIMPLEMENTED";
  }
  static constexpr const char* solved_part_1_result() { return "56506"; }
  static constexpr const char* solved_part_2_result() { return "56017"; }
};

#endif  // __DAY01_H__
