#ifndef __DAY17_H__
#define __DAY17_H__

#include <string>
#include <vector>

struct Day17Data {
   // TODO: Set data type
   typedef int Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day17Part1 : public Day17Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day17Part2 : public Day17Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day17Spec {
   using Data_Spec = Day17Data;
   using Part_1 = Day17Part1;
   using Part_2 = Day17Part2;
   static constexpr const char * day() { return "Day17"; }
   static constexpr const char * data_file_name() { return "data/day17.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day17.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * expected_sample_part_2_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * solved_part_1_result() { return "undefined"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __DAY17_H__
