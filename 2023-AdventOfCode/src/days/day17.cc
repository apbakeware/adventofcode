#include "day17.h"
#include "spdlog/spdlog.h"
#include "../framework/solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day17Spec>();
}

const bool _was_registered = _do_registration();

}

std::string Day17Part1::name() const {
   return "Day17-Part1";
}

std::string Day17Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}


std::string Day17Part2::name() const {
   return "Day17-Part2";
}

std::string Day17Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}

