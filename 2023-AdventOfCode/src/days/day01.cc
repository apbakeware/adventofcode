#include "day01.h"

#include <cassert>
#include <numeric>
#include <string>
#include <vector>

#include "../framework/solver_registry.h"
#include "../utils/string_utils.hpp"
#include "spdlog/spdlog.h"
namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day01Spec>();
}

const bool _was_registered = _do_registration();

struct String_Int_Record {
  std::string string_repr;
  int value;
};

const std::vector<String_Int_Record> NUMBER_STRINGS{
    {"1", 1},     {"2", 2},     {"3", 3},    {"4", 4},    {"5", 5},
    {"6", 6},     {"7", 7},     {"8", 8},    {"9", 9},    {"one", 1},
    {"two", 2},   {"three", 3}, {"four", 4}, {"five", 5}, {"six", 6},
    {"seven", 7}, {"eight", 8}, {"nine", 9}};
}  // namespace

std::string Day01Part1::name() const { return "Day01-Part1"; }

std::string Day01Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  int sumTotal = 0;
  for (const auto& inStr : data) {
    const auto firstDigit = find_first_digit(inStr.begin(), inStr.end());
    const auto lastDigit = find_first_digit(inStr.rbegin(), inStr.rend());
    const std::string valStr{*firstDigit, *lastDigit};
    int value = std::stoi(valStr);
    sumTotal += value;
    SPDLOG_INFO("String: {}  First Digit: {}   Last Digit: {}  Vale: {}", inStr,
                *firstDigit, *lastDigit, valStr);
  }

  return std::to_string(sumTotal);
}

std::string Day01Part2::name() const { return "Day01-Part2"; }

std::string Day01Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());
  unsigned int sumTotal = 0;

  for (const auto& inStr : data) {
    size_t first_digit_pos = std::string::npos;
    size_t last_digit_pos = 0;
    int first_digit = 0;
    int second_digit = 0;

    // SPDLOG_ERROR("Base string: {}", inStr);
    for (const auto& srchRec : NUMBER_STRINGS) {
      const auto& positions =
          find_first_and_last_occurance(inStr, srchRec.string_repr);
      // SPDLOG_ERROR("  {:6<}   first: {}  last: {}", srchRec.string_repr,
      //              positions.first, positions.second);

      if (positions.first < first_digit_pos) {
        first_digit_pos = positions.first;
        first_digit = srchRec.value;
      }

      if (positions.second != std::string::npos &&
          positions.second >= last_digit_pos) {
        last_digit_pos = positions.second;
        second_digit = srchRec.value;
      }
    }

    unsigned int thisVal = first_digit * 10 + second_digit;
    sumTotal += thisVal;

    // SPDLOG_ERROR(
    //     "  in: {}   firstNumberLocation: {}   lastNumberLocation: {} num {}
    //     ", inStr, first_digit_pos, last_digit_pos, thisVal);
  }

  return std::to_string(sumTotal);
}
