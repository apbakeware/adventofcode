#ifndef __DAY08_H__
#define __DAY08_H__

#include <string>
#include <vector>

#include "../framework/line_string.h"

struct Day08Data {
  typedef Line_String Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day08Part1 : public Day08Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day08Part2 : public Day08Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day08Spec {
  using Data_Spec = Day08Data;
  using Part_1 = Day08Part1;
  using Part_2 = Day08Part2;
  static constexpr const char* day() { return "Day08"; }
  static constexpr const char* data_file_name() { return "data/day08.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day08.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() { return "6"; }
  static constexpr const char* expected_sample_part_2_result() {
    return "UNIMPLEMENTED";
  }
  static constexpr const char* solved_part_1_result() { return "15517"; }
  static constexpr const char* solved_part_2_result() { return "undefined"; }
};

#endif  // __DAY08_H__
