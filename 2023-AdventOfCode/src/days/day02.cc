#include "day02.h"

#include <sstream>

#include "../framework/solver_registry.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day02Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day02Part1::name() const { return "Day02-Part1"; }

// DONT NEED STRINGS, NEED WHOLE LINES
std::string Day02Part1::solve(const Input_Data_Collection& data) {
  const int MAX_CUBES_RED = 12;
  const int MAX_CUBES_GREEN = 13;
  const int MAX_CUBES_BLUE = 14;

  int sumation = 0;

  for (const auto& line : data) {
    int max_red = 0;
    int max_green = 0;
    int max_blue = 0;

    int game_id;
    int cnt_red = 0;
    int cnt_green = 0;
    int cnt_blue = 0;

    int parsed_val;
    std::string parsed_str;
    std::string parsed_discard;

    std::istringstream in_line(line.ref());
    in_line >> parsed_str >> game_id >> parsed_discard;  // "next is :"
    while (!in_line.eof()) {
      in_line >> parsed_val >> parsed_str;
      switch (parsed_str[0]) {
        case 'r':
          cnt_red += parsed_val;
          break;
        case 'g':
          cnt_green += parsed_val;
          break;
        case 'b':
          cnt_blue += parsed_val;
          break;
        default:
          assert(false);
      }

      if (parsed_str.back() == ';' || parsed_str.back() != ',') {
        max_red = std::max(max_red, cnt_red);
        max_green = std::max(max_green, cnt_green);
        max_blue = std::max(max_blue, cnt_blue);

        cnt_red = 0;
        cnt_green = 0;
        cnt_blue = 0;
      }
    }

    if (max_red <= MAX_CUBES_RED && max_green <= MAX_CUBES_GREEN &&
        max_blue <= MAX_CUBES_BLUE) {
      sumation += game_id;
    }
  }

  return std::to_string(sumation);
}

std::string Day02Part2::name() const { return "Day02-Part2"; }

std::string Day02Part2::solve(const Input_Data_Collection& data) {
  const std::vector<std::string> singleLine{
      "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"};

  int sumation = 0;

  for (const auto& line : data) {
    int max_red = 0;
    int max_green = 0;
    int max_blue = 0;

    int game_id;
    int cnt_red = 0;
    int cnt_green = 0;
    int cnt_blue = 0;

    int parsed_val;
    std::string parsed_str;
    std::string parsed_discard;

    std::istringstream in_line(line.ref());
    in_line >> parsed_str >> game_id >> parsed_discard;  // "next is :"

    while (!in_line.eof()) {
      in_line >> parsed_val >> parsed_str;
      switch (parsed_str[0]) {
        case 'r':
          cnt_red += parsed_val;
          break;
        case 'g':
          cnt_green += parsed_val;
          break;
        case 'b':
          cnt_blue += parsed_val;
          break;
        default:
          assert(false);
      }

      if (parsed_str.back() == ';' || parsed_str.back() != ',') {
        max_red = std::max(max_red, cnt_red);
        max_green = std::max(max_green, cnt_green);
        max_blue = std::max(max_blue, cnt_blue);

        cnt_red = 0;
        cnt_green = 0;
        cnt_blue = 0;
      }
    }

    sumation += max_red * max_green * max_blue;
  }

  return std::to_string(sumation);
}
