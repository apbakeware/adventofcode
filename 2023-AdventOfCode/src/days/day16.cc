#include "day16.h"

#include "../framework/solver_registry.h"
#include "../types/day16_utils.h"
#include "spdlog/spdlog.h"

namespace {

bool _do_registration() {
  return Solver_Registry::register_solver<Day16Spec>();
}

const bool _was_registered = _do_registration();

}  // namespace

std::string Day16Part1::name() const { return "Day16-Part1"; }

std::string Day16Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());
  const auto& grid = Day16_Utils::create_from_input_data(data);
  const auto upper_left = top_of_column(grid, 0);
  const Day16_Utils::Grid_Mover initial_movement{
      upper_left.first, Day16_Utils::Movement_Direction::RIGHT, true};

  Day16_Utils::Movement_Grid_Type movement_grid(grid.number_of_rows(),
                                                grid.number_of_cols());
  int energized_count =
      Day16_Utils::do_beam_walking(grid, initial_movement, movement_grid);

  return std::to_string(energized_count);
}

std::string Day16Part2::name() const { return "Day16-Part2"; }

std::string Day16Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());
  int max_energized_count = 0;

  const auto& grid = Day16_Utils::create_from_input_data(data);
  for (ssize_t row = 0; row < grid.number_of_rows(); ++row) {
    const Grid_Location starting_point_l{row, 0};
    const Grid_Location starting_point_r{row, grid.number_of_cols() - 1};

    const Day16_Utils::Grid_Mover initial_movement_l{
        starting_point_l, Day16_Utils::Movement_Direction::RIGHT, true};

    const Day16_Utils::Grid_Mover initial_movement_r{
        starting_point_r, Day16_Utils::Movement_Direction::RIGHT, true};

    Day16_Utils::Movement_Grid_Type movement_grid(grid.number_of_rows(),
                                                  grid.number_of_cols());
    max_energized_count = std::max(
        max_energized_count,
        Day16_Utils::do_beam_walking(grid, initial_movement_l, movement_grid));

    movement_grid = Day16_Utils::Movement_Grid_Type(grid.number_of_rows(),
                                                    grid.number_of_cols());
    max_energized_count = std::max(
        max_energized_count,
        Day16_Utils::do_beam_walking(grid, initial_movement_r, movement_grid));
  }

  for (ssize_t col = 0; col < grid.number_of_cols(); ++col) {
    const Grid_Location starting_point_t{0, col};
    const Grid_Location starting_point_b{grid.number_of_rows() - 1, col};

    const Day16_Utils::Grid_Mover initial_movement_t{
        starting_point_t, Day16_Utils::Movement_Direction::RIGHT, true};

    const Day16_Utils::Grid_Mover initial_movement_b{
        starting_point_b, Day16_Utils::Movement_Direction::RIGHT, true};

    Day16_Utils::Movement_Grid_Type movement_grid(grid.number_of_rows(),
                                                  grid.number_of_cols());
    max_energized_count = std::max(
        max_energized_count,
        Day16_Utils::do_beam_walking(grid, initial_movement_t, movement_grid));

    movement_grid = Day16_Utils::Movement_Grid_Type(grid.number_of_rows(),
                                                    grid.number_of_cols());
    max_energized_count = std::max(
        max_energized_count,
        Day16_Utils::do_beam_walking(grid, initial_movement_b, movement_grid));
  }
  return std::to_string(max_energized_count);
}
