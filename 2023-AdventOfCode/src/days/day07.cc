#include "day07.h"

#include <limits>
#include <vector>

#include "../framework/solver_registry.h"
#include "spdlog/spdlog.h"

namespace {

// higer rank, lower number to srot highest rank first
bool _do_registration() {
  return Solver_Registry::register_solver<Day07Spec>();
}

const bool _was_registered = _do_registration();

const std::vector<int> _build_card_power() {
  std::vector<int> card_powers(std::numeric_limits<char>::max(), 0);
  card_powers['A'] = 0;
  card_powers['K'] = 1;
  card_powers['Q'] = 2;
  card_powers['J'] = 3;
  card_powers['T'] = 4;
  card_powers['9'] = 5;
  card_powers['8'] = 6;
  card_powers['7'] = 7;
  card_powers['6'] = 8;
  card_powers['5'] = 9;
  card_powers['4'] = 10;
  card_powers['3'] = 11;
  card_powers['2'] = 12;
  return card_powers;
}

// Value configured so "higher" ranked card is lower value to
// sort in ascending order by power by default
const std::vector<int> _build_card_power_j_as_joker() {
  std::vector<int> card_powers(std::numeric_limits<char>::max(), 0);
  card_powers['A'] = 0;
  card_powers['K'] = 1;
  card_powers['Q'] = 2;
  card_powers['T'] = 4;
  card_powers['9'] = 5;
  card_powers['8'] = 6;
  card_powers['7'] = 7;
  card_powers['6'] = 8;
  card_powers['5'] = 9;
  card_powers['4'] = 10;
  card_powers['3'] = 11;
  card_powers['2'] = 12;
  card_powers['J'] = 13;
  return card_powers;
}

static const std::vector<int> card_power = _build_card_power();
static const std::vector<int> card_power_with_wilds =
    _build_card_power_j_as_joker();

bool sort_hands_by_rank_then_cards(const Camel_Poker_Hand& lhand,
                                   const Camel_Poker_Hand& rhand) {
  const auto& lcards = lhand.cards;
  const auto& rcards = rhand.cards;

  return std::tie(lhand.hand_rank, card_power[lcards[0]], card_power[lcards[1]],
                  card_power[lcards[2]], card_power[lcards[3]],
                  card_power[lcards[4]]) <
         std::tie(rhand.hand_rank, card_power[rcards[0]], card_power[rcards[1]],
                  card_power[rcards[2]], card_power[rcards[3]],
                  card_power[rcards[4]]);
}

bool sort_hands_by_rank_then_cards_with_wilds(const Camel_Poker_Hand& lhand,
                                              const Camel_Poker_Hand& rhand) {
  const auto& lcards = lhand.cards;
  const auto& rcards = rhand.cards;

  return std::tie(lhand.hand_rank, card_power_with_wilds[lcards[0]],
                  card_power_with_wilds[lcards[1]],
                  card_power_with_wilds[lcards[2]],
                  card_power_with_wilds[lcards[3]],
                  card_power_with_wilds[lcards[4]]) <
         std::tie(rhand.hand_rank, card_power_with_wilds[rcards[0]],
                  card_power_with_wilds[rcards[1]],
                  card_power_with_wilds[rcards[2]],
                  card_power_with_wilds[rcards[3]],
                  card_power_with_wilds[rcards[4]]);
}

}  // namespace

std::string Day07Part1::name() const { return "Day07-Part1"; }

std::string Day07Part1::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  // TO manipulate
  Input_Data_Collection hands(data);
  for (auto& card : hands) {
    card.rank_hand<Day07_Part_1_Rank_Policy>();
    // SPDLOG_ERROR("Hand: {}  Rank: {}", card.cards, card.hand_rank);
  }

  // SPDLOG_ERROR("=================== SORTING ==================");

  std::sort(hands.begin(), hands.end(), sort_hands_by_rank_then_cards);

  // SPDLOG_ERROR("~~~~~~~~~~~~~~~~~~~~~~~~~~ RANKED SORT");
  // for (auto& card : hands) {
  //   SPDLOG_ERROR("Hand: {}  Rank: {}", card.cards, card.hand_rank);
  // }

  // SPDLOG_ERROR("~~~~~~~~~~~~~~~~~~~~~~~~~~ RANKED SORT");

  int bid_sum = 0;
  int power_rank = 1;
  for (int index = hands.size() - 1; index >= 0; --index) {
    const auto bid_x_rank = power_rank * hands[index].bid;
    bid_sum += bid_x_rank;
    // SPDLOG_ERROR("Hand: {}  Hand Type: {}  Bid: {}  Power Rank: {} BxP: {}",
    //              hands[index].cards, hands[index].hand_rank,
    //              hands[index].bid, power_rank, bid_x_rank);

    ++power_rank;
  }

  return std::to_string(bid_sum);
}

std::string Day07Part2::name() const { return "Day07-Part2"; }

std::string Day07Part2::solve(const Input_Data_Collection& data) {
  SPDLOG_INFO("{} input size: {}", name(), data.size());

  // TO manipulate
  Input_Data_Collection hands(data);
  for (auto& card : hands) {
    card.rank_hand<Day07_Part_2_Rank_Policy>();
    // SPDLOG_ERROR("Hand: {}  Rank: {}", card.cards, card.hand_rank);
  }

  // SPDLOG_ERROR("=================== SORTING ==================");

  std::sort(hands.begin(), hands.end(),
            sort_hands_by_rank_then_cards_with_wilds);

  // SPDLOG_ERROR("~~~~~~~~~~~~~~~~~~~~~~~~~~ RANKED SORT");
  // for (auto& card : hands) {
  //   SPDLOG_ERROR("Hand: {}  Rank: {}", card.cards, card.hand_rank);
  // }

  // SPDLOG_ERROR("~~~~~~~~~~~~~~~~~~~~~~~~~~ RANKED SORT");

  int bid_sum = 0;
  int power_rank = 1;
  for (int index = hands.size() - 1; index >= 0; --index) {
    const auto bid_x_rank = power_rank * hands[index].bid;
    bid_sum += bid_x_rank;
    // SPDLOG_ERROR(
    //     "Hand: {}  Hand Type: {}  Bid: {:>4}  Power Rank: {:>4} BxP: {:>6}",
    //     hands[index].cards, hands[index].hand_rank, hands[index].bid,
    //     power_rank, bid_x_rank);

    ++power_rank;
  }

  return std::to_string(bid_sum);
}
