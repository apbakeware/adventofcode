#include "day05.h"

#include <cassert>
#include <sstream>
#include <utility>

#include "../framework/data_loader.hpp"
#include "../framework/solver_registry.h"
#include "spdlog/spdlog.h"

namespace {

using Mapping_Value_Type = long long int;

bool _do_registration() {
  return Solver_Registry::register_solver<Day05Spec>();
}

const bool _was_registered = _do_registration();

class Map_Node {
 public:
  Map_Node() : _min(0), _max(0), _map_offset(0) {}

  Map_Node(Mapping_Value_Type range_min, Mapping_Value_Type range_count,
           Mapping_Value_Type map_offset)
      : _min(range_min),
        _max(range_min + range_count - 1),
        _map_offset(map_offset) {}

  bool in_range(Mapping_Value_Type value) const {
    return value >= _min && value <= _max;
  }

  Mapping_Value_Type map_value(Mapping_Value_Type value) const {
    return value + _map_offset;
  }

  // Check if its in the range for mapping from the destination to the source
  bool in_range_reverse(Mapping_Value_Type value) const {
    return value >= (_min + _map_offset) && value <= (_max + _map_offset);
  }

  Mapping_Value_Type map_value_reverse(Mapping_Value_Type value) const {
    return value - _map_offset;
  }

  Mapping_Value_Type map_from_min() const { return _min; }

  Mapping_Value_Type map_to_min() const { return _min + _map_offset; }

  friend std::istream& operator>>(std::istream& instr, Map_Node& obj);

  // sort by ascending _min source range values
  friend bool operator<(const Map_Node& lhs, const Map_Node& rhs);

  // operator less?

 private:
  // The lower bound in the mapping region.
  Mapping_Value_Type _min;
  // The upper bound in the mapping region
  Mapping_Value_Type _max;
  // Mapping offset
  Mapping_Value_Type _map_offset;
};

std::istream& operator>>(std::istream& instr, Map_Node& obj) {
  Mapping_Value_Type src_min = 0;
  Mapping_Value_Type map_min = 0;
  Mapping_Value_Type range_size = 0;
  instr >> map_min >> src_min >> range_size;

  obj = Map_Node(src_min, range_size, map_min - src_min);
  return instr;
}

bool operator<(const Map_Node& lhs, const Map_Node& rhs) {
  return lhs._min < rhs._min;
}

struct Seed_Range {
  Mapping_Value_Type min;
  Mapping_Value_Type max;

  bool in_range(Mapping_Value_Type value) const {
    return value >= min && value <= max;
  }
};

std::istream& operator>>(std::istream& instr, Seed_Range& obj) {
  Mapping_Value_Type min = 0;
  Mapping_Value_Type count = 0;

  instr >> min >> count;
  obj.min = min;
  obj.max = min + count - 1;
  return instr;
}

// Is sorted in ascending order
Day05Data::Input_Data_Collection::const_iterator extract_mapping_list(
    Day05Data::Input_Data_Collection::const_iterator begin,
    Day05Data::Input_Data_Collection::const_iterator end,
    std::vector<Map_Node>& vec) {
  if (begin == end) return begin;

  while (begin->get().empty()) {
    // SPDLOG_ERROR("skipping empyt line");
    ++begin;
    if (begin == end) return begin;
  }

  // Skip the label line:
  // SPDLOG_ERROR(" --> skipping label: {}", begin->get());
  ++begin;
  while (begin != end && !(begin->get().empty())) {
    // SPDLOG_ERROR("Parse map line: {}", begin->get());
    std::istringstream istr(begin->get());
    Map_Node node;
    istr >> node;
    vec.push_back(node);
    ++begin;
  }

  std::sort(vec.begin(), vec.end());

  return begin;
}

Mapping_Value_Type map_value(Mapping_Value_Type value,
                             std::vector<Map_Node>& mapping_table) {
  for (const auto& tbl : mapping_table) {
    if (tbl.in_range(value)) {
      // SPDLOG_ERROR("Value: {} was in table", value);
      return tbl.map_value(value);
    }
  }

  // SPDLOG_ERROR("Value: {} not in table", value);
  return value;
}

// Mapping table list sorted
Mapping_Value_Type map_value_reverse(Mapping_Value_Type value,
                                     std::vector<Map_Node>& mapping_table) {
  for (const auto& tbl : mapping_table) {
    if (tbl.in_range_reverse(value)) {
      // SPDLOG_ERROR("Value: {} was in table", value);
      return tbl.map_value_reverse(value);
    }
    // else if (value < tbl.map_to_min()) {
    //   return value;  // because its sorted if we exceed the min nothing else
    //  will pass
  }

  // SPDLOG_ERROR("Value: {} not in table", value);
  return value;
}

}  // namespace

std::string Day05Part1::name() const { return "Day05-Part1"; }

std::string Day05Part1::solve(const Input_Data_Collection& data) {
  // SPDLOG_INFO("{} input size: {}", name(), data.size());

  Mapping_Value_Type low_position =
      std::numeric_limits<Mapping_Value_Type>::max();

  std::vector<Mapping_Value_Type> seeds;
  std::vector<Map_Node> seed_to_soil;
  std::vector<Map_Node> soil_to_fertilizer;
  std::vector<Map_Node> fertilizer_to_water;
  std::vector<Map_Node> water_to_light;
  std::vector<Map_Node> light_to_temperature;
  std::vector<Map_Node> temperature_to_humidity;
  std::vector<Map_Node> humidity_to_location;

  std::string working_parse;
  auto data_iter = data.begin();
  std::istringstream instr(data_iter->get());
  ++data_iter;

  instr >> working_parse;
  seeds = load_structured_data<Mapping_Value_Type>(instr);

  // SPDLOG_ERROR("SEEDS: {}", fmt::join(seeds, ", "));

  data_iter = extract_mapping_list(data_iter, data.end(), seed_to_soil);
  data_iter = extract_mapping_list(data_iter, data.end(), soil_to_fertilizer);
  data_iter = extract_mapping_list(data_iter, data.end(), fertilizer_to_water);
  data_iter = extract_mapping_list(data_iter, data.end(), water_to_light);
  data_iter = extract_mapping_list(data_iter, data.end(), light_to_temperature);
  data_iter =
      extract_mapping_list(data_iter, data.end(), temperature_to_humidity);
  data_iter = extract_mapping_list(data_iter, data.end(), humidity_to_location);

  for (auto seed : seeds) {
    Mapping_Value_Type val = map_value(seed, seed_to_soil);
    // SPDLOG_ERROR("seed_to_soil: {}", val);

    val = map_value(val, soil_to_fertilizer);
    // SPDLOG_ERROR("soil_to_fertilizer: {}", val);

    val = map_value(val, fertilizer_to_water);
    // SPDLOG_ERROR("fertilizer_to_water: {}", val);

    val = map_value(val, water_to_light);
    // SPDLOG_ERROR("water_to_light: {}", val);

    val = map_value(val, light_to_temperature);
    // SPDLOG_ERROR("light_to_temperature: {}", val);

    val = map_value(val, temperature_to_humidity);
    // SPDLOG_ERROR("temperature_to_humidity: {}", val);

    val = map_value(val, humidity_to_location);
    // SPDLOG_ERROR("humidity_to_location: {}", val);

    low_position = std::min(low_position, val);
  }

  return std::to_string(low_position);
}

std::string Day05Part2::name() const { return "Day05-Part2"; }

// Start at end location 0 and work backwards.
// only will work if the solution is in a lower-ish range. worst
// case not an optimiaztion
std::string Day05Part2::solve(const Input_Data_Collection& data) {
  // SPDLOG_INFO("{} input size: {}", name(), data.size());

  std::vector<Map_Node> seed_to_soil;
  std::vector<Map_Node> soil_to_fertilizer;
  std::vector<Map_Node> fertilizer_to_water;
  std::vector<Map_Node> water_to_light;
  std::vector<Map_Node> light_to_temperature;
  std::vector<Map_Node> temperature_to_humidity;
  std::vector<Map_Node> humidity_to_location;

  std::string working_parse;
  auto data_iter = data.begin();
  std::istringstream instr(data_iter->get());
  ++data_iter;

  instr >> working_parse;
  auto seeds = load_structured_data<Seed_Range>(instr);
  std::sort(seeds.begin(), seeds.end(),
            [](const auto& lhs, const auto& rhs) { return lhs.min < rhs.min; });

  data_iter = extract_mapping_list(data_iter, data.end(), seed_to_soil);
  data_iter = extract_mapping_list(data_iter, data.end(), soil_to_fertilizer);
  data_iter = extract_mapping_list(data_iter, data.end(), fertilizer_to_water);
  data_iter = extract_mapping_list(data_iter, data.end(), water_to_light);
  data_iter = extract_mapping_list(data_iter, data.end(), light_to_temperature);
  data_iter =
      extract_mapping_list(data_iter, data.end(), temperature_to_humidity);
  data_iter = extract_mapping_list(data_iter, data.end(), humidity_to_location);

  Mapping_Value_Type location = 0;
  Mapping_Value_Type val = 0;
  bool keep_running = true;
  while (keep_running) {
    // if (location % 10000 == 0) {
    //   SPDLOG_ERROR("Location: {}", location);
    // }

    val = map_value_reverse(location, humidity_to_location);
    val = map_value_reverse(val, temperature_to_humidity);
    val = map_value_reverse(val, light_to_temperature);
    val = map_value_reverse(val, water_to_light);
    val = map_value_reverse(val, fertilizer_to_water);
    val = map_value_reverse(val, soil_to_fertilizer);
    val = map_value_reverse(val, seed_to_soil);

    // SPDLOG_ERROR("Location: {}  seed: {}", location, val);

    // if (val in seed range) break;
    for (const auto& seed_range : seeds) {
      if (seed_range.in_range(val)) {
        keep_running = false;
      } else if (val < seed_range.min) {
        break;  // skip the rest of the for..
      }
    }

    ++location;
  }

  return std::to_string(location - 1);
}

// Brute force...didnt return
// std::string Day05Part2::solve(const Input_Data_Collection& data) {
//   SPDLOG_INFO("{} input size: {}", name(), data.size());

//   typedef std::pair<Mapping_Value_Type, Mapping_Value_Type> Seed_Range_Type;

//   Mapping_Value_Type low_position =
//       std::numeric_limits<Mapping_Value_Type>::max();

//   std::vector<Map_Node> seed_to_soil;
//   std::vector<Map_Node> soil_to_fertilizer;
//   std::vector<Map_Node> fertilizer_to_water;
//   std::vector<Map_Node> water_to_light;
//   std::vector<Map_Node> light_to_temperature;
//   std::vector<Map_Node> temperature_to_humidity;
//   std::vector<Map_Node> humidity_to_location;

//   std::string working_parse;
//   auto data_iter = data.begin();
//   std::istringstream instr(data_iter->get());
//   ++data_iter;

//   instr >> working_parse;
//   const auto seeds = load_structured_data<Seed_Range>(instr);

//   // SPDLOG_ERROR("SEEDS: {}", fmt::join(seeds, ", "));

//   data_iter = extract_mapping_list(data_iter, data.end(), seed_to_soil);
//   data_iter = extract_mapping_list(data_iter, data.end(),
//   soil_to_fertilizer); data_iter = extract_mapping_list(data_iter,
//   data.end(), fertilizer_to_water); data_iter =
//   extract_mapping_list(data_iter, data.end(), water_to_light); data_iter =
//   extract_mapping_list(data_iter, data.end(), light_to_temperature);
//   data_iter =
//       extract_mapping_list(data_iter, data.end(), temperature_to_humidity);
//   data_iter = extract_mapping_list(data_iter, data.end(),
//   humidity_to_location);

//   for (auto& seed_range : seeds) {
//     SPDLOG_ERROR("Mix: {}  Max: {}", seed_range.min, seed_range.max);
//     for (Mapping_Value_Type seed_loc = seed_range.min;
//          seed_loc <= seed_range.max; ++seed_loc) {
//       Mapping_Value_Type val = map_value(seed_loc, seed_to_soil);
//       // SPDLOG_ERROR("seed_to_soil: {}", val);

//       val = map_value(val, soil_to_fertilizer);
//       // SPDLOG_ERROR("soil_to_fertilizer: {}", val);

//       val = map_value(val, fertilizer_to_water);
//       // SPDLOG_ERROR("fertilizer_to_water: {}", val);

//       val = map_value(val, water_to_light);
//       // SPDLOG_ERROR("water_to_light: {}", val);

//       val = map_value(val, light_to_temperature);
//       // SPDLOG_ERROR("light_to_temperature: {}", val);

//       val = map_value(val, temperature_to_humidity);
//       // SPDLOG_ERROR("temperature_to_humidity: {}", val);

//       val = map_value(val, humidity_to_location);
//       // SPDLOG_ERROR("humidity_to_location: {}", val);

//       low_position = std::min(low_position, val);
//     }
//   }

//   return "Unimplemented";
// }
