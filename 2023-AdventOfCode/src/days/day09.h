#ifndef __DAY09_H__
#define __DAY09_H__

#include <string>
#include <vector>

#include "../framework/line_string.h"

struct Day09Data {
  typedef Line_String Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day09Part1 : public Day09Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day09Part2 : public Day09Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day09Spec {
  using Data_Spec = Day09Data;
  using Part_1 = Day09Part1;
  using Part_2 = Day09Part2;
  static constexpr const char* day() { return "Day09"; }
  static constexpr const char* data_file_name() { return "data/day09.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day09.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() { return "114"; }
  static constexpr const char* expected_sample_part_2_result() { return "2"; }
  static constexpr const char* solved_part_1_result() { return "1938731307"; }
  static constexpr const char* solved_part_2_result() { return "948"; }
};

#endif  // __DAY09_H__
