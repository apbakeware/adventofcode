#ifndef __DAY14_H__
#define __DAY14_H__

#include <string>
#include <vector>

struct Day14Data {
  typedef std::string Input_Data_Type;
  typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day14Part1 : public Day14Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

class Day14Part2 : public Day14Data {
 public:
  std::string name() const;

  std::string solve(const Input_Data_Collection& data);
};

struct Day14Spec {
  using Data_Spec = Day14Data;
  using Part_1 = Day14Part1;
  using Part_2 = Day14Part2;
  static constexpr const char* day() { return "Day14"; }
  static constexpr const char* data_file_name() { return "data/day14.txt"; }
  static constexpr const char* sample_data_file_name() {
    return "data/day14.sample.txt";
  }
  static constexpr const char* expected_sample_part_1_result() { return "136"; }
  static constexpr const char* expected_sample_part_2_result() { return "64"; }
  static constexpr const char* solved_part_1_result() { return "105003"; }
  static constexpr const char* solved_part_2_result() { return "93742"; }
};

#endif  // __DAY14_H__
