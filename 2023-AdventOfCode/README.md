# 2023 - Advent Of Code

Back (ping/pong) to C++. But also Docker, VSCode and how to
use a more modern toolset!

## Directory Structure

The project directory structure is as follows:

```
|-- .vscode
|-- build.debug
|-- build.release
|-- build
|-- libs
|   |-- Catch2
|   |-- cxxopts
|   `-- spdlog
|-- src
|   |-- data
|   |-- days
|   |-- framework
|   |-- types
|   `-- utils
`-- test
    |-- days
    |-- types
    `-- utils
```
.vscode
  : Stores Visual Studio Code configuration files. Creates common development environment across machines.

build.debug
  : Build artifact directory for building Debug targets by hand or the VSCode tasks.

build.release
  : Build artifact directory for building Release targets by hand or the VSCode tasks.

build
  : Build artifact directory used by the VSCode CMake extension. Configured either as Debug or Release.

libs
  : Where 3rd party libraries are download (usually from GitHub). These are not git submodules, but downloaded code.

src
  : Top level directory of source code for solving Advent of Code daily problems.

src/data
  : Data files for daily input. Includes both sample and actual daily input.

src/days
  : Solution for each day. Each day has a structure for solving each part and a configuration structure.

src/framework
  : Source code for registering and running daily solutions by the main application.

src/types
  : Types used to solve daily problems. Not generally general purpose.

src/utils
  : General purpose types and utility classes (ex: graph, grid, etc). These common types are used as the basis for daily solution structures.

test
  : Top level directory for Catch2 unit test code. Directory structure mirrors the src/ directory.

test/types
  : Unit tests for the code in src/types.

test/utils
  : Unit tests for the code in src/utils.

## Binaries

Binaries are built using the CMake system. 

To support unit testing, the bulk of the src/ code is built into the `aoc_days`
static library. this library is linked into both the main application
and the unit test application. The structures in `src/days`, however,
use statics to auto-register with the application. However, when
built in a library, the linker drops these symbols as they aren't
pulled into the application so the daily solvers can't run. 

## Development 

Development was done in a Docker image to control the environment.

To build the Docker image in PowerShell:

```
docker build -f .\Dockerfile.dev . -t apbakeware/gcc-cmake
```

### Development - VSCode Dev Container plugin

Ensure that VSCode has the  _Visual Studio Code Dev Containers_
plugin installed. Open the 2023-AdventOfCode directory in VSCode
and the devcontainer.json file will build and run the development
container.

See https://code.visualstudio.com/docs/devcontainers/create-dev-container

* Start with Dev Containers: Add Dev Container Configuration Files... in the Command Palette (F1).
* Edit the contents of the .devcontainer folder as required.
* Try it with Dev Containers: Reopen in Container.
* If you see an error, select Open Folder Locally in the dialog that appears.
  * After the window reloads, a copy of the build log will appear in the console so you can investigate the problem. Edit the contents of the .devcontainer folder as required. (You can also use the Dev Containers: Show Container Log command to see the log again if you close it.)
* Run Dev Containers: Rebuild and Reopen in Container and jump to step 4 if needed.


### Development - Stand alone container

#### Launch Container

Run Docker container and mount attach VSCode to the conatiner.

```
# Launch the Docker image
docker run -d -it --name aoc2023 --mount type=bind,source=${PWD},target=/home/dev apbakeware/gcc-cmake
```

#### Attached VSCode

Launch VSCode. Make sure _Remote Development_ plugin is installed.

* F1 - Dev Containers - Attach to running container
* Select aoc2023
* Open Directory /home/dev


## Building

The application can be built by hand using the CMake scripts themselves or
the VSCode tasks which use the CMake scripts under the hood. The VSCode CMake
extension can also be used which magically uses the CMake files and can be
configured as to the toolchain and configuration.

### Terminal

To build manually from the terminal:

```
mkdir -p build.debug build.releas
 
# To build debug (w/ tests)
cmake ../ -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
make -j 4

# To build release
cmake ../ -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
make -j 4
```

### VS Code Tasks

To build using VS Code tasks, which uses the same terminal commands, use F1 -> Run Task.

First run the Configure task, then the build tasks. For example:

```
F1 -> Run Tasks -> Configure Debug
F1 -> Run Tasks -> Build Debug
```

### VS Code CMake Extension

To build with the CMake extension, click on the sidebar or 
use the F1 command pallette with CMake commands.

### Build Application

Build in an out-of-src build directory.

```
mkdir build
cd build
cmake ../
cmake --build src/ -j 4
# aoc application will be build in build/src
```
These need to be maintained with VS code tasks.

https://cmake.org/cmake/help/book/mastering-cmake/chapter/Testing%20With%20CMake%20and%20CTest.html

```
# Run tests (from project root)
( cd build/debug/test ; ./aoc_tests )
```

## Build Application Images

Build stand-alone application container with the C++ application.

_TBD_

Cmake plugin color output not working by 
default...

try: https://offlinemark.com/2023/02/05/how-to-enable-colored-compiler-output-with-cmake-vscode/


Learmning opportunity....figure out std::function and std::bind
without using lambda

## Reference links

https://stackoverflow.com/questions/20373466/nested-c-template-parameters-for-functions

## Code Examples

### Regular Expressions / Capture

From Day08:

```
static const std::regex MAP_LINE_REGEX{
    "^([A-Z]{3}) = .([A-Z]{3}), ([A-Z]{3}).*$"};

if (std::regex_search(line, regex_match, MAP_LINE_REGEX)) {
  // SPDLOG_ERROR("FOUND REGEX MATCH");
  // SPDLOG_ERROR("  Index: 1 {}", regex_match[1].str());  // 0 is whole
  // thing SPDLOG_ERROR("  Index: 2 {}", regex_match[2].str());
  // SPDLOG_ERROR("  Index: 3 {}", regex_match[3].str());
  auto node = graph.add(regex_match[1].str());
  node->at(0) = regex_match[2].str();
  node->at(1) = regex_match[3].str();
}

```