//#include "types/day16_utils.h"

#include "types/day18_utils.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <limits>
#include <queue>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "framework/data_loader.hpp"
#include "spdlog/spdlog.h"
#include "utils/grid/grid.h"
#include "utils/grid/grid_flood_fill.h"
#include "utils/grid/grid_traversal.h"
#include "utils/grid/grid_utils.h"

TEST_CASE("Test Day18_Utils", "[Day18_Utils]") {
  SECTION("Test dig single trench") {
    // 10x10 is sample size
    auto dig_site = Day18_Utils::create_dig_site(15);
    const Day18_Utils::Dig_Instructions instruction{'R', 6, "(#70c710)"};

    Day18_Utils::dig_trench(dig_site.dig_grid, instruction, dig_site.origin);
    std::cout << "Dig Site:\n" << dig_site.dig_grid << "\n";

    // TODO: need to figure out assertion
    // Likely create a local grid and compare the strings.
    // Check result locaiton....
  }

  SECTION("Test dig two trench") {
    // 10x10 is sample size
    auto dig_site = Day18_Utils::create_dig_site(15);
    const Day18_Utils::Dig_Instructions instruction{'R', 6, "DONT_CARE"};
    const Day18_Utils::Dig_Instructions instruction2{'D', 5, "DONT_CARE"};

    auto loc = dig_site.origin;
    loc = Day18_Utils::dig_trench(dig_site.dig_grid, instruction, loc);
    loc = Day18_Utils::dig_trench(dig_site.dig_grid, instruction2, loc);

    std::cout << "Dig Site:\n" << dig_site.dig_grid << "\n";
  }

  SECTION("Test dig_trench instructions") {
    auto dig_site = Day18_Utils::create_dig_site(15);
    const std::vector<Day18_Utils::Dig_Instructions> instructions{
        {'R', 6, "DONT_CARE"},
        {'D', 5, "DONT_CARE"},
        {'L', 2, "DONT_CARE"},
        {'U', 1, "DONT_CARE"}};

    Day18_Utils::dig_trench(dig_site, instructions);

    std::cout << "Dig Site:\n" << dig_site.dig_grid << "\n";
  }

  SECTION("Test dig_trench sample data") {
    auto dig_site = Day18_Utils::create_dig_site(25);
    const std::vector<Day18_Utils::Dig_Instructions> instructions{
        {'R', 6, "(#70c710)"}, {'D', 5, "(#0dc571)"}, {'L', 2, "(#5713f0)"},
        {'D', 2, "(#d2c081)"}, {'R', 2, "(#59c680)"}, {'D', 2, "(#411b91)"},
        {'L', 5, "(#8ceee2)"}, {'U', 2, "(#caa173)"}, {'L', 1, "(#1b58a2)"},
        {'U', 2, "(#caa171)"}, {'R', 2, "(#7807d2)"}, {'U', 3, "(#a77fa3)"},
        {'L', 2, "(#015232)"}, {'U', 2, "(#7a21e3)"}};

    Day18_Utils::dig_trench(dig_site, instructions);

    std::cout << "Dig Site:\n" << dig_site.dig_grid << "\n";

    // TODO: what is the assertion?
  }

  SECTION("Test dig_trench sample data flood fill") {
    auto dig_site = Day18_Utils::create_dig_site(25);
    const std::vector<Day18_Utils::Dig_Instructions> instructions{
        {'R', 6, "(#70c710)"}, {'D', 5, "(#0dc571)"}, {'L', 2, "(#5713f0)"},
        {'D', 2, "(#d2c081)"}, {'R', 2, "(#59c680)"}, {'D', 2, "(#411b91)"},
        {'L', 5, "(#8ceee2)"}, {'U', 2, "(#caa173)"}, {'L', 1, "(#1b58a2)"},
        {'U', 2, "(#caa171)"}, {'R', 2, "(#7807d2)"}, {'U', 3, "(#a77fa3)"},
        {'L', 2, "(#015232)"}, {'U', 2, "(#7a21e3)"}};

    Day18_Utils::dig_trench(dig_site, instructions);
    grid_flood_fill(dig_site.dig_grid, {0, 0}, '.', '*');

    std::cout << "Dig Site:\n" << dig_site.dig_grid << "\n";

    // TODO: what is the assertion?
  }

  SECTION("Test dig_trench sample data area") {
    auto dig_site = Day18_Utils::create_dig_site(25);
    const std::vector<Day18_Utils::Dig_Instructions> instructions{
        {'R', 6, "(#70c710)"}, {'D', 5, "(#0dc571)"}, {'L', 2, "(#5713f0)"},
        {'D', 2, "(#d2c081)"}, {'R', 2, "(#59c680)"}, {'D', 2, "(#411b91)"},
        {'L', 5, "(#8ceee2)"}, {'U', 2, "(#caa173)"}, {'L', 1, "(#1b58a2)"},
        {'U', 2, "(#caa171)"}, {'R', 2, "(#7807d2)"}, {'U', 3, "(#a77fa3)"},
        {'L', 2, "(#015232)"}, {'U', 2, "(#7a21e3)"}};

    Day18_Utils::dig_trench(dig_site, instructions);
    int filled = grid_flood_fill(dig_site.dig_grid, {0, 0}, '.', '*');
    int trench_area = dig_site.dig_grid.number_of_rows() *
                          dig_site.dig_grid.number_of_cols() -
                      filled;

    REQUIRE(trench_area == 62);
  }
}

TEST_CASE("Test Day18_Utils::find_dig_bounds_using_decoded_color_code",
          "[Day18_Utils]") {
  SECTION("Test single record sets all the values the same") {
    Day18_Utils::Dig_Instructions inst{'R', 6, "#70c710"};

    const auto& sut =
        Day18_Utils::find_dig_bounds_using_decoded_color_code({inst});

    std::cout << "Uppermost: " << sut.upper_most_location
              << "\nLeftmost:  " << sut.left_most_location
              << "\nLowermost: " << sut.lower_most_location
              << "\nRightmost: " << sut.right_most_location << "\n";

    const Grid_Location EXP{0, 461937};
    CHECK(EXP == sut.upper_most_location);
  }

  SECTION("Sample data set.bounds") {
    std::vector<Day18_Utils::Dig_Instructions> instructions{
        {'R', 6, "#70c710"}, {'D', 5, "#0dc571"}, {'L', 2, "#5713f0"},
        {'D', 2, "#d2c081"}, {'R', 2, "#59c680"}, {'D', 2, "#411b91"},
        {'L', 5, "#8ceee2"}, {'U', 2, "#caa173"}, {'L', 1, "#1b58a2"},
        {'U', 2, "#caa171"}, {'R', 2, "#7807d2"}, {'U', 3, "#a77fa3"},
        {'L', 2, "#015232"}, {'U', 2, "#7a21e3"}};
    const auto& sut =
        Day18_Utils::find_dig_bounds_using_decoded_color_code(instructions);

    std::cout << "Uppermost: " << sut.upper_most_location
              << "\nLeftmost:  " << sut.left_most_location
              << "\nLowermost: " << sut.lower_most_location
              << "\nRightmost: " << sut.right_most_location << "\n";

    // TODO: assertion?
  }

  SECTION("Sample data set.frame_of_reference") {
    std::vector<Day18_Utils::Dig_Instructions> instructions{
        {'R', 6, "#70c710"}, {'D', 5, "#0dc571"}, {'L', 2, "#5713f0"},
        {'D', 2, "#d2c081"}, {'R', 2, "#59c680"}, {'D', 2, "#411b91"},
        {'L', 5, "#8ceee2"}, {'U', 2, "#caa173"}, {'L', 1, "#1b58a2"},
        {'U', 2, "#caa171"}, {'R', 2, "#7807d2"}, {'U', 3, "#a77fa3"},
        {'L', 2, "#015232"}, {'U', 2, "#7a21e3"}};
    const auto& bounds =
        Day18_Utils::find_dig_bounds_using_decoded_color_code(instructions);

    const auto& sut = Day18_Utils::create_dig_site_reference(bounds);

    std::cout << "Dig_Site_Frame_Of_Reference"
              << "\n  origin_row_offset: " << sut.origin_row_offset
              << "\n  origin_col_offset: " << sut.origin_col_offset
              << "\n  grid_row_height:   " << sut.grid_row_height
              << "\n  grid_col_width:    " << sut.grid_col_width << "\n";
  }

  SECTION("Sample data with shifted dig frame of reference") {
    std::vector<Day18_Utils::Dig_Instructions> instructions{
        {'R', 6, "#70c710"}, {'D', 5, "#0dc571"}, {'L', 2, "#5713f0"},
        {'D', 2, "#d2c081"}, {'R', 2, "#59c680"}, {'D', 2, "#411b91"},
        {'L', 5, "#8ceee2"}, {'U', 2, "#caa173"}, {'L', 1, "#1b58a2"},
        {'U', 2, "#caa171"}, {'R', 2, "#7807d2"}, {'U', 3, "#a77fa3"},
        {'L', 2, "#015232"}, {'U', 2, "#7a21e3"}};

    const auto& bounds =
        Day18_Utils::find_dig_bounds_using_decoded_color_code(instructions);
    const auto& reference_frame =
        Day18_Utils::create_dig_site_reference(bounds);

    // size
    auto grid_size = std::max(reference_frame.grid_col_width,
                              reference_frame.grid_row_height);

    // TODO: This is too much memory, it dies.
    // CAN WE BREAK INTO 1000x1000 grids, but how to know what is inside
    // vs outside.

    // auto dig_site = Day18_Utils::create_dig_site(grid_size);

    // Day18_Utils::dig_trench(dig_site, instructions);
    // int filled = grid_flood_fill(dig_site.dig_grid, {0, 0}, '.', '*');
    // int trench_area = dig_site.dig_grid.number_of_rows() *
    //                       dig_site.dig_grid.number_of_cols() -
    //                   filled;

    // REQUIRE(trench_area == 62);
  }
}

TEST_CASE("Test Day18_Utils::Dig_Instructions", "[Day18_Utils]") {
  SECTION("Test parse color code") {
    const std::string LINE = "R 6 (#70c710)";
    std::istringstream instr(LINE);

    Day18_Utils::Dig_Instructions sut;

    instr >> sut;

    REQUIRE(sut.color_code == "#70c710");
  }

  SECTION("Test decode part 2 color code") {
    const std::string LINE = "R 6 (#70c710)";
    std::istringstream instr(LINE);
    Day18_Utils::Dig_Instructions sut;

    instr >> sut;

    const auto& decoded = Day18_Utils::decode_part2_color_string(sut);

    // R 461937
    CHECK('R' == decoded.first);
    CHECK(461937 == decoded.second);
  }
}

TEST_CASE("Test Day18_Utils::Part2_Vertical_Trenches_Method", "[Day18_Utils]") {
  // SECTION("find_vertical_trenches") {
  //   // Already parsed sampmle data into structures
  //   std::vector<Day18_Utils::Dig_Instructions> instructions{
  //       {'R', 6, "#70c710"}, {'D', 5, "#0dc571"}, {'L', 2, "#5713f0"},
  //       {'D', 2, "#d2c081"}, {'R', 2, "#59c680"}, {'D', 2, "#411b91"},
  //       {'L', 5, "#8ceee2"}, {'U', 2, "#caa173"}, {'L', 1, "#1b58a2"},
  //       {'U', 2, "#caa171"}, {'R', 2, "#7807d2"}, {'U', 3, "#a77fa3"},
  //       {'L', 2, "#015232"}, {'U', 2, "#7a21e3"}};

  //   auto vsegments = Day18_Utils::find_vertical_trenches(instructions);

  //   std::cout << "Initial trench finding\n";
  //   for (const auto& seg : vsegments) {
  //     std::cout << "VSegment: " << seg.start << "  " << seg.end << "\n";
  //   }

  //   Day18_Utils::sort_trenches(vsegments);
  //   std::cout << "Sorted trench finding\n";
  //   ssize_t min_row = std::numeric_limits<ssize_t>::max();
  //   ssize_t max_row = std::numeric_limits<ssize_t>::min();
  //   for (const auto& seg : vsegments) {
  //     std::cout << "VSegment: " << seg.start << "  " << seg.end << "\n";
  //     min_row = std::min(min_row, seg.start.row);
  //     max_row = std::max(max_row, seg.end.row);
  //   }

  //   std::cout << "Starting row: " << min_row << "  Max row: " << max_row
  //             << "\n";

  //   // TODO: add assertions, just manually looking at output now
  // }

  SECTION("vertical_segment_crosses_row") {
    Day18_Utils::Line_Segment sut{{5, 10}, {10, 10}};

    CHECK(vertical_segment_crosses_row(sut, 4) == false);
    CHECK(vertical_segment_crosses_row(sut, 5) == true);
    CHECK(vertical_segment_crosses_row(sut, 7) == true);
    CHECK(vertical_segment_crosses_row(sut, 10) == true);
    CHECK(vertical_segment_crosses_row(sut, 11) == false);
  }

  SECTION("test approach on part 1") {
    std::vector<Day18_Utils::Dig_Instructions> instructions{
        {'R', 6, "#70c710"}, {'D', 5, "#0dc571"}, {'L', 2, "#5713f0"},
        {'D', 2, "#d2c081"}, {'R', 2, "#59c680"}, {'D', 2, "#411b91"},
        {'L', 5, "#8ceee2"}, {'U', 2, "#caa173"}, {'L', 1, "#1b58a2"},
        {'U', 2, "#caa171"}, {'R', 2, "#7807d2"}, {'U', 3, "#a77fa3"},
        {'L', 2, "#015232"}, {'U', 2, "#7a21e3"}};

    auto standard_dist_dir =
        [](const Day18_Utils::Dig_Instructions& instr) -> std::pair<char, int> {
      return {instr.direction, instr.distance};
    };

    auto vsegments =
        Day18_Utils::find_vertical_trenches(instructions, standard_dist_dir);

    std::cout << "Initial trench finding  ------ test approach on part 1\n";
    for (const auto& seg : vsegments) {
      std::cout << "VSegment: " << seg.start << "  " << seg.end << "\n";
    }

    std::cout << "Sorting trenches\n";
    Day18_Utils::sort_trenches(vsegments);
    std::cout << "Combining loop\n";
    Day18_Utils::combine_close_loop(vsegments);

    // Close the loop if the last 2 segments need to be join to form 1 segment

    std::cout << "Sorted trench finding\n";
    ssize_t min_row = std::numeric_limits<ssize_t>::max();
    ssize_t max_row = std::numeric_limits<ssize_t>::min();
    for (const auto& seg : vsegments) {
      std::cout << "VSegment: " << seg.start << "  " << seg.end << "\n";
      min_row = std::min(min_row, seg.start.row);
      max_row = std::max(max_row, seg.end.row);
    }

    // todo:...current issue are the horizontal lines between segment 1 and 2

    std::cout << "Starting row: " << min_row << "  Max row: " << max_row
              << "\n";

    // TODO: add assertions, just manually looking at output now

    unsigned long long area = 0;
    for (ssize_t row = min_row; row <= max_row; ++row) {
      ssize_t col = -1;
      for (const auto& seg : vsegments) {
        if (Day18_Utils::vertical_segment_crosses_row(seg, row)) {
          if (col == -1) {
            col = seg.start.col;
          } else {
            unsigned long long this_area =
                seg.start.col - col + 1;  // +1 to in clude last col
            area += this_area;

            std::cout << "Adding area of row: " << row << "  start col: " << col
                      << " end col: " << seg.start.col
                      << "  this area: " << this_area
                      << "  total area: " << area << "\n";

            col = -1;
          }
        }
      }
    }

    std::cout << "Area: " << area << "\n";
    // REQUIRE(area == 62ULL);
  }
}