#include "types/day20_utils.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <memory>
#include <sstream>

#include "spdlog/spdlog.h"

using namespace Day20_Utils;
using namespace Catch::Matchers;

namespace {

const std::vector<std::string> SAMPLE_1_LINES{"broadcaster -> a, b, c",
                                              "%a -> b"
                                              "%b -> c"
                                              "%c -> in",
                                              "&inv -> a"};

const std::vector<std::string> SAMPLE_2_LINES{"broadcaster -> a",
                                              "%a -> inv, con", "&inv -> b",
                                              "%b -> con", "&con -> output"};

class Stub_Pulse_Generator : public Pulse_Generator {
 public:
  Stub_Pulse_Generator(const std::string& name)
      : Day20_Utils::Pulse_Generator(name) {}

  virtual bool handle_pulse(const Pulse_Event& event,
                            Pulse_Command_Q& in_out_q) override {
    return false;
  }
};

}  // namespace

TEST_CASE("Day20_Utils::Flip_Flop_Pulse_Generator", "[Day20_Utils]") {
  SECTION("Test no pulse generted on high") {
    Pulse_Command_Q pulse_q;
    auto sut = std::make_unique<Flip_Flop_Pulse_Generator>("SUT");
    sut->add_output_module("OUT-Module 1");

    sut->handle_pulse({"Dont Care", Pulse::HIGH}, pulse_q);

    REQUIRE(pulse_q.empty());
  }

  SECTION("Test no pulse generated on multiple high") {
    Pulse_Command_Q pulse_q;
    auto sut = std::make_unique<Flip_Flop_Pulse_Generator>("SUT");
    sut->add_output_module("OUT");

    sut->handle_pulse({"Dont Care", Pulse::HIGH}, pulse_q);
    sut->handle_pulse({"Dont Care", Pulse::HIGH}, pulse_q);
    sut->handle_pulse({"Dont Care", Pulse::HIGH}, pulse_q);
    sut->handle_pulse({"Dont Care", Pulse::HIGH}, pulse_q);
    sut->handle_pulse({"Dont Care", Pulse::HIGH}, pulse_q);

    REQUIRE(pulse_q.empty());
  }

  SECTION("Test first low pulse sends high") {
    Pulse_Command_Q pulse_q;
    auto sut = std::make_unique<Flip_Flop_Pulse_Generator>("SUT");
    sut->add_output_module("OUT");

    sut->handle_pulse({"Dont Care", Pulse::LOW}, pulse_q);

    REQUIRE(pulse_q.size() == 1);
    CHECK(pulse_q.front().destination_module == "OUT");
    CHECK(pulse_q.front().pulse_value == Pulse::HIGH);
  }

  SECTION("Test low pulses toggle sent pulse") {
    Pulse_Command_Q pulse_q;
    auto sut = std::make_unique<Flip_Flop_Pulse_Generator>("SUT");
    sut->add_output_module("O U T");

    sut->handle_pulse({"Dont Care", Pulse::LOW}, pulse_q);
    sut->handle_pulse({"Dont Care", Pulse::LOW}, pulse_q);

    REQUIRE(pulse_q.size() == 2);
    CHECK(pulse_q.front().destination_module == "O U T");
    CHECK(pulse_q.front().pulse_value == Pulse::HIGH);
    pulse_q.pop();
    CHECK(pulse_q.front().destination_module == "O U T");
    CHECK(pulse_q.front().pulse_value == Pulse::LOW);
  }

  SECTION("Test output to multiple modules") {
    Pulse_Command_Q pulse_q;
    auto sut = std::make_unique<Flip_Flop_Pulse_Generator>("SUT");
    sut->add_output_module("OUT - 1");
    sut->add_output_module("OUT - 2");
    sut->add_output_module("OUT - 3");

    sut->handle_pulse({"Dont Care", Pulse::LOW}, pulse_q);

    REQUIRE(pulse_q.size() == 3);
    CHECK(pulse_q.front().destination_module == "OUT - 1");
    CHECK(pulse_q.front().pulse_value == Pulse::HIGH);

    pulse_q.pop();
    CHECK(pulse_q.front().destination_module == "OUT - 2");
    CHECK(pulse_q.front().pulse_value == Pulse::HIGH);

    pulse_q.pop();
    CHECK(pulse_q.front().destination_module == "OUT - 3");
    CHECK(pulse_q.front().pulse_value == Pulse::HIGH);
  }
}

TEST_CASE("Day20_Utils::Conjunction_Pulse_Generator", "[Day20_Utils]") {
  SECTION("Test HIGH pulse generated on HIGH when inputs are not set") {
    Pulse_Command_Q pulse_q;
    Stub_Pulse_Generator in1("In-1");
    Stub_Pulse_Generator in2("In-2");
    Stub_Pulse_Generator out1("Out-1");
    auto sut = std::make_unique<Conjunction_Pulse_Generator>("SUT");

    sut->add_input_module(in1.name());
    sut->add_input_module(in2.name());
    sut->add_output_module(out1.name());

    sut->handle_pulse({in1.name(), Pulse::HIGH}, pulse_q);

    REQUIRE(pulse_q.size() == 1);
    CHECK(pulse_q.front().destination_module == out1.name());
    CHECK(pulse_q.front().pulse_value == Pulse::HIGH);
  }

  SECTION("Test HIGH pulse generated on HIGH when other input is HIGH") {
    Pulse_Command_Q pulse_q;
    Stub_Pulse_Generator in1("In-1");
    Stub_Pulse_Generator in2("In-2");
    Stub_Pulse_Generator out1("Out-1");
    auto sut = std::make_unique<Conjunction_Pulse_Generator>("SUT");

    sut->add_input_module(in1.name());
    sut->add_input_module(in2.name());
    sut->add_output_module(out1.name());

    sut->handle_pulse({in2.name(), Pulse::HIGH}, pulse_q);

    REQUIRE(pulse_q.size() == 1);
    CHECK(pulse_q.front().destination_module == out1.name());
    CHECK(pulse_q.front().pulse_value == Pulse::HIGH);
  }

  SECTION("Test LOW pulse generated on HIGH when 1 input is HIGH") {
    Pulse_Command_Q pulse_q;
    Stub_Pulse_Generator in1("In-1");
    Stub_Pulse_Generator in2("In-2");
    Stub_Pulse_Generator out1("Out-1");
    auto sut = std::make_unique<Conjunction_Pulse_Generator>("SUT");

    sut->add_input_module(in1.name());
    sut->add_input_module(in2.name());
    sut->add_output_module(out1.name());

    sut->handle_pulse({in1.name(), Pulse::HIGH}, pulse_q);
    sut->handle_pulse({in2.name(), Pulse::HIGH}, pulse_q);

    REQUIRE(pulse_q.size() == 2);
    CHECK(pulse_q.front().destination_module == out1.name());
    CHECK(pulse_q.front().pulse_value == Pulse::HIGH);

    pulse_q.pop();
    CHECK(pulse_q.front().destination_module == out1.name());
    CHECK(pulse_q.front().pulse_value == Pulse::LOW);
  }
}

TEST_CASE("Day20_Utils::Part 1", "[Day20_Utils]") {
  SECTION("Test basic part 1 manually constructed") {
    // broadcaster -> a, b, c
    // %a -> b
    // %b -> c
    // %c -> inv
    // &inv -> a

    Pulse_Command_Q pulse_q;
    auto broadcaster = Day20_Utils::Broadcast_Pulse_Generator();
    auto ff_a = Day20_Utils::Flip_Flop_Pulse_Generator("a");
    auto ff_b = Day20_Utils::Flip_Flop_Pulse_Generator("b");
    auto ff_c = Day20_Utils::Flip_Flop_Pulse_Generator("c");
    auto conj_inv = Day20_Utils::Conjunction_Pulse_Generator("inv");

    broadcaster.add_output_module(ff_a.name());
    broadcaster.add_output_module(ff_b.name());
    broadcaster.add_output_module(ff_c.name());
    ff_a.add_output_module(ff_b.name());
    ff_b.add_output_module(ff_c.name());
    ff_c.add_output_module(conj_inv.name());
    conj_inv.add_input_module(ff_c.name());
    conj_inv.add_output_module(ff_a.name());

    // TODO: make this a runner structure
    std::unordered_map<std::string, Pulse_Generator*> generator_map{
        {broadcaster.name(), &broadcaster},
        {ff_a.name(), &ff_a},
        {ff_b.name(), &ff_b},
        {ff_c.name(), &ff_c},
        {conj_inv.name(), &conj_inv},
    };

    broadcaster.handle_pulse({"button", Pulse::LOW}, pulse_q);
    int low_pulses = 1;
    int high_pulses = 0;
    while (!pulse_q.empty()) {
      auto generated_pulse = pulse_q.front();
      pulse_q.pop();
      auto next_generator =
          generator_map.find(generated_pulse.destination_module);
      if (next_generator == generator_map.end()) {
        SPDLOG_ERROR("Could not find generator '{}'",
                     generated_pulse.destination_module);
      }

      if (generated_pulse.pulse_value == Pulse::LOW) {
        ++low_pulses;
      } else {
        ++high_pulses;
      }

      const Pulse_Event event{generated_pulse.originating_module,
                              generated_pulse.pulse_value};
      next_generator->second->handle_pulse(event, pulse_q);
    }

    CHECK(low_pulses == 8);
    CHECK(high_pulses == 4);
  }

  SECTION("Test basic part 1 using Pulse_Generator_Runner") {
    Pulse_Generator_Runner runner;
    auto broadcaster = Day20_Utils::Broadcast_Pulse_Generator();
    auto ff_a = Day20_Utils::Flip_Flop_Pulse_Generator("a");
    auto ff_b = Day20_Utils::Flip_Flop_Pulse_Generator("b");
    auto ff_c = Day20_Utils::Flip_Flop_Pulse_Generator("c");
    auto conj_inv = Day20_Utils::Conjunction_Pulse_Generator("inv");

    broadcaster.add_output_module(ff_a.name());
    broadcaster.add_output_module(ff_b.name());
    broadcaster.add_output_module(ff_c.name());
    ff_a.add_output_module(ff_b.name());
    ff_b.add_output_module(ff_c.name());
    ff_c.add_output_module(conj_inv.name());
    conj_inv.add_input_module(ff_c.name());
    conj_inv.add_output_module(ff_a.name());

    runner.add_generator(broadcaster)
        .add_generator(ff_a)
        .add_generator(ff_b)
        .add_generator(ff_c)
        .add_generator(conj_inv);

    auto counts = runner.press_button();

    CHECK(counts.first == 8);
    CHECK(counts.second == 4);
  }

  SECTION("Test basic part 1 1000 button presses") {
    Pulse_Generator_Runner runner;
    auto broadcaster = Day20_Utils::Broadcast_Pulse_Generator();
    auto ff_a = Day20_Utils::Flip_Flop_Pulse_Generator("a");
    auto ff_b = Day20_Utils::Flip_Flop_Pulse_Generator("b");
    auto ff_c = Day20_Utils::Flip_Flop_Pulse_Generator("c");
    auto conj_inv = Day20_Utils::Conjunction_Pulse_Generator("inv");

    broadcaster.add_output_module(ff_a.name());
    broadcaster.add_output_module(ff_b.name());
    broadcaster.add_output_module(ff_c.name());
    ff_a.add_output_module(ff_b.name());
    ff_b.add_output_module(ff_c.name());
    ff_c.add_output_module(conj_inv.name());
    conj_inv.add_input_module(ff_c.name());
    conj_inv.add_output_module(ff_a.name());

    runner.add_generator(broadcaster)
        .add_generator(ff_a)
        .add_generator(ff_b)
        .add_generator(ff_c)
        .add_generator(conj_inv);

    std::pair<int, int> totals{0, 0};
    for (int i = 0; i < 1000; ++i) {
      auto counts = runner.press_button();
      totals.first += counts.first;
      totals.second += counts.second;
    }

    CHECK(totals.first == 8000);
    CHECK(totals.second == 4000);
  }

  SECTION("Test 2nd part 1 button 1 time") {
    // broadcaster -> a
    // %a -> inv, con
    // &inv -> b
    // %b -> con
    // &con -> output

    Pulse_Generator_Runner runner;
    auto broadcaster = Day20_Utils::Broadcast_Pulse_Generator();
    auto ff_a = Day20_Utils::Flip_Flop_Pulse_Generator("a");
    auto conj_inv = Day20_Utils::Conjunction_Pulse_Generator("inv");

    auto ff_b = Day20_Utils::Flip_Flop_Pulse_Generator("b");
    auto conj_con = Day20_Utils::Conjunction_Pulse_Generator("con");
    auto output = Day20_Utils::Termination_Pulse_Generator("output");

    // Output?

    broadcaster.add_output_module(ff_a.name());

    ff_a.add_output_module(conj_inv.name());
    ff_a.add_output_module(conj_con.name());
    conj_inv.add_output_module(ff_b.name());
    ff_b.add_output_module(conj_con.name());
    conj_con.add_output_module(output.name());

    conj_inv.add_input_module(ff_a.name());
    conj_con.add_input_module(ff_a.name());
    conj_con.add_input_module(ff_b.name());

    // Setup inputs
    runner.add_generator(broadcaster)
        .add_generator(ff_a)
        .add_generator(ff_b)
        .add_generator(conj_inv)
        .add_generator(conj_con)
        .add_generator(output);

    std::pair<int, int> totals{0, 0};
    for (int i = 0; i < 1000; ++i) {
      auto counts = runner.press_button();
      totals.first += counts.first;
      totals.second += counts.second;
    }

    CHECK(totals.first == 4250);
    CHECK(totals.second == 2750);
  }
}

TEST_CASE("Day20_Utils::parser", "[Day20_Utils]") {
  SECTION("Parse part 1 sample line 'broadcaster -> a, b, c'") {
    Day20_Utils::In_Out_Pairs io_pairs;
    std::unique_ptr<Pulse_Generator> sut(
        Day20_Utils::build_generator("broadcaster -> a, b, c", io_pairs));

    REQUIRE(sut.get() != nullptr);
    CHECK(sut->name() == "broadcaster");
    // REQUIRE_THAT(
    //     io_pairs,
    //     Equals(std::vector<std::string>{
    //         {"broadcast", "a"}, {"broadcast", "b"}, {"broadcast", "c"}}));
  }

  SECTION("Parse part 1 sample line '%a -> b'") {
    Day20_Utils::In_Out_Pairs io_pairs;
    std::unique_ptr<Pulse_Generator> sut(
        Day20_Utils::build_generator("%a -> b", io_pairs));

    REQUIRE(sut.get() != nullptr);
    CHECK(sut->name() == "a");
    // check the type
  }

  SECTION("Parse part 1 sample line '&inv -> a'") {
    Day20_Utils::In_Out_Pairs io_pairs;
    std::unique_ptr<Pulse_Generator> sut(
        Day20_Utils::build_generator("&inv -> a", io_pairs));

    REQUIRE(sut.get() != nullptr);
    CHECK(sut->name() == "inv");
    // check the type
  }
}

TEST_CASE("Day20_Utils build and run generators") {
  SECTION("Build and run") {
    std::vector<std::string> LINES{"broadcaster -> a, b, c", "%a -> b",
                                   "%b -> c", "%c -> inv", "&inv -> a"};

    Pulse_Generator_Runner runner;
    auto generators = build_generators(LINES);
    for (auto gen : generators) {
      runner.add_generator(*gen);
    }

    std::pair<int, int> totals{0, 0};
    for (int i = 0; i < 1000; ++i) {
      auto counts = runner.press_button();
      totals.first += counts.first;
      totals.second += counts.second;
    }

    CHECK(totals.first == 8000);
    CHECK(totals.second == 4000);
  }
}