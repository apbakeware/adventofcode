#include "types/day16_utils.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <queue>
#include <set>
#include <string>
#include <vector>

#include "spdlog/spdlog.h"
#include "utils/grid/grid.h"
#include "utils/grid/grid_utils.h"

namespace {

/*
 Sample data as listed in Day16 and processed by line
     .|...\....
     |.-.\.....
     .....|-...
     ........|.
     ..........
     .........\
     ..../.\\..
     .-.-/..|..
     .|....-|.\
     ..//.|....
*/

const std::vector<std::string> SAMPLE_DATA{
    ".|...\\....", "|.-.\\.....",  ".....|-...", "........|.",  "..........",
    ".........\\", "..../.\\\\..", ".-.-/..|..", ".|....-|.\\", "..//.|...."};

}  // namespace

TEST_CASE("Test Day16_Utils.do_movement_maintain_direction", "[Day16_Utils]") {
  SECTION("test movement right is on grid") {
    const bool EXP_IN_MOTION = true;
    const Grid_Location EXP_LOC{0, 1};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::RIGHT;

    const std::vector<std::string> GRID_SETUP{"...", "...", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {0, 0}, Day16_Utils::Movement_Direction::RIGHT, true};

    const auto mvmt =
        Day16_Utils::do_movement_maintain_direction(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION("test movement right is off grid") {
    const bool EXP_IN_MOTION = false;
    const Grid_Location EXP_LOC{0, 2};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::NONE;

    const std::vector<std::string> GRID_SETUP{"...", "...", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {0, 2}, Day16_Utils::Movement_Direction::RIGHT, true};

    const auto mvmt =
        Day16_Utils::do_movement_maintain_direction(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION("test movement up is on grid") {
    const bool EXP_IN_MOTION = true;
    const Grid_Location EXP_LOC{2, 1};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::UP;

    const std::vector<std::string> GRID_SETUP{"...", "...", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 1}, Day16_Utils::Movement_Direction::UP, true};

    const auto mvmt =
        Day16_Utils::do_movement_maintain_direction(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION("test movement up is off grid") {
    const bool EXP_IN_MOTION = false;
    const Grid_Location EXP_LOC{2, 1};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::NONE;

    const std::vector<std::string> GRID_SETUP{"...", "...", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {2, 1}, Day16_Utils::Movement_Direction::UP, true};

    const auto mvmt =
        Day16_Utils::do_movement_maintain_direction(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION("test movement left is on grid") {
    const bool EXP_IN_MOTION = true;
    const Grid_Location EXP_LOC{1, 0};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::LEFT;

    const std::vector<std::string> GRID_SETUP{"...", "...", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 1}, Day16_Utils::Movement_Direction::LEFT, true};

    const auto mvmt =
        Day16_Utils::do_movement_maintain_direction(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION("test movement left is off grid") {
    const bool EXP_IN_MOTION = false;
    const Grid_Location EXP_LOC{1, 0};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::NONE;

    const std::vector<std::string> GRID_SETUP{"...", "...", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 0}, Day16_Utils::Movement_Direction::LEFT, true};

    const auto mvmt =
        Day16_Utils::do_movement_maintain_direction(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION("test movement down is on grid") {
    const bool EXP_IN_MOTION = true;
    const Grid_Location EXP_LOC{0, 1};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::DOWN;

    const std::vector<std::string> GRID_SETUP{"...", "...", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 1}, Day16_Utils::Movement_Direction::DOWN, true};

    const auto mvmt =
        Day16_Utils::do_movement_maintain_direction(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION("test movement down is off grid") {
    const bool EXP_IN_MOTION = false;
    const Grid_Location EXP_LOC{0, 1};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::NONE;

    const std::vector<std::string> GRID_SETUP{"...", "...", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {0, 1}, Day16_Utils::Movement_Direction::DOWN, true};

    const auto mvmt =
        Day16_Utils::do_movement_maintain_direction(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION("test movement direction is none") {
    const bool EXP_IN_MOTION = false;
    const Grid_Location EXP_LOC{1, 1};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::NONE;

    const std::vector<std::string> GRID_SETUP{"...", "...", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 1}, Day16_Utils::Movement_Direction::NONE, true};

    const auto mvmt =
        Day16_Utils::do_movement_maintain_direction(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }
}

TEST_CASE("Test Day16_Utils.do_movement_horizontal_splitter", "[Day16_Utils]") {
  SECTION(
      "test movement right when on horizontal splitter location and right is "
      "on grid") {
    const bool EXP_IN_MOTION = true;
    const Grid_Location EXP_LOC{1, 2};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::RIGHT;

    const std::vector<std::string> GRID_SETUP{"...", ".-.", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 1}, Day16_Utils::Movement_Direction::RIGHT, true};

    const auto mvmt =
        Day16_Utils::do_movement_horizontal_splitter(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION(
      "test movement right when on horizontal splitter location and right is "
      "off grid") {
    const bool EXP_IN_MOTION = false;
    const Grid_Location EXP_LOC{1, 2};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::NONE;

    const std::vector<std::string> GRID_SETUP{"...", "..-", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 2}, Day16_Utils::Movement_Direction::RIGHT, true};

    const auto mvmt =
        Day16_Utils::do_movement_horizontal_splitter(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION(
      "test movement left when on horizontal splitter location and right is "
      "on grid") {
    const bool EXP_IN_MOTION = true;
    const Grid_Location EXP_LOC{1, 0};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::LEFT;

    const std::vector<std::string> GRID_SETUP{"...", ".-.", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 1}, Day16_Utils::Movement_Direction::LEFT, true};

    const auto mvmt =
        Day16_Utils::do_movement_horizontal_splitter(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION(
      "test movement left when on horizontal splitter location and right is "
      "off grid") {
    const bool EXP_IN_MOTION = false;
    const Grid_Location EXP_LOC{1, 0};
    const Day16_Utils::Movement_Direction EXP_DIR =
        Day16_Utils::Movement_Direction::NONE;

    const std::vector<std::string> GRID_SETUP{"...", "-..", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 0}, Day16_Utils::Movement_Direction::LEFT, true};

    const auto mvmt =
        Day16_Utils::do_movement_horizontal_splitter(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == EXP_LOC);
    CHECK(mvmt.front().direction == EXP_DIR);
    CHECK(mvmt.front().in_motion == EXP_IN_MOTION);
  }

  SECTION(
      "test movement up when on horizontal splitter location L/R are on grid") {
    const std::vector<std::string> GRID_SETUP{"...", ".-.", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 1}, Day16_Utils::Movement_Direction::UP, true};

    const auto mvmt =
        Day16_Utils::do_movement_horizontal_splitter(grid, initial_movement);

    REQUIRE(mvmt.size() == 2);
    CHECK(mvmt.front().location == Grid_Location::left({1, 1}));
    CHECK(mvmt.front().direction == Day16_Utils::Movement_Direction::LEFT);
    CHECK(mvmt.front().in_motion == true);

    CHECK(mvmt.back().location == Grid_Location::right({1, 1}));
    CHECK(mvmt.back().direction == Day16_Utils::Movement_Direction::RIGHT);
    CHECK(mvmt.back().in_motion == true);
  }

  SECTION("test movement up when on horizontal splitter location L on grid") {
    const std::vector<std::string> GRID_SETUP{"...", "..-", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 2}, Day16_Utils::Movement_Direction::UP, true};

    const auto mvmt =
        Day16_Utils::do_movement_horizontal_splitter(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == Grid_Location::left({1, 2}));
    CHECK(mvmt.front().direction == Day16_Utils::Movement_Direction::LEFT);
    CHECK(mvmt.front().in_motion == true);
  }

  SECTION("test movement up when on horizontal splitter location R on grid") {
    const std::vector<std::string> GRID_SETUP{"...", "-..", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 0}, Day16_Utils::Movement_Direction::UP, true};

    const auto mvmt =
        Day16_Utils::do_movement_horizontal_splitter(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == Grid_Location::right({1, 0}));
    CHECK(mvmt.front().direction == Day16_Utils::Movement_Direction::RIGHT);
    CHECK(mvmt.front().in_motion == true);
  }

  SECTION(
      "test movement down when on horizontal splitter location L/R are on "
      "grid") {
    const std::vector<std::string> GRID_SETUP{"...", ".-.", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 1}, Day16_Utils::Movement_Direction::DOWN, true};

    const auto mvmt =
        Day16_Utils::do_movement_horizontal_splitter(grid, initial_movement);

    REQUIRE(mvmt.size() == 2);
    CHECK(mvmt.front().location == Grid_Location::left({1, 1}));
    CHECK(mvmt.front().direction == Day16_Utils::Movement_Direction::LEFT);
    CHECK(mvmt.front().in_motion == true);

    CHECK(mvmt.back().location == Grid_Location::right({1, 1}));
    CHECK(mvmt.back().direction == Day16_Utils::Movement_Direction::RIGHT);
    CHECK(mvmt.back().in_motion == true);
  }

  SECTION("test movement down when on horizontal splitter location L on grid") {
    const std::vector<std::string> GRID_SETUP{"...", "..-", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 2}, Day16_Utils::Movement_Direction::DOWN, true};

    const auto mvmt =
        Day16_Utils::do_movement_horizontal_splitter(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == Grid_Location::left({1, 2}));
    CHECK(mvmt.front().direction == Day16_Utils::Movement_Direction::LEFT);
    CHECK(mvmt.front().in_motion == true);
  }

  SECTION("test movement down when on horizontal splitter location R on grid") {
    const std::vector<std::string> GRID_SETUP{"...", "-..", "..."};
    const auto& grid = Day16_Utils::create_from_input_data(GRID_SETUP);
    const Day16_Utils::Grid_Mover initial_movement{
        {1, 0}, Day16_Utils::Movement_Direction::DOWN, true};

    const auto mvmt =
        Day16_Utils::do_movement_horizontal_splitter(grid, initial_movement);

    REQUIRE(mvmt.size() == 1);
    CHECK(mvmt.front().location == Grid_Location::right({1, 0}));
    CHECK(mvmt.front().direction == Day16_Utils::Movement_Direction::RIGHT);
    CHECK(mvmt.front().in_motion == true);
  }
}

TEST_CASE("Test Day16_Utils.do_movement_vertical_splitter", "[Day16_Utils]") {
  // TODO: do test cases. Same as horizontal splitter but changing LR UD
}

// TODO: test angles

TEST_CASE("Test do_beam_walking", "[Day16_Utils]") {
  SECTION("Test part1 sample data") {
    const auto& grid = Day16_Utils::create_from_input_data(SAMPLE_DATA);
    const auto upper_left = top_of_column(grid, 0);
    const Day16_Utils::Grid_Mover initial_movement{
        upper_left.first, Day16_Utils::Movement_Direction::RIGHT, true};

    Day16_Utils::Movement_Grid_Type movement_grid(grid.number_of_rows(),
                                                  grid.number_of_cols());
    int energized_count =
        Day16_Utils::do_beam_walking(grid, initial_movement, movement_grid);

    REQUIRE(46 == energized_count);
  }

  // SECTION("Test part1 sample data r3c7 up") {
  //   const auto& grid = Day16_Utils::create_from_input_data(SAMPLE_DATA);
  //   const Day16_Utils::Grid_Mover initial_movement{
  //       {3, 7}, Day16_Utils::Movement_Direction::UP, true};

  //   Day16_Utils::Movement_Grid_Type movement_grid(grid.number_of_rows(),
  //                                                 grid.number_of_cols());
  //   int energized_count =
  //       Day16_Utils::do_beam_walking(grid, initial_movement, movement_grid);

  //   REQUIRE(46 == energized_count);
  // }
}