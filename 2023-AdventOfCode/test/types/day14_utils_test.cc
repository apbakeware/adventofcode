#include "types/day14_utils.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <queue>
#include <sstream>

#include "spdlog/spdlog.h"
#include "utils/grid/grid.h"
#include "utils/grid/grid_traversal.h"
#include "utils/grid/grid_utils.h"

using namespace Catch::Matchers;

using namespace Day14_Utils;

namespace {

std::vector<std::string> TEST_LINES{
    "O....#....", "O.OO#....#", ".....##...", "OO.#O....O", ".O.....O#.",
    "O.#..O.#.#", "..O..#O..O", ".......O..", "#....###..", "#OO..#...."};

Fixed_Sized_Grid<char> create_test_grid() {
  return create_char_grid_from_string_rows(TEST_LINES.begin(),
                                           TEST_LINES.end());
}

template <class Grid_T>
std::string column_to_top_down_string(const Grid_T& grid, size_t column) {
  auto location = top_of_column(grid, column);
  if (!location.second) {
    return "";
  }

  std::string result;
  Grid_Location walk_loc = location.first;
  while (is_on_grid(walk_loc, grid)) {
    result += grid.at(walk_loc);
    walk_loc = Grid_Location::down(walk_loc);
  }

  return result;
}
}  // namespace

TEST_CASE("Test Day14_Utils") {
  SECTION("test column_to_top_down_string") {
    const std::string EXPECTED = "OO.O.O..##";

    const auto& grid = create_test_grid();
    const auto actual = column_to_top_down_string(grid, 0);

    REQUIRE(EXPECTED == actual);
  }

  SECTION("test do_tilt_up column 0") {
    const std::string EXPECTED = "OOOO....##";
    auto grid = create_test_grid();

    do_tilt_up(grid, 0);

    const auto actual = column_to_top_down_string(grid, 0);
    REQUIRE(EXPECTED == actual);
  }

  SECTION("test do_tilt_up column 7") {
    const std::string EXPECTED = "O....#O.#.";
    auto grid = create_test_grid();

    do_tilt_up(grid, 7);

    const auto actual = column_to_top_down_string(grid, 7);
    REQUIRE(EXPECTED == actual);
  }

  SECTION("test tilt_and_evaluate_load") {
    auto grid = create_test_grid();

    int total_load = tilt_and_evaluate_load(grid);

    REQUIRE(136 == total_load);
  }

  SECTION("test grid_after_spin_cycle_1") {
    std::vector<std::string> exp_lines{
        ".....#....", "....#...O#", "...OO##...", ".OO#......", ".....OOO#.",
        ".O#...O#.#", "....O#....", "......OOOO", "#...O###..", "#..OO#...."};

    auto grid = create_test_grid();
    const auto exp_grid =
        create_char_grid_from_string_rows(exp_lines.begin(), exp_lines.end());

    do_spin_cycle(grid);

    std::ostringstream exp_grid_stream;
    std::ostringstream act_grid_stream;

    exp_grid_stream << exp_grid;
    act_grid_stream << grid;

    REQUIRE(exp_grid_stream.str() == act_grid_stream.str());
  }

  SECTION("test grid_after_spin_cycle_2") {
    std::vector<std::string> exp_lines{
        ".....#....", "....#...O#", ".....##...", "..O#......", ".....OOO#.",
        ".O#...O#.#", "....O#...O", ".......OOO", "#..OO###..", "#.OOO#...O "};

    auto grid = create_test_grid();
    const auto exp_grid =
        create_char_grid_from_string_rows(exp_lines.begin(), exp_lines.end());

    do_spin_cycle(grid);
    do_spin_cycle(grid);

    std::ostringstream exp_grid_stream;
    std::ostringstream act_grid_stream;

    exp_grid_stream << exp_grid;
    act_grid_stream << grid;

    REQUIRE(exp_grid_stream.str() == act_grid_stream.str());
  }

  SECTION("test load_after_spin_cycles") {
    auto grid = create_test_grid();

    int total_load = load_after_spin_cycles(grid, 1000000000UL);

    REQUIRE(64 == total_load);

    //    REQUIRE(false);  // force failure
  }
}