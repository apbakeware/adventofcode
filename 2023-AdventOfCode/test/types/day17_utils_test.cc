#include "types/day17_utils.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
// #include <catch2/matchers/catch_matchers.hpp>
// #include <catch2/matchers/catch_matchers_vector.hpp>
// #include <queue>
// #include <set>
// #include <string>
// #include <vector>

#include "spdlog/spdlog.h"
#include "utils/grid/grid.h"
#include "utils/grid/grid_utils.h"

using namespace Day17_Utils;

namespace {

const std::vector<std::string> SAMPLE_LINES{
    "2413432311323", "3215453535623", "3255245654254", "3446585845452",
    "4546657867536", "1438598798454", "4457876987766", "3637877979653",
    "4654967986887", "4564679986453", "1224686865563", "2546548887735",
    "4322674655533"};

// 123
// 456
// 789
const std::vector<std::string> SIMPLE_3x3_GRID_LINES{"123", "456", "789"};

static const Day17_Utils::Heat_Loss_Grid& grid3x3 =
    create_digit_grid_from_string_rows(SIMPLE_3x3_GRID_LINES.begin(),
                                       SIMPLE_3x3_GRID_LINES.end());

}  // namespace

TEST_CASE("Test Day17_Utils building functions from sample data",
          "[Day17_Utils]") {
  SECTION("get_origin_and_destination_location simple data") {
    const auto sut = Day17_Utils::get_origin_and_destination_location(grid3x3);

    CHECK(sut.origin.row == 2);
    CHECK(sut.origin.col == 0);
    CHECK(sut.destination.row == 0);
    CHECK(sut.destination.col == 2);
    CHECK(grid3x3.at(sut.origin) == 1);
    CHECK(grid3x3.at(sut.destination) == 9);
  }

  SECTION("get_origin_and_destination_location sample data") {
    const auto grid = create_digit_grid_from_string_rows(SAMPLE_LINES.begin(),
                                                         SAMPLE_LINES.end());

    const auto sut = Day17_Utils::get_origin_and_destination_location(grid);

    CHECK(sut.origin.row == 12);
    CHECK(sut.origin.col == 0);
    CHECK(sut.destination.row == 0);
    CHECK(sut.destination.col == 12);
    CHECK(grid.at(sut.origin) == 2);
    CHECK(grid.at(sut.destination) == 3);
  }

  SECTION("create_visitation_grid") {
    const auto grid = create_digit_grid_from_string_rows(SAMPLE_LINES.begin(),
                                                         SAMPLE_LINES.end());

    const auto sut = Day17_Utils::create_visitation_grid(grid);

    CHECK(grid.number_of_rows() == sut.number_of_rows());
    CHECK(grid.number_of_cols() == sut.number_of_cols());

    sut.for_each_cell([](__attribute__((unused)) const auto& loc,
                         const auto val) { CHECK(val == false); });
  }
}

TEST_CASE("Test Day17_Utils::heat_value_if_unvisited_direction",
          "[Day17_Utils]") {
  SECTION("value on valid grid location") {
    // https://github.com/catchorg/Catch2/blob/ce42deb72fab2be85a862f559984580c24cb76c4/projects/SelfTest/UsageTests/Generators.tests.cpp#L199
    // using Test_Data = std::tuple<Orthogonal_Direction::Direction, int>;
    // Not sure what table type is but its used in catch2
    auto cases = GENERATE(table<Orthogonal_Direction::Direction, int>(
        {{Orthogonal_Direction::UP, 2},
         {Orthogonal_Direction::RIGHT, 6},
         {Orthogonal_Direction::DOWN, 8},
         {Orthogonal_Direction::LEFT, 4}}));

    auto direction = std::get<0>(cases);
    auto exp = std::get<1>(cases);

    const Visitation_Grid visitation = create_visitation_grid(grid3x3);
    const Grid_Location location{1, 1};

    CHECK(heat_value_if_unvisited_direction(grid3x3, visitation, location,
                                            direction) == exp);
  }

  SECTION("value to off-grid location") {
    const Visitation_Grid visitation = create_visitation_grid(grid3x3);

    CHECK(heat_value_if_unvisited_direction(grid3x3, visitation, {2, 1},
                                            Orthogonal_Direction::UP) ==
          UNACCEPTABLE_MOVEMENT);
  }

  SECTION("value of visited location") {
    Visitation_Grid visitation = create_visitation_grid(grid3x3);

    visitation.set({2, 1}, true);

    CHECK(heat_value_if_unvisited_direction(grid3x3, visitation, {1, 1},
                                            Orthogonal_Direction::UP) ==
          UNACCEPTABLE_MOVEMENT);
  }
}

TEST_CASE("Test Day17_Utils::create_least_cost_path", "[Day17_Utils]") {
  SECTION("Test simple grid no 3 space turn required") {
    const auto start_end = get_origin_and_destination_location(grid3x3);
    const Path EXP{
        {{2, 0}, 1}, {{2, 1}, 2}, {{2, 2}, 3}, {{1, 2}, 6}, {{0, 2}, 9}};

    const auto& generated_path = create_least_cost_path(grid3x3, start_end);
    CHECK(generated_path == EXP);
    CHECK(path_heat_const(generated_path) == 21);
  }

  SECTION("Test simple basic grid imposing 3 consecutive limit", "[!mayfail]") {
    std::vector<std::string> LINES{"11111", "22222", "333333"};
    const Heat_Loss_Grid grid =
        create_digit_grid_from_string_rows(LINES.begin(), LINES.end());
    const auto start_end = get_origin_and_destination_location(grid);

    const Path EXP{{{2, 0}, 1}, {{2, 1}, 1}, {{2, 2}, 1}, {{1, 2}, 2},
                   {{1, 3}, 2}, {{1, 4}, 2}, {{0, 5}, 3}};

    const auto& generated_path = create_least_cost_path(grid, start_end);

    SKIP();
    REQUIRE(generated_path == EXP);
  }
}