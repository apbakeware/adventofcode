#include "types/day13_utils.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

#include "utils/grid/grid_utils.h"

namespace {
const std::vector<std::string> lines_with_vertical_reflection{
    "#.##..##.", "..#.##.#.", "##......#", "##......#",
    "..#.##.#.", "..##..##.", "#.#.##.#."};

const std::vector<std::string> lines_with_horizontal_reflection{
    "#...##..#", "#....#..#", "..##..###", "#####.##.",
    "#####.##.", "..##..###", "#....#..#"};

}  // namespace

// Test cases taken from day13 samples
TEST_CASE("Test Day13_Utils.find_common_fold_reflection", "[Day13_Utils]") {
  SECTION("test when common fold index exist") {
    auto reflections = Day13_Utils::find_common_fold_reflection(
        lines_with_vertical_reflection);
    REQUIRE(reflections.size() == 1);
    REQUIRE(5 == reflections.front());
  }

  SECTION("test when common fold index doesnt exist") {
    REQUIRE(Day13_Utils::find_common_fold_reflection(
                lines_with_horizontal_reflection)
                .empty() == true);
  }
}

TEST_CASE("Test Day13_Utils.find_common_vertical_fold_reflection",
          "[Day13_Utils]") {
  SECTION("test grid with common vertical reflection") {
    const auto& grid = create_char_grid_from_string_rows(
        lines_with_vertical_reflection.begin(),
        lines_with_vertical_reflection.end());

    REQUIRE(5 ==
            Day13_Utils::find_common_vertical_fold_reflection(grid).front());
  }

  SECTION("test grid without common vertical reflection") {
    const auto& grid = create_char_grid_from_string_rows(
        lines_with_horizontal_reflection.begin(),
        lines_with_horizontal_reflection.end());

    REQUIRE(Day13_Utils::find_common_vertical_fold_reflection(grid).empty() ==
            true);
  }
}

TEST_CASE("Test Day13_Utils.find_common_horizontal_fold_reflection",
          "[Day13_Utils]") {
  SECTION("test grid with common horizontal reflection") {
    const auto& grid = create_char_grid_from_string_rows(
        lines_with_horizontal_reflection.begin(),
        lines_with_horizontal_reflection.end());

    REQUIRE(4 ==
            Day13_Utils::find_common_horizontal_fold_reflection(grid).front());
  }

  SECTION("test grid without common horizontal reflection") {
    const auto& grid = create_char_grid_from_string_rows(
        lines_with_vertical_reflection.begin(),
        lines_with_vertical_reflection.end());

    REQUIRE(Day13_Utils::find_common_horizontal_fold_reflection(grid).empty() ==
            true);
  }
}

TEST_CASE("Test Day13_Utils.evaluate_part1_reflection_score", "[Day13_Utils]") {
  const auto& grid1 = create_char_grid_from_string_rows(
      lines_with_horizontal_reflection.begin(),
      lines_with_horizontal_reflection.end());

  const auto& grid2 =
      create_char_grid_from_string_rows(lines_with_vertical_reflection.begin(),
                                        lines_with_vertical_reflection.end());

  int score = 0;
  score += Day13_Utils::evaluate_part1_reflection_score(grid1);
  score += Day13_Utils::evaluate_part1_reflection_score(grid2);

  REQUIRE(405 == score);
}

TEST_CASE("Test Day13_Utils.evaluate_part2_reflection_score", "[Day13_Utils]") {
  SECTION("Test smudge case 1 expected grid flip") {
    std::vector<std::string> LINES{"..##..##.", "..#.##.#.", "##......#",
                                   "##......#", "..#.##.#.", "..##..##.",
                                   "#.#.##.#."};
    auto grid = create_char_grid_from_string_rows(LINES.begin(), LINES.end());
    REQUIRE_THAT(Day13_Utils::find_common_horizontal_fold_reflection(grid),
                 Catch::Matchers::Equals(std::vector<int>{3}));
  }

  SECTION("Test smudge case 1") {
    auto grid = create_char_grid_from_string_rows(
        lines_with_vertical_reflection.begin(),
        lines_with_vertical_reflection.end());

    REQUIRE(Day13_Utils::evaluate_part2_reflection_score(grid) == 300);
  }

  SECTION("Test smudge case 2") {
    auto grid = create_char_grid_from_string_rows(
        lines_with_horizontal_reflection.begin(),
        lines_with_horizontal_reflection.end());

    REQUIRE(Day13_Utils::evaluate_part2_reflection_score(grid) == 100);
  }

  SECTION("Test mirror not detecting a change (last)") {
    std::vector<std::string> LINES{
        "#...#..######.#.#", "...#.###.##..#.#.", "###....#..#..#.#.",
        ".##.######.###.#.", "##..#..#..###....", "##..#..#..###....",
        ".##.##########.#.", "###....#..#..#.#.", "...#.###.##..#.#.",
        "#...#..######.#.#", ".##.#.#..#....##.", ".##.#.#..#....##.",
        "#...#..######.#.#"};

    auto grid = create_char_grid_from_string_rows(LINES.begin(), LINES.end());
    REQUIRE(Day13_Utils::evaluate_part2_reflection_score(grid) != -1);
  }

  // SECTION("Test mirror not detecting change (2)") {
  //   std::vector<std::string> LINES{
  //       "..#.#......#.", "###.#.####.#.", "##.##.#..#.##",
  //       "##..#.####.#.", "...###.##.###", "###.##.##.##.",
  //       "###..##..#...", "###..##..##..", "####.#.##.#.#"};

  //   std::vector<std::string> EXP{
  //       "..#.#......#.", "###.#.####.#.", "##.##.#..#.##",
  //       "##..#.####.#.", "...###.##.###", "###.##.##.##.",
  //       "###..##..##..", "###..##..##..", "####.#.##.#.#"};

  //   EXP should have a vertical fold on column 8
  // ..#.#... | ...#.
  // ###.#.## | ##.#.
  // ##.##.#. | .#.##
  // ##..#.## | ##.#.
  // ...###.# | #.###
  // ###.##.# | #.##.
  // ###..##. | .##..  <- change here (last #)
  // ###..##. | .##..
  // ####.#.# | #.#.#

  // Need to get 2 or more of the lines of reflection from the single
  // case....cuze the first is still valid, and is detercted prior
  // to new colum

  // seems like row location row 2 (from bottom) and col 11 . -> #

  // auto grid = create_char_grid_from_string_rows(LINES.begin(), LINES.end());
  // auto exp_grid = create_char_grid_from_string_rows(EXP.begin(), EXP.end());

  // std::cout << "EXP: LOR r: "
  //           <<
  //           Day13_Utils::find_common_horizontal_fold_reflection(exp_grid)
  //           << "  c: "
  //           << Day13_Utils::find_common_vertical_fold_reflection(exp_grid)
  //           << "\n"
  //           << exp_grid << "\n";

  // REQUIRE(Day13_Utils::evaluate_part2_reflection_score(grid) != -1);
  // }
}
