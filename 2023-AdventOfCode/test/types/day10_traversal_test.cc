
#include <algorithm>
#include <catch2/catch_test_macros.hpp>
#include <functional>
#include <unordered_map>

#include "spdlog/spdlog.h"
#include "types/day10_traversal_utils.h"
#include "utils/grid/grid.h"
#include "utils/grid/grid_flood_fill.h"
#include "utils/grid/grid_utils.h"

// | is a vertical pipe connecting north and south.
// - is a horizontal pipe connecting east and west.
// L is a 90-degree bend connecting north and east.
// J is a 90-degree bend connecting north and west.
// 7 is a 90-degree bend connecting south and west.
// F is a 90-degree bend connecting south and east.
// . is ground; there is no pipe in this tile.
// S is the starting position of the animal; there is a pipe on this tile, but
// your sketch doesn't show what shape the pipe has.

// How to handle S

//////////////////////////////////////////////////////////////
// Check if a cell is inscribed by the pipe. This is done
// by checking if the cell hits a pipe cell in each
// of the 4 cardinal directions.
//
// Optimization is that we can flood it if its
// inscribed we can skip it cuz its been marked.
template <class Grid_T>
class Inscribed_By_Pipe {
 public:
  Inscribed_By_Pipe(Grid_T& grid) : _grid(grid) {}

  bool is_cell_inscribed(const Grid_Location& location) {
    // It is a pipe, not inscribed
    const char value = _grid.at(location);
    if (Day10_Traversal_Utils::is_pipe_cell(value)) return false;

    return pipe_lies_towards(location, Grid_Location::up) &&
           pipe_lies_towards(location, Grid_Location::right) &&
           pipe_lies_towards(location, Grid_Location::down) &&
           pipe_lies_towards(location, Grid_Location::left);
  }

  bool pipe_lies_towards(
      const Grid_Location& loc,
      std::function<Grid_Location(Grid_Location, ssize_t)> move) {
    Grid_Location current = loc;

    while (is_on_grid(current, _grid)) {
      const auto cell_value = _grid.at(current);
      SPDLOG_DEBUG("Checking cell @ {} : {} ", current, cell_value);

      if (Day10_Traversal_Utils::is_pipe_cell(cell_value)) {
        return true;
      }
      current = move(current, 1);
    }
    return false;
  }

 private:
  // eventually template, but just move forward

  Grid_T& _grid;
};

TEST_CASE("Day10_Traversal_Utils_Test") {
  SECTION("Test can_move_up") {
    CHECK(Day10_Traversal_Utils::can_move_up('|', '|'));
    CHECK(Day10_Traversal_Utils::can_move_up('|', '7'));
    CHECK(Day10_Traversal_Utils::can_move_up('|', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('|', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('|', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('|', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('|', '.'));

    CHECK(Day10_Traversal_Utils::can_move_up('L', '|'));
    CHECK(Day10_Traversal_Utils::can_move_up('L', '7'));
    CHECK(Day10_Traversal_Utils::can_move_up('L', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('L', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('L', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('L', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('L', '.'));

    CHECK(Day10_Traversal_Utils::can_move_up('J', '|'));
    CHECK(Day10_Traversal_Utils::can_move_up('J', '7'));
    CHECK(Day10_Traversal_Utils::can_move_up('J', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('J', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('J', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('J', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('J', '.'));

    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('-', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('-', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('-', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('-', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('-', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('-', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('-', '.'));

    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('7', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('7', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('7', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('7', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('7', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('7', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('7', '.'));

    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('F', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('F', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('F', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('F', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('F', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('F', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_up('F', '.'));
  }

  SECTION("Test can_move_right") {
    // TESTS which have valid cell values for originating a right movement
    CHECK(Day10_Traversal_Utils::can_move_right('-', '-'));
    CHECK(Day10_Traversal_Utils::can_move_right('-', 'J'));
    CHECK(Day10_Traversal_Utils::can_move_right('-', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('-', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('-', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('-', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('-', '.'));

    CHECK(Day10_Traversal_Utils::can_move_right('L', '-'));
    CHECK(Day10_Traversal_Utils::can_move_right('L', 'J'));
    CHECK(Day10_Traversal_Utils::can_move_right('L', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('L', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('L', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('L', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('L', '.'));

    CHECK(Day10_Traversal_Utils::can_move_right('F', '-'));
    CHECK(Day10_Traversal_Utils::can_move_right('F', 'J'));
    CHECK(Day10_Traversal_Utils::can_move_right('F', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('F', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('F', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('F', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('F', '.'));

    // TESTS which cannot move right from the origin
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('|', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('|', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('|', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('|', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('|', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('|', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('|', '.'));

    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('7', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('7', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('7', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('7', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('7', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('7', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('7', '.'));

    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('J', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('J', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('J', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('J', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('J', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('J', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_right('J', '.'));
  }

  SECTION("Test can_move_down") {
    // TESTS which have valid cell values for originating a down movement
    CHECK(Day10_Traversal_Utils::can_move_down('|', '|'));
    CHECK(Day10_Traversal_Utils::can_move_down('|', 'J'));
    CHECK(Day10_Traversal_Utils::can_move_down('|', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('|', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('|', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('|', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('|', '.'));

    CHECK(Day10_Traversal_Utils::can_move_down('7', '|'));
    CHECK(Day10_Traversal_Utils::can_move_down('7', 'J'));
    CHECK(Day10_Traversal_Utils::can_move_down('7', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('7', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('7', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('7', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('7', '.'));

    CHECK(Day10_Traversal_Utils::can_move_down('F', '|'));
    CHECK(Day10_Traversal_Utils::can_move_down('F', 'J'));
    CHECK(Day10_Traversal_Utils::can_move_down('F', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('F', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('F', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('F', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('F', '.'));

    // TESTS which cannot move down from the origin
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('-', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('-', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('-', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('-', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('-', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('-', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('-', '.'));

    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('J', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('J', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('J', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('J', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('J', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('J', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('J', '.'));

    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('L', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('L', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('L', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('L', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('L', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('L', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_down('L', '.'));
  }

  SECTION("Test can_move_left") {
    // TESTS which have valid cell values for originating a left movement
    CHECK(Day10_Traversal_Utils::can_move_left('-', '-'));
    CHECK(Day10_Traversal_Utils::can_move_left('-', 'F'));
    CHECK(Day10_Traversal_Utils::can_move_left('-', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('-', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('-', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('-', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('-', '.'));

    CHECK(Day10_Traversal_Utils::can_move_left('J', '-'));
    CHECK(Day10_Traversal_Utils::can_move_left('J', 'F'));
    CHECK(Day10_Traversal_Utils::can_move_left('J', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('J', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('J', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('J', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('J', '.'));

    CHECK(Day10_Traversal_Utils::can_move_left('7', '-'));
    CHECK(Day10_Traversal_Utils::can_move_left('7', 'F'));
    CHECK(Day10_Traversal_Utils::can_move_left('7', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('7', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('7', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('7', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('7', '.'));

    // TESTS which cannot move left from the origin
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('|', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('|', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('|', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('|', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('|', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('|', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('|', '.'));

    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('L', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('L', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('L', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('L', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('L', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('L', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('L', '.'));

    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('F', '|'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('F', '7'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('F', 'F'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('F', '-'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('F', 'L'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('F', 'J'));
    CHECK_FALSE(Day10_Traversal_Utils::can_move_left('F', '.'));
  }

  SECTION("Test moves from starting location") {
    CHECK(Day10_Traversal_Utils::can_move_up('S', '|'));
    CHECK(Day10_Traversal_Utils::can_move_up('S', 'F'));
    CHECK(Day10_Traversal_Utils::can_move_up('S', '7'));

    CHECK(Day10_Traversal_Utils::can_move_right('S', '-'));
    CHECK(Day10_Traversal_Utils::can_move_right('S', 'J'));
    CHECK(Day10_Traversal_Utils::can_move_right('S', '7'));

    CHECK(Day10_Traversal_Utils::can_move_down('S', '|'));
    CHECK(Day10_Traversal_Utils::can_move_down('S', 'J'));
    CHECK(Day10_Traversal_Utils::can_move_down('S', 'L'));

    CHECK(Day10_Traversal_Utils::can_move_left('S', '-'));
    CHECK(Day10_Traversal_Utils::can_move_left('S', 'F'));
    CHECK(Day10_Traversal_Utils::can_move_left('S', 'L'));
  }

  SECTION("Test moves to starting location") {
    CHECK(Day10_Traversal_Utils::can_move_up('|', 'S'));
    CHECK(Day10_Traversal_Utils::can_move_up('L', 'S'));
    CHECK(Day10_Traversal_Utils::can_move_up('J', 'S'));

    CHECK(Day10_Traversal_Utils::can_move_right('-', 'S'));
    CHECK(Day10_Traversal_Utils::can_move_right('L', 'S'));
    CHECK(Day10_Traversal_Utils::can_move_right('F', 'S'));

    CHECK(Day10_Traversal_Utils::can_move_down('|', 'S'));
    CHECK(Day10_Traversal_Utils::can_move_down('7', 'S'));
    CHECK(Day10_Traversal_Utils::can_move_down('F', 'S'));

    CHECK(Day10_Traversal_Utils::can_move_left('-', 'S'));
    CHECK(Day10_Traversal_Utils::can_move_left('7', 'S'));
    CHECK(Day10_Traversal_Utils::can_move_left('J', 'S'));
  }
}

TEST_CASE("Day10_Traversal_Utils_Test.find_left_most_upper_location") {
  SECTION("Test simple loop") {
    const std::vector<std::string> lines{"-L|F7", "7S-7|", "L|7||", "-L-J|",
                                         "L|-JF"};
    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());
    auto start_loc = find_value(grid, 'S');

    auto pipe_isolated_grid =
        Day10_Traversal_Utils::isolate_pipe(grid, start_loc.first);

    // Ensure the grid is created with the first array line on top and
    // a lower left 0,0 corrdinate reference.
    REQUIRE(grid.at({0, 0}) == 'L');

    Grid_Location sut = Day10_Traversal_Utils::find_left_most_upper_location(
        grid, start_loc.first);

    const Grid_Location EXPECTED{3, 1};

    REQUIRE(sut == EXPECTED);
  }

  SECTION("Test simple loop") {
    const std::vector<std::string> lines{
        ".F----7F7F7F7F-7....", ".|F--7||||||||FJ....", ".||.FJ||||||||L7....",
        "FJL7L7LJLJ||LJ.L-7..", "L--J.L7...LJS7F-7L7.", "....F-J..F7FJ|L7L7L7",
        "....L7.F7||L7|.L7L7|", ".....|FJLJ|FJ|F7|.LJ", "....FJL-7.||.||||...",
        "....L---J.LJ.LJLJ..."};
    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());
    auto start_loc = find_value(grid, 'S');
    auto pipe_isolated_grid =
        Day10_Traversal_Utils::isolate_pipe(grid, start_loc.first);

    // Ensure the grid is created with the first array line on top and
    // a lower left 0,0 corrdinate reference.
    REQUIRE(grid.at({0, 4}) == 'L');

    Grid_Location sut = Day10_Traversal_Utils::find_left_most_upper_location(
        grid, start_loc.first);

    const Grid_Location EXPECTED{6, 0};

    REQUIRE(sut == EXPECTED);
  }
}

TEST_CASE("Test Day10.1") {
  SECTION("Test simple loop example") {
    const std::vector<std::string> lines{"-L|F7", "7S-7|", "L|7||", "-L-J|",
                                         "L|-JF"};
    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());
    auto start_loc = find_value(grid, 'S');

    // Ensure the grid is created with the first array line on top and
    // a lower left 0,0 corrdinate reference.
    REQUIRE(grid.at({0, 0}) == 'L');

    SPDLOG_DEBUG("\n GRID\n{}", grid);
    SPDLOG_DEBUG("Start Location: {}, {}", start_loc.first, start_loc.second);

    int steps =
        Day10_Traversal_Utils::determine_pipe_length(grid, start_loc.first);

    REQUIRE(steps / 2 == 4);
  }

  SECTION("Test complex loop example") {
    const std::vector<std::string> lines{"..F7.", ".FJ|.", "SJ.L7", "|F--J",
                                         "LJ..."};

    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());

    auto start_loc = find_value(grid, 'S');

    SPDLOG_DEBUG("\n GRID\n{}", grid);
    SPDLOG_DEBUG("Start Location: {}, {}", start_loc.first, start_loc.second);

    Day10_Traversal_Utils::render_unicode_grid(grid);

    int steps =
        Day10_Traversal_Utils::determine_pipe_length(grid, start_loc.first);

    REQUIRE(steps / 2 == 8);
  }
}

TEST_CASE("Test Day10.2") {
  SECTION("Test simple loop example") {
    const std::vector<std::string> lines{
        "...........", ".S-------7.", ".|F-----7|.",
        ".||.....||.", ".||.....||.", ".|L-7OF-J|.",
        ".|..|O|..|.", ".L--JOL--J.", ".....O....."};

    // likely much more efficient way, this traverses pipe muliple times,
    // but its stages are easier to develop and test

    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());
    auto start_loc = find_value(grid, 'S');

    auto pipe_isolated_grid =
        Day10_Traversal_Utils::isolate_pipe(grid, start_loc.first);

    auto flood_start_pipe_loc =
        Day10_Traversal_Utils::find_left_most_upper_location(grid,
                                                             start_loc.first);

    std::cout << "Original Grid" << std::endl;
    Day10_Traversal_Utils::render_unicode_grid(grid);

    std::cout << "Isolated Pipe" << std::endl;
    Day10_Traversal_Utils::render_unicode_grid(pipe_isolated_grid);

    int cell_flood_count = Day10_Traversal_Utils::flood_inscribed_cells(
        pipe_isolated_grid, flood_start_pipe_loc);
    std::cout << "Flooded" << std::endl;
    Day10_Traversal_Utils::render_unicode_grid(pipe_isolated_grid);

    REQUIRE(cell_flood_count == 4);
  }

  SECTION("Test simple loop example 2") {
    const std::vector<std::string> lines{
        ".F----7F7F7F7F-7....", ".|F--7||||||||FJ....", ".||.FJ||||||||L7....",
        "FJL7L7LJLJ||LJ.L-7..", "L--J.L7...LJS7F-7L7.", "....F-J..F7FJ|L7L7L7",
        "....L7.F7||L7|.L7L7|", ".....|FJLJ|FJ|F7|.LJ", "....FJL-7.||.||||...",
        "....L---J.LJ.LJLJ..."};

    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());
    auto start_loc = find_value(grid, 'S');

    auto pipe_isolated_grid =
        Day10_Traversal_Utils::isolate_pipe(grid, start_loc.first);

    auto flood_start_pipe_loc =
        Day10_Traversal_Utils::find_left_most_upper_location(grid,
                                                             start_loc.first);

    std::cout << "Original Grid" << std::endl;
    Day10_Traversal_Utils::render_unicode_grid(grid);

    std::cout << "Isolated Pipe" << std::endl;
    Day10_Traversal_Utils::render_unicode_grid(pipe_isolated_grid);

    int cell_flood_count = Day10_Traversal_Utils::flood_inscribed_cells(
        pipe_isolated_grid, flood_start_pipe_loc);
    std::cout << "Flooded" << std::endl;
    Day10_Traversal_Utils::render_unicode_grid(pipe_isolated_grid);

    // Is not correct.
    REQUIRE(cell_flood_count == 8);
  }

  SECTION("Test simple loop example 2") {
    const std::vector<std::string> lines{
        ".F----7F7F7F7F-7....", ".|F--7||||||||FJ....", ".||.FJ||||||||L7....",
        "FJL7L7LJLJ||LJ.L-7..", "L--J.L7...LJS7F-7L7.", "....F-J..F7FJ|L7L7L7",
        "....L7.F7||L7|.L7L7|", ".....|FJLJ|FJ|F7|.LJ", "....FJL-7.||.||||...",
        "....L---J.LJ.LJLJ..."};

    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());
    auto start_loc = find_value(grid, 'S');

    auto pipe_isolated_grid =
        Day10_Traversal_Utils::isolate_pipe(grid, start_loc.first);

    auto inscribed_cell_checker = Inscribed_By_Pipe(pipe_isolated_grid);

    // Different approach. start from 0,0 (but any cell). walk in 4 ordinal
    // directions. if any 1 way runs off grid then its not inscribed. if
    // a pipe is hit, early about that direction

    std::cout << "Original Grid" << std::endl;
    Day10_Traversal_Utils::render_unicode_grid(grid);

    std::cout << "Isolated Pipe" << std::endl;
    Day10_Traversal_Utils::render_unicode_grid(pipe_isolated_grid);

    // TODO: flood and use as short cut
    int inscribed_count = 0;
    auto inscribed_wrapper = [&](const auto& loc,
                                 __attribute__((unused)) char value) {
      bool is_inscribed = inscribed_cell_checker.is_cell_inscribed(loc);
      SPDLOG_DEBUG("Location {}: inscribed? {}", loc, is_inscribed);
      if (is_inscribed) {
        ++inscribed_count;
        pipe_isolated_grid.set(loc, 'X');
      }
    };

    grid.for_each_cell(inscribed_wrapper);

    SPDLOG_DEBUG("  =====> count: {}", inscribed_count);
    // Grid_Location test_cell{0, 0};
    // bool check = inscribed_cell_checker.is_cell_inscribed(test_cell);

    // SPDLOG_DEBUG("Location: {}  pipe above: {}", test_cell, check);

    Day10_Traversal_Utils::render_unicode_grid(pipe_isolated_grid);

    // Still finds cells which are wrong. Inscribe if hits pipe in all
    // directions but its the outside of the pipe.

    REQUIRE(inscribed_count == 8);
  }
}
TEST_CASE("Grid_Flood_Filler") {
  SECTION("Test simple loop example") {
    const std::vector<std::string> lines{
        "...........", ".S-------7.", ".|F-----7|.",
        ".||.....||.", ".||.....||.", ".|L-7OF-J|.",
        ".|..|O|..|.", ".L--JOL--J.", ".....O....."};

    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());

    auto start_loc = find_value(grid, 'S');

    auto pipe_isolated_grid =
        Day10_Traversal_Utils::isolate_pipe(grid, start_loc.first);

    Day10_Traversal_Utils::render_unicode_grid(pipe_isolated_grid);

    Grid_Flood_Filler<Fixed_Sized_Grid<char>> flood_filler;
    flood_filler.unfloodable('S')
        .unfloodable('F')
        .unfloodable('-')
        .unfloodable('7')
        .unfloodable('|')
        .unfloodable('J')
        .unfloodable('L');

    SPDLOG_DEBUG("\n GRID\n{}", grid);
    SPDLOG_DEBUG("Start Location: {}, {}", start_loc.first, start_loc.second);
    SPDLOG_DEBUG("\n PIPE\n{}", pipe_isolated_grid);

    flood_filler.do_flood_fill(pipe_isolated_grid, start_loc.first, 'X');
    Day10_Traversal_Utils::render_unicode_grid(pipe_isolated_grid);

    // REQUIRE(flood_result.num_flodded_cells == 4);
  }
}