#include "types/day09_sensor_predictor.h"

#include <algorithm>
#include <catch2/catch_test_macros.hpp>
#include <functional>

#include "spdlog/spdlog.h"

TEST_CASE("Day09_Sensor_Predictor_Test") {
  SECTION("Test first sample line") {
    std::vector<int> input{0, 3, 6, 9, 12, 15};
    REQUIRE(Day09_Sensor_Predictor::predict_next_reading(input) == 18);
  }

  SECTION("Test sample line 2") {
    std::vector<int> input{1, 3, 6, 10, 15, 21};
    REQUIRE(Day09_Sensor_Predictor::predict_next_reading(input) == 28);
  }

  SECTION("Test sample line 3") {
    std::vector<int> input{10, 13, 16, 21, 30, 45};
    REQUIRE(Day09_Sensor_Predictor::predict_next_reading(input) == 68);
  }
}