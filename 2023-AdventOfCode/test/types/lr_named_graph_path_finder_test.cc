#include "types/lr_named_graph_path_finder.h"

#include <catch2/catch_test_macros.hpp>

#include "utils/graph/named_route_graph.h"

TEST_CASE("Lr_Named_Graph_Path_Finder") {
  SECTION("Test known non-cyclic path") {
    using Route_Graph_Type = Named_Route_Graph<2>;

    // From Day08 page
    // RL
    // AAA = (BBB, CCC)
    // BBB = (DDD, EEE)
    // CCC = (ZZZ, GGG)
    // DDD = (DDD, DDD)
    // EEE = (EEE, EEE)
    // GGG = (GGG, GGG)
    // ZZZ = (ZZZ, ZZZ)
    Cyclic_Direction_List directions("RL");
    Route_Graph_Type graph;
    auto node = graph.add("AAA");
    node->at(0) = "BBB";
    node->at(1) = "CCC";

    node = graph.add("BBB");
    node->at(0) = "DDD";
    node->at(1) = "EEE";

    node = graph.add("CCC");
    node->at(0) = "ZZZ";
    node->at(1) = "GGG";

    node = graph.add("DDD");
    node->at(0) = "DDD";
    node->at(1) = "DDD";

    node = graph.add("EEE");
    node->at(0) = "EEE";
    node->at(1) = "EEE";

    node = graph.add("GGG");
    node->at(0) = "GGG";
    node->at(1) = "GGG";

    node = graph.add("ZZZ");
    node->at(0) = "ZZZ";
    node->at(1) = "ZZZ";

    const size_t distance = Lr_Named_Graph_Path_Finder::distance_between(
        graph, "AAA", "ZZZ", directions);

    REQUIRE(distance == 2);
  }

  SECTION("Test known cyclic path") {
    using Route_Graph_Type = Named_Route_Graph<2>;

    // From Day08 page
    // LLR
    // AAA = (BBB, BBB)
    // BBB = (AAA, ZZZ)
    // ZZZ = (ZZZ, ZZZ)
    Cyclic_Direction_List directions("LLR");
    Route_Graph_Type graph;
    auto node = graph.add("AAA");
    node->at(0) = "BBB";
    node->at(1) = "BBB";

    node = graph.add("BBB");
    node->at(0) = "AAA";
    node->at(1) = "ZZZ";

    node = graph.add("ZZZ");
    node->at(0) = "ZZZ";
    node->at(1) = "ZZZ";

    const size_t distance = Lr_Named_Graph_Path_Finder::distance_between(
        graph, "AAA", "ZZZ", directions);

    REQUIRE(distance == 6);
  }
}

TEST_CASE("Lr_Named_Graph_Concurrent_Path_Finder") {
  using Route_Graph_Type = Named_Route_Graph<2>;
  Cyclic_Direction_List directions("LR");
  Route_Graph_Type graph;

  // LR
  // 11A = (11B, XXX)
  // 11B = (XXX, 11Z)
  // 11Z = (11B, XXX)
  // 22A = (22B, XXX)
  // 22B = (22C, 22C)
  // 22C = (22Z, 22Z)
  // 22Z = (22B, 22B)
  // XXX = (XXX, XXX)

  auto node = graph.add("11A");
  node->at(0) = "11B";
  node->at(1) = "XXX";

  node = graph.add("11B");
  node->at(0) = "XXX";
  node->at(1) = "11Z";

  node = graph.add("11Z");
  node->at(0) = "11B";
  node->at(1) = "XXX";

  node = graph.add("22A");
  node->at(0) = "22B";
  node->at(1) = "XXX";

  node = graph.add("22B");
  node->at(0) = "22C";
  node->at(1) = "22C";

  node = graph.add("22C");
  node->at(0) = "22Z";
  node->at(1) = "22Z";

  node = graph.add("22Z");
  node->at(0) = "22B";
  node->at(1) = "22B";

  node = graph.add("XXX");
  node->at(0) = "XXX";
  node->at(1) = "XXX";

  const size_t distance =
      Lr_Named_Graph_Concurrent_Path_Finder::distance_between(
          graph, {"11A", "22A"}, directions);

  REQUIRE(distance == 6);
}