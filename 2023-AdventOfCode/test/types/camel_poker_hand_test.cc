#include "types/camel_poker_hand.h"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Day07_Part_1_Rank_Policy hand ranks are computed") {
  REQUIRE(Day07_Part_1_Rank_Policy::rank_hand("32T3K") ==
          Camel_Poker_Hand_Rank::ONE_PAIR);

  REQUIRE(Day07_Part_1_Rank_Policy::rank_hand("KK677") ==
          Camel_Poker_Hand_Rank::TWO_PAIR);

  REQUIRE(Day07_Part_1_Rank_Policy::rank_hand("KTJJT") ==
          Camel_Poker_Hand_Rank::TWO_PAIR);

  REQUIRE(Day07_Part_1_Rank_Policy::rank_hand("QQQJA") ==
          Camel_Poker_Hand_Rank::THREE_OF_A_KIND);

  REQUIRE(Day07_Part_1_Rank_Policy::rank_hand("A66A6") ==
          Camel_Poker_Hand_Rank::FULL_HOUSE);

  REQUIRE(Day07_Part_1_Rank_Policy::rank_hand("22232") ==
          Camel_Poker_Hand_Rank::FOUR_OF_A_KIND);

  REQUIRE(Day07_Part_1_Rank_Policy::rank_hand("88888") ==
          Camel_Poker_Hand_Rank::FIVE_OF_A_KIND);

  REQUIRE(Day07_Part_1_Rank_Policy::rank_hand("23458") ==
          Camel_Poker_Hand_Rank::HIGH_CARD);

  REQUIRE(Day07_Part_1_Rank_Policy::rank_hand("J27AK") ==
          Camel_Poker_Hand_Rank::HIGH_CARD);
}

TEST_CASE("Day07_Part_2_Rank_Policy hand ranks are computed") {
  SECTION("No wilds in hand") {
    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("32T3K") ==
            Camel_Poker_Hand_Rank::ONE_PAIR);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("KK677") ==
            Camel_Poker_Hand_Rank::TWO_PAIR);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("A66A6") ==
            Camel_Poker_Hand_Rank::FULL_HOUSE);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("22232") ==
            Camel_Poker_Hand_Rank::FOUR_OF_A_KIND);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("88888") ==
            Camel_Poker_Hand_Rank::FIVE_OF_A_KIND);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("23458") ==
            Camel_Poker_Hand_Rank::HIGH_CARD);
  }

  SECTION("one wild in hand") {
    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("22J22") ==
            Camel_Poker_Hand_Rank::FIVE_OF_A_KIND);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("KTJTT") ==
            Camel_Poker_Hand_Rank::FOUR_OF_A_KIND);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("QQQJA") ==
            Camel_Poker_Hand_Rank::FOUR_OF_A_KIND);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("QQQJA") ==
            Camel_Poker_Hand_Rank::FOUR_OF_A_KIND);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("AA4J4") ==
            Camel_Poker_Hand_Rank::FULL_HOUSE);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("AA4J3") ==
            Camel_Poker_Hand_Rank::THREE_OF_A_KIND);

    // TWO PAIR SHOULD NEVER OCCUR WITH A WILD, IT WOULD BE
    // 3 of a kind or full house

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("3456J") ==
            Camel_Poker_Hand_Rank::ONE_PAIR);

    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("J27AK") ==
            Camel_Poker_Hand_Rank::ONE_PAIR);

    // HIGH CARD can never occur with a wild
  }

  SECTION("multiple wilds in hand") {
    REQUIRE(Day07_Part_2_Rank_Policy::rank_hand("KTJJT") ==
            Camel_Poker_Hand_Rank::FOUR_OF_A_KIND);
  }
}