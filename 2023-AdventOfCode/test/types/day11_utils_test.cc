#include "types/day11_utils.h"

#include <algorithm>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

#include "spdlog/spdlog.h"
#include "utils/grid/grid_distance.h"

using namespace Catch::Matchers;
using namespace Day11_Utils;

namespace {

std::vector<std::string> SAMPLE_GALAXY{
    "...#......", ".......#..", "#.........", "..........", "......#...",
    ".#........", ".........#", "..........", ".......#..", "#...#....."};

Galaxy_Grid create_test_galaxy_grid() {
  return create_galaxy_grid(SAMPLE_GALAXY.begin(), SAMPLE_GALAXY.end());
}

}  // namespace

TEST_CASE("Test Day11_Utils") {
  SECTION("test rows without galaxies") {
    const auto& grid = create_test_galaxy_grid();
    const std::vector<Galaxy_Value_Type> EXPECTED{2, 6};
    const std::vector<Galaxy_Value_Type> actual =
        get_rows_without_galaxies(grid);

    REQUIRE(EXPECTED == actual);
  }

  SECTION("test cols without galaxies") {
    const auto& grid = create_test_galaxy_grid();
    const std::vector<Galaxy_Value_Type> EXPECTED{2, 5, 8};
    const std::vector<Galaxy_Value_Type> actual =
        get_cols_without_galaxies(grid);

    REQUIRE(EXPECTED == actual);
  }

  SECTION("test get galaxy cells") {
    const auto& grid = create_test_galaxy_grid();
    const std::vector<Galaxy_Location_Type> EXPECTED{
        {0, 0}, {0, 4}, {1, 7}, {3, 9}, {4, 1}, {5, 6}, {7, 0}, {8, 7}, {9, 3}};
    const auto actual = get_galaxy_locations(grid);

    REQUIRE(EXPECTED == actual);
  }

  SECTION("test expand space") {
    // Get the rows and columsn to expand.
    // get locations with galaxyies

    // Then, shift the row, col by the number of
    // expansion rows less than the original galaxy cell

    const auto& grid = create_test_galaxy_grid();
    const auto& rows_to_expand =
        get_rows_without_galaxies(grid);  // return sorted
    const auto& cols_to_expand =
        get_cols_without_galaxies(grid);  // return sorted
    auto galaxy_locations = get_galaxy_locations(grid);

    const std::vector<Galaxy_Location_Type> EXPECTED_EXPANSION_LOCATIONS{
        {0, 0}, {0, 5}, {1, 9},  {4, 12}, {5, 1},
        {6, 8}, {9, 0}, {10, 9}, {11, 4}};

    do_galaxy_expansion(galaxy_locations, rows_to_expand, cols_to_expand);
    // that a vector permutation matches.
    REQUIRE_THAT(galaxy_locations,
                 UnorderedEquals(EXPECTED_EXPANSION_LOCATIONS));
  }

  SECTION("test creating pair combinations") {
    // TODO: from the vector of galaxy locations,
    // create a collection of all possible pairs
    using Pair_List = std::vector<std::pair<int, int>>;
    std::vector<int> sut{1, 2, 3, 4};
    Pair_List EXPECTED = {{1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4}, {3, 4}};

    Pair_List actual = create_pair_combinations(sut);

    REQUIRE_THAT(actual, UnorderedEquals(EXPECTED));
  }

  SECTION("test galaxy pair count") {
    const auto& grid = create_test_galaxy_grid();
    const auto& rows_to_expand =
        get_rows_without_galaxies(grid);  // return sorted
    const auto& cols_to_expand =
        get_cols_without_galaxies(grid);  // return sorted
    auto galaxy_locations = get_galaxy_locations(grid);

    do_galaxy_expansion(galaxy_locations, rows_to_expand, cols_to_expand);

    const auto galaxy_pairs = create_pair_combinations(galaxy_locations);

    REQUIRE(36 == galaxy_pairs.size());
  }

  SECTION("test pair distance sum") {
    const auto& grid = create_test_galaxy_grid();
    const auto& rows_to_expand =
        get_rows_without_galaxies(grid);  // return sorted
    const auto& cols_to_expand =
        get_cols_without_galaxies(grid);  // return sorted
    auto galaxy_locations = get_galaxy_locations(grid);

    do_galaxy_expansion(galaxy_locations, rows_to_expand, cols_to_expand);
    const auto galaxy_pairs = create_pair_combinations(galaxy_locations);

    int distance_sum = 0;
    std::for_each(galaxy_pairs.begin(), galaxy_pairs.end(),
                  [&](const auto& galaxy_pair) {
                    distance_sum +=
                        galaxy_distance(galaxy_pair.first, galaxy_pair.second);
                  });

    REQUIRE(374 == distance_sum);
  }

  SECTION("test pair distance sum expansion factor 10") {
    const auto& grid = create_test_galaxy_grid();
    const auto& rows_to_expand =
        get_rows_without_galaxies(grid);  // return sorted
    const auto& cols_to_expand =
        get_cols_without_galaxies(grid);  // return sorted
    auto galaxy_locations = get_galaxy_locations(grid);

    // NOTE: not 10 times, but replace 1 with 10 so need to expand 9 more
    do_galaxy_expansion(galaxy_locations, rows_to_expand, cols_to_expand, 9UL);
    const auto galaxy_pairs = create_pair_combinations(galaxy_locations);

    int distance_sum = 0;
    std::for_each(galaxy_pairs.begin(), galaxy_pairs.end(),
                  [&](const auto& galaxy_pair) {
                    distance_sum +=
                        galaxy_distance(galaxy_pair.first, galaxy_pair.second);
                  });

    REQUIRE(1030 == distance_sum);
  }

  SECTION("test pair distance sum expansion factor 10") {
    const auto& grid = create_test_galaxy_grid();
    const auto& rows_to_expand =
        get_rows_without_galaxies(grid);  // return sorted
    const auto& cols_to_expand =
        get_cols_without_galaxies(grid);  // return sorted
    auto galaxy_locations = get_galaxy_locations(grid);

    do_galaxy_expansion(galaxy_locations, rows_to_expand, cols_to_expand, 99UL);
    const auto galaxy_pairs = create_pair_combinations(galaxy_locations);

    int distance_sum = 0;
    std::for_each(galaxy_pairs.begin(), galaxy_pairs.end(),
                  [&](const auto& galaxy_pair) {
                    distance_sum +=
                        galaxy_distance(galaxy_pair.first, galaxy_pair.second);
                  });

    REQUIRE(8410 == distance_sum);
  }
}