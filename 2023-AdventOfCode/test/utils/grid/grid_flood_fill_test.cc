#include "utils/grid/grid_flood_fill.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <iostream>

#include "spdlog/spdlog.h"
#include "utils/grid/grid.h"
#include "utils/grid/grid_utils.h"

namespace {
static const char OPEN = '.';
static const char FLOOD = 'X';
}  // namespace

TEST_CASE("Grid_Flood_Fill", "[grid]") {
  SECTION("Test 3x3 no open space") {
    std::vector<std::string> lines{"SSS", "SSS", "WWW"};
    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());

    const auto flood_count = grid_flood_fill(grid, {1, 1}, OPEN, FLOOD);

    REQUIRE(flood_count == 0);
  }

  SECTION("Test 3x3 open center") {
    std::vector<std::string> lines{"SSS", "S.S", "WWW"};
    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());

    const auto flood_count = grid_flood_fill(grid, {1, 1}, OPEN, FLOOD);

    REQUIRE(flood_count == 1);
  }

  SECTION("Test 3x3 open center, open side") {
    std::vector<std::string> lines{"S.S", "S.S", "WWW"};
    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());

    const auto flood_count = grid_flood_fill(grid, {1, 1}, OPEN, FLOOD);

    REQUIRE(flood_count == 2);
  }

  SECTION("Test 3x3 all open") {
    std::vector<std::string> lines{"...", "...", "..."};
    auto grid = create_char_grid_from_string_rows(lines.begin(), lines.end());

    const auto flood_count = grid_flood_fill(grid, {1, 0}, OPEN, FLOOD);

    REQUIRE(flood_count == 9);
  }
}
