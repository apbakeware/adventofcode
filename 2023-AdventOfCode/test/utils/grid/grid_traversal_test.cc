#include "utils/grid/grid_traversal.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <iostream>

#include "spdlog/spdlog.h"
#include "utils/grid/grid.h"
#include "utils/grid/grid_traversal.h"
#include "utils/grid/grid_utils.h"

namespace {

std::vector<std::string> grid_5x5_lines{"12345", "..-..", ".[.].", "..+..",
                                        "ABCDE"};

}  // namespace

TEST_CASE("Test Grid Traversal", "[grid]") {
  // TODO: test -1 if off grid
  // TODO: test other directions
  // TODO: test custom movement function

  SECTION("walk_up") {
    auto grid = create_char_grid_from_string_rows(grid_5x5_lines.begin(),
                                                  grid_5x5_lines.end());
    const Grid_Location start_loc{2, 2};
    const int EXP_STEP_COUNT = 3;
    const std::string EXP_BUILT_STR = ".-3";
    std::string step_builder;

    auto walk_builder = [&](__attribute__((unused)) const auto& loc,
                            char value) { step_builder += value; };

    int actual_step_count = walk_up(grid, start_loc, walk_builder);

    CHECK(EXP_STEP_COUNT == actual_step_count);
    CHECK(EXP_BUILT_STR == step_builder);
  }

  SECTION("walk_up_until") {
    auto grid = create_char_grid_from_string_rows(grid_5x5_lines.begin(),
                                                  grid_5x5_lines.end());
    const Grid_Location start_loc{2, 2};
    const int EXP_STEP_COUNT = 1;
    const std::string EXP_BUILT_STR = ".";
    std::string step_builder;

    auto walk_builder = [&](__attribute__((unused)) const auto& loc,
                            char value) { step_builder += value; };

    auto pred = [](__attribute__((unused)) const auto& loc,
                   char value) -> bool { return value == '-'; };

    int actual_step_count = walk_up_until(grid, start_loc, walk_builder, pred);

    CHECK(EXP_STEP_COUNT == actual_step_count);
    CHECK(EXP_BUILT_STR == step_builder);
  }
}

// TEST_CASE("Grid Traversal Test.walk_up_to_edge") {
//   SECTION("returns false if location is off the grid") {
//     auto grid = create_char_grid_from_string_rows(grid_5x5_lines.begin(),
//                                                   grid_5x5_lines.end());
//     const Grid_Location start_loc{6, 7};

//     int steps = walk_up_to_edge(grid, start_loc);

//     REQUIRE(steps == -1);
//   }

//   SECTION("steps at top edge") {
//     auto grid = create_char_grid_from_string_rows(grid_5x5_lines.begin(),
//                                                   grid_5x5_lines.end());
//     const Grid_Location start_loc{2, 2};

//     int steps = walk_up_to_edge(grid, start_loc);

//     REQUIRE(steps == 2);
//   }
// }

// TEST_CASE("Grid Traversal Test.walk_up_until") {
//   SECTION("returns proper distance when value found") {
//     auto grid = create_char_grid_from_string_rows(grid_5x5_lines.begin(),
//                                                   grid_5x5_lines.end());
//     Grid_Location start_loc{2, 2};

//     const char value_to_find = '-';
//     auto find_minus = [=](__attribute__((unused)) const auto& loc, char
//     value) {
//       return value == value_to_find;
//     };

//     int steps = walk_up_until(grid, start_loc, find_minus);

//     REQUIRE(steps == 1);
//   }

//   SECTION("returns proper distance when value is on edge") {
//     auto grid = create_char_grid_from_string_rows(grid_5x5_lines.begin(),
//                                                   grid_5x5_lines.end());
//     Grid_Location start_loc{2, 2};

//     auto find_minus = [](__attribute__((unused)) const auto& loc, char value)
//     {
//       return value == '3';
//     };

//     int steps = walk_up_until(grid, start_loc, find_minus);

//     REQUIRE(steps == 2);
//   }

//   SECTION("returns -1 when value not found") {
//     auto grid = create_char_grid_from_string_rows(grid_5x5_lines.begin(),
//                                                   grid_5x5_lines.end());
//     Grid_Location start_loc{2, 2};

//     auto find_minus = [](__attribute__((unused)) const auto& loc, char value)
//     {
//       return value == 'z';
//     };

//     int steps = walk_up_until(grid, start_loc, find_minus);

//     REQUIRE(steps == -1);
//   }
// }
