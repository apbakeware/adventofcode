#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <string>
#include <vector>

#include "spdlog/spdlog.h"
#include "utils/grid/grid.h"
#include "utils/grid/grid_utils.h"

namespace {

// This input mimics how an input file would be read (from the top down).
const std::vector<std::string> INPUT{
    "123",
    "456",
    "789",
};

}  // namespace

TEST_CASE("Test default grid coordinate system", "[grid][grid.coordinates]") {
  SECTION("test coordinate system built from input array") {
    const auto& sut =
        create_char_grid_from_string_rows(INPUT.begin(), INPUT.end());

    CHECK('7' == sut.at({0, 0}));
    CHECK('1' == sut.at({2, 0}));
    CHECK('3' == sut.at({2, 2}));
    CHECK('9' == sut.at({0, 2}));
  }

  SECTION("test coordinate system built from reversed input array", "[grid]") {
    const auto& sut =
        create_char_grid_from_string_rows(INPUT.rbegin(), INPUT.rend());

    CHECK('1' == sut.at({0, 0}));
    CHECK('3' == sut.at({0, 2}));
    CHECK('7' == sut.at({2, 0}));
    CHECK('9' == sut.at({2, 2}));
  }

  SECTION(
      "test coordinate system built from input array and flipped vertically") {
    auto sut = create_char_grid_from_string_rows(INPUT.begin(), INPUT.end());

    sut = flip_vertical(sut);

    CHECK('9' == sut.at({0, 0}));
    CHECK('7' == sut.at({0, 2}));
    CHECK('3' == sut.at({2, 0}));
    CHECK('1' == sut.at({2, 2}));
  }

  SECTION(
      "test coordinate system built from input array and flipped "
      "horizontally") {
    auto sut = create_char_grid_from_string_rows(INPUT.begin(), INPUT.end());

    sut = flip_horizontal(sut);

    CHECK('1' == sut.at({0, 0}));
    CHECK('3' == sut.at({0, 2}));
    CHECK('7' == sut.at({2, 0}));
    CHECK('9' == sut.at({2, 2}));
  }
}