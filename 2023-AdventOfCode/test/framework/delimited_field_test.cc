#include "framework/delimited_field.h"

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <sstream>
#include <string>

#include "framework/data_loader.hpp"

TEST_CASE("Test Delimited_Field") {
  SECTION("test input string with no delimiter extracts all content") {
    std::string LINE = "asdf";
    std::istringstream instr(LINE);

    Delimited_Field<';'> sut;

    instr >> sut;

    REQUIRE(LINE == sut.field);
  }

  SECTION("test input string with one trailing delimiter") {
    std::string LINE = "asdf;";
    std::istringstream instr(LINE);

    Delimited_Field<';'> sut;

    instr >> sut;

    REQUIRE("asdf" == sut.field);
  }

  SECTION("test input string with one delimiter two fields") {
    std::string LINE = "asdf;7890";
    std::istringstream instr(LINE);

    Delimited_Field<';'> sut;
    Delimited_Field<';'> sut2;

    instr >> sut >> sut2;

    REQUIRE("asdf" == sut.field);
    REQUIRE("7890" == sut2.field);
  }

  SECTION("test input string with multiple fields") {
    using Field_Type = Delimited_Field<','>;
    std::string LINE = "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7";
    std::istringstream instr(LINE);
    std::vector<std::string> EXPECTED{"rn=1", "cm-",  "qp=3", "cm=2",
                                      "qp-",  "pc=4", "ot=9", "ab=5",
                                      "pc-",  "pc=6", "ot=7"};

    auto fields = load_structured_data<Field_Type>(instr);

    REQUIRE(EXPECTED.size() == fields.size());

    auto exp_it = EXPECTED.begin();
    auto field_it = fields.begin();

    while (exp_it != EXPECTED.end() && field_it != fields.end()) {
      REQUIRE(*exp_it == field_it->field);

      exp_it++;
      field_it++;
    }
  }
}