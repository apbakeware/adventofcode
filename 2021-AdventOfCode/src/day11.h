#ifndef __DAY11_H__
#define __DAY11_H__

#include <string>
#include <vector>

struct Day11Data {
   typedef std::string Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day11Part1 : public Day11Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day11Part2 : public Day11Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day11Spec {
   using Data_Spec = Day11Data;
   using Part_1 = Day11Part1;
   using Part_2 = Day11Part2;
   static constexpr const char * day() { return "Day11"; }
   static constexpr const char * data_file_name() { return "data/day11.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day11.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "1656"; }
   static constexpr const char * expected_sample_part_2_result() { return "195"; }
   static constexpr const char * solved_part_1_result() { return "1681"; }
   static constexpr const char * solved_part_2_result() { return "276"; }
};

#endif // __DAY11_H__
