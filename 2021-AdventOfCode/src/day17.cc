#include "day17.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day17Spec>();
}

const bool _was_registered = _do_registration();


enum Trajectory_Status {
   SHORT_OF_TARGET,
   TARGET_EXCEEDED,
   WITHIN_TARGET_BOUNDS
};

struct Projectile {
   int x;
   int y;
   int vel_x;
   int vel_y;
};

template<class Ostream>
Ostream & operator<<(Ostream & ostr, const Projectile & record ) {
   ostr << "[ x: " << record.x << "  y: " << record.y 
      << "  velx: " << record.vel_x << "  vely: " << record.vel_y << "]";
   return ostr; 
}

Trajectory_Status get_trajectory_result( const Projectile & projectile, const Target_Bounds & bounds ) {
   
   if( projectile.x > bounds.x_max || projectile.y < bounds.y_min ) {
      return Trajectory_Status::TARGET_EXCEEDED;

   } else if( projectile.x >= bounds.x_min && projectile.y <= bounds.y_max ) {
      return Trajectory_Status::WITHIN_TARGET_BOUNDS;
   
   }

   return Trajectory_Status::SHORT_OF_TARGET;
}

Projectile trajectory_frame( const Projectile & pos ) {

   int vel_x = pos.vel_x;
   if( vel_x > 0 ) {
      --vel_x;
   } else if( vel_x < 0 ) {
      ++vel_x;
   }

   return {
      pos.x + pos.vel_x,
      pos.y + pos.vel_y,
      vel_x,
      pos.vel_y - 1
   };
}

}





std::istream & operator>>
(std::istream & instr, Target_Bounds & bounds ) {
   // #include <sstream>

   const int ASCII_EQUAL = 61;  
   int value = instr.get();

   while( value != ASCII_EQUAL ) {
      value = instr.get();
   }

   instr >> bounds.x_min;
   instr.ignore(2);
   instr >> bounds.x_max;

   value = instr.get();
   while( value != ASCII_EQUAL ) {
      value = instr.get();
   }

   instr >> bounds.y_min;
   instr.ignore(2);
   instr >> bounds.y_max;

   instr.ignore();
   return instr;
}


std::string Day17Part1::name() const {
   return "Day17-Part1";
}

std::string Day17Part1::solve( const Input_Data_Collection & data ) {

   // TODO: cant just try .. there has to be an equation or shorter way....
   // Plots the known plot

   int max_y = -999;
   for( int velx = 0; velx < 1000; ++velx ) {
      for( int vely = 0; vely < 1000; ++vely ) {

         Projectile projectile = { 0, 0, velx, vely};
         Trajectory_Status status = Trajectory_Status::SHORT_OF_TARGET;

         while( status == Trajectory_Status::SHORT_OF_TARGET ) {
            projectile = trajectory_frame( projectile );
            status = get_trajectory_result( projectile, data );
         }

         if( status == Trajectory_Status::WITHIN_TARGET_BOUNDS ) {
            max_y = std::max( max_y, vely );
         }
      }
   }

   // TODO: track in the loop
   int max_height = 0;
   for(int cnt = 0 ; cnt <= max_y; ++cnt ) {
      max_height += cnt;
   }
   

   return std::to_string(max_height);
}


std::string Day17Part2::name() const {
   return "Day17-Part2";
}

std::string Day17Part2::solve( const Input_Data_Collection & data ) {
   int count = 0;
   for( int velx = 0; velx < 1000; ++velx ) {
      for( int vely = -1000; vely < 1000; ++vely ) {

         Projectile projectile = { 0, 0, velx, vely};
         Trajectory_Status status = Trajectory_Status::SHORT_OF_TARGET;

         while( status == Trajectory_Status::SHORT_OF_TARGET ) {
            projectile = trajectory_frame( projectile );
            status = get_trajectory_result( projectile, data );
         }

         if( status == Trajectory_Status::WITHIN_TARGET_BOUNDS ) {
            count++;
         }
      }
   }

   return std::to_string(count);
}

