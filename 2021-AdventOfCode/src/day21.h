#ifndef __DAY21_H__
#define __DAY21_H__

#include <string>
#include <vector>

struct Day21Data {
   // TODO: Set data type
   typedef int Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day21Part1 : public Day21Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day21Part2 : public Day21Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day21Spec {
   using Data_Spec = Day21Data;
   using Part_1 = Day21Part1;
   using Part_2 = Day21Part2;
   static constexpr const char * day() { return "Day21"; }
   static constexpr const char * data_file_name() { return "data/day21.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day21.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * expected_sample_part_2_result() { return "UNIMPLEMENTED"; }
   static constexpr const char * solved_part_1_result() { return "undefined"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __DAY21_H__
