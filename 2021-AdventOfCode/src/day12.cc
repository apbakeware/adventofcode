#include <sstream>
#include "day12.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"
#include "solver_registry.h"
#include "graph.h"
#include "graph_utils.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day12Spec>();
}

const bool _was_registered = _do_registration();

}


struct Big_Caves_Reentry_No_Small_Reentry {

   std::unordered_map<std::string, int> visitations;

   bool try_visit( const std::string & node ) {
      if( std::isupper(node.front())) {
         return true;
      }

      auto iter = visitations.find(node);
      if( iter == visitations.end() ) {
         auto insert = visitations.insert( {node, 0} );
         iter = insert.first;
         // TODO: check the 'bool' of the insert
      }

      auto & value = iter -> second;
      bool can_visit = value < 1;
      if( can_visit ) {
         value += 1;
      }

      return can_visit;
   }

   void remove_visit( const std::string & node ) {
      auto iter = visitations.find(node);
      if( iter == visitations.end() ) {
         return;
      }

      (iter -> second) -= 1;
   }

};

struct Big_Caves_Reentry_Small_Cave_Singe_Reentry {

   std::unordered_map<std::string, int> visitations;

   bool try_visit( const std::string & node ) {
      
      if( std::isupper(node.front())) {
         return true;
      }

      if( node == "start" ) {
         auto & count = get_visitation_count(node);
         if(count > 0) {
            return false;
         }
      }

      auto & value = get_visitation_count(node);
      bool can_visit = value < 1;
      can_visit |= (value == 1) && all_entries_less_than_2();
      if( can_visit ) {
         value += 1;
      }

      return can_visit;
   }

   void remove_visit( const std::string & node ) {
      auto iter = visitations.find(node);
      if( iter == visitations.end() ) {
         return;
      }

      (iter -> second) -= 1;
   }

   bool all_entries_less_than_2() const {
      const auto iter = std::find_if(
         visitations.begin(),
         visitations.end(),
         [](const auto & visitation ) {
            return visitation.second > 1;
         }
      );

      return iter == visitations.end();
   }

   int & get_visitation_count(const std::string & node) {
      auto iter = visitations.find(node);
      if( iter == visitations.end() ) {
         auto insert = visitations.insert( {node, 0} );
         iter = insert.first;
      }

      return iter -> second;
   }
};


std::string Day12Part1::name() const {
   return "Day12-Part1";
}

std::string Day12Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   Big_Caves_Reentry_No_Small_Reentry cave_visitation_policy;
   auto graph = create_graph_from_string_collection( data.begin(), data.end() );
   SPDLOG_INFO("Graph: {}", graph);

   const auto & all_paths = find_all_paths(graph, "start", "end", cave_visitation_policy);

   return std::to_string(all_paths.size());
}


std::string Day12Part2::name() const {
   return "Day12-Part2";
}

std::string Day12Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   Big_Caves_Reentry_Small_Cave_Singe_Reentry cave_visitation_policy;
   auto graph = create_graph_from_string_collection( data.begin(), data.end() );
   SPDLOG_INFO("Graph: {}", graph);

   const auto & all_paths = find_all_paths(graph, "start", "end", cave_visitation_policy);

   return std::to_string(all_paths.size());
}

