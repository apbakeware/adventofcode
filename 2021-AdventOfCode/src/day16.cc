#include <algorithm>
#include <memory>
#include <numeric>
#include <string>
#include <vector>
#include "day16.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/fmt.h"
#include "spdlog/fmt/ostr.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day16Spec>();
}

const bool _was_registered = _do_registration();

// Create a tree of nodes

struct Packet_Node {
   unsigned long version;
   unsigned long type_id;

   // Can be 1 reused packet
   unsigned long literal;

   bool is_literal() const { return type_id == 4; }

   std::vector<std::unique_ptr<Packet_Node>> sub_packets;

   // TODO: cache value
};

unsigned long node_value(const std::unique_ptr<Packet_Node> &node) {

   using Value_Vector = std::vector<unsigned long>;

   if (node->is_literal()) {
      return node->literal;
   }

   unsigned long result = 0;
   Value_Vector sub_packet_values(node->sub_packets.size());
   std::transform(node->sub_packets.begin(),
     node->sub_packets.end(),
     sub_packet_values.begin(),
     &node_value);

   switch (node->type_id) {
   case 0:
      result = std::accumulate(
        sub_packet_values.begin(), sub_packet_values.end(), 0ul);
      break;

   case 1:
      result = std::accumulate(sub_packet_values.begin(),
        sub_packet_values.end(),
        1ul,
        std::multiplies<unsigned long>());
      break;

   case 2:
      result =
        *std::min_element(sub_packet_values.begin(), sub_packet_values.end());
      break;

   case 3:
      result =
        *std::max_element(sub_packet_values.begin(), sub_packet_values.end());
      break;

   case 5:
      result =
        (sub_packet_values.front() > sub_packet_values.back()) ? 1ul : 0ul;
      break;

   case 6:
      result =
        (sub_packet_values.front() < sub_packet_values.back()) ? 1ul : 0ul;
      break;

   case 7:
      result =
        (sub_packet_values.front() == sub_packet_values.back()) ? 1ul : 0ul;
      break;

      // default:
      // todo throw
   }

   return result;
}

template <class Ostream>
Ostream &operator<<(Ostream &ostr, const Packet_Node &node) {
   ostr << "[version: " << node.version << "  type_id: " << node.type_id
        << "  literal: " << node.literal
        << "  SubPackets: " << node.sub_packets.size() << "]";

   for (const auto &sub : node.sub_packets) {
      ostr << "\n  " << *sub;
   }

   return ostr;
}

std::string hex_char_to_bin(char hex_c) {

   switch (hex_c) {
   case '0': return "0000";
   case '1': return "0001";
   case '2': return "0010";
   case '3': return "0011";
   case '4': return "0100";
   case '5': return "0101";
   case '6': return "0110";
   case '7': return "0111";
   case '8': return "1000";
   case '9': return "1001";
   case 'A': return "1010";
   case 'B': return "1011";
   case 'C': return "1100";
   case 'D': return "1101";
   case 'E': return "1110";
   case 'F': return "1111";
   }

   // todo: throw

   return "";
}

std::string hex_string_to_binary(const std::string &hexstring) {

   std::string bin_string;
   bin_string.reserve(hexstring.size() * 4);
   for (const auto hex_c : hexstring) {
      bin_string += hex_char_to_bin(hex_c);
   }
   return bin_string;
}

const size_t VER_LEN = 3;
const size_t TYPE_LEN = 3;
const size_t LITERAL_LEN = 5;

// TODO: replace fmt::print with SPDLOG_INFO
// TODO: refactor conditions into methods
Packet_Node *extract_packet(const std::string &bin_string, size_t &pos) {

   if (pos >= bin_string.size()) {
      // SPDLOG_INFO("Reached end of binary string -- pos: {}  length: {}", pos,
      // bin_string.size());
      return nullptr;
   }

   Packet_Node *node = new Packet_Node();

   fmt::print("Extacting packet from position: {}\n", pos);
   node->version = std::stoul(bin_string.substr(pos, VER_LEN), 0, 2);
   pos += VER_LEN;
   node->type_id = std::stoul(bin_string.substr(pos, VER_LEN), 0, 2);
   pos += VER_LEN;

   fmt::print("Node -- version: {}  type: {}\n", node->version, node->type_id);
   if (node->is_literal()) {
      fmt::print("  Found literal type, extracting literal\n");

      std::string builder;
      char prefix;

      do {
         prefix = bin_string[pos];
         builder += bin_string.substr(pos + 1, 4);
         pos += LITERAL_LEN;
      } while (prefix != '0');

      fmt::print("Extracted literal string: {}\n", builder);
      node->literal = std::stoul(builder, 0, 2);

   } else {
      const char op_mode = bin_string[pos];
      pos += 1;
      if (op_mode == '0') {
         const auto &length_bits = bin_string.substr(pos, 15);
         pos += 15;
         const auto end_of_subpkts = pos + std::stoul(length_bits, 0, 2);

         while (pos < end_of_subpkts) {
            auto subpkt =
              std::unique_ptr<Packet_Node>(extract_packet(bin_string, pos));
            node->sub_packets.push_back(std::move(subpkt));
            fmt::print("extracted subpacket -- bits remaining: {}\n",
              (end_of_subpkts - pos));
         }

         int num_bits = std::stoi(length_bits, 0, 2);
         fmt::print("  Found length bits operator -- length: {}", num_bits);

         // extract_packet while pos not reached

      } else if (op_mode == '1') {

         const auto &pkt_count_bits = bin_string.substr(pos, 11);
         pos += 11;
         int num_pkts = std::stoul(pkt_count_bits, 0, 2);
         fmt::print("  Found num pkts operator -- length: {}", num_pkts);
         while (num_pkts-- > 0) {
            auto subpkt =
              std::unique_ptr<Packet_Node>(extract_packet(bin_string, pos));
            node->sub_packets.push_back(std::move(subpkt));
            fmt::print("extracted subpacket -- pkts remaining: {}\n", num_pkts);
         }
      }
   }

   fmt::print("\nNode: {}\n\n", *node);

   return node;
}

int total_version_values(const Packet_Node &node) {
   int total = node.version;

   std::for_each(node.sub_packets.begin(),
     node.sub_packets.end(),
     [&](const auto &pkt) { total += total_version_values(*pkt); });

   return total;
}

} // namespace

std::string Day16Part1::name() const { return "Day16-Part1"; }

std::string Day16Part1::solve(const Input_Data_Collection &data) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   // TODO: maybe try and decode as read?
   auto bin_packet = hex_string_to_binary(data.front());
   size_t index = 0;

   fmt::print(" BIN PACKET: {}\n", bin_packet);

   // stoi --> string to integer
   std::unique_ptr<Packet_Node> node(extract_packet(bin_packet, index));
   return std::to_string(total_version_values(*node));
}

std::string Day16Part2::name() const { return "Day16-Part2"; }

std::string Day16Part2::solve(const Input_Data_Collection &data) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   auto bin_packet = hex_string_to_binary(data.front());
   size_t index = 0;

   fmt::print(" BIN PACKET: {}\n", bin_packet);

   // stoi --> string to integer
   std::unique_ptr<Packet_Node> node(extract_packet(bin_packet, index));
   return std::to_string(node_value(node));
}
