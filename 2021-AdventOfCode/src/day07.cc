#include <algorithm>
#include <sstream>
#include "day07.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day07Spec>();
}

const bool _was_registered = _do_registration();

}

std::string Day07Part1::name() const {
   return "Day07-Part1";
}

// again need ints from CSV
std::string Day07Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   
   std::string line = data.front();
   std::replace(line.begin(), line.end(), ',', ' ');
   std::istringstream instr( line );
   std::vector<int> locations( (std::istream_iterator<int>(instr)), std::istream_iterator<int>());
   std::sort(locations.begin(), locations.end());

   const auto min = locations.front();
   const auto max = locations.back();
   
   int lowest_cost = std::numeric_limits<int>::max();
   for(auto position = min; position <= max; ++position) {

      int fuel_cost = 0;
      for(auto location : locations) {
         fuel_cost += std::abs(position - location);
      }

      lowest_cost = std::min(lowest_cost, fuel_cost);

   }

   return std::to_string(lowest_cost);
}


std::string Day07Part2::name() const {
   return "Day07-Part2";
}

std::string Day07Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   std::string line = data.front();
   std::replace(line.begin(), line.end(), ',', ' ');
   std::istringstream instr( line );
   std::vector<int> locations( (std::istream_iterator<int>(instr)), std::istream_iterator<int>());
   std::sort(locations.begin(), locations.end());

   const auto min = locations.front();
   const auto max = locations.back();
   
   int lowest_cost = std::numeric_limits<int>::max();
   for(auto position = min; position <= max; ++position) {

      int fuel_cost = 0;
      for(auto location : locations) {
         int distance = std::abs(position - location);
         // Is there a better way than a loop  preferably cache the cost of a distance and look it up
         for( int step = 1; step <= distance; ++step) {
            fuel_cost += step;
         }
         
      }

      lowest_cost = std::min(lowest_cost, fuel_cost);

   }

   return std::to_string(lowest_cost);
}

