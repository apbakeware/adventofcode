#ifndef __TABLE_WRITER_RESULT_PROCESSOR_H__
#define __TABLE_WRITER_RESULT_PROCESSOR_H__

#include <iostream>
#include "i_result_processor.h"
#include "solver_result.h"

/**
 * @brief Create a ascii table and write it to the stream.
 * 
 */
class Table_Writer_Solver_Result_Processor
  : public IResult_Processor<Solver_Result> {
public:

   explicit Table_Writer_Solver_Result_Processor( const std:: string & title, std::ostream & outstr = std::cout );

   virtual ~Table_Writer_Solver_Result_Processor() = default;

   virtual void process( const Solver_Result & result ) override;

private: 

   std::ostream & outstr;

};

#endif // __TABLE_WRITER_RESULT_PROCESSOR_H__