#include "grid.h"

Grid_Location Grid_Location::up(const Grid_Location &loc, ssize_t dist) {
   return {loc.row + dist, loc.col};
}

Grid_Location Grid_Location::right(const Grid_Location &loc, ssize_t dist) {
   return {loc.row, loc.col + dist};
}

Grid_Location Grid_Location::down(const Grid_Location &loc, ssize_t dist) {
   return {loc.row - dist, loc.col};
}

Grid_Location Grid_Location::left(const Grid_Location &loc, ssize_t dist) {
   return {loc.row, loc.col - dist};
}

bool operator<(const Grid_Location &lhs, const Grid_Location &rhs) {
   return std::tie(lhs.row, lhs.col) < std::tie(rhs.row, rhs.col);
}

bool operator==(const Grid_Location &lhs, const Grid_Location &rhs) {
   return std::tie(lhs.row, lhs.col) == std::tie(rhs.row, rhs.col);
}

bool operator!=(const Grid_Location &lhs, const Grid_Location &rhs) {
   return std::tie(lhs.row, lhs.col) != std::tie(rhs.row, rhs.col);
}

std::istream &operator>>(std::istream &istr, Grid_Location record) {
   istr >> record.row >> record.col;
   return istr;
}
