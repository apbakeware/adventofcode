#include "bingo_card.h"

std::ostream & operator<<( std::ostream & ostr, const Bingo_Cell & obj ) {
   ostr << "{" << obj.value << ", " << obj.marked << "}";
   return ostr;
}


Bingo_Card::Bingo_Card( Bingo_Card::Card_Grid && card ) 
   : card( card ), card_has_bingo( false ) {

   setup_location_cache();
}

bool Bingo_Card::value_called( int number ) {
   SPDLOG_INFO("Called value: {}", number);
   if( location_cache.count( number ) == 0 ) {
      SPDLOG_DEBUG("Called number {} is not on board", number);
      return false;
   }
   SPDLOG_DEBUG("Called number {} was found on board", number);
   
   const auto location_on_card = location_cache.at(number);
   auto cell = card.at(location_on_card);
   cell.marked = true;
   card.set(location_on_card, cell);

   check_for_bingo();

   return has_bingo();
}

bool Bingo_Card::has_bingo() const {
   return card_has_bingo;
}


void Bingo_Card::setup_location_cache() {
   location_cache.clear();
   card.for_each_cell( [=](const auto & location, const auto & value) {
      location_cache[value.value] = location;
   });
}

void Bingo_Card::check_for_bingo() {

   // TODO: figure out location / grid operations to return collection or for locations, etc
   // This loop is repeated all over

   // Check rows
   for( unsigned int row = 0; row < card.number_of_rows(); ++row) {
      bool row_bingo = true;
      for( unsigned int col = 0; col < card.number_of_cols(); ++col ) {

         const auto & cell = card.at( {row, col} );
         row_bingo &= cell.marked;
      }

      if( row_bingo ) {
         SPDLOG_INFO("Row {} has bingo", row);
         card_has_bingo = true;
         return;
      }
   }

   // Check columns
   for( unsigned int col = 0; col < card.number_of_cols(); ++col ) {
      bool col_bingo = true;
      for( unsigned int row = 0; row < card.number_of_rows(); ++row) {
         const auto & cell = card.at( {row, col} );
         col_bingo &= cell.marked;
      }

      if( col_bingo ) {
         SPDLOG_INFO("Col {} has bingo", col);
         card_has_bingo = true;
         return;
      }
   }
}

std::ostream & operator<<( std::ostream & ostr, const Bingo_Card & obj ) {
   ostr << obj.card;
   return ostr;
}
