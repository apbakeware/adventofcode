#ifndef __DAY04_H__
#define __DAY04_H__

#include <queue>
#include <string>
#include <vector>

#include "bingo_card.h"



struct Day04Data {
   typedef std::string Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day04Part1 : public Day04Data {
public:
   std::string name() const;

   std::string solve(const Input_Data_Collection &data);

private:
   
   std::queue<int> numbers_to_call;
   std::vector<Bingo_Card> bingo_cards;
};

class Day04Part2 : public Day04Data {
public:
   std::string name() const;

   std::string solve(const Input_Data_Collection &data);


private:
   std::queue<int> numbers_to_call;
   std::vector<Bingo_Card> bingo_cards;
};

struct Day04Spec {
   using Data_Spec = Day04Data;
   using Part_1 = Day04Part1;
   using Part_2 = Day04Part2;
   static constexpr const char *day() { return "Day04"; }
   static constexpr const char *data_file_name() { return "data/day04.txt"; }
   static constexpr const char *sample_data_file_name() {
      return "data/day04.sample.txt";
   }
   static constexpr const char *expected_sample_part_1_result() {
      return "4512";
   }
   static constexpr const char *expected_sample_part_2_result() {
      return "1924";
   }
   static constexpr const char *solved_part_1_result() { return "5685"; }
   static constexpr const char *solved_part_2_result() { return "21070"; }
};

#endif // __DAY04_H__
