#ifndef __DAY07_H__
#define __DAY07_H__

#include <string>
#include <vector>

struct Day07Data {
   typedef std::string Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day07Part1 : public Day07Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day07Part2 : public Day07Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day07Spec {
   using Data_Spec = Day07Data;
   using Part_1 = Day07Part1;
   using Part_2 = Day07Part2;
   static constexpr const char * day() { return "Day07"; }
   static constexpr const char * data_file_name() { return "data/day07.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day07.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "37"; }
   static constexpr const char * expected_sample_part_2_result() { return "168"; }
   static constexpr const char * solved_part_1_result() { return "347509"; }
   static constexpr const char * solved_part_2_result() { return "98257206"; }
};

#endif // __DAY07_H__
