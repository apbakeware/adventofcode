#include "day25.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day25Spec>();
}

const bool _was_registered = _do_registration();

}

std::string Day25Part1::name() const {
   return "Day25-Part1";
}

std::string Day25Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}


std::string Day25Part2::name() const {
   return "Day25-Part2";
}

std::string Day25Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}

