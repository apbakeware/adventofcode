#ifndef __DAY14_H__
#define __DAY14_H__

#include <iosfwd>
#include <unordered_map>
#include <string>
#include <vector>

struct Polymer_Generator {
   std::string polymer_sequence;

   // What nn becomes nc .. etc
   std::unordered_map<std::string, char> pair_insertions;
};

std::istream& operator>>(std::istream & instr, Polymer_Generator & record );


struct Day14Data {
   typedef Polymer_Generator Input_Data_Type;
   
   // todo: Change the loader type to not spec collection
   // to support collection or structure loading
   typedef Polymer_Generator Input_Data_Collection;
};

class Day14Part1 : public Day14Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day14Part2 : public Day14Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day14Spec {
   using Data_Spec = Day14Data;
   using Part_1 = Day14Part1;
   using Part_2 = Day14Part2;
   static constexpr const char * day() { return "Day14"; }
   static constexpr const char * data_file_name() { return "data/day14.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day14.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "1588"; }
   static constexpr const char * expected_sample_part_2_result() { return "2188189693529"; }
   static constexpr const char * solved_part_1_result() { return "2447"; }
   static constexpr const char * solved_part_2_result() { return "undefined"; }
};

#endif // __DAY14_H__
