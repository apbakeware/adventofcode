#ifndef __DATA_LOADER_H__
#define __DATA_LOADER_H__

#include <iterator>
#include <vector>

/**
 * @brief Load formatted data from an input stream into a collection
 * of fixed data records.
 *
 * @tparam Data_T Formatted data structure supporing operator>>
 * @param instream Stream to extract structured data from
 * @return std::vector<Data_T> Vector of read structures
 */
template <class Data_T>
std::vector<Data_T> load_structured_data(std::istream &instream) {
   return std::vector<Data_T>(
     std::istream_iterator<Data_T>(instream), std::istream_iterator<Data_T>());
}

#endif // __DATA_LOADER_H__