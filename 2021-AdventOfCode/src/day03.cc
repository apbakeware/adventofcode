#include <algorithm>
#include <bitset>
#include <string>
#include <iostream>
#include "day03.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

using Diag_Code = std::bitset<32>;
using Diag_Codes = std::vector<Diag_Code>;

bool _do_registration() {
   return Solver_Registry::register_solver<Day03Spec>();
}

const bool _was_registered = _do_registration();

template <class Iter_T>
Diag_Codes create_diag_codes_from_strings(Iter_T begin, Iter_T end) {

   const auto data_size = std::distance(begin, end);
   Diag_Codes diag_codes(data_size);
   std::transform(begin, end, diag_codes.begin(), [](const auto &row) {
      return Diag_Code(row);
   });

   return diag_codes;
}

template <class Iter_T>
bool most_common_bit_is_1(Iter_T codes_begin, Iter_T codes_end, size_t index) {

   const size_t count_of_1 = std::count_if(
     codes_begin, codes_end, [=](const auto &row) { return row.test(index); });

   const size_t count_of_0 = std::distance(codes_begin, codes_end) - count_of_1;
   return count_of_1 >= count_of_0;
}

template <class Iter_T>
Iter_T filter_keep_index_value_is(
  Iter_T begin, Iter_T end, size_t index, bool desired) {

   return std::remove_if(
     begin, end, [&](const auto &data) { return data.test(index) == desired; });
}

} // namespace

std::string Day03Part1::name() const { return "Day03-Part1"; }

std::string Day03Part1::solve(const Input_Data_Collection &data) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   Diag_Code gamma;
   Diag_Code epsilon;
   const auto &diag_codes =
     create_diag_codes_from_strings(data.begin(), data.end());

   const auto line_len = data.front().size();
   for (size_t idx = 0; idx < line_len; ++idx) {

      if (most_common_bit_is_1(diag_codes.begin(), diag_codes.end(), idx)) {
         gamma.set(idx, true);
         epsilon.set(idx, false);
      } else {
         gamma.set(idx, false);
         epsilon.set(idx, true);
      }
   }

   const auto result = gamma.to_ulong() * epsilon.to_ulong();
   return std::to_string(result);
}

std::string Day03Part2::name() const { return "Day03-Part2"; }

std::string Day03Part2::solve(const Input_Data_Collection &data) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   auto oxygen_diag_codes =
     create_diag_codes_from_strings(data.begin(), data.end());

   auto oxygen_end_iter = oxygen_diag_codes.end();
   auto oxygen_code_count =
     std::distance(oxygen_diag_codes.begin(), oxygen_end_iter);

   auto scrubber_diag_codes = oxygen_diag_codes;
   auto scrubber_end_iter = scrubber_diag_codes.end();
   auto scrubber_code_count =
     std::distance(scrubber_diag_codes.begin(), scrubber_end_iter);

   size_t check_idx = data.front().size() - 1;
   while (oxygen_code_count > 1 || scrubber_code_count > 1) {

      if (oxygen_code_count > 1) {
         auto most_common_bit = most_common_bit_is_1(
           oxygen_diag_codes.begin(), oxygen_end_iter, check_idx);
         oxygen_end_iter = filter_keep_index_value_is(oxygen_diag_codes.begin(),
           oxygen_end_iter,
           check_idx,
           most_common_bit);
         oxygen_code_count =
           std::distance(oxygen_diag_codes.begin(), oxygen_end_iter);
      }

      if (scrubber_code_count > 1) {
         auto most_common_bit = !most_common_bit_is_1(
           scrubber_diag_codes.begin(), scrubber_end_iter, check_idx);
         scrubber_end_iter =
           filter_keep_index_value_is(scrubber_diag_codes.begin(),
             scrubber_end_iter,
             check_idx,
             most_common_bit);
         scrubber_code_count =
           std::distance(scrubber_diag_codes.begin(), scrubber_end_iter);
         ;
      }
      --check_idx;
   }

   const auto result = oxygen_diag_codes.front().to_ulong() *
                       scrubber_diag_codes.front().to_ulong();
   return std::to_string(result);
}
