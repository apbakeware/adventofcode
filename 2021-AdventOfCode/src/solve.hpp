#ifndef __SOLVE_H__
#define __SOLVE_H__

#include <chrono>
#include <fstream>
#include <string>
#include "spdlog/spdlog.h"
#include "spdlog/fmt/fmt.h"
#include "data_loader.hpp"
#include "solver_result.h"

namespace {

using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::milliseconds;


template<class Result_T>
struct Data_With_Duration {
   const std::chrono::milliseconds duration;
   const Result_T data;
};


template<class Input_T>
struct Data_Loader {

   using Result = Data_With_Duration<Input_T>;

   static Result load( std::istream & instr ) {
   
      SPDLOG_INFO("Loading single input record");
      auto start_time = high_resolution_clock::now();
      Input_T input_data;
      instr >> input_data;
      auto end_time = high_resolution_clock::now();
      auto load_duration = duration_cast<milliseconds>(end_time - start_time);
      SPDLOG_INFO("Done loading data");

      return { load_duration, std::move(input_data) };
   }

};

// TODO: need to specialize for other classes?
template<typename Input_T,typename Alloc>
struct Data_Loader<std::vector<Input_T, Alloc>> {

   using Input_Collection = std::vector<Input_T, Alloc>;
   using Result = Data_With_Duration<Input_Collection>;

   static Result load( std::istream & instr ) {
   
      SPDLOG_INFO("Loading single input record");
      auto start_time = high_resolution_clock::now();
      const auto input_data = load_structured_data<Input_T>(instr);
      auto end_time = high_resolution_clock::now();
      auto load_duration = duration_cast<milliseconds>(end_time - start_time);
      SPDLOG_INFO("Done loading data");

      return { load_duration, std::move(input_data) };
   }

};

template<class Input_Data_T>
Data_With_Duration<Input_Data_T> 
load_data_set(const std::string & fname ) {

   std::ifstream instream(fname);
   if (!instream) {
     SPDLOG_ERROR("Failed to open input file: {}", fname);
      throw std::runtime_error("Failed to open input file");
   }

   return Data_Loader<Input_Data_T>::load( instream );
}

template <class Solver_T> 
Data_With_Duration<std::string> run_solver
( Solver_T & solver, const typename Solver_T::Input_Data_Collection & data ) {

   SPDLOG_INFO("Solving {}", solver.name() );
   const auto start_time = high_resolution_clock::now();
   const auto solution = solver.solve(data);
   const auto end_time = high_resolution_clock::now();
   SPDLOG_INFO("Done solving");

   return { 
      duration_cast<milliseconds>(end_time - start_time), 
      solution 
   };
}

} // namespace

template <class Day_Spec_T> Expected_Solver_Result solve_and_evaluate_sample_cases() {

   using Input_Data_Type = typename Day_Spec_T::Data_Spec::Input_Data_Collection;
   using Part_1_Solver = typename Day_Spec_T::Part_1;
   using Part_2_Solver = typename Day_Spec_T::Part_2;

   const std::string data_file_name( Day_Spec_T::sample_data_file_name() ); 
   Part_1_Solver solver_part_1;
   Part_2_Solver solver_part_2;

   const auto &input = load_data_set<Input_Data_Type>( data_file_name );
   const auto &part_1_solution = run_solver( solver_part_1, input.data );
   const auto &part_2_solution = run_solver( solver_part_2, input.data );
   
   SPDLOG_INFO("{}: {}", solver_part_1.name(), part_1_solution.data);
   SPDLOG_INFO("{}: {}", solver_part_2.name(), part_2_solution.data);

   return {Day_Spec_T::day(),
     input.duration,
     part_1_solution.duration,
     part_2_solution.duration,
     Day_Spec_T::expected_sample_part_1_result(),
     part_1_solution.data,
     Day_Spec_T::expected_sample_part_2_result(),
     part_2_solution.data};

}

template <class Day_Spec_T> Expected_Solver_Result refactor_and_ensure_result() {

   using Input_Data_Type = typename Day_Spec_T::Data_Spec::Input_Data_Collection;
   using Part_1_Solver = typename Day_Spec_T::Part_1;
   using Part_2_Solver = typename Day_Spec_T::Part_2;

   const std::string data_file_name( Day_Spec_T::data_file_name() ); 
   Part_1_Solver solver_part_1;
   Part_2_Solver solver_part_2;

   const auto &input = load_data_set<Input_Data_Type>( data_file_name );
   const auto &part_1_solution = run_solver( solver_part_1, input.data );
   const auto &part_2_solution = run_solver( solver_part_2, input.data );
   
   SPDLOG_INFO("{}: {}", solver_part_1.name(), part_1_solution.data);
   SPDLOG_INFO("{}: {}", solver_part_2.name(), part_2_solution.data);

   return {Day_Spec_T::day(),
     input.duration,
     part_1_solution.duration,
     part_2_solution.duration,
     Day_Spec_T::solved_part_1_result(),
     part_1_solution.data,
     Day_Spec_T::solved_part_2_result(),
     part_2_solution.data};

}

template <class Day_Spec_T> Solver_Result solve() {

   using Input_Data_Type = typename Day_Spec_T::Data_Spec::Input_Data_Collection;
   using Part_1_Solver = typename Day_Spec_T::Part_1;
   using Part_2_Solver = typename Day_Spec_T::Part_2;

   const std::string data_file_name( Day_Spec_T::data_file_name() ); 
   Part_1_Solver solver_part_1;
   Part_2_Solver solver_part_2;

   const auto &input = load_data_set<Input_Data_Type>( data_file_name );
   const auto &part_1_solution = run_solver( solver_part_1, input.data );
   const auto &part_2_solution = run_solver( solver_part_2, input.data );
   
   SPDLOG_INFO("{}: {}", solver_part_1.name(), part_1_solution.data);
   SPDLOG_INFO("{}: {}", solver_part_2.name(), part_2_solution.data);

   return {Day_Spec_T::day(),
     input.duration,
     part_1_solution.duration,
     part_2_solution.duration,
     part_1_solution.data,
     part_2_solution.data};
}

#endif // __SOLVE_H__