#include <algorithm>
#include <iostream>
#include "day02.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day02Spec>();
}

const bool _was_registered = _do_registration();

}

std::istream & operator>>(std::istream & instr, Control_Command & record ) {
   instr >> record.direction >> record.distance;
   return instr;
}

std::string Day02Part1::name() const {
   return "Day02-Part1";
}

std::string Day02Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   
   int depth = 0;
   int horizontal = 0;

   std::for_each( 
      data.begin(), 
      data.end(),
      [&](const auto & command ) {
         if( command.direction == "forward" ) {
            horizontal += command.distance;
         } else if( command.direction == "down" ) {
            depth += command.distance;
         } else if( command.direction == "up" ) {
            depth -= command.distance;
         }
      }
   );

   int result = depth * horizontal;
   return std::to_string(result);
}


std::string Day02Part2::name() const {
   return "Day02-Part2";
}

std::string Day02Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   
   int aim = 0;
   int horizontal = 0;
   int depth = 0;
   std::for_each( 
      data.begin(), 
      data.end(),
      [&](const auto & command ) {
         if( command.direction == "forward" ) {
            horizontal += command.distance;
            depth += aim * command.distance;
         } else if( command.direction == "down" ) {
            aim += command.distance;
         } else if( command.direction == "up" ) {
            aim -= command.distance;
         }
      }
   );

   int result = depth * horizontal;
   return std::to_string(result);
}

