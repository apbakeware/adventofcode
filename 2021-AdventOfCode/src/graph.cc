#include <sstream>
#include "graph.h"
#include "spdlog/spdlog.h"

// TODO: better way than replace and going to istr
std::istream & operator>>(std::istream & instr, Graph_Input_Record & rec ) {

   std::string working;
   instr >> working;

   std::replace(working.begin(), working.end(), '-', ' ');
   std::istringstream tmpstr( working );
   tmpstr >> rec.origin >> rec.destination;

   return instr; 
}

void Graph::add_connection(const std::string & orig, const std::string & dest) {
      SPDLOG_DEBUG("Adding connection {} to {}", orig, dest);
      if( !graph.count(orig) ) {
         graph[orig] = { dest };
      } else {
         graph[orig].push_back( dest );
      }

      if( !graph.count(dest) ) {
         graph[dest]= { orig };
      } else {
         graph[dest].push_back( orig );
      }
   }