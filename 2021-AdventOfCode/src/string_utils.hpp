#ifndef __STRING_UTILS_H__
#define __STRING_UTILS_H__

#include <algorithm>
#include <functional>
#include <string>

template<class Str_Iter_T>
struct Str_Int_Extraction {

   operator bool() const {
      return extraction_begin != extraction_end;
   }

   int extracted;
   Str_Iter_T extraction_begin;
   Str_Iter_T extraction_end;
};

template<class Str_Iter_T>
Str_Iter_T find_first_digit( Str_Iter_T begin, Str_Iter_T end ) {
   return std::find_if(
      begin, 
      end, 
      [](const auto value) {
         return std::isdigit(value);
      }
   );
}

template<class Str_Iter_T>
Str_Iter_T find_first_nondigit( Str_Iter_T begin, Str_Iter_T end ) {
   return std::find_if(
      begin, 
      end, 
      [](const auto value) {
         return !std::isdigit(value);
      }
   );
}

template<class Str_Iter_T>
std::pair<Str_Iter_T, Str_Iter_T> find_first_digit_sequence
(Str_Iter_T begin, Str_Iter_T end) {

   auto dig_iter = find_first_digit( begin, end );
   return {
      dig_iter,
      find_first_nondigit( dig_iter, end )
   };
}

template<class Str_Iter_T>
Str_Int_Extraction<Str_Iter_T> extract_first_int_seqeuence_from_string
( Str_Iter_T begin, Str_Iter_T end ) {

   const auto & digit_seq = find_first_digit_sequence( begin, end );
   if( digit_seq.first != digit_seq.second ) {

      std::cout << "Found digit sequence\n";
      const std::string extraction(digit_seq.first, digit_seq.second);
      return {
         std::stoi(extraction),
         digit_seq.first, 
         digit_seq.second
      };
   }

   return { 0, end, end };
}

template<class Str_Iter_T>
Str_Int_Extraction<Str_Iter_T> extract_last_int_seqeuence_from_string
( Str_Iter_T begin, Str_Iter_T end ) {

   auto extraction = find_first_digit_sequence(begin, end);
   while(extraction) {
      auto prev = extraction;
      extraction = find_first_digit_sequence(begin, end);
      if(!extraction) {
         extraction = std::move(prev);
      }
   }

   return extraction;
}

#endif // __STRING_UTILS_H__