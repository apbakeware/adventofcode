#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <sstream>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include "day18.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"
#include "string_utils.hpp"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day18Spec>();
}

const bool _was_registered = _do_registration();

// got parsing fix from: https://itnext.io/modern-c-in-advent-of-code-day18-54942485460b

// Binary tree
// number acts as implice surrounding '[]' an and left, right value
class Snailfish_Number {
public:
   
   static Snailfish_Number * add
   ( std::unique_ptr<Snailfish_Number> && left, std::unique_ptr<Snailfish_Number> && right ) {
      Snailfish_Number * snf = new Snailfish_Number();
      snf -> left = std::move(left);
      snf -> right = std::move(right);
      return snf;
   }

   Snailfish_Number(int value = 0) 
      : value(value), left(nullptr), right(nullptr) {
   }

   Snailfish_Number(Snailfish_Number && orig) = default;

   bool has_left() const {
      return left != nullptr;
   }

   bool has_right() const {
      return right != nullptr;
   }

   bool is_terminal() const {
      return !(has_left() || has_right());
   }

   friend std::istream& operator>>
   (std::istream & instr, Snailfish_Number & num );

   friend std::ostream& operator<<
   (std::ostream & ostr, const Snailfish_Number & num);

   bool try_explode() {

      bool result = false;
      auto exploding_node = find_exploding_node( *left, 1);
      if( exploding_node == nullptr ) {
         exploding_node = find_exploding_node( *right, 1 );
      }

      if( exploding_node != nullptr ) {
         const auto explosion_lf = find_left_right_explosion( *exploding_node );

         if( explosion_lf.first != nullptr ) {
            explosion_lf.first -> value += exploding_node -> left -> value;
         }

         if( explosion_lf.second != nullptr ) {
            explosion_lf.second -> value += exploding_node -> right -> value;
         }

         exploding_node -> value = 0;
         (exploding_node -> left).reset();
         (exploding_node -> right).reset();

         result = true;
      }

      return result;
   }

   bool try_split() {

      std::stack<Snailfish_Number*> nodes;      
      nodes.push( this );
      while(!nodes.empty()) {
         auto current = nodes.top();
         nodes.pop();
         
         // Add children for traversal
         if( current -> has_right() ) {
            nodes.push( current -> right.get() );
         }

         if( current -> has_left() ) {
            nodes.push( current -> left.get() );
         }

         // Process current
         if( current -> is_terminal() ) {
         
            if( current -> value > 9 ) {
               int split_value_l = (current -> value) / 2;
               int split_value_r = (current -> value) & 0x1 ? split_value_l + 1 : split_value_l;

               current -> value = 0;
               current -> left = std::make_unique<Snailfish_Number>(split_value_l);
               current -> right = std::make_unique<Snailfish_Number>(split_value_r);
               return true;
            }
         }
      }

      return false;

   }

   void reduce() {

      bool keep_reducing = true; 
      do {
         keep_reducing = try_explode() || try_split();
      } while(keep_reducing);
   }

   // Destructive
   int magnitude() {
      while(!is_terminal()) {
         std::stack<Snailfish_Number*> nodes;      
         nodes.push( this );
         while(!nodes.empty()) {
            auto current = nodes.top();
            nodes.pop();

            if( current -> has_left() && current -> left -> is_terminal()  &&
                current -> has_right() && current -> right -> is_terminal() ) {

               current -> value = current -> left -> value * 3 +
                  current -> right -> value * 2;

               current -> left.reset();
               current -> right.reset();

            } else {
               if( current -> has_right() ) {
                  nodes.push( current -> right.get() );
               }

               if( current -> has_left() ) {
                  nodes.push( current -> left.get() );
               }
            }
         }
      }

      return this -> value;
   }

private:

   // nullptr if not found
   // Return the node which has 2 terminal children to explode
   Snailfish_Number * find_exploding_node( Snailfish_Number & snf, int depth ) {

      if( depth == 4 &&
          snf.has_left() && snf.left -> is_terminal()  &&
          snf.has_right() && snf.right -> is_terminal() ) {
         
         return &snf;

      } else {
         
         if( snf.has_left() ) {
            auto explode_left = find_exploding_node( *(snf.left), depth + 1 );
            if(explode_left != nullptr) {
               return explode_left;
            }
         }

         if( snf.has_right() ) {
            auto explode_right = find_exploding_node( *(snf.right), depth + 1 );
            if(explode_right != nullptr) {
               return explode_right;
            }
         }
      }

      return nullptr;
   }

   // Non recusrive tree traversal: https://stackoverflow.com/questions/5278580/non-recursive-depth-first-search-algorithm
   std::pair<Snailfish_Number*, Snailfish_Number*> find_left_right_explosion
   (const Snailfish_Number & exploding_node) {

      bool exploding_node_reached = false;
      std::pair<Snailfish_Number*, Snailfish_Number*> result = {nullptr, nullptr};
      std::stack<Snailfish_Number*> nodes;
      
      nodes.push( this );
      while(!nodes.empty()) {
         auto current = nodes.top();
         nodes.pop();
         
         // Dont traverse the exploding node's children
         if( current == &exploding_node ) {
            exploding_node_reached = true;
            continue;
         }

         // Add children for traversal
         if( current -> has_right() ) {
            nodes.push( current -> right.get() );
         }

         if( current -> has_left() ) {
            nodes.push( current -> left.get() );
         }

         // Process current
         if( current -> is_terminal() ) {

            // Find first value node after the explosgion
            if(exploding_node_reached) {
               result.second = current;
               return result;
            } else {
               result.first = current;
            }
         }

      }

      return result;
   }


   int value;
   std::unique_ptr<Snailfish_Number> left;
   std::unique_ptr<Snailfish_Number> right;
};

std::istream & operator>>
(std::istream & instr, Snailfish_Number & tree) {

   char sep;

   // Extract the opening bracket for the node
   if (!(instr >> sep)) return instr;

   if( sep != '[' ) {
      throw std::runtime_error("Failed to get expected start of node bracket '['");
   }

   // todo; check for stream failure
   tree.left = std::make_unique<Snailfish_Number>();
   if(instr.peek() == '[') {
      instr >> *(tree.left);
   } else {
      instr >> tree.left->value;
   }

   // Extract the separator for the node
   if (!(instr >> sep) || sep != ',') {
      throw std::runtime_error("Failed to get expected node delimiter ','");
   }

   // todo; check for failure or ,
   tree.right = std::make_unique<Snailfish_Number>();
   if(instr.peek() == '[') {
      instr >> *(tree.right);
   } else {
      instr >> tree.right->value;
   }

   // Extract the closing bracket for the node
   if (!(instr >> sep) || sep != ']') {
      throw std::runtime_error("Failed to get expected end of node bracket ']'");
   }
    
   return instr;
}

std::ostream& operator<<
(std::ostream & ostr, const Snailfish_Number & num) {

   if(num.is_terminal()) {
      ostr << num.value;
   } else {
      ostr << "[" << *(num.left) << "," << *(num.right) << "]";
   }
   return ostr;
}

}

std::string Day18Part1::name() const {
   return "Day18-Part1";
}

std::string Day18Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   std::istringstream instr( data.front() );
   auto snailfish_number = std::make_unique<Snailfish_Number>();
   instr >> *snailfish_number;

   for( size_t idx = 1; idx < data.size(); ++idx) {
      std::istringstream addend_stream( data[idx] );
      auto addend = std::make_unique<Snailfish_Number>();
      addend_stream >> *addend;

      snailfish_number -> reduce();
      
      auto add_result = Snailfish_Number::add( std::move(snailfish_number), std::move(addend) );
      snailfish_number.reset(add_result);
   }

   snailfish_number -> reduce();
   return std::to_string(snailfish_number -> magnitude());
}


std::string Day18Part2::name() const {
   return "Day18-Part2";
}

std::string Day18Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   int max_magnitude = 0;

   for(const auto & ostr : data) {
      for(const auto & istr : data) {
         if(ostr != istr) {
            
            std::istringstream onstr( ostr );
            std::istringstream instr( istr );
            
            auto o_snailfish_number = std::make_unique<Snailfish_Number>();
            auto i_snailfish_number = std::make_unique<Snailfish_Number>();
            onstr >> *o_snailfish_number;
            instr >> *i_snailfish_number;

            auto snailfish_number = std::unique_ptr<Snailfish_Number>(
               Snailfish_Number::add( std::move(o_snailfish_number), std::move(i_snailfish_number) )
            );

            snailfish_number -> reduce();
            max_magnitude = std::max(max_magnitude, snailfish_number -> magnitude() );
         }
      }
   }

   return std::to_string(max_magnitude);
}

