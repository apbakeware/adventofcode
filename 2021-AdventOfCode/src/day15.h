#ifndef __DAY15_H__
#define __DAY15_H__

#include <string>
#include <vector>

struct Day15Data {
   typedef std::string Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day15Part1 : public Day15Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day15Part2 : public Day15Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day15Spec {
   using Data_Spec = Day15Data;
   using Part_1 = Day15Part1;
   using Part_2 = Day15Part2;
   static constexpr const char * day() { return "Day15"; }
   static constexpr const char * data_file_name() { return "data/day15.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day15.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "40"; }
   static constexpr const char * expected_sample_part_2_result() { return "315"; }
   static constexpr const char * solved_part_1_result() { return "656"; }
   static constexpr const char * solved_part_2_result() { return "2979"; }
};

#endif // __DAY15_H__
