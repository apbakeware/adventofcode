#include <algorithm>
#include <numeric>
#include <unordered_set>
#include <set>
#include "day09.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"
#include "grid.h"
#include "grid_utils.h"

namespace {



bool _do_registration() {
   return Solver_Registry::register_solver<Day09Spec>();
}

const bool _was_registered = _do_registration();

// need hash coding
//using Visitation_List = std::unordered_set<Grid_Location>;
using Visitation_List = std::set<Grid_Location>;
using Heatmap_Grid = Fixed_Sized_Grid<int>;
using Int_Value_Location = Location_Value<int>;

Heatmap_Grid create_grid( const Day09Data::Input_Data_Collection & data ) {
   const int num_rows = static_cast<int>( data.size() );
   const int num_cols = static_cast<int>( data.front().size() );
   Heatmap_Grid grid(num_rows, num_cols);

   int row_num = grid.number_of_rows() - 1;
   std::for_each(
      data.begin(),
      data.end(),
      [&]( const auto & row) {
         for(int col = 0; col < num_cols; ++col ) {
            const int value = row[col] - '0'; // Single char use ASCII..otherwise stoi
            const Grid_Location location = { static_cast<unsigned int>(row_num), static_cast<unsigned int>(col) };
            grid.set( location, value );
            
         }
         --row_num;
      }
   );

   return grid;
}

std::vector<Int_Value_Location> get_low_points( const Heatmap_Grid & height_map_grid ) {
   
   std::vector<Int_Value_Location> low_points;

   height_map_grid.for_each_cell(
      [&](const auto & location, const auto value) {

         bool less_than_all_neighbors = true;
         SPDLOG_DEBUG("Evaluating othogonal neighbors of: {} with value: {}", location, value);
         for_each_orthogonal( 
            height_map_grid, 
            location,
            [&]( const auto & neighbor_location, const auto neighbor_value ) {
               const bool less_than_neighbor = (value < neighbor_value);
               less_than_all_neighbors &= less_than_neighbor;
               SPDLOG_DEBUG("  I'm less than neighbor at {} with value: {} is GTE to value: {}  running less than: {}",
                  neighbor_location, neighbor_value, less_than_neighbor, less_than_neighbor);
               
            }
         );

         SPDLOG_DEBUG("Location: {} with value: {} is less than all neighbors: {}",
            location, value, less_than_all_neighbors);

         if( less_than_all_neighbors ) {
            // TODO: emplace? needs to accept params
            low_points.push_back( {location, value} );
         }
      }
   );

   return low_points;
}


int walk_basin( const Heatmap_Grid & grid, const Grid_Location & loc, Visitation_List & visitation ) {

   SPDLOG_DEBUG("Walking Basing -- location: {}", loc);

   const auto grid_value = grid.at( loc );
   if( visitation.count( loc ) ) {
      SPDLOG_DEBUG("Already visited: {}", loc);
      return 0;
   } else if( grid_value == 9 ) {
      SPDLOG_DEBUG("Value is 9: {}", loc);
      return 0;
   }

   int count = 1;
   visitation.insert( loc );
   for_each_orthogonal(
      grid,
      loc,
      [&]( const auto & location, const auto orth_value ) {
         if( orth_value > grid_value ) {
            SPDLOG_DEBUG( "Orthogonal neighbor higer than me, walking" );
            count += walk_basin( grid, location, visitation );
         }
      }
   );

   return count;
}

}

std::string Day09Part1::name() const {
   return "Day09-Part1";
}

std::string Day09Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   
   const auto & height_map_grid = create_grid( data );
   const auto & low_points = get_low_points( height_map_grid );
   int risk = std::accumulate(
      low_points.begin(),
      low_points.end(),
      0,
      [&]( auto value, const auto & low_point ) {
         return value + low_point.value + 1;
      }
   );
   
   return std::to_string(risk);
}


std::string Day09Part2::name() const {
   return "Day09-Part2";
}


std::string Day09Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   SPDLOG_INFO("====================================================");

   const auto & height_map_grid = create_grid( data );
   const auto & low_points = get_low_points( height_map_grid );

   std::vector<int> basin_sizes;
   Visitation_List visitation;
   std::transform(
      low_points.begin(),
      low_points.end(),
      std::back_inserter(basin_sizes),
      [&]( const auto & low_point ) {
         int basin_size = walk_basin( height_map_grid, low_point.location, visitation );
         return basin_size;
      }
   );

   std::sort(basin_sizes.begin(), basin_sizes.end());
   int result = std::accumulate(
      basin_sizes.rbegin(),
      basin_sizes.rbegin() + 3,
      1,
      std::multiplies<int>()
   );
   

   return std::to_string(result);
}

