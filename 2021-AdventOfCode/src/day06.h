#ifndef __DAY06_H__
#define __DAY06_H__

#include <string>
#include <vector>

struct Day06Data {
   typedef std::string Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day06Part1 : public Day06Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day06Part2 : public Day06Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day06Spec {
   using Data_Spec = Day06Data;
   using Part_1 = Day06Part1;
   using Part_2 = Day06Part2;
   static constexpr const char * day() { return "Day06"; }
   static constexpr const char * data_file_name() { return "data/day06.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day06.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "5934"; }
   static constexpr const char * expected_sample_part_2_result() { return "26984457539"; }
   static constexpr const char * solved_part_1_result() { return "386536"; }
   static constexpr const char * solved_part_2_result() { return "1732821262171"; }
};

#endif // __DAY06_H__
