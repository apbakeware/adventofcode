#include "day23.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day23Spec>();
}

const bool _was_registered = _do_registration();

}

std::string Day23Part1::name() const {
   return "Day23-Part1";
}

std::string Day23Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}


std::string Day23Part2::name() const {
   return "Day23-Part2";
}

std::string Day23Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   return "Unimplemented";
}

