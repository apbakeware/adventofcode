#ifndef __GRID_H__
#define __GRID_H__

#include <array>
#include <iostream>
#include <iterator>
#include <vector>
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"

struct Grid_Location {
   ssize_t row;
   ssize_t col;

   static Grid_Location up( const Grid_Location & loc, ssize_t dist = 1 );
   
   static Grid_Location right( const Grid_Location & loc, ssize_t dist = 1 );

   static Grid_Location down( const Grid_Location & loc, ssize_t dist = 1 );
   
   static Grid_Location left( const Grid_Location & loc, ssize_t dist = 1 );

   template<typename OStream>
    friend OStream &operator<<(OStream &os, const Grid_Location &rec)
    {
        return os << "[row: " << rec.row << ", col: " << rec.col << "]";
    }

};

bool operator<(const Grid_Location & lhs, const Grid_Location & rhs);
bool operator==(const Grid_Location & lhs, const Grid_Location & rhs);
bool operator!=(const Grid_Location & lhs, const Grid_Location & rhs);


std::istream & operator>>(std::istream & istr, Grid_Location record);

template<class Value_T>
struct Location_Value {
   using Value_Type = Value_T;
   Grid_Location location;
   Value_T value;

   template<typename OStream>
   friend OStream &operator<<(OStream &os, const Location_Value &rec)
   {
      return os << "[" << rec.value << " @ " << rec.location << "]";
   }
};

template <class Data_T, size_t Num_Rows, size_t Num_Cols>
class Static_Sized_Grid {
public:
   typedef Data_T value_type;

   constexpr ssize_t number_of_cols() const { return static_cast<ssize_t>(Num_Cols); }

   constexpr ssize_t number_of_rows() const { return static_cast<ssize_t>(Num_Rows); }

   Data_T at(const Grid_Location &location) const {
      return grid.at(location.row).at(location.col);
   }

   void set(const Grid_Location &location, const Data_T &data) {
      grid.at(location.row).at(location.col) = data;
   }

   template <class Op> void for_each_cell(Op op) {
      for (auto row = 0; row < number_of_rows(); ++row) {
         for (auto col = 0; col < number_of_cols(); ++col) {
            const Grid_Location location = {row, col};
            op(location, at(location));
         }
      }
   }

   template <class Op> void for_each_cell(Op op) const {
      for (auto row = 0; row < number_of_rows(); ++row) {
         for (auto col = 0; col < number_of_cols(); ++col) {
            const Grid_Location location = {row, col};
            op(location, at(location));
         }
      }
   }

   template <typename OStream>
   friend OStream &operator<<(OStream &os, const Static_Sized_Grid<Data_T, Num_Rows, Num_Cols> &grid) {
      
      for (auto row = grid.grid.rbegin(); row != grid.grid.rend(); ++row) {
         std::copy(row->begin(),
           row->end(),
           std::ostream_iterator<value_type>(os, "  "));
         os << "\n";
      }

      return os;
   }

private:

   using Row = std::array<Data_T, Num_Cols>;
   using Grid = std::array<Row, Num_Rows>;

   Grid grid;
};

// Set on initialization
template <class Data_T>
class Fixed_Sized_Grid {
public:
   typedef Data_T value_type;

   Fixed_Sized_Grid( int num_rows, int num_cols ) 
      : num_rows(num_rows), num_cols(num_cols) {

      grid.resize(num_rows);
      for(auto & row : grid) {
         row.resize(num_cols);
      }
   }

   ssize_t number_of_cols() const { return static_cast<ssize_t>(num_cols); }

   ssize_t number_of_rows() const { return static_cast<ssize_t>(num_rows); }


   Data_T at(const Grid_Location &location) const {
      return grid.at(location.row).at(location.col);
   }

   void set(const Grid_Location &location, const Data_T &data) {
      grid.at(location.row).at(location.col) = data;
   }

   template <class Op> void for_each_cell(Op op) {
      for (auto row = 0; row < number_of_rows(); ++row) {
         for (auto col = 0; col < number_of_cols(); ++col) {
            const Grid_Location location = {row, col};
            op(location, at(location));
         }
      }
   }

   template <class Op> void for_each_cell(Op op) const {
      for (auto row = 0; row < number_of_rows(); ++row) {
         for (auto col = 0; col < number_of_cols(); ++col) {
            const Grid_Location location = {row, col};
            op(location, at(location));
         }
      }
   }

   template <typename OStream>
   friend OStream &operator<<(OStream &os, const Fixed_Sized_Grid<Data_T> &grid) {
      
      for (auto row = grid.grid.rbegin(); row != grid.grid.rend(); ++row) {
         std::copy(row->begin(),
           row->end(),
           std::ostream_iterator<value_type>(os, "  "));
         os << "\n";
      }

      return os;
   }

private:

   using Row = std::vector<Data_T>;
   using Grid = std::vector<Row>;

   ssize_t num_rows;
   ssize_t num_cols;

   Grid grid;
};


#endif // __GRID_H__