#include <algorithm>
#include <functional>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <unordered_map>
#include <vector>
#include <cxxopts/cxxopts.hpp>
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"

#include "solve.hpp"
#include "solver_result.h"
#include "solver_registry.h"
#include "i_result_processor.h"
#include "table_writer_result_processor.h"
#include "table_writer_expected_actual_solver_result_processor.h"

namespace {

bool _initialize_logger() {
   auto logger = spdlog::basic_logger_mt("root", "logs/aoc2021.log", true);
   logger -> set_level(spdlog::level::warn);
   logger -> set_pattern("[%H:%M:%S.%e] [%n] [%l] %v  [%s:%#]");
   spdlog::set_default_logger(logger);
   return true;
}

const bool logger_was_initialized = _initialize_logger();

}


bool solver_should_run
( const std::string & name, const std::vector<std::string> & solver_name_filter ) {

   if( solver_name_filter.empty() ) {
      SPDLOG_INFO("No name filters provided, solver {} will run", name);
      return true;
   }

   const auto filter_iter = std::find( solver_name_filter.begin(), solver_name_filter.end(), name );
   if( filter_iter != solver_name_filter.end() ) {
      SPDLOG_INFO("Solver name found in filter list, solver {} will run", name);
      return true;
   }

   return false;
}


void perform_run_mode_solve( const std::vector<std::string> & solver_filter ) {
   SPDLOG_INFO("Running solvers in solver mode: real problem set; report solution");
   SPDLOG_INFO("Number of solvers specified: {}", solver_filter.size());

   std::unique_ptr<IResult_Processor<Solver_Result> > solve_processor = std::make_unique<Table_Writer_Solver_Result_Processor>("AOC Solutions");
   Solver_Registry::add_result_processor(solve_processor.get());
         

   const auto & sample_solver_names = Solver_Registry::get_solver_names();
   SPDLOG_INFO("Number of total sample solvers: {}", sample_solver_names.size());
   for(const auto & name : sample_solver_names) {
      SPDLOG_DEBUG("Trying to run solver: {}", name);
      try {
         if( solver_should_run( name, solver_filter ) ) {
            if(!Solver_Registry::try_run_solver(name)) {
               SPDLOG_WARN("Could not find solver '{}' to run", name );
            }
         }
      } catch (std::exception &exc) {
         SPDLOG_ERROR("Failed to solve: {} -- Message: {}",
            name,
            exc.what());
      }
   }
}

void perform_run_mode_sample( const std::vector<std::string> & solver_names ) {
   SPDLOG_INFO("Running solvers in sample mode: sample problem set; evaluate against expected solutions");
   SPDLOG_INFO("Number of solvers specified: {}", solver_names.size());
   
   std::unique_ptr<IResult_Processor<Expected_Solver_Result> > solve_processor = std::make_unique<Table_Writer_Expected_Actual_Solver_Result_Processor>("AOC Sample Solutions");
   Solver_Registry::add_result_processor(solve_processor.get());

   const auto & sample_solver_names = Solver_Registry::get_solver_names();
   SPDLOG_INFO("Number of total sample solvers: {}", sample_solver_names.size());
   for(const auto & name : sample_solver_names) {
      SPDLOG_DEBUG("Trying to run solver: {}", name);
      try {
         if( solver_should_run( name, solver_names ) ) {
            if(!Solver_Registry::try_run_sample_solver(name)) {
               SPDLOG_WARN("Could not find solver '{}' to run", name );
            }
         }
      } catch (std::exception &exc) {
         SPDLOG_ERROR("Failed to solve: {} -- Message: {}",
            name,
            exc.what());
      }
   }
}

void perform_run_mode_refactor( const std::vector<std::string> & solver_names ) {
   SPDLOG_INFO("Running solvers in refactor mode: real problem set; evaluate against expected solutions");
   SPDLOG_INFO("Number of solvers specified: {}", solver_names.size());
   
   std::unique_ptr<IResult_Processor<Expected_Solver_Result> > solve_processor = std::make_unique<Table_Writer_Expected_Actual_Solver_Result_Processor>("AOC Refactored Solutions");
   Solver_Registry::add_result_processor(solve_processor.get());

   const auto & sample_solver_names = Solver_Registry::get_solver_names();
   SPDLOG_INFO("Number of total sample solvers: {}", sample_solver_names.size());
   for(const auto & name : sample_solver_names) {
      SPDLOG_DEBUG("Trying to run solver: {}", name);
      try {
         if( solver_should_run( name, solver_names ) ) {
            if(!Solver_Registry::try_run_refactor_solver(name)) {
               SPDLOG_WARN("Could not find solver '{}' to run", name );
            }
         }
      } catch (std::exception &exc) {
         SPDLOG_ERROR("Failed to solve: {} -- Message: {}",
            name,
            exc.what());
      }
   }
}

using Mode_Executor = std::function<void(const std::vector<std::string>)>;
std::unordered_map<std::string, Mode_Executor> mode_executors = {
   { "solve", perform_run_mode_solve },
   { "sample", perform_run_mode_sample },
   { "refactor", perform_run_mode_refactor }
};

int main(int argc, char **argv) {

   cxxopts::Options options("AOC-2021", "Santa Saver's Unite!");
   options.add_options()
      ("m,mode", "Execution mode of the solver [solve, sample, refactor]", cxxopts::value<std::string>()->default_value("solve"))
      ("s,solvers", "Run the named solvers (ex. Day01,Day02)", cxxopts::value<std::vector<std::string>>()) // Day01,Day02
      ("l,list", "List the available solvers and exit", cxxopts::value<bool>())
      ("v,verbose", "Set the logger to INFO for more output", cxxopts::value<bool>())
      ("V,very-verbose", "Set the logger to DEBUG for more output", cxxopts::value<bool>())
      ("h,help", "Display help and exit", cxxopts::value<bool>())
   ;

   auto opts = options.parse(argc, argv);
   int exit_code = 0;

   try {

      if(opts.count("very-verbose")) {
         std::cout << "Setting logger to DEBUG" << std::endl;
         spdlog::set_level(spdlog::level::debug);
      } else if(opts.count("verbose")) {
         std::cout << "Setting logger to INFO" << std::endl;
         spdlog::set_level(spdlog::level::info);
      }

      if(opts.count("help")) {

         std::cout << options.help({"", "Group"}) << std::endl;
         exit_code = 0;

      } else if(opts.count("list")) {

         const auto & solver_names = Solver_Registry::get_solver_names();
         std::cout << "Avialable solvers:\n";
         std::copy( solver_names.begin(), solver_names.end(),
            std::ostream_iterator<std::string>(std::cout, "  "));
         std::cout << "\n";
            
         exit_code = 0;

      } else {
         std::vector<std::string> solver_names_to_run;
         if(opts.count("solvers")) {
            solver_names_to_run = opts["solvers"].as<std::vector<std::string>>();
            SPDLOG_INFO("Number of command line solvers specified: {}", solver_names_to_run.size());
         } else {
            SPDLOG_INFO("No solvers specified, will use them all");
         }

         // TODO: builder
         std::string mode = opts.count("mode") ? opts["mode"].as<std::string>() : "solve";
         SPDLOG_INFO("Running execution mode: {}", mode);         
         if( mode_executors.count(mode) ) {
            auto & executor = mode_executors[mode];
            executor(solver_names_to_run);
         } else {
            SPDLOG_ERROR("Unknown execution mode: '{}'", mode);
            std::cerr << "Unknown execution mode '" << mode << "'!\n";
            std::cerr << "Supported modes:";
            for(const auto& mode : mode_executors) {
               std::cerr << "  " << mode.first;
            }
            std::cerr << std::endl;
            exit_code = 1;
         }
      } 

   } catch( std::exception & exc ) {
      SPDLOG_ERROR("Exception: {}", exc.what());
      std::cerr << "Exception caught while running solvers. Message: "
         << exc.what() << std::endl;
      exit_code = 1;
   }

   exit(exit_code);
}