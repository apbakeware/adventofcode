#include <algorithm>
#include <vector>
#include <iterator>
#include <iostream>
#include <numeric>
#include <sstream>
#include "day06.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day06Spec>();
}

const bool _was_registered = _do_registration();

}

std::string Day06Part1::name() const {
   return "Day06-Part1";
}

// TODO: Read in 1 string commo separated. Need a way to read in with getline with a different delimiter
std::string Day06Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   
   std::string line = data.front();
   std::replace(line.begin(), line.end(), ',', ' ');
   std::istringstream instr( line );
   std::vector<int> fishies( (std::istream_iterator<int>(instr)), std::istream_iterator<int>());
   fishies.reserve(10000);

   for( int day = 1; day <= 80; ++day) {
      std::vector<int> spawned_fish;
      
      for(auto & fish : fishies ) {
         if( fish == 0 ) {
            fish = 6;
            spawned_fish.push_back( 8 );
         } else {
            --fish;
         }
      }

      std::copy( 
         spawned_fish.begin(),
         spawned_fish.end(),
         std::back_inserter(fishies)
      );
   }

   return std::to_string(fishies.size());
}


std::string Day06Part2::name() const {
   return "Day06-Part2";
}

std::string Day06Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   using Storage_Type = unsigned long long int;

   std::string line = data.front();
   std::replace(line.begin(), line.end(), ',', ' ');
   std::istringstream instr( line );
   std::vector<Storage_Type> initial_values( (std::istream_iterator<Storage_Type>(instr)), std::istream_iterator<Storage_Type>());

   // Create an array where the index is the day and the value is the count
   // Then we can compute how many will be in the next frame.
   std::vector<Storage_Type> fishies(9);
   std::vector<Storage_Type> next_fishies(9);

   std::for_each( initial_values.begin(), 
      initial_values.end(),
      [&](const auto val) {
         fishies[val]++;
      }
   );

   for( int day = 1; day <= 256; ++day) {

      next_fishies[0] = fishies[1];
      next_fishies[1] = fishies[2];
      next_fishies[2] = fishies[3];
      next_fishies[3] = fishies[4];
      next_fishies[4] = fishies[5];
      next_fishies[5] = fishies[6];
      next_fishies[6] = fishies[7];
      next_fishies[7] = fishies[8];
      next_fishies[8] = fishies[0];

      next_fishies[6] += fishies[0];
      
      fishies.swap(next_fishies);

      // std::cout << "Day: " << day << " -- ";
      // std::copy( fishies.begin(), fishies.end(), std::ostream_iterator<Storage_Type>(std::cout, "  "));
      // std::cout << "\n";
   }

   auto count = std::accumulate(
      fishies.begin(), 
      fishies.end(),
      0ull
   );

   return std::to_string(count);
}

