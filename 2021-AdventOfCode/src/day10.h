#ifndef __DAY10_H__
#define __DAY10_H__

#include <string>
#include <vector>

struct Day10Data {
   typedef std::string Input_Data_Type;
   typedef std::vector<Input_Data_Type> Input_Data_Collection;
};

class Day10Part1 : public Day10Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

class Day10Part2 : public Day10Data {
public:

   std::string name() const;

   std::string solve( const Input_Data_Collection & data );

};

struct Day10Spec {
   using Data_Spec = Day10Data;
   using Part_1 = Day10Part1;
   using Part_2 = Day10Part2;
   static constexpr const char * day() { return "Day10"; }
   static constexpr const char * data_file_name() { return "data/day10.txt"; }
   static constexpr const char * sample_data_file_name() { return "data/day10.sample.txt"; }
   static constexpr const char * expected_sample_part_1_result() { return "26397"; }
   static constexpr const char * expected_sample_part_2_result() { return "288957"; }
   static constexpr const char * solved_part_1_result() { return "362271"; }
   static constexpr const char * solved_part_2_result() { return "1698395182"; }
};

#endif // __DAY10_H__
