#include "day11.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h"
#include "solver_registry.h"
#include "grid.h"
#include "grid_utils.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day11Spec>();
}

const bool _was_registered = _do_registration();

class Octopus_Cell {
public:

   Octopus_Cell() : value(0), has_flashed(false) {

   }

   explicit Octopus_Cell(int value) 
      : value(value), has_flashed(false) {

   }

   void increment() {
      ++value;
   }

   void flash() {
      has_flashed = true;
   }

   bool flash_pending() const {
      return !has_flashed && value > 9;
   }

   bool has_already_flashed() const {
      return has_flashed;
   }

   void reset_flash() {
      if(has_flashed) {
         value = 0;
         has_flashed = false;
      }
   }

   template<typename OStream>
    friend OStream &operator<<(OStream &os, const Octopus_Cell &rec)
    {
        return os << "[" << rec.value << " | " << rec.has_flashed << "]";
    }

private:

   int value;
   bool has_flashed;
   
};

// TODO: ostream and Ostream

using Octopus_Grid = Fixed_Sized_Grid<Octopus_Cell>;

// Returns number of flashed octopi
int run_octopus_phase(Octopus_Grid & grid ) {
   int number_of_flashes = 0;
   std::vector<Grid_Location> pending_flashes;

   std::function<void(const Grid_Location&, const Octopus_Cell&)> propogate_flash_op = 
      [&](const auto & loc, const auto & cell) {
         if(!cell.has_already_flashed()) {
            auto cell_update = cell;
            cell_update.increment();
            if(cell_update.flash_pending()) {
               ++number_of_flashes;
               cell_update.flash();
               grid.set(loc, cell_update);
               for_each_surrounding(grid, loc, propogate_flash_op);
            } else {
               grid.set(loc, cell_update);
            }
         }
      };

   auto flash_op = [&](const auto & loc, const auto & cell) {
      if(cell.flash_pending()) {
         ++number_of_flashes;
         auto cell_update = cell;
         cell_update.flash();
         grid.set(loc, cell_update);
         for_each_surrounding(grid, loc, propogate_flash_op);
      }
   };

   auto increment_phase_op = [&](const auto & loc, const auto & cell) {
      auto cell_update = cell;
      cell_update.increment();
      grid.set(loc, cell_update);
      if(cell_update.flash_pending()) {
         pending_flashes.push_back(loc);
      }
   };

   auto reset_flashes = [&](const auto & loc, const auto & cell ) {
      if(cell.has_already_flashed()) {
         auto cell_update = cell;
         cell_update.reset_flash();
         grid.set(loc, cell_update);
      }
   };

   grid.for_each_cell( increment_phase_op );
   for_each_cell_location(grid, pending_flashes.begin(), pending_flashes.end(), flash_op);
   grid.for_each_cell( reset_flashes );
   
   return number_of_flashes;
}

}

std::string Day11Part1::name() const {
   return "Day11-Part1";
}

std::string Day11Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   int steps = 100;
   int number_of_flashes = 0;

   Octopus_Grid grid = create_digit_grid_from_string_rows<Octopus_Cell>
      ( data.begin(), data.end());
   
   while( steps > 0 ) {
      number_of_flashes += run_octopus_phase( grid );
      --steps;
   }   

   return std::to_string(number_of_flashes);
}


std::string Day11Part2::name() const {
   return "Day11-Part2";
}

std::string Day11Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   Octopus_Grid grid = create_digit_grid_from_string_rows<Octopus_Cell>
      ( data.begin(), data.end());

   int num_phases_run = 0;
   int octopi_flashed_in_phase = 0;
   const auto number_of_cells_on_grid = grid.number_of_rows() * grid.number_of_cols();

   while( octopi_flashed_in_phase != number_of_cells_on_grid ) {
      octopi_flashed_in_phase = run_octopus_phase( grid );
      ++num_phases_run;
   }

   return std::to_string(num_phases_run);
}

