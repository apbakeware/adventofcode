#include <algorithm>
#include <numeric>
#include "day01.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day01Spec>();
}

const bool _was_registered = _do_registration();

}

std::string Day01Part1::name() const {
   return "Day01-Part1";
}

std::string Day01Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   
   int increases = 0;
   int previous_value = data[0];
   std::for_each( 
      data.begin(),
      data.end(),
      [&](auto value) {
         if( value > previous_value ) {
            ++increases;
         } 
         previous_value = value;
      }
   );

   return std::to_string(increases);
}


std::string Day01Part2::name() const {
   return "Day01-Part2";
}

std::string Day01Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   const int window_size = 3;
   int increases = 0;
   int prev_window = std::numeric_limits<int>::max();
   for(auto iter = data.begin(); iter != data.end(); ++iter ) {
      auto window_end = std::distance(iter, data.end()) >= window_size ? iter + window_size : data.end();
      auto window = std::accumulate(iter, window_end, 0);
      if( window > prev_window) {
         ++increases;
      }
      prev_window = window;
   }

   return std::to_string(increases);
}

