#include <regex>
#include <sstream>
#include "day13.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"
#include "grid.h"
#include "grid_utils.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day13Spec>();
}

const bool _was_registered = _do_registration();

struct Oragami_Instructions {

   std::vector<Grid_Location> dots;

   // Will have 0 for one of the values.
   std::vector<Grid_Location> folds;

   ssize_t row_max;
   ssize_t col_max;

};

bool starts_with(const std::string & str, const std::string & to_match) {

   return str.find(to_match) == 0;
}


// Better if we had getline() but we need to push
// that into the day spec
template<class String_Iter_T>
Oragami_Instructions build_oragami_instructions
( String_Iter_T begin, String_Iter_T end ) {

   Oragami_Instructions instructions;

   const std::regex dot_regex(R"((\d+),(\d+))"); 
   std::smatch dot_match;
  
   instructions.row_max = 0;
   instructions.col_max = 0;

   SPDLOG_DEBUG("Extracting dot locations");
   while(std::regex_match(*begin, dot_match, dot_regex)) {
      Grid_Location dot_loc = { std::stoi(dot_match[2]), std::stoi(dot_match[1]) };

      instructions.row_max = std::max( instructions.row_max, dot_loc.row);
      instructions.col_max = std::max( instructions.col_max, dot_loc.col);

      instructions.dots.emplace_back( std::move(dot_loc) );
      ++begin;
   }

   // This is going to be a bit clunky as reading
   // strings instead of readline() delimits on space
   SPDLOG_DEBUG("Extracting fold locations");
   while(begin != end) {

      if(starts_with(*begin, "x=")) {
         const auto pos = (*begin).find("=");
         const auto digit = (*begin).substr(pos + 1);

         Grid_Location fold = {0, std::stoi(digit)};
         instructions.folds.emplace_back( fold );
   
      } else if(starts_with(*begin, "y=")) {
         const auto pos = (*begin).find("=");
         const auto digit = (*begin).substr(pos + 1);

         Grid_Location fold = {std::stoi(digit), 0};
         instructions.folds.emplace_back( fold );
   
      }

      ++begin;
   }

   return instructions;
}


   // consolidate the loops ... put ths initial
   // improve efficiency
   // maybe keep the original grid and maintain the active regions?

Fixed_Sized_Grid<bool> fold_on_row
( const Fixed_Sized_Grid<bool> & grid, ssize_t fold ) {

   Fixed_Sized_Grid<bool> folded( fold, grid.number_of_cols() );
   
   const ssize_t num_overlapping_rows = std::min( 
      fold, 
      grid.number_of_rows() - fold);

   for(ssize_t row = 0; row < folded.number_of_rows(); ++row ) {
      for(ssize_t col = 0; col < folded.number_of_cols(); ++col ) {
         const Grid_Location loc = {row, col};
         if( grid.at(loc) ) {
            folded.set( loc, true );
         }
      }
   }

   for( ssize_t cnt = 1; cnt <= num_overlapping_rows; ++cnt ) {
      for(ssize_t col = 0; col < folded.number_of_cols(); ++col ) {
         const Grid_Location get_loc = { fold + cnt, col };
         if(grid.at(get_loc)) {
            const Grid_Location set_loc = { fold - cnt, col };
            folded.set( set_loc, true );
         }
      }
   }

   return folded;
}

Fixed_Sized_Grid<bool> fold_on_col
( const Fixed_Sized_Grid<bool> & grid, ssize_t fold ) {

   Fixed_Sized_Grid<bool> folded( grid.number_of_rows(), fold );

   const ssize_t num_overlapping_cols = std::min( 
      fold, 
      grid.number_of_cols() - fold);

   for(ssize_t row = 0; row < folded.number_of_rows(); ++row ) {
      for(ssize_t col = 0; col < folded.number_of_cols(); ++col ) {
         const Grid_Location loc = {row, col};
         if( grid.at(loc) ) {
            folded.set( loc, true );
         }
      }
   }

   for( ssize_t row = 0; row < folded.number_of_rows(); ++row ) {
      for( ssize_t cnt = 1; cnt <= num_overlapping_cols; ++cnt ) {
         const Grid_Location get_loc = { row, fold + cnt };
         if(grid.at(get_loc)) {
            const Grid_Location set_loc = { row, fold - cnt };
            folded.set( set_loc, true );
         }
      }
   }

   return folded;
}


}

std::string Day13Part1::name() const {
   return "Day13-Part1";
}


// Grid will be upside down because grid is 0,0 lower left
std::string Day13Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   const auto & instructions = build_oragami_instructions( data.begin(), data.end() );
   Fixed_Sized_Grid<bool> grid( instructions.row_max + 1, instructions.col_max + 1);

   std::for_each(
      instructions.dots.begin(),
      instructions.dots.end(),
      [&](const auto & loc ) {
         grid.set(loc, true);
      }
   );


   const auto & fold = instructions.folds.front();
   if(fold.row > 0) {
      grid = fold_on_row(grid, fold.row);
   } else {
      grid = fold_on_col(grid, fold.col);
   }

   int count = 0;
   grid.for_each_cell(
      [&](const auto & loc, auto val) {
         if(val) {
            ++count;
         }
      }
   );

   return std::to_string(count);
}


std::string Day13Part2::name() const {
   return "Day13-Part2";
}

std::string Day13Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   const auto & instructions = build_oragami_instructions( data.begin(), data.end() );
   Fixed_Sized_Grid<bool> grid( instructions.row_max + 1, instructions.col_max + 1);

   std::for_each(
      instructions.dots.begin(),
      instructions.dots.end(),
      [&](const auto & loc ) {
         grid.set(loc, true);
      }
   );

   std::for_each(
      instructions.folds.begin(),
      instructions.folds.end(),
      [&]( const auto & fold ) {

         if(fold.row > 0) {
            grid = fold_on_row(grid, fold.row);
         } else {
            grid = fold_on_col(grid, fold.col);
         }
      }
   );

   grid = flip_horizontal( grid );
   std::ostringstream soln;
   soln << "\n";
   for(auto row = grid.number_of_rows() - 1; row >= 0; --row) {
      std::string line;
      for(auto col = 0; col < grid.number_of_cols(); ++col) {
         soln << (grid.at( {row, col} ) ? "X" : " ");
      }
      soln << "\n";
   }

   return soln.str();
}

