#include <algorithm>
#include <iterator>
#include <numeric>
#include "day08.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day08Spec>();
}

const bool _was_registered = _do_registration();

bool is_digit_1(const std::string &segments) { return segments.length() == 2; }

bool is_digit_4(const std::string &segments) { return segments.length() == 4; }

bool is_digit_7(const std::string &segments) { return segments.length() == 3; }

bool is_digit_8(const std::string &segments) { return segments.length() == 7; }

std::unordered_map<std::string, int> determine_display_digits_by_segment_code(
  const Seven_Digit_Segment_Input_Record &input) {

   std::array<std::string, 10> mapping;
   std::vector<std::string> unmapped;
   std::unordered_map<std::string, int> segment_coding;

   for (const auto &segment_str : input.digits) {
      if (is_digit_1(segment_str)) {
         SPDLOG_DEBUG("Segment: {} is 1", segment_str);
         mapping[1] = segment_str;
      } else if (is_digit_4(segment_str)) {
         SPDLOG_DEBUG("Segment: {} is 4", segment_str);
         mapping[4] = segment_str;
      } else if (is_digit_7(segment_str)) {
         SPDLOG_DEBUG("Segment: {} is 7", segment_str);
         mapping[7] = segment_str;
      } else if (is_digit_8(segment_str)) {
         SPDLOG_DEBUG("Segment: {} is 8", segment_str);
         mapping[8] = segment_str;
      } else {
         unmapped.push_back(segment_str);
      }
   }

   std::vector<std::string> working_store(10);
   for (auto iter = unmapped.begin(); iter != unmapped.end(); ++iter) {
      working_store.clear();
      auto diff_end = std::set_difference(mapping[1].begin(),
        mapping[1].end(),
        iter->begin(),
        iter->end(),
        working_store.begin());

      auto num_unmatched_segments =
        std::distance(working_store.begin(), diff_end);
      if (num_unmatched_segments == 1 && iter->length() == 6) {
         mapping[6] = *iter;
      } else if (num_unmatched_segments == 0 && iter->length() == 5) {
         mapping[3] = *iter;
      } else {

         working_store.clear();
         auto diff_end = std::set_difference(mapping[4].begin(),
           mapping[4].end(),
           iter->begin(),
           iter->end(),
           working_store.begin());

         auto num_unmatched_segments =
           std::distance(working_store.begin(), diff_end);
         if (num_unmatched_segments == 0) {
            mapping[9] = *iter;
         } else if (num_unmatched_segments == 1) {
            if (iter->length() == 6) {
               mapping[0] = *iter;
            } else {
               mapping[5] = *iter;
            }
         } else {
            mapping[2] = *iter;
         }
      }
   }

   SPDLOG_DEBUG("7 Segment Wire Encoding");
   for (size_t idx = 0; idx < mapping.size(); ++idx) {
      SPDLOG_DEBUG("{:<7} => {}", mapping[idx], idx);
      segment_coding[mapping[idx]] = idx;
   }

   return segment_coding;
}

} // namespace

std::istream &operator>>(
  std::istream &instr, Seven_Digit_Segment_Input_Record &record) {
   std::string delim;

   for (auto &digit : record.digits) {
      instr >> digit;
      std::sort(digit.begin(), digit.end());
   }

   instr >> delim;

   for (auto &illum : record.illuminated) {
      instr >> illum;
      std::sort(illum.begin(), illum.end());
   }

   return instr;
}

std::string Day08Part1::name() const { return "Day08-Part1"; }

std::string Day08Part1::solve(const Input_Data_Collection &data) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   int count = std::accumulate(
     data.begin(), data.end(), 0, [](auto current_accum, const auto &record) {
        return current_accum + std::count_if(record.illuminated.begin(),
                                 record.illuminated.end(),
                                 [](const auto &illum) {
                                    return is_digit_1(illum) ||
                                           is_digit_4(illum) ||
                                           is_digit_7(illum) ||
                                           is_digit_8(illum);
                                 });
     });

   return std::to_string(count);
}

std::string Day08Part2::name() const { return "Day08-Part2"; }

std::string Day08Part2::solve(const Input_Data_Collection &data) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());

   int total_value = std::accumulate(data.begin(),
     data.end(),
     0,
     [](const auto current_accum, const auto &row) {
        auto encoding = determine_display_digits_by_segment_code(row);
        int row_value = encoding[row.illuminated[0]] * 1000 +
                        encoding[row.illuminated[1]] * 100 +
                        encoding[row.illuminated[2]] * 10 +
                        encoding[row.illuminated[3]];

        return current_accum + row_value;
     });

   return std::to_string(total_value);
}
