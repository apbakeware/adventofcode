#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <stack>
#include "day10.h"
#include "spdlog/spdlog.h"
#include "solver_registry.h"

namespace {

bool _do_registration() {
   return Solver_Registry::register_solver<Day10Spec>();
}

const bool _was_registered = _do_registration();

char expected_open( char close ) {
   if(close == ')') { return '('; }
   if(close == ']') { return '['; }
   if(close == '}') { return '{'; }
   if(close == '>') { return '<'; }

   return 0x0;
}

char expected_close( char open ) {
   if(open == '(') { return ')'; }
   if(open == '[') { return ']'; }
   if(open == '{') { return '}'; }
   if(open == '<') { return '>'; }

   return 0x0;
}

int corrupt_char_points( char corrupt ) {
   if( corrupt == ')' ) return 3;
   if( corrupt == ']' ) return 57;
   if( corrupt == '}' ) return 1197;
   if( corrupt == '>' ) return 25137;

   return 0;
}

int completion_char_points( char completed ) {
   if( completed == ')') return 1;
   if( completed == ']') return 2;
   if( completed == '}') return 3;
   if( completed == '>') return 4;
   return 0;
}

struct Parse_Result {
   bool is_corrupted() const {
      return corrupt_char != 0x0;
   }

   bool is_incomplete() const {
      return !remaining_sequence.empty();
   }

   char corrupt_char;
   std::stack<char> remaining_sequence;
};

Parse_Result parse_navigation_string( const std::string & line ) {

   int cntr = 0;
   char corrupt_char = 0x0;
   std::stack<char> chunk_stack;
   for(const auto instr : line) {

      const auto exp_open = expected_open( instr );
      // NULL is nothing
      if( exp_open == 0x0 ) {
         chunk_stack.push( instr );
      } else {
         const auto top = chunk_stack.top();
         chunk_stack.pop();
         if( top != exp_open ) {
            corrupt_char = instr;
         }
      }
      ++cntr;
   }

    return { corrupt_char, std::move(chunk_stack) };
}

const std::string complete_navigation_string( const Parse_Result & parsed ) {

   std::string completion;
   // TODO: ideally don't compy
   auto remaining = parsed.remaining_sequence;
   while( !remaining.empty() ) {
      const auto value = remaining.top();
      remaining.pop();
      completion += expected_close(value);
   }

   return completion;
}

unsigned long score_completion_string( const std::string & completion ) {

   return std::accumulate(
      completion.begin(),
      completion.end(),
      0ul,
      []( const auto accum, const auto completed ) {
         return 5 * accum + completion_char_points( completed );
      }
   );
}


}

std::string Day10Part1::name() const {
   return "Day10-Part1";
}

std::string Day10Part1::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   
   std::vector<Parse_Result> line_parse_results;
   
   std::transform(
      data.begin(),
      data.end(),
      std::back_inserter(line_parse_results),
      [](const auto & line ) {
         return parse_navigation_string( line );
      }
   );

   int score = std::accumulate(
      line_parse_results.begin(),
      line_parse_results.end(),
      0,
      [](const auto value, const auto & record ) {
         
         if( !record.is_corrupted() ) return value;

         return value + corrupt_char_points( record.corrupt_char );
      }
   );

   return std::to_string(score);
}


std::string Day10Part2::name() const {
   return "Day10-Part2";
}

std::string Day10Part2::solve( const Input_Data_Collection & data ) {
   SPDLOG_INFO("{} input size: {}", name(), data.size());
   
   std::vector<Parse_Result> line_parse_results;
   std::transform(
      data.begin(),
      data.end(),
      std::back_inserter(line_parse_results),
      [](const auto & line ) {
         return parse_navigation_string( line );
      }
   );

   const auto end_of_incomplete = std::remove_if(
      line_parse_results.begin(), 
      line_parse_results.end(), 
      [](const auto & record ) {
         return record.is_corrupted();
      }
   );

   std::vector<unsigned long> points_to_complete;
   std::transform(
      line_parse_results.begin(),
      end_of_incomplete,
      std::back_inserter(points_to_complete),
      []( const auto & record ) {
         const std::string completion = complete_navigation_string( record );
         return score_completion_string( completion );
      }
   );

   std::sort( points_to_complete.begin(), points_to_complete.end() );

   const auto midpoint_score = points_to_complete[ points_to_complete.size() / 2 ];

   return std::to_string(midpoint_score);
}

