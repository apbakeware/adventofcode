#ifndef __SEASON2024_SOLVERS_H__
#define __SEASON2024_SOLVERS_H__

#include <string>
#include <tuple>

#include "aocsolver/solver_typelist.h"
#include "season_dfn_macros.h"

#include "days/day01part1.h"
#include "days/day01part2.h"
#include "days/day02part1.h"
#include "days/day02part2.h"
#include "days/day03part1.h"
#include "days/day03part2.h"
#include "days/day04part1.h"
#include "days/day04part2.h"
#include "days/day05part1.h"
#include "days/day05part2.h"
#include "days/day06part1.h"
#include "days/day06part2.h"
#include "days/day07part1.h"
#include "days/day07part2.h"
#include "days/day08part1.h"
#include "days/day08part2.h"
#include "days/day09part1.h"
#include "days/day09part2.h"
#include "days/day10part1.h"
#include "days/day10part2.h"
#include "days/day11part1.h"
#include "days/day11part2.h"
#include "days/day12part1.h"
#include "days/day12part2.h"
#include "days/day13part1.h"
#include "days/day13part2.h"
#include "days/day14part1.h"
#include "days/day14part2.h"
#include "days/day15part1.h"
#include "days/day15part2.h"
#include "days/day16part1.h"
#include "days/day16part2.h"
#include "days/day17part1.h"
#include "days/day17part2.h"
#include "days/day18part1.h"
#include "days/day18part2.h"
#include "days/day19part1.h"
#include "days/day19part2.h"
#include "days/day20part1.h"
#include "days/day20part2.h"
#include "days/day21part1.h"
#include "days/day21part2.h"
#include "days/day22part1.h"
#include "days/day22part2.h"
#include "days/day23part1.h"
#include "days/day23part2.h"
#include "days/day24part1.h"
#include "days/day24part2.h"
#include "days/day25part1.h"
#include "days/day25part2.h"

namespace aoc::season2024 {

using aoc::solver::NullSolver;

// clang-format off
FILE_SOLVER_SPEC("Day01.1", Day01_1_Spec, Day01Part1, "data2024/day01.txt", "data2024/day01.sample.txt", "2031679", "11");
FILE_SOLVER_SPEC("Day01.2", Day01_2_Spec, Day01Part2, "data2024/day01.txt", "data2024/day01.sample.txt", "19678534", "31");
GETLINE_SOLVER_SPEC("Day02.1", Day02_1_Spec, Day02Part1, "data2024/day02.txt", "data2024/day02.sample.txt", "246", "2");
GETLINE_SOLVER_SPEC("Day02.2", Day02_2_Spec, Day02Part2, "data2024/day02.txt", "data2024/day02.sample.txt", "318", "4");
GETLINE_SOLVER_SPEC("Day03.1", Day03_1_Spec, Day03Part1, "data2024/day03.txt", "data2024/day03.sample.txt", "178886550", "161");
GETLINE_SOLVER_SPEC("Day03.2", Day03_2_Spec, Day03Part2, "data2024/day03.txt", "data2024/day03.sample2.txt", "87163705", "48");
FILE_SOLVER_SPEC("Day04.1", Day04_1_Spec, Day04Part1, "data2024/day04.txt", "data2024/day04.sample.txt", "2524", "18");
FILE_SOLVER_SPEC("Day04.2", Day04_2_Spec, Day04Part2, "data2024/day04.txt", "data2024/day04.sample.txt", "1873", "9");
FILE_SOLVER_SPEC("Day05.1", Day05_1_Spec, Day05Part1, "data2024/day05.txt", "data2024/day05.sample.txt", "5374", "143");
FILE_SOLVER_SPEC("Day05.2", Day05_2_Spec, Day05Part2, "data2024/day05.txt", "data2024/day05.sample.txt", "4260", "123");
FILE_SOLVER_SPEC("Day06.1", Day06_1_Spec, Day06Part1, "data2024/day06.txt", "data2024/day06.sample.txt", "5242", "41");
FILE_SOLVER_SPEC("Day06.2", Day06_2_Spec, Day06Part2, "data2024/day06.txt", "data2024/day06.sample.txt", "?", "6");
GETLINE_SOLVER_SPEC("Day07.1", Day07_1_Spec, Day07Part1, "data2024/day07.txt", "data2024/day07.sample.txt", "1399219271639", "3749");
GETLINE_SOLVER_SPEC("Day07.2", Day07_2_Spec, Day07Part2, "data2024/day07.txt", "data2024/day07.sample.txt", "275791737999003", "11387");
GETLINE_SOLVER_SPEC("Day08.1", Day08_1_Spec, Day08Part1, "data2024/day08.txt", "data2024/day08.sample.txt", "254", "14");
GETLINE_SOLVER_SPEC("Day08.2", Day08_2_Spec, Day08Part2, "data2024/day08.txt", "data2024/day08.sample.txt", "951", "34");
FILE_SOLVER_SPEC("Day09.1", Day09_1_Spec, Day09Part1, "data2024/day09.txt", "data2024/day09.sample.txt", "6398252054886", "1928");
FILE_SOLVER_SPEC("Day09.2", Day09_2_Spec, Day09Part2, "data2024/day09.txt", "data2024/day09.sample.txt", "6415666220005", "2858");
GETLINE_SOLVER_SPEC("Day10.1", Day10_1_Spec, Day10Part1, "data2024/day10.txt", "data2024/day10.sample.txt", "514", "36");
GETLINE_SOLVER_SPEC("Day10.2", Day10_2_Spec, Day10Part2, "data2024/day10.txt", "data2024/day10.sample.txt", "1162", "81");
FILE_SOLVER_SPEC("Day11.1", Day11_1_Spec, Day11Part1, "data2024/day11.txt", "data2024/day11.sample.txt", "218956", "55312");
FILE_SOLVER_SPEC("Day11.2", Day11_2_Spec, Day11Part2, "data2024/day11.txt", "data2024/day11.sample.txt", "?", "?");
GETLINE_SOLVER_SPEC("Day12.1", Day12_1_Spec, Day12Part1, "data2024/day12.txt", "data2024/day12.sample.txt", "1359028", "1930");
NULL_SOLVER_SPEC("Day12.2", Day12_2_Spec, Day12Part2, "data2024/day12.txt", "data2024/day12.sample.txt", "?", "?");
FILE_SOLVER_SPEC("Day13.1", Day13_1_Spec, Day13Part1, "data2024/day13.txt", "data2024/day13.sample.txt", "27157", "480");
FILE_SOLVER_SPEC("Day13.2", Day13_2_Spec, Day13Part2, "data2024/day13.txt", "data2024/day13.sample.txt", "104015411578548", "875318608908");
FILE_SOLVER_SPEC("Day14.1", Day14_1_Spec, Day14Part1, "data2024/day14.txt", "data2024/day14.sample.txt", "231019008", "12");
FILE_SOLVER_SPEC("Day14.2", Day14_2_Spec, Day14Part2, "data2024/day14.txt", "data2024/day14.sample.txt", "8280", "?");
FILE_SOLVER_SPEC("Day15.1", Day15_1_Spec, Day15Part1, "data2024/day15.txt", "data2024/day15.sample.txt", "?", "10092");
NULL_SOLVER_SPEC("Day15.2", Day15_2_Spec, Day15Part2, "data2024/day15.txt", "data2024/day15.sample.txt", "?", "?");
FILE_SOLVER_SPEC("Day16.1", Day16_1_Spec, Day16Part1, "data2024/day16.txt", "data2024/day16.sample.txt", "85420", "11048");
FILE_SOLVER_SPEC("Day16.2", Day16_2_Spec, Day16Part2, "data2024/day16.txt", "data2024/day16.sample.txt", "?", "64");
FILE_SOLVER_SPEC("Day17.1", Day17_1_Spec, Day17Part1, "data2024/day17.txt", "data2024/day17.sample.txt", "3,6,3,7,0,7,0,3,0", "4,6,3,5,6,3,5,2,1,0");
FILE_SOLVER_SPEC("Day17.2", Day17_2_Spec, Day17Part2, "data2024/day17.txt", "data2024/day17.2.sample.txt", "?", "117440");
FILE_SOLVER_SPEC("Day18.1", Day18_1_Spec, Day18Part1, "data2024/day18.txt", "data2024/day18.sample.txt", "296", "22");
FILE_SOLVER_SPEC("Day18.2", Day18_2_Spec, Day18Part2, "data2024/day18.txt", "data2024/day18.sample.txt", "?", "6,1");
FILE_SOLVER_SPEC("Day19.1", Day19_1_Spec, Day19Part1, "data2024/day19.txt", "data2024/day19.sample.txt", "242", "6");
FILE_SOLVER_SPEC("Day19.2", Day19_2_Spec, Day19Part2, "data2024/day19.txt", "data2024/day19.sample.txt", "595975512785325", "16");
FILE_SOLVER_SPEC("Day20.1", Day20_1_Spec, Day20Part1, "data2024/day20.txt", "data2024/day20.sample.txt", "1381", "?");
NULL_SOLVER_SPEC("Day20.2", Day20_2_Spec, Day20Part2, "data2024/day20.txt", "data2024/day20.sample.txt", "?", "?");
NULL_SOLVER_SPEC("Day21.1", Day21_1_Spec, Day21Part1, "data2024/day21.txt", "data2024/day21.sample.txt", "?", "?");
NULL_SOLVER_SPEC("Day21.2", Day21_2_Spec, Day21Part2, "data2024/day21.txt", "data2024/day21.sample.txt", "?", "?");
FILE_SOLVER_SPEC("Day22.1", Day22_1_Spec, Day22Part1, "data2024/day22.txt", "data2024/day22.sample.txt", "17612566393", "37327623");
FILE_SOLVER_SPEC("Day22.2", Day22_2_Spec, Day22Part2, "data2024/day22.txt", "data2024/day22.2.sample.txt", "1968", "23");
FILE_SOLVER_SPEC("Day23.1", Day23_1_Spec, Day23Part1, "data2024/day23.txt", "data2024/day23.sample.txt", "1119", "7");
NULL_SOLVER_SPEC("Day23.2", Day23_2_Spec, Day23Part2, "data2024/day23.txt", "data2024/day23.sample.txt", "?", "?");
FILE_SOLVER_SPEC("Day24.1", Day24_1_Spec, Day24Part1, "data2024/day24.txt", "data2024/day24.sample.txt", "53755311654662", "2024");
NULL_SOLVER_SPEC("Day24.2", Day24_2_Spec, Day24Part2, "data2024/day24.txt", "data2024/day24.sample.txt", "?", "?");
FILE_SOLVER_SPEC("Day25.1", Day25_1_Spec, Day25Part1, "data2024/day25.txt", "data2024/day25.sample.txt", "3223", "3");
NULL_SOLVER_SPEC("Day25.2", Day25_2_Spec, Day25Part2, "data2024/day25.txt", "data2024/day25.sample.txt", "?", "?");
// clang-format on

struct SeasonSolvers {
  using Actual = aoc::solver::SolverTypeList<
      Day01_1_Spec::Actual, Day01_2_Spec::Actual, Day02_1_Spec::Actual,
      Day02_2_Spec::Actual, Day03_1_Spec::Actual, Day03_2_Spec::Actual,
      Day04_1_Spec::Actual, Day04_2_Spec::Actual, Day05_1_Spec::Actual,
      Day05_2_Spec::Actual, Day06_1_Spec::Actual, Day06_2_Spec::Actual,
      Day07_1_Spec::Actual, Day07_2_Spec::Actual, Day08_1_Spec::Actual,
      Day08_2_Spec::Actual, Day09_1_Spec::Actual, Day09_2_Spec::Actual,
      Day10_1_Spec::Actual, Day10_2_Spec::Actual, Day11_1_Spec::Actual,
      Day11_2_Spec::Actual, Day12_1_Spec::Actual, Day12_2_Spec::Actual,
      Day13_1_Spec::Actual, Day13_2_Spec::Actual, Day14_1_Spec::Actual,
      Day14_2_Spec::Actual, Day15_1_Spec::Actual, Day15_2_Spec::Actual,
      Day16_1_Spec::Actual, Day16_2_Spec::Actual, Day17_1_Spec::Actual,
      Day17_2_Spec::Actual, Day18_1_Spec::Actual, Day18_2_Spec::Actual,
      Day19_1_Spec::Actual, Day19_2_Spec::Actual, Day20_1_Spec::Actual,
      Day20_2_Spec::Actual, Day21_1_Spec::Actual, Day21_2_Spec::Actual,
      Day22_1_Spec::Actual, Day22_2_Spec::Actual, Day23_1_Spec::Actual,
      Day23_2_Spec::Actual, Day24_1_Spec::Actual, Day24_2_Spec::Actual,
      Day25_1_Spec::Actual, Day25_2_Spec::Actual>;

  using Sample = aoc::solver::SolverTypeList<
      Day01_1_Spec::Sample, Day01_2_Spec::Sample, Day02_1_Spec::Sample,
      Day02_2_Spec::Sample, Day03_1_Spec::Sample, Day03_2_Spec::Sample,
      Day04_1_Spec::Sample, Day04_2_Spec::Sample, Day05_1_Spec::Sample,
      Day05_2_Spec::Sample, Day06_1_Spec::Sample, Day06_2_Spec::Sample,
      Day07_1_Spec::Sample, Day07_2_Spec::Sample, Day08_1_Spec::Sample,
      Day08_2_Spec::Sample, Day09_1_Spec::Sample, Day09_2_Spec::Sample,
      Day10_1_Spec::Sample, Day10_2_Spec::Sample, Day11_1_Spec::Sample,
      Day11_2_Spec::Sample, Day12_1_Spec::Sample, Day12_2_Spec::Sample,
      Day13_1_Spec::Sample, Day13_2_Spec::Sample, Day14_1_Spec::Sample,
      Day14_2_Spec::Sample, Day15_1_Spec::Sample, Day15_2_Spec::Sample,
      Day16_1_Spec::Sample, Day16_2_Spec::Sample, Day17_1_Spec::Sample,
      Day17_2_Spec::Sample, Day18_1_Spec::Sample, Day18_2_Spec::Sample,
      Day19_1_Spec::Sample, Day19_2_Spec::Sample, Day20_1_Spec::Sample,
      Day20_2_Spec::Sample, Day21_1_Spec::Sample, Day21_2_Spec::Sample,
      Day22_1_Spec::Sample, Day22_2_Spec::Sample, Day23_1_Spec::Sample,
      Day23_2_Spec::Sample, Day24_1_Spec::Sample, Day24_2_Spec::Sample,
      Day25_1_Spec::Sample, Day25_2_Spec::Sample>;
};

} // namespace aoc::season2024

#endif /* __SEASON2024_SOLVERS_H__ */