#ifndef __DAY07PART1_H__
#define __DAY07PART1_H__

#include <cstdint>

#include "aocsolver/solver.h"
#include "season2024/types/rope_bridge_equation.h"

namespace aoc::season2024 {

class Day07Part1
    : public aoc::solver::Solver<Day07Part1, RopeBridgeEquations, uint64_t> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY07PART1_H__ */
