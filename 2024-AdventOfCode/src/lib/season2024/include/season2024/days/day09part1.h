#ifndef __DAY09PART1_H__
#define __DAY09PART1_H__

#include "season2024/types/disk.h"
#include "aocsolver/solver.h"

namespace aoc::season2024 {

class Day09Part1 : public aoc::solver::Solver<Day09Part1, Disk, long> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY09PART1_H__ */
