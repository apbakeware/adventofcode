#ifndef __DAY18PART1_H__
#define __DAY18PART1_H__

#include "aocsolver/solver.h"
#include "season2024/types/ram_run.h"

namespace aoc::season2024 {

class Day18Part1
    : public aoc::solver::Solver<Day18Part1, types::RamLocationLoader, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY18PART1_H__ */
