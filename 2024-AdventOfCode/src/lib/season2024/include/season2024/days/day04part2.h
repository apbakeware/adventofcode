#ifndef __DAY04PART2_H__
#define __DAY04PART2_H__

#include "aoc/grid/grid.h"
#include "aocsolver/solver.h"

namespace aoc::season2024 {

class Day04Part2
    : public aoc::solver::Solver<Day04Part2, aoc::grid::Fixed_Sized_Grid<char>,
                                 int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY04PART2_H__ */
