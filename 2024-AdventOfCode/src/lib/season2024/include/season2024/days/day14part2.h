#ifndef __DAY14PART2_H__
#define __DAY14PART2_H__

#include "aocinput/value_collection.h"
#include "aocsolver/solver.h"
#include "season2024/types/bathroom_robot.h"

namespace aoc::season2024 {

class Day14Part2
    : public aoc::solver::Solver<
          Day14Part2, aoc::input::ValueCollection<BathroomRobot>, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY14PART2_H__ */
