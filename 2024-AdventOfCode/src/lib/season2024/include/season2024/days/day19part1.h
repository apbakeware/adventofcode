#ifndef __DAY19PART1_H__
#define __DAY19PART1_H__

#include "aocsolver/solver.h"
#include "season2024/types/onsen_towels.h"

namespace aoc::season2024 {

class Day19Part1
    : public aoc::solver::Solver<Day19Part1, aoc::season2024::OnsenTowels,
                                 int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY19PART1_H__ */
