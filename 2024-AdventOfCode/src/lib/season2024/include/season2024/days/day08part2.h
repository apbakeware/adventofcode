#ifndef __DAY08PART2_H__
#define __DAY08PART2_H__

#include "aocsolver/solver.h"
#include "season2024/types/easter_bunny_transmitter.h"

namespace aoc::season2024 {

class Day08Part2
    : public aoc::solver::Solver<Day08Part2, EasterBunnyTransmitters, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY08PART2_H__ */
