#ifndef __DAY11PART2_H__
#define __DAY11PART2_H__

#include <cstdint>

#include "aocinput/value_collection.h"
#include "aocsolver/solver.h"
#include "season2024/types/pluto_stones.h"

namespace aoc::season2024 {

class Day11Part2
    : public aoc::solver::Solver<
          Day11Part2,
          aoc::input::ValueCollection<aoc::season2024::types::StoneValue>,
          aoc::season2024::types::StoneValue> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY11PART2_H__ */
