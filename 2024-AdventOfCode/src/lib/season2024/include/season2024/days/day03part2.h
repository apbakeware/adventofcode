#ifndef __DAY03PART2_H__
#define __DAY03PART2_H__

#include "aocinput/line_collection.h"
#include "aocsolver/solver.h"

namespace aoc::season2024 {

class Day03Part2
    : public aoc::solver::Solver<Day03Part2, aoc::input::LineCollection, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY03PART2_H__ */
