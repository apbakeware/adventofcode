#ifndef __DAY18PART2_H__
#define __DAY18PART2_H__

#include "aocsolver/solver.h"
#include "season2024/types/ram_run.h"

namespace aoc::season2024 {

class Day18Part2
    : public aoc::solver::Solver<Day18Part2, types::RamLocationLoader,
                                 std::string> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY18PART2_H__ */
