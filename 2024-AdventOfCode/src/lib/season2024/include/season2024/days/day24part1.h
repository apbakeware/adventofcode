#ifndef __DAY24PART1_H__
#define __DAY24PART1_H__

#include <cstdint>

#include "aocsolver/solver.h"
#include "season2024/types/fruit_monitor.h"

namespace aoc::season2024 {

class Day24Part1
    : public aoc::solver::Solver<Day24Part1, fruit_monitor::FruitMonitor,
                                 uint64_t> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY24PART1_H__ */
