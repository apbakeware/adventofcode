#ifndef __DAY05PART1_H__
#define __DAY05PART1_H__

#include "aocsolver/solver.h"

#include "season2024/types/safety_manual_printer.h"

namespace aoc::season2024 {

class Day05Part1
    : public aoc::solver::Solver<Day05Part1, SafetyManualPrintJobs, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY05PART1_H__ */
