#ifndef __DAY20PART1_H__
#define __DAY20PART1_H__

#include "aocsolver/solver.h"
#include "season2024/types/racetrack.h"

namespace aoc::season2024 {

class Day20Part1 : public aoc::solver::Solver<Day20Part1, Racetrack, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY20PART1_H__ */
