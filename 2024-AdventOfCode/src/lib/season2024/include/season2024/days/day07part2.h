#ifndef __DAY07PART2_H__
#define __DAY07PART2_H__

#include <cstdint>

#include "aocsolver/solver.h"
#include "season2024/types/rope_bridge_equation.h"

namespace aoc::season2024 {

class Day07Part2
    : public aoc::solver::Solver<Day07Part2, RopeBridgeEquations, uint64_t> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY07PART2_H__ */
