#ifndef __DAY09PART2_H__
#define __DAY09PART2_H__

#include "aocsolver/solver.h"
#include "season2024/types/disk.h"

namespace aoc::season2024 {

class Day09Part2 : public aoc::solver::Solver<Day09Part2, Disk, long> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY09PART2_H__ */
