#ifndef __DAY17PART2_H__
#define __DAY17PART2_H__

#include "aocsolver/solver.h"
#include "season2024/types/chronospatial_computer.h"

namespace aoc::season2024 {

class Day17Part2
    : public aoc::solver::Solver<Day17Part2,
                                 aoc::season2024::ChronospatialComputer, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY17PART2_H__ */
