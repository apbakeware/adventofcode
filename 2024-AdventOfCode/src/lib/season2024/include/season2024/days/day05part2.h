#ifndef __DAY05PART2_H__
#define __DAY05PART2_H__

#include "aocsolver/solver.h"

#include "season2024/types/safety_manual_printer.h"

namespace aoc::season2024 {

class Day05Part2
    : public aoc::solver::Solver<Day05Part2, SafetyManualPrintJobs, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY05PART2_H__ */
