#ifndef __DAY01PART1_H__
#define __DAY01PART1_H__

#include "aoc/util/null_type.h"
#include "aocsolver/solver.h"
#include "season2024/types/integer_column_data.h"

namespace aoc::season2024 {

class Day01Part1
    : public aoc::solver::Solver<Day01Part1, types::IntegerColumnData, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY01PART1_H__ */
