#ifndef __DAY23PART1_H__
#define __DAY23PART1_H__

#include "aocsolver/solver.h"
#include "season2024/types/lan_party.h"

namespace aoc::season2024 {

class Day23Part1
    : public aoc::solver::Solver<Day23Part1, aoc::season2024::LanParty, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY23PART1_H__ */
