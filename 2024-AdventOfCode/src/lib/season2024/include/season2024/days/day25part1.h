#ifndef __DAY25PART1_H__
#define __DAY25PART1_H__

#include "aocsolver/solver.h"
#include "season2024/types/tumbler_lock.h"

namespace aoc::season2024 {

class Day25Part1
    : public aoc::solver::Solver<Day25Part1, season2024::types::LocksAndKeys,
                                 int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY25PART1_H__ */
