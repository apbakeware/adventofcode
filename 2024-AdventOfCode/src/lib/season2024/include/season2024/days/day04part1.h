#ifndef __DAY04PART1_H__
#define __DAY04PART1_H__

#include "aoc/grid/grid.h"
#include "aocsolver/solver.h"

namespace aoc::season2024 {

class Day04Part1
    : public aoc::solver::Solver<Day04Part1, aoc::grid::Fixed_Sized_Grid<char>,
                                 int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY04PART1_H__ */
