#ifndef __DAY24PART2_H__
#define __DAY24PART2_H__

#include "aoc/util/null_type.h"
#include "aocsolver/solver.h"

namespace aoc::season2024 {

class Day24Part2 
  : public aoc::solver::Solver<Day24Part2, aoc::util::NullType, aoc::util::NullType> {
public:

  ResultType solve(InputType & input);
};

} // namespace aoc::season2024

#endif /* __DAY24PART2_H__ */
