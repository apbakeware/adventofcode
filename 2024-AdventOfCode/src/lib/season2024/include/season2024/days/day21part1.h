#ifndef __DAY21PART1_H__
#define __DAY21PART1_H__

#include "aoc/util/null_type.h"
#include "aocsolver/solver.h"

namespace aoc::season2024 {

class Day21Part1 
  : public aoc::solver::Solver<Day21Part1, aoc::util::NullType, aoc::util::NullType> {
public:

  ResultType solve(InputType & input);
};

} // namespace aoc::season2024

#endif /* __DAY21PART1_H__ */
