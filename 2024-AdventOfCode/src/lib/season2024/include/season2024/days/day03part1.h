#ifndef __DAY03PART1_H__
#define __DAY03PART1_H__

#include <string>

#include "aocinput/line_collection.h"
#include "aocsolver/solver.h"

namespace aoc::season2024 {

class Day03Part1
    : public aoc::solver::Solver<Day03Part1, aoc::input::LineCollection, int> {
public:
  ResultType solve(InputType &input);
};

} // namespace aoc::season2024

#endif /* __DAY03PART1_H__ */
