#ifndef __LAN_PARTY_H__
#define __LAN_PARTY_H__

#include <iosfwd>
#include <unordered_map>
#include <unordered_set>

#include "spdlog/fmt/ostr.h"
#include "spdlog/fmt/ranges.h"

namespace aoc::season2024 {

struct LanParty {
  using Name = std::string;
  using Names = std::unordered_set<Name>;
  using Connections = std::unordered_map<std::string, Names>;
  // Store bi-directional connections (ie a->b and the b->a are added)
  Connections connections;
  Names historian_options;
};

int countThreeWayHistorianOptions(const LanParty &lan);

std::istream &operator>>(std::istream &instr, LanParty &obj);

} // namespace aoc::season2024

template <>
struct fmt::formatter<aoc::season2024::LanParty>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::LanParty instance
  template <typename FormatContext>
  auto format(const aoc::season2024::LanParty &obj, FormatContext &ctx) const
      -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    fmt::format_to(ctx.out(), "LanParty(Connetions:");

    for (const auto &connection : obj.connections) {
      fmt::format_to(
          ctx.out(), "\n {} -> [{}]", connection.first,
          fmt::join(connection.second.begin(), connection.second.end(), " "));
    }

    return fmt::format_to(ctx.out(), "\nPossibilities: [{}]\n)",
                          fmt::join(obj.historian_options.begin(),
                                    obj.historian_options.end(), " "));
  }
};

#endif /* __LAN_PARTY_H__ */