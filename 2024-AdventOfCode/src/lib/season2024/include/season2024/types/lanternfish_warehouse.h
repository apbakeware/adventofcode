#ifndef __LANTERNFISH_WAREHOUSE_H__
#define __LANTERNFISH_WAREHOUSE_H__

#include <iosfwd>

#include "spdlog/fmt/ostr.h"

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"

namespace aoc::season2024 {

// TODO: Move this to grid_utils
struct ValidGridLocation {
  grid::Grid_Location location;
  bool is_valid;

  operator bool() const { return is_valid; }
};

struct LanternfishWarehouse {

  static constexpr char ROBOT = '@';
  static constexpr char WALL = '#';
  static constexpr char CRATE = 'O';
  static constexpr char OPEN = '.';

  // Since the robot wont be in a wall, it should always be good
  char peekInDirection(char movement);
  ValidGridLocation findOpenInDirection(char direction);

  void executeRobotCommand(char command);

  std::vector<aoc::grid::Grid_Location> allCrateLocations() const;

  aoc::grid::Fixed_Sized_Grid<char> warehouse_grid;
  std::string robot_commands;
  aoc::grid::Grid_Location robot_location;
};

std::istream &operator>>(std::istream &instr, LanternfishWarehouse &obj);

} // namespace aoc::season2024

template <>
struct fmt::formatter<aoc::season2024::ValidGridLocation>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::ValidGridLocation instance
  template <typename FormatContext>
  auto format(const aoc::season2024::ValidGridLocation &obj,
              FormatContext &ctx) const -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(ctx.out(),
                          "ValidGridLocation(location: {}  valid: {})",
                          obj.location, obj.is_valid);
  }
};

#endif /* __LANTERNFISH_WAREHOUSE_H__ */