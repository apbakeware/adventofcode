#ifndef __DISK_H__
#define __DISK_H__

#include <iosfwd>
#include <vector>

#include "spdlog/fmt/ostr.h"

namespace aoc::season2024 {

struct DiskBlock {
  static constexpr long EMPTY = -1;
  long id;
  int count;
  bool has_been_moved;
};

struct Disk {
  std::vector<DiskBlock> blocks;
};

bool isEmpty(const DiskBlock &record);

bool isEmptyWithSpace(const DiskBlock &record);

bool isEmptyWithoutSpace(const DiskBlock &record);

bool blockFitsIn(const DiskBlock &block, const DiskBlock &dest);

int blockSizeDifference(const DiskBlock &block, const DiskBlock &test);

template <typename Blocks_Coll_T> void printDisk(const Blocks_Coll_T &blocks) {
  for (const auto &block : blocks) {
    for (int cnt = 0; cnt < block.count; ++cnt) {
      if (isEmpty(block)) {
        fmt::print(".");
      } else {
        fmt::print("{}", block.id);
      }
    }
  }
  fmt::print("\n");
}

std::istream &operator>>(std::istream &instr, Disk &obj);

} // namespace aoc::season2024

template <>
struct fmt::formatter<aoc::season2024::DiskBlock>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::DiskBlock instance
  template <typename FormatContext>
  auto format(const aoc::season2024::DiskBlock &obj, FormatContext &ctx) const
      -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(ctx.out(), "DiskBlock(id: {}  space: {}  open: {})",
                          obj.id, obj.count, aoc::season2024::isEmpty(obj));
  }
};

#endif /* __DISK_H__ */