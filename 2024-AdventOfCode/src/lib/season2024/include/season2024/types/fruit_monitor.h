#ifndef __FRUIT_MONITOR_H__
#define __FRUIT_MONITOR_H__

#include <functional>
#include <iosfwd>
#include <string>
#include <unordered_map>
#include <vector>

#include "spdlog/fmt/bundled/core.h"

namespace aoc::season2024::fruit_monitor {

enum WireValue { ZERO, ONE, NOT_SET };

using WireIdType = std::string;
using WireTable = std::unordered_map<WireIdType, WireValue>;

using GateOp = std::function<WireValue(WireValue, WireValue)>;

WireValue andGateOp(WireValue in1, WireValue in2);
WireValue orGateOp(WireValue in1, WireValue in2);
WireValue xorGateOp(WireValue in1, WireValue in2);

class Gate {
public:
  Gate(const WireIdType &in1, const WireIdType &in2, const WireIdType &out,
       GateOp gate_op);

  bool inputsAvailable(const WireTable &wire_table) const;

  // Try to solve the gate logic. if it can
  // be solved, the WireTable is updated.
  bool trySolve(WireTable &wire_table) const;

  friend struct fmt::formatter<aoc::season2024::fruit_monitor::Gate>;

private:
  WireIdType m_in_1;
  WireIdType m_in_2;
  WireIdType m_out;
  GateOp m_gate_op;
};

struct FruitMonitor {
  WireTable wire_table;
  std::vector<Gate> gates;
};

std::istream &operator>>(std::istream &instr, FruitMonitor &obj);

} // namespace aoc::season2024::fruit_monitor

template <>
struct fmt::formatter<aoc::season2024::fruit_monitor::WireValue>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::fruit_monitor::WireValue instance
  template <typename FormatContext>
  auto format(const aoc::season2024::fruit_monitor::WireValue &obj,
              FormatContext &ctx) const -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator

    std::string value = "?";

    if (obj == aoc::season2024::fruit_monitor::ZERO) {
      value = "ZERO";
    } else if (obj == aoc::season2024::fruit_monitor::ONE) {
      value = "ONE";
    } else if (obj == aoc::season2024::fruit_monitor::NOT_SET) {
      value = "NOT_SET";
    }
    return fmt::format_to(ctx.out(), "WireValue({})", value);
  }
};

template <>
struct fmt::formatter<aoc::season2024::fruit_monitor::Gate>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::fruit_monitor::Gate instance
  template <typename FormatContext>
  auto format(const aoc::season2024::fruit_monitor::Gate &obj,
              FormatContext &ctx) const -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(ctx.out(),
                          "Gate(m_in_1={}, m_in_2= {}, m_out={}, op=?)",
                          obj.m_in_1, obj.m_in_2, obj.m_out);
  }
};

#endif /* __FRUIT_MONITOR_H__ */