#ifndef __PATROL_GUARD_H__
#define __PATROL_GUARD_H__

#include "spdlog/fmt/ostr.h"

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_navigation.h"

namespace aoc::season2024 {

struct PatrolGuard {
  aoc::grid::Grid_Location location;
  aoc::grid::Orientation4 orientation;
};

bool operator==(const PatrolGuard &lhs, const PatrolGuard &rhs);

using PatrolGrid = aoc::grid::Fixed_Sized_Grid<char>;
using PatrolLocations = std::vector<aoc::grid::Grid_Location>;
using PatrolNavigation = std::vector<PatrolGuard>;

/**
 * Identify all the navigation (location and orientation) on the patrol grid the
 * guard visits before leaving the area (off the grid).
 */
PatrolNavigation patrolAreaUntilExit(const PatrolGuard &guard_origin,
                                     const PatrolGrid &patrol_grid);

/**
 * Determine if the guard patrol path loops (true) or will exit the
 * area (false). The origin is the location to start from
 */
bool detectPatrolLooFrom(const PatrolGrid &patrol_grid,
                         const PatrolGuard &origin,
                         PatrolNavigation::iterator nav_start,
                         PatrolNavigation::iterator nav_end);

} // namespace aoc::season2024

template <>
struct fmt::formatter<aoc::season2024::PatrolGuard>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the Guard instance
  template <typename FormatContext>
  auto format(const aoc::season2024::PatrolGuard &obj, FormatContext &ctx) const
      -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(ctx.out(), "Guard( location: {}  orientation: {})",
                          obj.location, obj.orientation);
  }
};

#endif /* __PATROL_GUARD_H__ */