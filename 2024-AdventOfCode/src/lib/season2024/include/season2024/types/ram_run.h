#ifndef __RAM_RUN_H__
#define __RAM_RUN_H__

#include <iosfwd>
#include <vector>

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"

namespace aoc::season2024::types {

struct RamRunGrid {
  bool destination_reached;
  grid::Fixed_Sized_Grid<int> cost_grid;
  std::vector<grid::Grid_Location> path;
};

struct RamLocationLoader {

  std::vector<aoc::grid::Grid_Location> locations;

  RamRunGrid runRamGrid(const grid::Grid_Location &start,
                        const grid::Grid_Location &dest, size_t num_to_drop);
};

std::istream &operator>>(std::istream &instr, RamLocationLoader &loader);

} // namespace aoc::season2024::types

#endif /* __RAM_RUN_H__ */