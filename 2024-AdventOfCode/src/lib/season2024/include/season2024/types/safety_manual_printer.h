#ifndef __SAFETY_MANUAL_PRINTER_H__
#define __SAFETY_MANUAL_PRINTER_H__

#include <iosfwd>
#include <map>
#include <sstream>
#include <vector>

namespace aoc::season2024 {

class SafteyManualPrintRules {
public:
  /**
   * Add a rule that page_number must be printed after
   * required if required is to be printed.
   */
  void addRule(int page_number, int dependency);

  /**
   * Determine if the provided print queue is valid according to the
   * rules.
   */
  bool validatePrintQueue(const std::vector<int> &print_queue);

  /**
   * Fix the print queue according to the rules. If the original
   * queue was already valid the out_fixed_queue is equal to the
   * print_queue.
   *
   * @return True if a fix had to be made, otherwise false.
   */
  bool fixPrintQueue(const std::vector<int> &print_queue,
                     std::vector<int> &out_fixed_queue);

private:
  // key preceeds value
  std::multimap<int, int> m_dependencies;
};

struct SafetyManualPrintJobs {
  using PrintQueue = std::vector<int>;
  using PrintQueues = std::vector<PrintQueue>;

  SafteyManualPrintRules rules;
  PrintQueues print_queues;
};

std::istream &operator>>(std::istream &instr, SafetyManualPrintJobs &rec);

} // namespace aoc::season2024

#endif /* __SAFETY_MANUAL_PRINTER_H__ */