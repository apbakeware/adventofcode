#ifndef __INTEGER_COLUMN_DATA_H__
#define __INTEGER_COLUMN_DATA_H__

#include <iosfwd>
#include <vector>

namespace aoc::season2024::types {

struct IntegerColumnData {

  std::vector<int> left;
  std::vector<int> right;
};

std::istream &operator>>(std::istream &instr, IntegerColumnData &record);

} // namespace aoc::season2024::types

#endif /* __INTEGER_COLUMN_DATA_H__ */