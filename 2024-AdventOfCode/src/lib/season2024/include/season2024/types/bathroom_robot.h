#ifndef __BATHROOM_ROBOT_H__
#define __BATHROOM_ROBOT_H__

#include <iosfwd>
#include <utility>

#include "spdlog/fmt/ostr.h"

namespace aoc::season2024 {

struct BathroomRobot {

  int start_x;
  int start_y;
  int vel_x;
  int vel_y;
};

std::istream &operator>>(std::istream &instr, BathroomRobot &obj);

std::pair<int, int> applyMotion(const BathroomRobot &robot, int frames,
                                int room_width, int room_height);

} // namespace aoc::season2024

template <>
struct fmt::formatter<aoc::season2024::BathroomRobot>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::BathroomRobot instance
  template <typename FormatContext>
  auto format(const aoc::season2024::BathroomRobot &obj,
              FormatContext &ctx) const -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(
        ctx.out(),
        "BathroomRobot(start_x: {}  start_y: {}  vel_x: {}  vel_y: {})",
        obj.start_x, obj.start_y, obj.vel_x, obj.vel_y);
  }
};

#endif /* __BATHROOM_ROBOT_H__ */