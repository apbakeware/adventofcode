#ifndef __MONKEY_MARKET_H__
#define __MONKEY_MARKET_H__

#include <cstddef>
#include <cstdint>

namespace aoc::season2024::monkey_market {

using ValueType = uint64_t;

ValueType computeNextSecret(ValueType secret);

static constexpr size_t MUL_64_SHIFT = 6;
static constexpr size_t MUL_2048_SHIFT = 11;
static constexpr size_t DIV_32_SHIFT = 5;
static constexpr ValueType MOD_16777216 = 0xFFFFFF;

ValueType doStepOne(ValueType secret);
ValueType doStepTwo(ValueType secret);
ValueType doStepThree(ValueType secret);

ValueType doMix(ValueType secret, ValueType value);

ValueType doPrune(ValueType secret);

} // namespace aoc::season2024::monkey_market

#endif /* __MONKEY_MARKET_H__ */