#ifndef __CLAW_MACHINE_H__
#define __CLAW_MACHINE_H__

#include <iosfwd>

#include "spdlog/fmt/ostr.h"

#include "aoc/grid/grid_location.h"

namespace aoc::season2024 {

class ClawMachine {
public:
  friend std::istream &operator>>(std::istream &instr, ClawMachine &obj);
  friend struct fmt::formatter<ClawMachine>;

  const aoc::grid::Grid_Location buttonAMovement() const {
    return m_button_a_movement;
  }
  const aoc::grid::Grid_Location buttonBMovement() const {
    return m_button_b_movement;
  }
  const aoc::grid::Grid_Location prizeLocation() const {
    return m_prize_location;
  }

private:
  aoc::grid::Grid_Location m_button_a_movement;
  aoc::grid::Grid_Location m_button_b_movement;
  aoc::grid::Grid_Location m_prize_location;
};

} // namespace aoc::season2024

template <>
struct fmt::formatter<aoc::season2024::ClawMachine>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::ClawMachine instance
  template <typename FormatContext>
  auto format(const aoc::season2024::ClawMachine &obj, FormatContext &ctx) const
      -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    return fmt::format_to(
        ctx.out(), "ClawMachine(ButtonA: {}  ButtonB: {}  Prize: {})",
        obj.m_button_a_movement, obj.m_button_b_movement, obj.m_prize_location);
  }
};

#endif /* __CLAW_MACHINE_H__ */