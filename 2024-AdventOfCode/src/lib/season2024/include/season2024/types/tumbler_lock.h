#ifndef __TUMBLER_LOCK_H__
#define __TUMBLER_LOCK_H__

#include <iosfwd>
#include <vector>

#include "spdlog/fmt/bundled/core.h"
#include "spdlog/fmt/bundled/ranges.h"

namespace aoc::season2024::types {

using HeightList = std::vector<int>;

struct LocksAndKeys {
  static constexpr int TUMBLER_HEIGHT = 5;
  std::vector<HeightList> locks;
  std::vector<HeightList> keys;
};

bool keyFitsInLock(const HeightList &key, const HeightList &lock);

std::istream &operator>>(std::istream &instr, LocksAndKeys &obj);

} // namespace aoc::season2024::types

template <>
struct fmt::formatter<aoc::season2024::types::LocksAndKeys>
    : fmt::formatter<std::string_view> {

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    return ctx.begin();
  }

  // Formatting the aoc::season2024::types::LocksAndKeys instance
  template <typename FormatContext>
  auto format(const aoc::season2024::types::LocksAndKeys &obj,
              FormatContext &ctx) const -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    fmt::format_to(ctx.out(), "LocksAndKeys(\n");
    fmt::format_to(ctx.out(), " Locks:\n");
    for (const auto &lock : obj.locks) {
      fmt::format_to(ctx.out(), "{}", fmt::join(lock.begin(), lock.end(), " "));
      fmt::format_to(ctx.out(), "\n");
    }
    fmt::format_to(ctx.out(), " Keys:\n");
    for (const auto &key : obj.keys) {
      fmt::format_to(ctx.out(), "{}", fmt::join(key.begin(), key.end(), " "));
      fmt::format_to(ctx.out(), "\n");
    }
    return fmt::format_to(ctx.out(), ")");
  }
};

#endif /* __TUMBLER_LOCK_H__ */