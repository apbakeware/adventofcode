#ifndef __SEASON_DFN_MACROS_H__
#define __SEASON_DFN_MACROS_H__

#include "aocinput/provider/file_get_line_provider.h"
#include "aocinput/provider/file_instream_provider.h"
#include "aocinput/provider/null_provider.h"
#include "aocsolver/null_solver.h"

// clang-format off
#define NULL_SOLVER_SPEC(key, struct_name, ...)                                \
  struct struct_name {                                                         \
                                                                               \
    struct Actual {                                                            \
      using SolverType = aoc::solver::NullSolver;                              \
      using InputProviderType = aoc::input::provider::NullProvider<aoc::util::NullType>;          \
      static std::string name() { return key; }                                \
      static std::string expected() { return "NullType"; }                     \
    };                                                                         \
                                                                               \
    struct Sample {                                                            \
      using SolverType = aoc::solver::NullSolver;                              \
      using InputProviderType = aoc::input::provider::NullProvider<aoc::util::NullType>;          \
      static std::string name() { return key; }                                \
      static std::string expected() { return "NullType"; }                     \
    };                                                                         \
  };

#define FILE_SOLVER_SPEC(key, struct_name, solver, data_file_path, sample_file_path, soln, sample_soln )   \
  struct struct_name {                                                         \
                                                                               \
    struct Actual {                                                            \
      using SolverType = solver;                                               \
      using InputProviderType = aoc::input::provider::FileInstreamProvider<typename SolverType::InputType>;      \
      static std::string name() { return key; }                                \
      static std::tuple<std::string> providerArgs() {                          \
        return std::tuple<std::string>(data_file_path);                        \
      }                                                                        \
      static std::string expected() { return soln; }                           \
    };                                                                         \
                                                                               \
    struct Sample {                                                            \
      using SolverType = solver;                                               \
      using InputProviderType = aoc::input::provider::FileInstreamProvider<typename SolverType::InputType>;      \
      static std::string name() { return key; }                                \
      static std::tuple<std::string> providerArgs() {                          \
        return std::tuple<std::string>(sample_file_path);                      \
      }                                                                        \
      static std::string expected() { return sample_soln; }                    \
    };                                                                         \
  };

#define GETLINE_SOLVER_SPEC(key, struct_name, solver, data_file_path, sample_file_path, soln, sample_soln )   \
  struct struct_name {                                                         \
                                                                               \
    struct Actual {                                                            \
      using SolverType = solver;                                               \
      using InputProviderType = aoc::input::provider::FileGetLineProvider<typename SolverType::InputType>;      \
      static std::string name() { return key; }                                \
      static std::tuple<std::string> providerArgs() {                          \
        return std::tuple<std::string>(data_file_path);                        \
      }                                                                        \
      static std::string expected() { return soln; }                           \
    };                                                                         \
                                                                               \
    struct Sample {                                                            \
      using SolverType = solver;                                               \
      using InputProviderType = aoc::input::provider::FileGetLineProvider<typename SolverType::InputType>;      \
      static std::string name() { return key; }                                \
      static std::tuple<std::string> providerArgs() {                          \
        return std::tuple<std::string>(sample_file_path);                      \
      }                                                                        \
      static std::string expected() { return sample_soln; }                    \
    };                                                                         \
  };

// clang-format on

#endif /* __SEASON_DFN_MACROS_H__ */