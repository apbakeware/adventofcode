#include <iostream>
#include <sstream>

#include "season2024/types/rope_bridge_equation.h"

namespace aoc::season2024 {

void RopeBridgeEquations::append(const std::string &line) {

  RopeBridgeEquation bridge_eqn;
  std::istringstream lstr(line);
  lstr >> bridge_eqn;

  equations.push_back(bridge_eqn);
}

std::istream &operator>>(std::istream &instr, RopeBridgeEquation &obj) {
  char discard = '\n';
  int value = 0;
  instr >> obj.answer >> discard;
  while (instr >> value) {
    obj.operands.push_back(value);
  }

  return instr;
}

} // namespace aoc::season2024