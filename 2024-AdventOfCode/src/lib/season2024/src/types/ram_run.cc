#include <array>
#include <deque>
#include <iostream>
#include <map> // unordered map...need has for Grid_Location
#include <stack>

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_utils.h"
#include "season2024/types/ram_run.h"

namespace aoc::season2024::types {

RamRunGrid RamLocationLoader::runRamGrid(const grid::Grid_Location &start,
                                         const grid::Grid_Location &dest,
                                         size_t num_to_drop) {
  using LocationDistance = grid::Location_Value<int>;
  using ParentMap =
      std::map<grid::Grid_Location,
               grid::Grid_Location>; // key is child value is parent
  static const int UNVISITED = -1;
  static const int BLOCKED = 99999;

  const int grid_width = dest.col + 1 - start.col;
  const int grid_height = dest.row + 1 - start.row;
  grid::Fixed_Sized_Grid<int> grid(grid_width, grid_height, UNVISITED);
  std::deque<LocationDistance> location_stack;
  std::array<grid::Grid_Location, 4> neighbors;
  ParentMap parent_of;
  bool desintation_reached = false;

  auto block_iter = locations.begin();
  auto block_stop_iter = block_iter;
  std::advance(block_stop_iter, std::min(locations.size(), num_to_drop));
  while (block_iter != block_stop_iter) {
    grid.set(*block_iter, BLOCKED);
    ++block_iter;
  }

  location_stack.push_back({start, 0});
  int stopper = 100000;
  while (!location_stack.empty()) {
    assert(--stopper > 0);
    const auto node = location_stack.front();
    location_stack.pop_front();

    const auto grid_value = grid.at(node.location);
    if (grid_value == UNVISITED) {
      grid.set(node.location, node.value);

      if (node.location != dest) {
        neighbors[0] = grid::Grid_Location::up(node.location);
        neighbors[1] = grid::Grid_Location::right(node.location);
        neighbors[2] = grid::Grid_Location::down(node.location);
        neighbors[3] = grid::Grid_Location::left(node.location);

        for (const auto &neighbor : neighbors) {
          bool add_neighbor = grid::is_on_grid(neighbor, grid) &&
                              grid.at(neighbor) == UNVISITED;
          if (add_neighbor) {
            parent_of[neighbor] = node.location;
            location_stack.push_back({neighbor, node.value + 1});
          }
        }
      }
    } else {
      desintation_reached = true;
    }
  }

  // Reverse walk the parent table to build the path
  std::vector<grid::Grid_Location> path;
  path.push_back(dest);
  auto parent = parent_of.find(dest);
  while (parent != parent_of.end()) {
    path.push_back(parent->second);
    parent = parent_of.find(parent->second);
  }

  std::reverse(path.begin(), path.end());

  return {desintation_reached, grid, path};
}

std::istream &operator>>(std::istream &instr, RamLocationLoader &loader) {

  aoc::grid::Grid_Location location;
  char comma;
  while (instr >> location.col >> comma >> location.row) {
    loader.locations.push_back(location);
  }
  return instr;
}

} // namespace aoc::season2024::types