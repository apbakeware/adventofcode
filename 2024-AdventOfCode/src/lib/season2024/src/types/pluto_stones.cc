#include <cstddef>
#include <cstdint>
#include <iterator>
#include <queue>
#include <sstream>
#include <string>
#include <unordered_map>

#include "spdlog/spdlog.h"

#include "season2024/types/pluto_stones.h"

namespace {

using Splits = std::pair<unsigned long, unsigned long>;
using SplitCache = std::unordered_map<unsigned long, Splits>;

SplitCache split_store;

} // namespace

namespace aoc::season2024::types {

unsigned long doBlinks(const StoneCollection::ValuesType &values,
                       int blinks_to_do) {

  const StoneCollection::ValueType ONE = 1;

  StoneCollection::ValuesType buffer1 = values;
  StoneCollection::ValuesType buffer2;

  StoneCollection::ValuesType *processing = &buffer1;
  StoneCollection::ValuesType *building = &buffer2;

  fmt::print("Blink: \n");
  for (int cnt = 0; cnt < blinks_to_do; ++cnt) {
    if (cnt % 5 == 0) {
      fmt::print("....{}\n", cnt);
    }
    for (const auto value : *processing) {
      if (value == 0) {
        building->push_back(ONE);
      } else {
        const std::string srepr = std::to_string(value);
        const bool is_even = (srepr.size() % 2) == 0;
        if (is_even) {

          const auto cache_iter = split_store.find(value);
          if (cache_iter != split_store.end()) {
            building->push_back(cache_iter->second.first);
            building->push_back(cache_iter->second.second);
          } else {
            size_t half_point = srepr.size() / 2;
            const std::string left_split = srepr.substr(0, half_point);
            const std::string right_split = srepr.substr(half_point);

            std::istringstream left_istr(left_split);
            std::istringstream right_istr(right_split);

            StoneCollection::ValueType l_val = 0;
            StoneCollection::ValueType r_val = 0;
            left_istr >> l_val;
            building->push_back(l_val);
            right_istr >> r_val;
            building->push_back(r_val);

            auto insert_pair =
                std::make_pair(value, std::make_pair(l_val, r_val));

            split_store.insert(insert_pair);
          }

        } else {
          building->push_back(value * 2024);
        }
      }
    }
    std::swap(building, processing);
    building->clear();
  }

  return processing->size();
}
Blink blinkStoneValue(StoneValue value) {

  Blink result = std::make_tuple(0, 0, 0);

  if (value == 0) {
    result = std::make_tuple(1, 1, 0);
  } else {
    const std::string srepr = std::to_string(value);
    const bool is_odd = (srepr.size() % 2) == 1;
    if (is_odd) {
      result = std::make_tuple(1, value * 2024, 0);
    } else {

      size_t half_point = srepr.size() / 2;
      const std::string left_split = srepr.substr(0, half_point);
      const std::string right_split = srepr.substr(half_point);
      const StoneValue lval = std::atoi(left_split.c_str());
      const StoneValue rval = std ::atoi(right_split.c_str());

      result = std::make_tuple(2, lval, rval);
    }
  }

  return result;
}

// Thanks @DaveFollett for explaining to me. I was hyper
// focused on caching the splits, he opened my eyes
// to just storing counts of rock numbers and doing
// breadth-first (all stones at a level) and decrementing
// the old stone value and increment the new stone
// value(s).
//
// I got the caching portion, just not the what-to-cache.
StoneValue doBlinksFast(const StoneCollection::ValuesType &values,
                        size_t blinks_to_do) {

  using CountType = uint64_t;
  using StoneCounts = std::unordered_map<StoneValue, CountType>;

  StoneCounts stones;
  // todo; try and track stone counts as we go

  // fmt::print("Initial set\n");
  for (const auto stone : values) {
    stones.insert(std::make_pair(stone, 1));
    // fmt::print("  Stone: {:>12}  Count: 1\n", stone);
  }

  for (size_t cnt = 1; cnt <= blinks_to_do; ++cnt) {
    StoneCounts new_stones;
    // fmt::print("BLINK {}\n", cnt);
    for (auto &stone_count : stones) {
      const auto stone_value = stone_count.first;
      const auto instances_of_stone = stone_count.second;
      const auto blink = blinkStoneValue(stone_value);
      // TODO use a cache later?
      // fmt::print(
      //     " Stone: {:>12}  Instances: {:>10}  -- Blink <0>: {}  <1>: {}  "
      //     "<2>: {}\n",
      //     stone_value, instances_of_stone, std::get<0>(blink),
      //     std::get<1>(blink), std::get<2>(blink));

      assert(std::get<0>(blink) > 0);
      new_stones[std::get<1>(blink)] += instances_of_stone;
      if (std::get<0>(blink) == 2) {
        new_stones[std::get<2>(blink)] += instances_of_stone;
      }
    }

    stones = new_stones;
    // fmt::print(" After Blink\n");
    // for (auto &stone_count : stones) {
    //   fmt::print("   Stone: {:>12}  Count: {:>8}\n", stone_count.first,
    //              stone_count.second);
    // }
  }

  StoneValue total_num_stones = 0;
  // fmt::print("FINAL STONES\n");
  for (auto &stone_count : stones) {
    // fmt::print("Stone: {:>12}  Count: {:>8}\n", stone_count.first,
    //            stone_count.second);
    total_num_stones += stone_count.second;
  }
  return total_num_stones;
}

} // namespace aoc::season2024::types

// 50 3321269182
// 75