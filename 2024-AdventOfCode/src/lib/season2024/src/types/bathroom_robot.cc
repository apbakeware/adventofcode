#include <iostream>
#include <regex>
#include <string>

#include "spdlog/spdlog.h"

#include "season2024/types/bathroom_robot.h"

namespace aoc::season2024 {

std::istream &operator>>(std::istream &instr, BathroomRobot &obj) {

  static std::regex regex(R"(p=(-?\d+),(-?\d+)\s+v=(-?\d+),(-?\d+))");
  std::string line;
  std::smatch match;

  std::getline(instr, line);
  if (std::regex_match(line, match, regex)) {
    obj.start_x = std::atoi(match[1].str().c_str());
    obj.start_y = std::atoi(match[2].str().c_str());
    obj.vel_x = std::atoi(match[3].str().c_str());
    obj.vel_y = std::atoi(match[4].str().c_str());
  } else {
    fmt::print("line does not match: '{}'\n", line);
  }

  return instr;
}

std::pair<int, int> applyMotion(const BathroomRobot &robot, int frames,
                                int room_width, int room_height) {

  std::pair<int, int> new_location =
      std::make_pair((robot.start_x + robot.vel_x * frames) % room_width,
                     (robot.start_y + robot.vel_y * frames) % room_height);

  if (new_location.first < 0) {
    new_location.first = room_width + new_location.first;
  }

  if (new_location.second < 0) {
    new_location.second = room_height + new_location.second;
  }

  return new_location;
}

} // namespace aoc::season2024