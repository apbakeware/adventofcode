#include <array>
#include <iostream>
#include <string>
#include <vector>

#include "spdlog/spdlog.h"

#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_utils.h"

#include "season2024/types/racetrack.h"

namespace aoc::season2024 {

void Racetrack::findPath() {
  std::map<grid::Grid_Location, grid::Grid_Location>
      parent_table; // [child] => parent

  int cost = 0;
  grid::Grid_Location current = start;
  std::array<grid::Grid_Location, 4> neighbors;

  location_cost[start] = cost;

  // will always be on the grid, so since there is just 1 path
  // look for the neighbor which doesnt have a cost yet and isn't a wall
  while (current != end) {
    ++cost;
    neighbors[0] = grid::Grid_Location::up(current);
    neighbors[1] = grid::Grid_Location::right(current);
    neighbors[2] = grid::Grid_Location::down(current);
    neighbors[3] = grid::Grid_Location::left(current);

    for (const auto &neighbor : neighbors) {
      if (racetrack.at(neighbor) != Racetrack::WALL &&
          parent_table.count(neighbor) == 0) {
        location_cost[neighbor] = cost;
        parent_table[neighbor] = current;
        current = neighbor;
        break;
      }
    }
  }
  location_cost[end] = cost;
  path.reserve(parent_table.size());
  while (current != start) {
    path.push_back(current);
    current = parent_table[current];
  }
  path.push_back(start);

  std::reverse(path.begin(), path.end());
}

// Just scan row and columns

std::vector<int> Racetrack::findCheats() {
  if (path.empty()) {
    SPDLOG_ERROR("Path not found, findPath() first");
    return {};
  }

  std::vector<int> cheat_savings;

  // Scan rows, since there is a border, start at {1,1} and skip the last line
  // TOOD: confirm end row
  for (int row = 1; row < racetrack.number_of_rows() - 3; ++row) {
    for (int col = 1; col < racetrack.number_of_cols() - 1; ++col) {
      const grid::Grid_Location base = {row, col};
      const grid::Grid_Location mid = {row + 1, col};
      const grid::Grid_Location cheat = {row + 2, col};

      const auto base_iter = location_cost.find(base);
      const auto cheat_iter = location_cost.find(cheat);

      if (base_iter != location_cost.end() &&
          cheat_iter != location_cost.end() &&
          racetrack.at(mid) == Racetrack::WALL) {

        const auto time_saved =
            std::abs(cheat_iter->second - base_iter->second) -
            2; // 2 for the steps in the wall

        cheat_savings.push_back(time_saved);
        // fmt::print("Found cheat from {}({}) -> {}({}) saving: {}\n", base,
        //            base_iter->second, cheat, cheat_iter->second, time_saved);
      }
    }
  }

  for (int col = 1; col < racetrack.number_of_cols() - 3; ++col) {
    for (int row = 1; row < racetrack.number_of_rows() - 1; ++row) {

      const grid::Grid_Location base = {row, col};
      const grid::Grid_Location mid = {row, col + 1};
      const grid::Grid_Location cheat = {row, col + 2};

      const auto base_iter = location_cost.find(base);
      const auto cheat_iter = location_cost.find(cheat);

      if (base_iter != location_cost.end() &&
          cheat_iter != location_cost.end() &&
          racetrack.at(mid) == Racetrack::WALL) {

        const auto time_saved =
            std::abs(cheat_iter->second - base_iter->second) -
            2; // 2 for the steps in the wall

        cheat_savings.push_back(time_saved);
        // fmt::print("Found cheat from {}({}) -> {}({}) saving: {}\n", base,
        //            base_iter->second, cheat, cheat_iter->second, time_saved);
      }
    }
  }

  std::sort(cheat_savings.begin(), cheat_savings.end());
  return cheat_savings;
}

std::istream &operator>>(std::istream &instr, Racetrack &obj) {
  std::vector<std::string> lines;
  std::string line;

  while (instr >> line) {
    lines.push_back(line);
  }

  obj.racetrack =
      aoc::grid::create_char_grid_from_string_rows(lines.begin(), lines.end());

  const auto &start = aoc::grid::find_value(obj.racetrack, Racetrack::START);
  const auto &end = aoc::grid::find_value(obj.racetrack, Racetrack::END);

  if (!start.second) {
    SPDLOG_ERROR("Start location not found");
  } else {
    obj.start = start.first;
  }

  if (!end.second) {
    SPDLOG_ERROR("End location not found");
  } else {
    obj.end = end.first;
  }

  return instr;
}

} // namespace aoc::season2024