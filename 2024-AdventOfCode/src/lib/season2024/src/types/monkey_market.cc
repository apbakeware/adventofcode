#include "spdlog/fmt/fmt.h"

#include "season2024/types/monkey_market.h"

namespace aoc::season2024::monkey_market {

ValueType computeNextSecret(ValueType secret) {
  return doStepThree(doStepTwo(doStepOne(secret)));
}

ValueType doMix(ValueType secret, ValueType value) {

  const ValueType result = (secret ^ value);
  // fmt::print("\tdoMix\n\t{:#064b} ({:>12}) Secret\n\t{:#064b} ({:>12}) "
  //            "Value\n\t{:#064b} ({:>12}) Mix\n",
  //            secret, secret, value, value, result, result);

  return result;
}

ValueType doPrune(ValueType secret) {
  const ValueType result = secret & MOD_16777216;
  // fmt::print(
  //     "\n\tdoPrune\n\t{:#064b} ({:>12}) Secret\n\t{:#064b} ({:>12}) Value\n",
  //     secret, secret, result, result);

  return result;
}

ValueType doStepOne(ValueType secret) {

  // fmt::print("step 1\n{:#064b} ({:>12}) Secret\n", secret, secret);

  const ValueType mul = secret << MUL_64_SHIFT; // mul 64
  const ValueType mix = doMix(secret, mul);
  const ValueType pruned = doPrune(mix);

  // fmt::print("step 1 Values\n{:#064b} ({:>12}) Secret\n{:#064b} ({:>12}) "
  //            "Mul64\n{:#064b} ({:>12}) Mix\n{:#064b} ({:>12}) Pruned\n",
  //            secret, secret, mul, mul, mix, mix, pruned, pruned);

  return pruned;
}

ValueType doStepTwo(ValueType secret) {
  // fmt::print("step 2\n{:#064b} ({:>12}) Secret\n", secret, secret);
  const ValueType div = secret >> DIV_32_SHIFT;
  const ValueType mix = doMix(secret, div);
  const ValueType pruned = doPrune(mix);

  // fmt::print("step 2 Values\n{:#064b} ({:>12}) Secret\n{:#064b} ({:>12}) "
  //            "Div32\n{:#064b} ({:>12}) Mix\n{:#064b} ({:>12}) Pruned\n",
  //            secret, secret, div, div, mix, mix, pruned, pruned);

  return pruned;
}

ValueType doStepThree(ValueType secret) {
  // fmt::print("step 3\n{:#064b} ({:>12}) Secret\n", secret, secret);
  const ValueType mul = secret << MUL_2048_SHIFT;
  const ValueType mix = doMix(secret, mul);
  const ValueType pruned = doPrune(mix);

  // fmt::print("step 3 Values\n{:#064b} ({:>12}) Secret\n{:#064b} ({:>12}) "
  //            "Mul2048\n{:#064b} ({:>12}) Mix\n{:#064b} ({:>12}) Pruned\n",
  //            secret, secret, mul, mul, mix, mix, pruned, pruned);

  return pruned;
}

ValueType doSteps(ValueType secret) {
  return doStepThree(doStepTwo(doStepOne(secret)));
  // fmt::print("Process Secret: {:#064b} ({:>12}) Secret\n", secret, secret);
  // ValueType next = (secret * 64) ^ secret;
  // fmt::print("Mul64 ^ secret = {:#064b} ({:>12})\n", next, next);
  // next = next % 16777216;
  // fmt::print("% 16777216 = {:#064b} ({:>12})\n", next, next);

  // next = (next / 32) ^ next;
  // fmt::print("Div32 ^ secret = {:#064b} ({:>12})\n", next, next);
  // next = next % 16777216;
  // fmt::print("% 16777216 = {:#064b} ({:>12})\n", next, next);

  // next = (next * 2048) ^ next;
  // fmt::print("Mul2048 ^ secret = {:#064b} ({:>12})\n", next, next);
  // next = next % 16777216;
  // fmt::print("% 16777216 = {:#064b} ({:>12})\n", next, next);

  // return next;
}

} // namespace aoc::season2024::monkey_market