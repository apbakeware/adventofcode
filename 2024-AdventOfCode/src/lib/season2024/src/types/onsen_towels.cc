#include <iostream>
#include <regex>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "spdlog/fmt/bundled/core.h"
#include "spdlog/spdlog.h"

#include "season2024/types/onsen_towels.h"

namespace {

class PatternCache {
public:
  PatternCache() : m_cache_hits(0), m_cache_misses(0) {}

  void storeInCache(const std::string &pattern) { m_cache.insert(pattern); }

  void storeInCache(const std::string &pattern, unsigned long count) {
    // fmt::print("Storing Cache combo: {} ({})\n", pattern, count);
    m_combination_cache.insert(std::make_pair(pattern, count));
  }

  bool isCached(const std::string &pattern) {
    bool in_cache = m_cache.count(pattern) > 0;
    if (in_cache) {
      ++m_cache_hits;
    } else {
      ++m_cache_misses;
    }
    return in_cache;
  }

  std::pair<bool, unsigned long>
  isCombinationCached(const std::string &pattern) const {
    std::pair<bool, unsigned long> result = std::make_pair(false, 0);
    const auto iter = m_combination_cache.find(pattern);

    result.first = iter != m_combination_cache.end();
    if (result.first) {
      // fmt::print("pattern: {} is cached with combo count: {}\n", pattern,
      //            iter->second);
      result.second = iter->second;
    }

    return result;
  }

  void stats() const {
    fmt::print("Pattern Cache:\n Cached patterns: {}\n Cache hits: {}\n Cache "
               "misses: {}\n",
               m_cache.size(), m_cache_hits, m_cache_misses);
  }

private:
  using CacheType = std::unordered_set<std::string>;
  using CombinationsCacheType = std::unordered_map<std::string, unsigned long>;

  CacheType m_cache;
  CombinationsCacheType m_combination_cache;
  int m_cache_hits;
  int m_cache_misses;
};

PatternCache pattern_cache;

bool matchesAtPosition(const std::string &str, const std::string &substring,
                       size_t position) {
  // Ensure the position is valid and the substring fits within the main string
  if (position + substring.size() > str.size()) {
    return false;
  }
  return str.compare(position, substring.size(), substring) == 0;
}

bool recursively_process_pattern(const std::string &pattern,
                                 const std::vector<std::string> &towels,
                                 size_t pattern_position) {

  if (pattern_position == pattern.size()) {
    // fmt::print("nothign left, successful pattern\n");
    return true;
  } else if (pattern_cache.isCached(pattern.substr(pattern_position))) {
    // fmt::print("Pattern: {} Final substring: {} position: {} cache hit\n",
    //            pattern, pattern.substr(pattern_position), pattern_position);
    return true;
  }

  // fmt::print("Checking remaining pattern: {}\n",
  //            pattern.substr(pattern_position));

  bool found = false;
  for (auto towel_iter = towels.begin(); towel_iter != towels.end() && !found;
       ++towel_iter) {

    const auto &towel = *towel_iter;
    const size_t remaining_size = pattern.size() - pattern_position;
    // fmt::print("  pattern: {} remaining: {}({})  pos: {} -- against:
    // {}({})\n",
    //            pattern, pattern.substr(pattern_position), remaining_size,
    //            pattern_position, towel, towel.size());

    bool begins_with = matchesAtPosition(pattern, towel, pattern_position);
    if (begins_with) {
      pattern_cache.storeInCache(
          pattern.substr(0, pattern_position + towel.size()));

      // fmt::print("Pattern: {} substring: {} matches: {}\n", pattern,
      //            pattern.substr(pattern_position), towel);
      found = recursively_process_pattern(pattern, towels,
                                          pattern_position + towel.size());

      if (found) {
        pattern_cache.storeInCache(pattern.substr(pattern_position));
      }
    }
  }

  // if (!found) {
  //   fmt::print("Substring not found: {}\n",
  //   pattern.substr(pattern_position));
  // }
  return found;
}

// When called from rwalk_pattern, the
// char a pos will be new. The string to the
// right of pos was previously evalauted and should
// therefore be cached.
void compute_combination_count(const std::string &pattern,
                               const std::vector<std::string> &towels) {

  unsigned long count = 0;

  // fmt::print("Checking pattern: {}\n", pattern);
  for (const auto &towel : towels) {
    // fmt::print("towel: {}\n", towel);
    if (towel.size() > pattern.size()) {
      continue;
    }
    if (pattern.compare(0, towel.size(), towel) == 0) {
      if (pattern.size() == towel.size()) {
        ++count;
      } else {
        const auto &cache_index = pattern.substr(towel.size());
        const auto &cached_right =
            pattern_cache.isCombinationCached(cache_index);
        if (!cached_right.first) {
          SPDLOG_ERROR("EXPECTED CACHE OF: {}", cache_index);
        }
        count += cached_right.second;
      }
    }
  }
  pattern_cache.storeInCache(pattern, count);
}

// call in reverseing order so the "right hand"
// should already be cached and the character
// to the left will be added.
unsigned long rwalk_pattern(const std::string &pattern,
                            const std::vector<std::string> &towels) {

  // fmt::print("PATTERN ~~~~~ {}\n", pattern);
  for (size_t pos = pattern.size(); pos > 1; --pos) {
    const auto &patter_right = pattern.substr(pos - 1);
    compute_combination_count(patter_right, towels);
  }

  compute_combination_count(pattern, towels);
  return pattern_cache.isCombinationCached(pattern).second;

  // return compute_combination_count(pattern, towels);
}

} // namespace

namespace aoc::season2024 {

bool isPatternPossible(const std::string &pattern,
                       const std::vector<std::string> &towels) {

  // Design recursive descent looing at the prefix of the pattern and
  // incrementing the the start.
  // Build up the possible patterns
  return recursively_process_pattern(pattern, towels, 0);
}

std::istream &operator>>(std::istream &instr, OnsenTowels &obj) {
  std::regex re(R"([^,\s]+)"); // Comman separted words
  std::string line;
  std::getline(instr, line);
  std::sregex_iterator begin(line.begin(), line.end(), re);
  std::sregex_iterator end;

  for (auto it = begin; it != end; ++it) {
    obj.towels.push_back(it->str());
  }

  while (instr >> line) {
    obj.patterns.push_back(line);
  }

  return instr;
}

std::vector<std::string>
eliminateTowelsFromPattern(const std::string &pattern,
                           const std::vector<std::string> &towels) {
  std::vector<std::string> possible_towels;

  for (const auto &towel : towels) {
    if (pattern.find(towel) != std::string::npos) {
      possible_towels.push_back(towel);
    }
  }

  return possible_towels;
}

size_t countPatternSolutions(const std::string &pattern,
                             const std::vector<std::string> &towels) {
  /**
   * What I think needs to be done is:
   *  - Walk the string backwards
   *     -- check if the substring is in count cache
   *     -- if not compute it
   *     -- walk back a step
   *
   * So for example: brwrr
   *    check and cache order:
   *           r
   *          rr
   *         wrr
   *           _ this woudl be
   *             - w + rr
   *             - wr + r  ( check all cache options)
   *        rwrr
   *       brwrr

  */

  return rwalk_pattern(pattern, towels);
}

void printOnsenTowelCacheStats() { pattern_cache.stats(); }

} // namespace aoc::season2024