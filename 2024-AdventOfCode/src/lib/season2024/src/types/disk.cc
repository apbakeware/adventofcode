#include <iostream>

#include "season2024/types/disk.h"

namespace aoc::season2024 {

bool isEmpty(const DiskBlock &record) { return record.id == DiskBlock::EMPTY; }

bool isEmptyWithSpace(const DiskBlock &record) {
  return isEmpty(record) && record.count > 0;
}

bool isEmptyWithoutSpace(const DiskBlock &record) {
  return isEmpty(record) && record.count == 0;
}

bool blockFitsIn(const DiskBlock &block, const DiskBlock &dest) {
  return block.count <= dest.count;
}

int blockSizeDifference(const DiskBlock &block, const DiskBlock &test) {
  return block.count - test.count;
}

std::istream &operator>>(std::istream &instr, Disk &obj) {
  char value;
  long id = 0;
  int size = 0;
  bool is_space = false;
  while (instr >> value) {
    size = value - '0';
    if (is_space) {
      DiskBlock block = {DiskBlock::EMPTY, size, false};
      obj.blocks.emplace_back(block);
    } else {
      DiskBlock block = {id, size, false};
      obj.blocks.emplace_back(block);
      ++id;
    }
    is_space = !is_space;
  }

  return instr;
}

} // namespace aoc::season2024