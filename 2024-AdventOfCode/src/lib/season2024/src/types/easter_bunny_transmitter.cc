#include "season2024/types/easter_bunny_transmitter.h"

#include "aoc/grid/grid_location.h"
#include "aoc/util/make_pair_combination.h"

namespace aoc::season2024 {

// Return all the antinodes
void findAllAntinodes(EasterBunnyTransmitters::FrequencyType freq,
                      const EasterBunnyTransmitters::GridLocations &locations,
                      std::set<aoc::grid::Grid_Location> &unique_positions) {
  (void)(freq);
  const auto &pairs =
      aoc::util::makePairCombination(locations.begin(), locations.end());

  for (const auto &location_pair : pairs) {
    const auto &anti_nodes =
        findAntiNodeLocations(location_pair.first, location_pair.second);

    unique_positions.insert(anti_nodes.first);
    unique_positions.insert(anti_nodes.second);
  }
}

void findAllLinearAntinodes(
    EasterBunnyTransmitters::FrequencyType freq,
    const EasterBunnyTransmitters::GridLocations &locations, int grid_width,
    int grid_height, std::set<aoc::grid::Grid_Location> &unique_positions) {
  (void)(freq);

  const auto &pairs =
      aoc::util::makePairCombination(locations.begin(), locations.end());

  // Add the transmitter locations
  for (const auto &location : locations) {
    unique_positions.insert(location);
  }

  // Add the positions for the extended line
  for (const auto &location_pair : pairs) {
    const auto &anti_nodes = findLinearAntiNodeLocations(
        location_pair.first, location_pair.second, grid_width, grid_height);

    for (const auto &location : anti_nodes) {
      unique_positions.insert(location);
    }
  }
}

std::pair<aoc::grid::Grid_Location, aoc::grid::Grid_Location>
findAntiNodeLocations(const aoc::grid::Grid_Location &l1,
                      const aoc::grid::Grid_Location &l2) {

  std::pair<aoc::grid::Grid_Location, aoc::grid::Grid_Location> result;
  const auto diff = l2 - l1;
  result.first = l1 - diff;
  result.second = l2 + diff;
  return result;
}

bool locationIsValid(const aoc::grid::Grid_Location &loc, int grid_width,
                     int grid_height) {
  const bool is_valid = loc.row >= 0 && loc.col >= 0 && loc.row < grid_height &&
                        loc.col < grid_width;

  return is_valid;
}

std::vector<aoc::grid::Grid_Location>
findLinearAntiNodeLocations(const aoc::grid::Grid_Location &l1,
                            const aoc::grid::Grid_Location &l2, int grid_width,
                            int grid_height) {
  std::vector<aoc::grid::Grid_Location> locations;
  if (l2.row > l1.row) {
    const auto diff = l2 - l1;
    aoc::grid::Grid_Location anti_node_loc = l2 + diff;
    while (locationIsValid(anti_node_loc, grid_width, grid_height)) {
      locations.push_back(anti_node_loc);
      anti_node_loc = anti_node_loc + diff;
    }

    anti_node_loc = l1 - diff;
    while (locationIsValid(anti_node_loc, grid_width, grid_height)) {
      locations.push_back(anti_node_loc);
      anti_node_loc = anti_node_loc - diff;
    }
  } else {
  }

  return locations;
}

EasterBunnyTransmitters::EasterBunnyTransmitters()
    : m_num_grid_rows(0), m_num_grid_cols(0) {}

bool EasterBunnyTransmitters::isOnGrid(
    const aoc::grid::Grid_Location &location) const {

  return location.row >= 0 && location.col >= 0 &&
         location.row < m_num_grid_rows && location.col < m_num_grid_cols;
}

void EasterBunnyTransmitters::append(const std::string &line) {

  aoc::grid::Grid_Location location{m_num_grid_rows, 0};
  for (const char freq_id : line) {
    if (freq_id != '.') {
      auto &freq_locations = m_transmitters[freq_id];
      freq_locations.push_back(location);
    }
    location = aoc::grid::Grid_Location::right(location);
  }

  m_num_grid_cols = line.size();
  ++m_num_grid_rows;
}

void EasterBunnyTransmitters::print() const {
  for (const auto &transmitter : m_transmitters) {
    const auto &pairs = aoc::util::makePairCombination(
        transmitter.second.begin(), transmitter.second.end());
    fmt::print(
        "Frequency: {} -- [{}]\n", transmitter.first,
        fmt::join(transmitter.second.begin(), transmitter.second.end(), " "));
    fmt::print("Pairs:\n");
    for (auto iter = pairs.begin(); iter != pairs.end(); ++iter) {
      fmt::print("  ({},{})\n", iter->first, iter->second);
    }
    fmt::print("\n");
  }
}

} // namespace aoc::season2024