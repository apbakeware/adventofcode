#include <iostream>
#include <string>

#include "spdlog/spdlog.h"

#include "season2024/types/fruit_monitor.h"

namespace aoc::season2024::fruit_monitor {

WireValue andGateOp(WireValue in1, WireValue in2) {
  if (in1 == WireValue::NOT_SET || in2 == WireValue::NOT_SET) {
    return WireValue::NOT_SET;
  }

  const bool condition = in1 == WireValue::ONE && in2 == WireValue::ONE;

  return condition ? WireValue::ONE : WireValue::ZERO;
}

WireValue orGateOp(WireValue in1, WireValue in2) {
  if (in1 == WireValue::NOT_SET || in2 == WireValue::NOT_SET) {
    return WireValue::NOT_SET;
  }

  const bool condition = in1 == WireValue::ONE || in2 == WireValue::ONE;

  return condition ? WireValue::ONE : WireValue::ZERO;
}

WireValue xorGateOp(WireValue in1, WireValue in2) {
  if (in1 == WireValue::NOT_SET || in2 == WireValue::NOT_SET) {
    return WireValue::NOT_SET;
  }

  const bool condition = in1 != in2;

  return condition ? WireValue::ONE : WireValue::ZERO;
}

Gate::Gate(const WireIdType &in1, const WireIdType &in2, const WireIdType &out,
           GateOp gate_op)
    : m_in_1(in1), m_in_2(in2), m_out(out), m_gate_op(gate_op) {}

bool Gate::inputsAvailable(const WireTable &wire_table) const {
  const auto iter_in1 = wire_table.find(m_in_1);
  const auto iter_in2 = wire_table.find(m_in_2);

  if (iter_in1 == wire_table.end()) {
    SPDLOG_ERROR("Could not find input wire: {}", m_in_1);
    return false;
  }

  if (iter_in2 == wire_table.end()) {
    SPDLOG_ERROR("Could not find input wire: {}", m_in_2);
    return false;
  }

  return iter_in1->second != WireValue::NOT_SET &&
         iter_in2->second != WireValue::NOT_SET;
}

bool Gate::trySolve(WireTable &wire_table) const {
  if (!inputsAvailable(wire_table)) {
    return false;
  }

  auto value = m_gate_op(wire_table[m_in_1], wire_table[m_in_2]);
  wire_table[m_out] = value;
  return true;
}

std::istream &operator>>(std::istream &instr, FruitMonitor &obj) {
  std::string line;

  while (std::getline(instr, line)) {
    if (line.empty()) {
      break;
    }

    const auto sep_pos = line.find(':');
    const std::string wire_id = line.substr(0, sep_pos);
    const WireValue wire_value =
        line.back() == '1' ? WireValue::ONE : WireValue::ZERO;

    obj.wire_table.insert(std::make_pair(wire_id, wire_value));
  }

  // Parse the gates
  std::string in1;
  std::string in2;
  std::string op;
  std::string junk;
  std::string out;

  while ((instr >> in1 >> op >> in2 >> junk >> out)) {
    if (op == "AND") {
      obj.gates.emplace_back(Gate(in1, in2, out, andGateOp));
    } else if (op == "OR") {
      obj.gates.emplace_back(Gate(in1, in2, out, orGateOp));
    } else if (op == "XOR") {
      obj.gates.emplace_back(Gate(in1, in2, out, xorGateOp));
    } else {
      SPDLOG_ERROR("Unexpected operation tag: {}", op);
    }

    if (obj.wire_table.count(in1) == 0) {
      obj.wire_table.insert(std::make_pair(in1, WireValue::NOT_SET));
    }

    if (obj.wire_table.count(in2) == 0) {
      obj.wire_table.insert(std::make_pair(in2, WireValue::NOT_SET));
    }

    if (obj.wire_table.count(out) == 0) {
      obj.wire_table.insert(std::make_pair(out, WireValue::NOT_SET));
    }
  }

  return instr;
}

} // namespace aoc::season2024::fruit_monitor