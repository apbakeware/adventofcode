#include <algorithm>
#include <iostream>
#include <sstream>
#include <stack>

#include "spdlog/fmt/bundled/core.h"
#include "spdlog/spdlog.h"

#include "season2024/types/chronospatial_computer.h"

namespace aoc::season2024 {

ChronospatialComputer::ChronospatialComputer()
    : m_reg_a(0), m_reg_b(0), m_reg_c(0) {}

ChronospatialComputer::ChronospatialComputer(ValueType ra, ValueType rb,
                                             ValueType rc,
                                             const std::vector<ValueType> &pgm)
    : m_reg_a(ra), m_reg_b(rb), m_reg_c(rc), m_instruction_ptr(0),
      m_program(pgm) {}

void ChronospatialComputer::reset(ValueType ra, ValueType rb, ValueType rc) {
  m_reg_a = ra;
  m_reg_b = rb;
  m_reg_c = rc;
  m_instruction_ptr = 0;
}

// true = program running, false = end of program
bool ChronospatialComputer::executeCurrentInstruction(OutputHandler handler) {
  SPDLOG_DEBUG("Current instruction: {}", m_instruction_ptr);

  if (m_instruction_ptr >= m_program.size()) {
    return false;
  }

  executeOpcode(handler);
  return true;
}

void ChronospatialComputer::executeProgram(OutputHandler handler) {
  while (executeCurrentInstruction(handler)) {
  }
}

bool ChronospatialComputer::runToSeeIfOutputMatchesProgram(ValueType ra,
                                                           ValueType rb,
                                                           ValueType rc) {
  bool output_out_of_order = false;
  std::stack<ChronospatialComputer::ValueType> output_stack;

  auto output_handler = [&](const auto value) {
    // fmt::print(" output --> exp: {}  act: {}\n", output_stack.top(), value);
    if (value == output_stack.top()) {
      // fmt::print("  Output matches expected, popping stack\n");
      output_stack.pop();
    } else {
      // fmt::print("  Output mismatch\n");
      output_out_of_order = true;
    }
  };

  reset(ra, rb, rc);
  // fmt::print("{}\n", *this);

  // fmt::print("Building output stack: ");
  for (auto piter = m_program.rbegin(); piter != m_program.rend(); ++piter) {
    // fmt::print("{} ", *piter);
    output_stack.push(*piter);
  }
  // fmt::print("\n");

  while (executeCurrentInstruction(output_handler)) {
    if (output_out_of_order) {
      // fmt::print("*** OUTPUT OUT OF ORDER, exiting\n");
      return false;
    }
  }
  return output_stack.empty();
}

ChronospatialComputer::ValueType
ChronospatialComputer::getLiteralOperandValue(ValueType operand) const {
  return operand;
}

ChronospatialComputer::ValueType
ChronospatialComputer::getComboOperandValue(ValueType operand) const {
  switch (operand) {
  case 0:
  case 1:
  case 2:
  case 3:
    return operand;
    break;
  case 4:
    return m_reg_a;
    break;
  case 5:
    return m_reg_b;
    break;
  case 6:
    return m_reg_c;
    break;
  case 7:
    SPDLOG_ERROR("Operand 7 should not occur");
    break;
  default:
    SPDLOG_ERROR("Unknown operand: {}", operand);
  }
  return operand;
}

void ChronospatialComputer::executeOpcode(OutputHandler handler) {

  const ValueType opcode = m_program[m_instruction_ptr];
  const ValueType operand = m_program[m_instruction_ptr + 1];

  switch (opcode) {
  case 0:
    m_reg_a = m_reg_a / (std::pow(2, getComboOperandValue(operand)));
    m_instruction_ptr += 2;
    break;
  case 1:
    m_reg_b = m_reg_b ^ getLiteralOperandValue(operand);
    m_instruction_ptr += 2;
    break;
  case 2:
    m_reg_b = getComboOperandValue(operand) & 0x7; // &0x7 is mod 8
    m_instruction_ptr += 2;
    break;
  case 3:
    if (m_reg_a != 0) {
      m_instruction_ptr = getLiteralOperandValue(operand);
    } else {
      m_instruction_ptr += 2;
    }
    break;
  case 4:
    m_reg_b = m_reg_b ^ m_reg_c;
    m_instruction_ptr += 2;
    break;
  case 5:
    handler(getComboOperandValue(operand) & 0x7); // &0x7 is mod 8
    m_instruction_ptr += 2;
    break;
  case 6:
    m_reg_b = m_reg_a / (std::pow(2, getComboOperandValue(operand)));
    m_instruction_ptr += 2;
    break;
  case 7:
    m_reg_c = m_reg_a / (std::pow(2, getComboOperandValue(operand)));
    m_instruction_ptr += 2;
    break;
  default:
    SPDLOG_ERROR("Unknown opcode: {}", operand);
  }
}

std::istream &operator>>(std::istream &instr, ChronospatialComputer &obj) {
  std::string tmp;
  instr >> tmp >> tmp >> obj.m_reg_a >> tmp >> tmp >> obj.m_reg_b >> tmp >>
      tmp >> obj.m_reg_c >> tmp >> tmp;

  std::replace(tmp.begin(), tmp.end(), ',', ' ');
  std::istringstream instruction_str(tmp);
  ChronospatialComputer::ValueType inst = 0;
  while (instruction_str >> inst) {
    obj.m_program.push_back(inst);
  }

  obj.m_instruction_ptr = 0;

  return instr;
}

} // namespace aoc::season2024