#include "aoc/grid/grid_location.h"
#include "spdlog/spdlog.h"

#include "season2024/types/patrol_guard.h"

#include "aoc/grid/grid_navigation.h"
#include "aoc/grid/grid_utils.h"

namespace {

const char GUARD = '^';
const char OBSTRUCTION = '#';
const char OPEN = '.';

} // namespace

namespace aoc::season2024 {

bool operator==(const PatrolGuard &lhs, const PatrolGuard &rhs) {
  return lhs.location == rhs.location && lhs.orientation == rhs.orientation;
}

PatrolNavigation patrolAreaUntilExit(const PatrolGuard &guard_origin,
                                     const PatrolGrid &grid) {

  PatrolGuard guard = guard_origin;
  PatrolNavigation guard_navigation;

  aoc::grid::Grid_Location ahead_loc =
      aoc::grid::ahead(guard.orientation, guard.location);

  guard_navigation.reserve(100);
  guard_navigation.push_back(guard);

  while (aoc::grid::is_on_grid(ahead_loc, grid)) {
    char ahead_value = grid.at(ahead_loc);
    // fmt::print("Guard: {}\n", guard);
    if (ahead_value == OBSTRUCTION) {
      // fmt::print("Obstruction ahead, turning\n");
      guard.orientation = aoc::grid::rotate_cw(guard.orientation);
    } else {
      // fmt::print("Moving ahead\n");
      guard.location = ahead_loc;
      guard_navigation.push_back(guard);
    }

    ahead_loc = aoc::grid::ahead(guard.orientation, guard.location);
  }

  // std::sort(guard_navigation.begin(), guard_navigation.end());
  // auto end_of_unique =
  //     std::unique(guard_navigation.begin(), guard_navigation.end());
  // guard_navigation.erase(end_of_unique, guard_navigation.end());

  // fmt::print("Visits: \n{}\n",
  //            fmt::join(visted_by_guard.begin(), visted_by_guard.end(),
  //            "\n"));

  return guard_navigation;
}

int findLoopingObstructionLocations(const PatrolGuard &origin,
                                    PatrolGrid patrol_grid) {
  int count = 0;

#if 0
  PatrolGuard guard = origin;
  PatrolNavigation navigation(patrol_grid.number_of_cols() *
                              patrol_grid.number_of_rows());
  PatrolNavigation::iterator nav_point = navigation.begin();

  aoc::grid::Grid_Location ahead_loc =
      aoc::grid::ahead(guard.orientation, guard.location);

  while (aoc::grid::is_on_grid(ahead_loc, grid)) {
    char ahead_value = grid.at(ahead_loc);
    // fmt::print("Guard: {}\n", guard);
    if (ahead_value == OPEN) {
      grid::Grid_Location ahead_loc =
          aoc::grid::ahead(guard.orientation, guard.location);

      patrol_grid.set(ahead_loc, OBSTRUCTION);
      // todo: Need to pass in visiation grid...
      bool loop_detected =
          guardLoops(patrol_grid, guard, navigation.begin(), nav_point);
      if (loop_detected) {
        // fmt::print("Guard Loops changing: {}\n", location);
        ++count;
      }
      patrol_grid.set(ahead_loc, OBSTRUCTION);

    } else if (ahead_value == OBSTRUCTION) {
      // fmt::print("Obstruction ahead, turning\n");
      guard.orientation = aoc::grid::rotate_cw(guard.orientation);
    } else {
      SPDLOG_ERROR("Unexpected grid value: {}", ahead_value);
    }

    *nav_point++ = guard;
  }
#endif
  return count;
}

bool detectPatrolLooFrom(const PatrolGrid &patrol_grid,
                         const PatrolGuard &origin,
                         PatrolNavigation::iterator nav_start,
                         PatrolNavigation::iterator nav_end) {

  bool patrol_loop_found = false;

#if 0
  aoc::season2024::PatrolGuard guard = origin;

  aoc::grid::Grid_Location ahead_loc =
      aoc::grid::ahead(guard.orientation, guard.location);

  while (aoc::grid::is_on_grid(ahead_loc, patrol_grid)) {
    const char ahead_value = patrol_grid.at(ahead_loc);
    if (ahead_value == OBSTRUCTION) {
      // fmt::print("Next is obstruction, rotating\n");
      guard.orientation = aoc::grid::rotate_cw(guard.orientation);

    } else {
      // fmt::print("Next is open, moving\n");
      guard.location = ahead_loc;
    }

    // fmt::print("Next guard: {}\n", next_guard);
    // fmt::print("[\n\t{}\n]\n\n",
    //            fmt::join(visits.begin(), visits.end(), "\n\t"));

    const auto loop_check_iter = std::find(nav_start, nav_end, guard);
    if (loop_check_iter != nav_end) {
      // fmt::print("FOUND: {}\n", *loop_check_iter);
      return true;
    }

    *nav_end = guard;
    ++nav_end;
    ahead_loc = aoc::grid::ahead(guard.orientation, guard.location);
  }
#endif
  return patrol_loop_found;
}

} // namespace aoc::season2024