#include "spdlog/spdlog.h"

#include "season2024/days/day24part2.h"

namespace aoc::season2024 {

Day24Part2::ResultType Day24Part2::solve(InputType & input) {
  SPDLOG_TRACE("Day24Part2::solve()");
  (void)(input);
  return ResultType{};
}

} // namespace aoc::season2024

