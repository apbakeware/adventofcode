#include <algorithm>
#include <array>
#include <cstddef>
#include <deque>
#include <fstream>
#include <numeric>
#include <set>

#include "spdlog/fmt/bundled/core.h"
#include "spdlog/spdlog.h"

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"
#include "season2024/days/day14part2.h"
#include "season2024/types/bathroom_robot.h"

namespace {

void generateAsciiArt(
    aoc::input::ValueCollection<aoc::season2024::BathroomRobot> &robots,
    int frames, int width, int height) {
  aoc::grid::Fixed_Sized_Grid<char> grid(height, width, ' ');
  for (const auto &robot : robots.values()) {
    const auto &final_location = applyMotion(robot, frames, width, height);
    aoc::grid::Grid_Location loc = {final_location.second,
                                    final_location.first};
    grid.set(loc, '#');
  }

  std::string art = fmt::format("{}", grid);
  std::string fname = "./robots." + std::to_string(frames) + ".txt";
  std::ofstream art_file(fname);
  art_file << art;
}

} // namespace

namespace aoc::season2024 {

/*
 * Note, this was done by finding the 4 frames where the
 * quadrant count was max and generating the ascii art
 * an manually looking.
 */
Day14Part2::ResultType Day14Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day14Part2::solve()");

  const int width = 101;
  const int height = 103;

  const int quad_width = width / 2;
  const int quad_height = height / 2;

  std::array<unsigned int, 4> quadrant_counts;

  unsigned int max_q0 = 0;
  unsigned int max_q1 = 0;
  unsigned int max_q2 = 0;
  unsigned int max_q3 = 0;

  int frame_m0 = 0;
  int frame_m1 = 0;
  int frame_m2 = 0;
  int frame_m3 = 0;

  for (unsigned int frame = width * height; frame > 0; --frame) {
    quadrant_counts.fill(0);

    for (const auto &robot : input.values()) {
      const auto &final_location = applyMotion(robot, frame, width, height);

      if (final_location.first != quad_width &&
          final_location.second != quad_height) {
        if (final_location.first <= quad_width) {
          if (final_location.second <= quad_height) {
            quadrant_counts[0] += 1;
          } else {
            quadrant_counts[2] += 1;
          }
        } else {
          if (final_location.second <= quad_height) {
            quadrant_counts[1] += 1;
          } else {
            quadrant_counts[3] += 1;
          }
        }
      }
    }

    max_q0 = std::max(max_q0, quadrant_counts[0]);
    if (max_q0 == quadrant_counts[0]) {
      frame_m0 = frame;
    }

    max_q1 = std::max(max_q1, quadrant_counts[1]);
    if (max_q1 == quadrant_counts[1]) {
      frame_m1 = frame;
    }

    max_q2 = std::max(max_q2, quadrant_counts[2]);
    if (max_q2 == quadrant_counts[2]) {
      frame_m2 = frame;
    }

    max_q3 = std::max(max_q3, quadrant_counts[3]);
    if (max_q3 == quadrant_counts[3]) {
      frame_m3 = frame;
    }
  }

  generateAsciiArt(input, frame_m0, width, height);
  generateAsciiArt(input, frame_m1, width, height);
  generateAsciiArt(input, frame_m2, width, height);
  generateAsciiArt(input, frame_m3, width, height);

  return 8280;
}

} // namespace aoc::season2024
