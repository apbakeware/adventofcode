#include <opencv2/core/types.hpp>
#include <opencv2/opencv.hpp>

#include "spdlog/spdlog.h"

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"

#include "season2024/days/day16part2.h"
#include "season2024/types/reindeer_maze.h"

namespace {

void generateMazeWithCostHeat(
    const aoc::season2024::ReindeerMaze &maze,
    const aoc::grid::Fixed_Sized_Grid<int> &cost_grid) {
  // Grid size
  const int blockSize = 5; // Size of each cell in pixels
  const int height = maze.maze.number_of_rows() * blockSize;
  const int width = maze.maze.number_of_cols() * blockSize;

  const cv::Scalar empty_cell = {255, 255, 255};
  const cv::Scalar wall_cell = {15, 15, 15};
  const cv::Scalar start_cell = {255, 0, 0};
  const cv::Scalar end_cell = {255, 0, 0};

  const int end_cost = cost_grid.at(maze.end);
  const cv::Scalar heat_cost_too_high = {25, 25, 25};
  const size_t heat_gradiant_size = 200;
  const int gradiant_bin = end_cost / heat_gradiant_size;

  // Create a blank image with a white background
  cv::Mat image(height, width, CV_8UC3, cv::Scalar(255, 255, 255));

  auto cell_renderer = [&](const auto &loc, const auto val) {
    // upper left of image is 0,0 so need to invert it

    const cv::Point ll(loc.col * blockSize, height - (loc.row * blockSize));
    const cv::Point ur(ll.x + blockSize, ll.y - blockSize);

    cv::Scalar color = empty_cell;
    if (maze.isWall(loc)) {
      color = wall_cell;
    } else if (loc == maze.end) {
      color = end_cell;
    } else if (loc == maze.start) {
      color = start_cell;
    } else {
      const auto cost = cost_grid.at(loc);
      if (cost > end_cost) {
        color = heat_cost_too_high;
      } else {
        const size_t idx = cost / gradiant_bin;
        color = {0, 255 - (cost / gradiant_bin), 255 - (cost / gradiant_bin)};
      }
    }

    cv::rectangle(image, ll, ur, color, cv::FILLED);
  };

  maze.maze.for_each_cell(cell_renderer);
  // Save the image to a BMP file
  cv::imwrite("color_grid.bmp", image);
}

} // namespace

namespace aoc::season2024 {

Day16Part2::ResultType Day16Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day16Part2::solve()");

  const auto &cost_grid = input.createCostGrid();
  generateMazeWithCostHeat(input, cost_grid);

  // input.findAllLeastCostPaths(cost_grid);
  // input.renderCostGridPath(cost_grid);

  return ResultType{};
}

} // namespace aoc::season2024
