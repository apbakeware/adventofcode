#include "spdlog/fmt/bundled/core.h"
#include "spdlog/fmt/bundled/format.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day19part1.h"

namespace aoc::season2024 {

Day19Part1::ResultType Day19Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day19Part1::solve()");

  int count = 0;
  for (const auto &pattern : input.patterns) {
    if (isPatternPossible(pattern, input.towels)) {
      ++count;
    }
  }
  aoc::season2024::printOnsenTowelCacheStats();
  return count;
}

} // namespace aoc::season2024
