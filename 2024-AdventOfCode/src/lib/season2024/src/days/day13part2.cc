#include "spdlog/spdlog.h"

#include "season2024/days/day13part2.h"

namespace aoc::season2024 {

long long
solveAsSystemOffset(aoc::input::ValueCollection<ClawMachine> &machines) {
  const long long A_COST = 3;
  const long long B_COST = 1;
  const long long offset = 10000000000000;
  long long all_prize_cost = 0;

  for (const auto &cm : machines.values()) {

    // solve by elimiation of system of equations...
    // Eliminate "A" presses to get number of B
    // presses, then fiure A

    const long long px = cm.prizeLocation().col + offset;
    const long long py = cm.prizeLocation().row + offset;
    const long long ax = cm.buttonAMovement().col;
    const long long ay = cm.buttonAMovement().row;
    const long long bx = cm.buttonBMovement().col;
    const long long by = cm.buttonBMovement().row;

    // fmt::print("px: {} py: {} ax: {} ay: {} bx: {} by: {}\n", px, py, ax, ay,
    // bx,
    //            by);

    // fmt::print("Px(-Ay): {}\n", (px * -ay));
    // fmt::print("Py(Ax): {}\n", (py * ax));
    // fmt::print("  + {}\n", (px * -ay) + (py * ax));

    const long long b_presses =
        ((px * -ay) + (py * ax)) / ((bx * -ay) + (by * ax));
    const long long a_presses = (px - bx * b_presses) / ax;

    const long long b_remainder =
        ((px * -ay) + (py * ax)) % ((bx * -ay) + (by * ax));
    const long long a_remainder = (px - bx * b_presses) % ax;

    const bool is_valid = a_presses >= 0 && b_presses >= 0 &&
                          a_remainder == 0 && b_remainder == 0;
    if (is_valid) {
      all_prize_cost += a_presses * A_COST + b_presses * B_COST;
    }
  }

  return all_prize_cost;
}

Day13Part2::ResultType Day13Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day13Part2::solve()");

  return solveAsSystemOffset(input);
}

} // namespace aoc::season2024
