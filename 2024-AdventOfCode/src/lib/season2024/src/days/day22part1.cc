#include "season2024/types/monkey_market.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day22part1.h"
#include <cstddef>

namespace {} // namespace

namespace aoc::season2024 {

Day22Part1::ResultType Day22Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day22Part1::solve()");

  const size_t SECRET_LOOPS = 2000;
  ResultType sum = 0;

  for (auto secret : input.values()) {
    for (size_t cnt = 0; cnt < SECRET_LOOPS; ++cnt) {
      secret = monkey_market::computeNextSecret(secret);
    }
    sum += secret;
  }

  return sum;
}

} // namespace aoc::season2024
