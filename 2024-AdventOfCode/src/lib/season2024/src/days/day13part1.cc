#include <map>

#include "aoc/grid/grid_location.h"
#include "season2024/types/claw_machine.h"
#include "spdlog/fmt/bundled/core.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day13part1.h"

namespace aoc::season2024 {

/**
 * Thought about a system, and coded it, but I think that doesn't
 * work (get wrong answer) because there may be different paths
 * rather than just 2 equations.
 */
long solveAsSystem(aoc::input::ValueCollection<ClawMachine> &machines) {
  const int MAX_PRESSES = 100;
  const long A_COST = 3;
  const long B_COST = 1;
  long all_prize_cost = 0;
  for (const auto &cm : machines.values()) {

    // solve by elimiation of system of equations...
    // Eliminate "A" presses to get number of B
    // presses, then fiure A

    const long px = cm.prizeLocation().col;
    const long py = cm.prizeLocation().row;
    const long ax = cm.buttonAMovement().col;
    const long ay = cm.buttonAMovement().row;
    const long bx = cm.buttonBMovement().col;
    const long by = cm.buttonBMovement().row;

    // fmt::print("px: {} py: {} ax: {} ay: {} bx: {} by: {}\n", px, py, ax, ay,
    // bx,
    //            by);

    // fmt::print("Px(-Ay): {}\n", (px * -ay));
    // fmt::print("Py(Ax): {}\n", (py * ax));
    // fmt::print("  + {}\n", (px * -ay) + (py * ax));

    const long b_presses = ((px * -ay) + (py * ax)) / ((bx * -ay) + (by * ax));
    const long a_presses = (px - bx * b_presses) / ax;

    const long b_remainder =
        ((px * -ay) + (py * ax)) % ((bx * -ay) + (by * ax));
    const long a_remainder = (px - bx * b_presses) % ax;

    const bool is_valid = a_presses <= MAX_PRESSES &&
                          b_presses <= MAX_PRESSES && a_presses >= 0 &&
                          b_presses >= 0 && a_remainder == 0 &&
                          b_remainder == 0;
    if (is_valid) {
      all_prize_cost += a_presses * A_COST + b_presses * B_COST;
    }
  }

  return all_prize_cost;
}

// want to use unordred but grid_Location not hashable
using LocationCost = std::map<grid::Grid_Location, long>;

void traverseClawMachineToPrize(const ClawMachine &claw_machine,
                                const grid::Grid_Location &location,
                                LocationCost &cost_by_location,
                                long location_cost, int a_presses,
                                int b_presses) {

  // Stop traversing if we've moved past the prize, this isnt an acceptable
  // path
  if (location.row > claw_machine.prizeLocation().row ||
      location.col > claw_machine.prizeLocation().col) {
    return;
  } else if (a_presses > 100 || b_presses > 100) {
    return;
  }

  bool prize_location_reached = location == claw_machine.prizeLocation();
  auto cost_iter = cost_by_location.find(location);
  if (prize_location_reached) {
    if (cost_iter == cost_by_location.end()) {
      cost_by_location.insert(std::make_pair(location, location_cost));
    } else {
      cost_iter->second = std::min(location_cost, cost_iter->second);
    }
  } else {
    if (cost_iter == cost_by_location.end() ||
        cost_iter->second > location_cost) {

      if (cost_iter == cost_by_location.end()) {
        cost_by_location.insert(std::make_pair(location, location_cost));
      } else {
        cost_iter->second = location_cost;
      }

      const grid::Grid_Location a_location =
          location + claw_machine.buttonAMovement();
      const grid::Grid_Location b_location =
          location + claw_machine.buttonBMovement();

      /**
       * Todo: This is double running. It finds the essentially the same path
       * meaning A the B is the same as B then A. Need to optimize
       */
      traverseClawMachineToPrize(claw_machine, a_location, cost_by_location,
                                 location_cost + 3, a_presses + 1, b_presses);
      traverseClawMachineToPrize(claw_machine, b_location, cost_by_location,
                                 location_cost + 1, a_presses, b_presses + 1);
    }
  }
}

long solveWithTraversal(aoc::input::ValueCollection<ClawMachine> &machines) {

  const grid::Grid_Location origin = {0, 0};
  long all_prize_cost = 0;

  // TODO: Challenge, use a queue to try and do it non-recursive
  for (const auto &cm : machines.values()) {
    LocationCost location_costs;
    traverseClawMachineToPrize(cm, origin, location_costs, 0, 0, 0);

    auto prize_cost = location_costs.find(cm.prizeLocation());
    if (prize_cost != location_costs.end()) {
      all_prize_cost += prize_cost->second;
    }
  }

  return all_prize_cost;
}

Day13Part1::ResultType Day13Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day13Part1::solve()");

  aoc::input::ValueCollection<ClawMachine> traversalValues = input;
  aoc::input::ValueCollection<ClawMachine> systemValues = input;

  // fmt::print("========Traversal\n");
  // long traversal_result = solveWithTraversal(traversalValues);
  fmt::print("========System\n");
  long system_result = solveAsSystem(traversalValues);
  // fmt::print("Traversal: {}   System: {}\n", traversal_result,
  // system_result);

  return system_result;
}

} // namespace aoc::season2024
