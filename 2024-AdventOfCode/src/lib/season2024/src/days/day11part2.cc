#include <vector>

#include "spdlog/fmt/bundled/format.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day11part2.h"
#include "season2024/types/pluto_stones.h"

namespace aoc::season2024 {

Day11Part2::ResultType Day11Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day11Part2::solve()");
  return types::doBlinksFast(input.values(), 75);
}

} // namespace aoc::season2024
