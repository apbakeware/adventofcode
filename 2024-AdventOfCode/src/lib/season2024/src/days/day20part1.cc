#include "aoc/grid/grid.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day20part1.h"

namespace aoc::season2024 {

Day20Part1::ResultType Day20Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day20Part1::solve()");

  // fmt::print("{}\n", input);
  input.findPath();
  SPDLOG_WARN("Sample doesnt have answer with counts more than 100");

  const auto &cheats = input.findCheats();
  std::map<int, int> cheat_counts;
  for (const auto cheat : cheats) {
    ++cheat_counts[cheat];
  }

  int cheats_saving_100_or_more = 0;
  for (const auto &cheat_count : cheat_counts) {
    // fmt::print("Saving: {}, occurances: {}\n", cheat_count.first,
    //            cheat_count.second);
    if (cheat_count.first >= 100) {
      cheats_saving_100_or_more += cheat_count.second;
    }
  }

  return cheats_saving_100_or_more;
}

} // namespace aoc::season2024
