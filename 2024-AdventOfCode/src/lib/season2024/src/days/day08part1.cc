#include <iostream>
#include <set>
#include <string>

#include "spdlog/fmt/bundled/format.h"
#include "spdlog/spdlog.h"

#include "aoc/grid/grid_location.h"
#include "season2024/days/day08part1.h"

namespace aoc::season2024 {

Day08Part1::ResultType Day08Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day08Part1::solve()");

  std::set<aoc::grid::Grid_Location> antinode_locations;
  auto antinode_finder =
      std::bind(findAllAntinodes,
                std::placeholders::_1,         // Frequency
                std::placeholders::_2,         // Grid locations
                std::ref(antinode_locations)); // Bind the set by reference

  input.forEachTransmitter(antinode_finder);

  int valid_node_count = 0;
  for (const auto &anti_node_location : antinode_locations) {
    const bool is_valid_location = input.isOnGrid(anti_node_location);
    if (is_valid_location) {
      ++valid_node_count;
    }
  }

  return valid_node_count;
}

} // namespace aoc::season2024
