#include <cstdint>
#include <limits>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include "spdlog/spdlog.h"

#include "season2024/days/day22part2.h"
#include "season2024/types/monkey_market.h"

namespace aoc::season2024 {

using PriceType = int8_t;
using PriceDiffType = int8_t;
using PriceDiffBfrType = uint32_t;

PriceType priceOf(monkey_market::ValueType secret) {
  return static_cast<PriceType>(secret % 10);
}

// Store last 4 prices in a buffer
// Since the price difference will fit
// in an 8bit value store the price
// buffer in a 32 bit value. each
// insertion will shift left and
// put the value as the least significant
// byte
class PriceDifference4 {
public:
  PriceDifference4() : m_price_buffer(0) {}

  explicit PriceDifference4(PriceDiffBfrType seq) : m_price_buffer(seq) {}

  PriceDiffBfrType insert(PriceDiffType diff) {
    assert(diff < 18);
    assert(diff > -18);

    // cast it to uint8_t to stop sign extension
    m_price_buffer = (m_price_buffer << 8) | static_cast<uint8_t>(diff);
    return m_price_buffer;
  }

  void displaySequence() const {
    const int8_t m3 = (m_price_buffer & 0xFF000000) >> 24;
    const int8_t m2 = (m_price_buffer & 0x00FF0000) >> 16;
    const int8_t m1 = (m_price_buffer & 0x0000FF00) >> 8;
    const int8_t m0 = (m_price_buffer & 0x000000FF);

    fmt::print("m3: {:>3}  m2: {:>3}  m1: {:>3}  m0: {:>3}\n", m3, m2, m1, m0);
  }

  PriceDiffBfrType hash() const { return m_price_buffer; }

private:
  // Keep a unsigned 32 bit value which is the last 4 differences, in
  // groups of 8 bits (as signed chars)
  // m3 | m2 | m1 | m0
  PriceDiffBfrType m_price_buffer;
};

class Vendor {
public:
  using MonkeyOrders = std::unordered_set<PriceDiffBfrType>;

  Vendor() : m_skip(3), m_last_price(0) {}

  // op( hash_value, price )
  template <typename Callback_T>
  void addSecret(monkey_market::ValueType secret, Callback_T cb) {

    const auto price = priceOf(secret);
    const auto dprice = price - m_last_price;
    const auto hash = m_diff_seq.insert(dprice);

    if (m_skip == 0) {
      const auto inserted = m_monkey_orders.insert(hash);
      if (inserted.second) {
        cb(hash, price);
      }

    } else {
      --m_skip;
    }

    m_last_price = price;
  }

private:
  size_t m_skip;
  PriceType m_last_price;
  PriceDifference4 m_diff_seq;
  MonkeyOrders m_monkey_orders;
};

Day22Part2::ResultType Day22Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day22Part2::solve()");

  const size_t SECRET_LOOPS = 2000;

  std::unordered_map<PriceDiffBfrType, int> price_by_sequence_hash;
  int max_price = 0;

  auto handler = [&](auto hash, auto price) {
    auto iter = price_by_sequence_hash.find(hash);
    if (iter == price_by_sequence_hash.end()) {
      price_by_sequence_hash.insert(std::make_pair(hash, price));
    } else {
      iter->second += price;
      max_price = std::max(max_price, iter->second);
    }
  };

  for (auto secret : input.values()) {

    Vendor vendor;
    for (size_t cnt = 0; cnt < SECRET_LOOPS; ++cnt) {
      vendor.addSecret(secret, handler);
      secret = monkey_market::computeNextSecret(secret);
    }
  }

  return max_price;
}

} // namespace aoc::season2024
