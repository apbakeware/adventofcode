#include <regex>

#include "spdlog/spdlog.h"

#include "season2024/days/day03part1.h"

namespace aoc::season2024 {

Day03Part1::ResultType Day03Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day03Part1::solve()");

  std::regex reg_search(R"(mul\((\d{1,3}),(\d{1,3})\))");
  int sum = 0;

  for (const auto &line : input.lines()) {
    auto matches_begin =
        std::sregex_iterator(line.begin(), line.end(), reg_search);
    auto matches_end = std::sregex_iterator();

    for (std::sregex_iterator iter = matches_begin; iter != matches_end;
         ++iter) {
      std::smatch match = *iter;
      std::string match_str = match.str();
      std::string xxx = match[1].str();
      std::string yyy = match[2].str();
      int m1 = std::atoi(xxx.c_str());
      int m2 = std::atoi(yyy.c_str());

      sum += m1 * m2;
    }
  }

  return sum;
}

} // namespace aoc::season2024
