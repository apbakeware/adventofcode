#include <algorithm>
#include <vector>

#include "spdlog/fmt/ostr.h"
#include "spdlog/spdlog.h"

#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_navigation.h"
#include "aoc/grid/grid_utils.h"

#include "season2024/days/day06part2.h"
#include "season2024/types/patrol_guard.h"

namespace {

const char GUARD = '^';
const char OBSTRUCTION = '#';
const char OPEN = '.';

bool guardLoops(const aoc::season2024::PatrolGuard &guard,
                const aoc::season2024::Day06Part2::InputType &grid) {

  std::vector<aoc::season2024::PatrolGuard> visits;
  visits.reserve(grid.number_of_cols() * grid.number_of_rows());
  visits.push_back(guard);

  aoc::grid::Grid_Location ahead_loc =
      aoc::grid::ahead(guard.orientation, guard.location);

  aoc::season2024::PatrolGuard next_guard = guard;

  // TODO: IDEA: I think we run it once and mark them...only a touched
  // path woudl block...so we can eliminate any one that doesnt ever need
  // to get hit....

  // From part 1
  while (aoc::grid::is_on_grid(ahead_loc, grid)) {
    const char ahead_value = grid.at(ahead_loc);
    if (ahead_value == OBSTRUCTION) {
      // fmt::print("Next is obstruction, rotating\n");
      next_guard.orientation = aoc::grid::rotate_cw(next_guard.orientation);

    } else {
      // fmt::print("Next is open, moving\n");
      next_guard.location = ahead_loc;
    }

    // fmt::print("Next guard: {}\n", next_guard);
    // fmt::print("[\n\t{}\n]\n\n",
    //            fmt::join(visits.begin(), visits.end(), "\n\t"));

    const auto loop_check_iter =
        std::find(visits.begin(), visits.end(), next_guard);

    if (loop_check_iter != visits.end()) {
      // fmt::print("FOUND: {}\n", *loop_check_iter);
      return true;
    }

    visits.push_back(next_guard);
    ahead_loc = aoc::grid::ahead(next_guard.orientation, next_guard.location);
  }

  return false;
}

} // namespace

namespace aoc::season2024 {

Day06Part2::ResultType Day06Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day06Part2::solve()");

  // Works for sample, algorithm not efficient for actual.
  //
  // What I think I need to do, is in stead for running the path each time
  // which does a significant amount of rerunning the same path is
  // to store the patrol route, and then make a modification and replay
  // or continue it from that point.
  //
  // Work backward so the last spot visited normally is the first
  // replacement.

  SPDLOG_WARN("Day06Part2::solve() implemented by time is excessive...no "
              "solution found.");
  return -1;

  auto grid = aoc::grid::flip_horizontal(input);
  auto find_guard = aoc::grid::find_value(grid, GUARD);

  if (!find_guard.second) {
    SPDLOG_ERROR("Guard not found!!!!");
    return -1;
  }
#if 0
  // Run the patrol area
  // then start walking the patrol route
  //   if the front of the current position hasn't been tried, block it
  //      mark the position tried
  //      walk and look for a loop
  //

  const PatrolGuard guard = {find_guard.first, aoc::grid::Orientation4::UP};
  std::vector<grid::Grid_Location> attempted_block_locations;
  int valid_positions = 0;

  attempted_block_locations.reserve(original_patrol_route.size());

  const char ahead_value = patrol_grid.at(ahead_loc);
  if (ahead_value == OBSTRUCTION) {
    // fmt::print("Next is obstruction, rotating\n");
    guard.orientation = aoc::grid::rotate_cw(guard.orientation);

  } else {
    // fmt::print("Next is open, moving\n");
    guard.location = ahead_loc;
  }

  for (const auto &nav : original_patrolled_locations) {
  }

  for (const auto &location : original_patrolled_locations) {
    fmt::print("Patrol locations remaining: {}\n", locations_remaining--);
    const char value = grid.at(location);
    if (value == OPEN) {
      grid.set(location, OBSTRUCTION);
      if (guardLoops(guard, grid)) {
        // fmt::print("Guard Loops changing: {}\n", location);
        ++valid_positions;
      }
      grid.set(location, OPEN);
    }
  }

  return valid_positions;
#endif
}

} // namespace aoc::season2024
