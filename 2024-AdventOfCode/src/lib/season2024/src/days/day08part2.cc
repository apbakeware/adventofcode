#include "aoc/grid/grid_location.h"
#include "season2024/types/easter_bunny_transmitter.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day08part2.h"

namespace aoc::season2024 {

Day08Part2::ResultType Day08Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day08Part2::solve()");

  std::set<aoc::grid::Grid_Location> antinode_locations;
  auto antinode_finder =
      std::bind(findAllLinearAntinodes,
                std::placeholders::_1, // Frequency
                std::placeholders::_2, // Grid locations
                input.gridWidth(), input.gridHeight(),
                std::ref(antinode_locations)); // Bind the set by reference

  input.forEachTransmitter(antinode_finder);

  return antinode_locations.size();
}

} // namespace aoc::season2024
