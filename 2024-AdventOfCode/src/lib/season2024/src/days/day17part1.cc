#include "season2024/types/chronospatial_computer.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day17part1.h"

namespace aoc::season2024 {

Day17Part1::ResultType Day17Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day17Part1::solve()");

  std::vector<ChronospatialComputer::ValueType> output;

  auto output_builder = [&output](const auto value) -> void {
    output.push_back(value);
  };

  input.executeProgram(output_builder);

  return fmt::format("{}", fmt::join(output.begin(), output.end(), ","));
}

} // namespace aoc::season2024
