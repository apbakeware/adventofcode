#include <algorithm>
#include <iterator>

#include "spdlog/spdlog.h"

#include "season2024/days/day01part1.h"

namespace aoc::season2024 {

Day01Part1::ResultType Day01Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day01Part1::solve()");

  std::sort(input.left.begin(), input.left.end());
  std::sort(input.right.begin(), input.right.end());

  int distance = 0;

  auto liter = input.left.begin();
  auto riter = input.right.begin();

  while (liter != input.left.end()) {
    distance += std::abs(*liter - *riter);
    ++liter;
    ++riter;
  }

  return distance;
}

} // namespace aoc::season2024
