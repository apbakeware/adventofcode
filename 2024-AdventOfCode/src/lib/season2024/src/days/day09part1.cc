#include <iostream>

#include "spdlog/common.h"
#include "spdlog/fmt/bundled/core.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day09part1.h"

namespace aoc::season2024 {

long computeDiskChecksum(const Disk &disk) {
  long position = 0;
  long checksum = 0;
  for (const auto &block : disk.blocks) {
    for (int cnt = 0; cnt < block.count; ++cnt) {
      checksum += position * block.id;
      // fmt::print("Position: {} ID: {} mul: {}  checksum now: {}\n", position,
      //            block.id, position * block.id, checksum);
      ++position;
    }
  }
  return checksum;
}

Day09Part1::ResultType Day09Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day09Part1::solve()");

  // Create a new defragmented disk to prevent insertions and such
  // on the original vector.
  InputType defragrmented;
  defragrmented.blocks.reserve(input.blocks.size() * 2);
  auto space_to_fill = input.blocks.begin();
  auto block_to_move = input.blocks.rbegin();

  // int control_check = 50;

  while (block_to_move.base() != space_to_fill) {
    // assert(--control_check > 0);

    if (isEmpty(*block_to_move)) {
      ++block_to_move;
    } else if (!isEmpty(*space_to_fill)) {
      defragrmented.blocks.push_back(*space_to_fill);
      ++space_to_fill;
    } else {

      if (blockFitsIn(*block_to_move, *space_to_fill)) {
        DiskBlock block = {block_to_move->id, block_to_move->count, false};
        defragrmented.blocks.emplace_back(block);
        space_to_fill->count -= block_to_move->count;
        ++block_to_move;
      } else {
        int size_to_move = space_to_fill->count;
        DiskBlock block = {block_to_move->id, size_to_move, false};
        defragrmented.blocks.emplace_back(block);
        block_to_move->count -= space_to_fill->count;
        ++space_to_fill;
      }
    }
  }

  return computeDiskChecksum(defragrmented);
}

} // namespace aoc::season2024
