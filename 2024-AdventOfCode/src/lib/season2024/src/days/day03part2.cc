#include <regex>

#include "spdlog/spdlog.h"

#include "season2024/days/day03part2.h"

namespace aoc::season2024 {

Day03Part2::ResultType Day03Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day03Part2::solve()");

  std::regex reg_search(R"(mul\((\d{1,3}),(\d{1,3})\))");

  int sum = 0;

  // Make an uber line, I didn't get that at first cuz the
  // dont's carry over
  std::string line;
  for (const auto &in : input.lines()) {
    line.append(in);
  }

  auto pos_start = line.find("don't()");
  auto pos_end = pos_start;
  while (pos_start != std::string::npos) {
    if ((pos_end = line.find("do()", pos_start + 4)) != std::string::npos) {
      pos_end += 3; // lenght of "do()"
    }
    line.replace(pos_start, pos_end - pos_start, "");
    pos_start = line.find("don't()", pos_start + 1);
  }

  auto matches_begin =
      std::sregex_iterator(line.begin(), line.end(), reg_search);
  auto matches_end = std::sregex_iterator();

  for (std::sregex_iterator iter = matches_begin; iter != matches_end; ++iter) {
    std::smatch match = *iter;
    std::string match_str = match.str();
    std::string xxx = match[1].str();
    std::string yyy = match[2].str();
    int m1 = std::atoi(xxx.c_str());
    int m2 = std::atoi(yyy.c_str());

    sum += m1 * m2;
  }

  return sum;
}

} // namespace aoc::season2024
