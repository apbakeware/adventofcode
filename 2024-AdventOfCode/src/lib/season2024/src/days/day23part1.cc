#include "spdlog/spdlog.h"

#include "season2024/days/day23part1.h"

namespace aoc::season2024 {

Day23Part1::ResultType Day23Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day23Part1::solve()");

  return countThreeWayHistorianOptions(input);
}

} // namespace aoc::season2024
