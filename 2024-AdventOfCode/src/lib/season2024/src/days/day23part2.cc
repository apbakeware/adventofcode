#include "spdlog/spdlog.h"

#include "season2024/days/day23part2.h"

namespace aoc::season2024 {

Day23Part2::ResultType Day23Part2::solve(InputType & input) {
  SPDLOG_TRACE("Day23Part2::solve()");
  (void)(input);
  return ResultType{};
}

} // namespace aoc::season2024

