#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_utils.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day04part2.h"

using GL = aoc::grid::Grid_Location;

namespace {

struct Xmas_String_Processor {

  Xmas_String_Processor() : x_mases_found(0) {}

  void operator()(const GL &loc, char value,
                  aoc::season2024::Day04Part2::InputType &grid) {

    if (value != 'A') {
      return;
    }

    SPDLOG_DEBUG("Location: {}  value: {}, found 'A'", loc, value);

    auto ul = GL::up(GL::left(loc));
    auto ur = GL::up(GL::right(loc));
    auto dl = GL::down(GL::left(loc));
    auto dr = GL::down(GL::right(loc));

    const bool on_grid =
        aoc::grid::is_on_grid(ul, grid) && aoc::grid::is_on_grid(ur, grid) &&
        aoc::grid::is_on_grid(dl, grid) && aoc::grid::is_on_grid(dr, grid);

    if (on_grid) {
      SPDLOG_DEBUG("Cross locations on grid");
      const bool cross_1 = (grid.at(ul) == 'M' && grid.at(dr) == 'S') ||
                           (grid.at(ul) == 'S' && grid.at(dr) == 'M');

      const bool cross_2 = (grid.at(dl) == 'M' && grid.at(ur) == 'S') ||
                           (grid.at(dl) == 'S' && grid.at(ur) == 'M');

      SPDLOG_DEBUG("Cross_1: {}  Cross_2: {}", cross_1, cross_2);

      if (cross_1 && cross_2) {
        SPDLOG_DEBUG("X-MAS found at: {}", loc);
        ++x_mases_found;
      }
    }
  }

  int x_mases_found;
};

} // namespace

namespace aoc::season2024 {

Day04Part2::ResultType Day04Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day04Part2::solve()");
  Xmas_String_Processor processor;
  processor = input.for_each_cell_with_grid(processor);
  return processor.x_mases_found;
}

} // namespace aoc::season2024
