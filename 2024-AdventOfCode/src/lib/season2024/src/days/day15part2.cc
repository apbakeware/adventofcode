#include "spdlog/spdlog.h"

#include "season2024/days/day15part2.h"

namespace aoc::season2024 {

Day15Part2::ResultType Day15Part2::solve(InputType & input) {
  SPDLOG_TRACE("Day15Part2::solve()");
  (void)(input);
  return ResultType{};
}

} // namespace aoc::season2024

