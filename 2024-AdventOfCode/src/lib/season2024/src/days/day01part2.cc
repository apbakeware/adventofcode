#include <algorithm>
#include <iterator>

#include "spdlog/spdlog.h"

#include "season2024/days/day01part2.h"

namespace aoc::season2024 {

Day01Part2::ResultType Day01Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day01Part2::solve()");

  int score = 0;

  std::sort(input.right.begin(), input.right.end());

  for (const auto value : input.left) {
    const auto iter_range =
        std::equal_range(input.right.begin(), input.right.end(), value);
    const auto occurances = std::distance(iter_range.first, iter_range.second);

    score += value * occurances;
  }

  return score;
}

} // namespace aoc::season2024
