#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_utils.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day15part1.h"
#include <numeric>

namespace aoc::season2024 {

Day15Part1::ResultType Day15Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day15Part1::solve()");

  // fmt::print("Warehouse:\n{:h}\nRobot at: {}\n", input.warehouse_grid,
  //            input.robot_location);

  // fmt::print("\n\nWarehouse:\n{}\nRobot at: {}\n", input.warehouse_grid,
  //            input.robot_location);

  // fmt::print("Direction '<' --> {}\n", input.findOpenInDirection('<'));
  // fmt::print("Direction '^' --> {}\n", input.findOpenInDirection('^'));
  // fmt::print("Direction '>' --> {}\n", input.findOpenInDirection('>'));
  // fmt::print("Direction 'v' --> {}\n", input.findOpenInDirection('v'));

  // input.executeRobotCommand('<');
  // fmt::print("Warehouse:\n{}\nRobot at: {}\n", input.warehouse_grid,
  //            input.robot_location);

  // input.executeRobotCommand('<');
  // fmt::print("Warehouse:\n{}\nRobot at: {}\n", input.warehouse_grid,
  //            input.robot_location);

  // input.executeRobotCommand('<');
  // fmt::print("Warehouse:\n{}\nRobot at: {}\n", input.warehouse_grid,
  //            input.robot_location);

  // input.executeRobotCommand('v');
  // fmt::print("Warehouse:\n{}\nRobot at: {}\n", input.warehouse_grid,
  //            input.robot_location);

  fmt::print("Executing {} commands\n", input.robot_commands.size());
  for (auto cmd : input.robot_commands) {
    input.executeRobotCommand(cmd);
  }

  // fmt::print("Warehouse:\n{}\nRobot at: {}\n", input.warehouse_grid,
  //            input.robot_location);

  int sum = 0;
  const auto &crates = input.allCrateLocations();
  for (const auto &crate : crates) {
    // Because we store the grid with 0,0 at bottom left
    const int gps =
        (input.warehouse_grid.number_of_rows() - 1 - crate.row) * 100 +
        crate.col;
    sum += gps;
  }

  return sum;
}

} // namespace aoc::season2024
