#include <sstream>
#include <vector>

#include "spdlog/spdlog.h"

#include "season2024/days/day02part2.h"

namespace {

bool report_is_safe(const std::vector<int> &report) {

  if (report.size() < 2) {
    return true;
  }

  const bool decreasing = report.front() > report.back();
  auto p_iter = report.begin();
  auto c_iter = report.begin();
  ++c_iter;

  while (c_iter != report.end()) {

    const int value = *c_iter;
    const int previous = *p_iter;
    const int diff = std::abs(value - previous);

    if (diff < 1 || diff > 3) {
      SPDLOG_INFO("Difference ({}) ({} - {}) outside, range, unsafe", diff,
                  value, previous);
      return false;
    } else if (decreasing && value > previous) {
      SPDLOG_INFO("Decreasing sequence, value ({}) > previous ({})", value,
                  previous);
      return false;
    } else if (!decreasing && value < previous) {
      SPDLOG_INFO("Increasing sequence, value ({}) < previous ({})", value,
                  previous);
      return false;
    }

    ++c_iter;
    ++p_iter;
  }
  return true;
}

template <typename T>
void create_from_spaced_string(const std::string &line, std::vector<T> &out_v) {
  std::istringstream instr(line);
  T value;

  while (instr >> value) {
    out_v.push_back(value);
  }
}

} // namespace

namespace aoc::season2024 {

Day02Part2::ResultType Day02Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day02Part2::solve()");
  const auto &reports = input.lines();
  int safe_count = 0;

  for (const auto &report : reports) {
    std::vector<int> values;
    create_from_spaced_string<int>(report, values);
    if (report_is_safe(values)) {
      ++safe_count;
    } else {
      for (size_t skip = 0; skip < values.size(); ++skip) {
        std::vector<int> retry;
        retry.reserve(values.size());

        for (size_t idx = 0; idx < values.size(); ++idx) {
          if (idx != skip) {
            retry.push_back(values[idx]);
          }
        }
        if (report_is_safe(retry)) {
          ++safe_count;
          break;
        }
      }
    }
  }

  return safe_count;
}

} // namespace aoc::season2024
