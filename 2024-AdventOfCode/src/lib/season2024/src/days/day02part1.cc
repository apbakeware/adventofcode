#include <cmath>
#include <sstream>

#include "spdlog/spdlog.h"

#include "season2024/days/day02part1.h"

namespace {

bool report_is_safe(const std::string &report) {
  std::istringstream instr(report);

  int previous = 0;
  int value = 0;
  int diff = 0;
  bool decreasing = true;

  instr >> previous;
  instr >> value;

  decreasing = previous > value;

  do {
    if (decreasing && value > previous) {
      return false;
    } else if (!decreasing && value < previous) {
      return false;
    }

    diff = std::abs(value - previous);
    if (diff < 1 || diff > 3) {
      return false;
    }

    previous = value;
  } while (instr >> value);

  return true;
}

} // namespace

namespace aoc::season2024 {

Day02Part1::ResultType Day02Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day02Part1::solve()");
  const auto &reports = input.lines();
  int safe_count = 0;
  for (const auto &report : reports) {
    if (report_is_safe(report)) {
      ++safe_count;
    }
  }
  return safe_count;
}

} // namespace aoc::season2024
