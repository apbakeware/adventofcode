#include <sstream>

#include "spdlog/fmt/bundled/format.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day05part1.h"

namespace aoc::season2024 {

Day05Part1::ResultType Day05Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day05Part1::solve()");

  int midpoint_sum = 0;

  for (const auto &pq : input.print_queues) {
    if (input.rules.validatePrintQueue(pq)) {
      SPDLOG_DEBUG("{} queue is valid", fmt::join(pq.begin(), pq.end(), " "));
      midpoint_sum += pq.at(pq.size() / 2);
    } else {
      SPDLOG_DEBUG("{} queue is NOT valid",
                   fmt::join(pq.begin(), pq.end(), " "));
    }
  }

  return midpoint_sum;
}

} // namespace aoc::season2024
