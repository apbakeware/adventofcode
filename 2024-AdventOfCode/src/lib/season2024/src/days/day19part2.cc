#include "season2024/types/onsen_towels.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day19part2.h"

namespace aoc::season2024 {

Day19Part2::ResultType Day19Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day19Part2::solve()");

  int possible_pattern_count = 0;
  int impossible_pattern_count = 0;
  int breaker = 10;
  unsigned long total_combos = 0;
  for (const auto &pattern : input.patterns) {
    // assert(--breaker != 0);
    const auto &filtered_towels =
        aoc::season2024::eliminateTowelsFromPattern(pattern, input.towels);

    const bool is_buildable_pattern =
        aoc::season2024::isPatternPossible(pattern, filtered_towels);
    // fmt::print("Pattern Possible: {}  Possible Towels ({}/{})\n",
    //            is_buildable_pattern, filtered_towels.size(),
    //            input.towels.size());

    if (is_buildable_pattern) {
      size_t count = countPatternSolutions(
          pattern, input.towels); // filtered_towels but illegal isntruction?
      // fmt::print("Pattern: {} Possible options: {}\n", pattern, count);
      total_combos += count;
      ++possible_pattern_count;
    } else {
      ++impossible_pattern_count;
    }
  }

  // fmt::print("Possible patterns: {}  Impossible patterns: {}\n",
  //            possible_pattern_count, impossible_pattern_count);
  // fmt::print("Number of towels: {}\n", input.towels.size());

  return total_combos;
}

} // namespace aoc::season2024
