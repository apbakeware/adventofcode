#include "aoc/grid/grid.h"
#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_utils.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day18part2.h"

namespace aoc::season2024 {

Day18Part2::ResultType Day18Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day18Part2::solve()");
  // Real values
  // const aoc::grid::Grid_Location start = {0, 0};
  // const aoc::grid::Grid_Location dest = {70, 70};
  // size_t drop_count = 1024;

  SPDLOG_WARN("Day18.2 incomplete");
  return "";

  // Test values
  const aoc::grid::Grid_Location start = {0, 0};
  const aoc::grid::Grid_Location dest = {6, 6};
  size_t drop_count = 12;

  aoc::grid::Grid_Location blocker = {0, 0};

  for (; drop_count < input.locations.size(); ++drop_count) {
    const auto &run = input.runRamGrid(start, dest, drop_count);

    grid::Fixed_Sized_Grid<char> vis(run.cost_grid.number_of_rows(),
                                     run.cost_grid.number_of_cols(), '.');
    for (size_t idx = 0; idx < drop_count; ++idx) {

      vis.set(input.locations[idx], '#');
    }
    for (const auto &loc : run.path) {
      vis.set(loc, 'O');
    };
    // vis = grid::flip_horizontal(vis);
    // fmt::print("Grid:\n{:h}\n", vis);
    if (!run.destination_reached) {
      blocker = input.locations[drop_count - 1];
      break;
    }
  }

  return fmt::format("{},{}", blocker.col, blocker.row);
}

} // namespace aoc::season2024
