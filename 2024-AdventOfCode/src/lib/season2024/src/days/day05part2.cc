#include "spdlog/spdlog.h"

#include "season2024/days/day05part2.h"

namespace aoc::season2024 {

Day05Part2::ResultType Day05Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day05Part2::solve()");
  int midpoint_sum = 0;

  InputType::PrintQueue fixed_queue;
  for (const auto &pq : input.print_queues) {
    if (input.rules.fixPrintQueue(pq, fixed_queue)) {
      SPDLOG_DEBUG("Queue was fixed -- orig: {}  fix: {}",
                   fmt::join(pq.begin(), pq.end(), " "),
                   fmt::join(fixed_queue.begin(), fixed_queue.end(), " "));
      midpoint_sum += fixed_queue.at(fixed_queue.size() / 2);
      // return 0;
    } else {
      SPDLOG_DEBUG("Queue was valid");
    }
  }

  return midpoint_sum;
}

} // namespace aoc::season2024
