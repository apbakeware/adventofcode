#include <sstream>

#include "spdlog/spdlog.h"

#include "season2024/days/day11part1.h"
#include "season2024/types/pluto_stones.h"

namespace aoc::season2024 {

Day11Part1::ResultType Day11Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day11Part1::solve()");

  return types::doBlinks(input.values(), 25);
}

} // namespace aoc::season2024
