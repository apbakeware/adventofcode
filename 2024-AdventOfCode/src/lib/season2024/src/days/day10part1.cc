#include <set>

#include "aoc/grid/grid_location.h"
#include "spdlog/spdlog.h"

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_utils.h"

#include "season2024/days/day10part1.h"

namespace aoc::season2024 {

template <int Terminal_Height>
void findValidPaths(
    const aoc::grid::Fixed_Sized_Grid<int> &grid,
    const aoc::grid::Grid_Location &location,
    std::function<void(aoc::grid::Grid_Location)> terminus_found_cb) {

  const int height = grid.at(location);
  if (height == Terminal_Height) {
    terminus_found_cb(location);
    return;
  }

  const std::vector<grid::Grid_Location> surrounding = {
      grid::Grid_Location::up(location), grid::Grid_Location::right(location),
      grid::Grid_Location::down(location), grid::Grid_Location::left(location)};
  const int next_height = height + 1;

  for (const auto &next_location : surrounding) {
    const bool next_location_valid = grid::is_on_grid(next_location, grid) &&
                                     grid.at(next_location) == next_height;
    if (next_location_valid) {
      findValidPaths<Terminal_Height>(grid, next_location, terminus_found_cb);
    }
  }
}

Day10Part1::ResultType Day10Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day10Part1::solve()");

  const auto &lines = input.lines();
  const auto &grid =
      aoc::grid::create_digit_grid_from_string_rows(lines.begin(), lines.end());

  const auto &trailheads = aoc::grid::find_all_locations_of(grid, 0);

  int result = 0;
  std::set<aoc::grid::Grid_Location> terminal_locations;
  auto terminus_found_callback =
      [&terminal_locations](const aoc::grid::Grid_Location &location) {
        terminal_locations.insert(location);
      };

  for (const auto &trailhead : trailheads) {
    terminal_locations.clear();
    findValidPaths<9>(grid, trailhead, terminus_found_callback);
    result += terminal_locations.size();
  }

  return result;
}

} // namespace aoc::season2024
