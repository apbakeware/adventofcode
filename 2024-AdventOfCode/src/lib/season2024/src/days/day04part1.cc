#include "aoc/grid/grid_location.h"
#include "aoc/grid/grid_traversal.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day04part1.h"

namespace {

auto move_left = [](const auto &loc) -> aoc::grid::Grid_Location {
  return aoc::grid::Grid_Location::left(loc);
};

auto move_down = [](const auto &loc) -> aoc::grid::Grid_Location {
  return aoc::grid::Grid_Location::down(loc);
};

auto move_right = [](const auto &loc) -> aoc::grid::Grid_Location {
  return aoc::grid::Grid_Location::right(loc);
};

auto move_up = [](const auto &loc) -> aoc::grid::Grid_Location {
  return aoc::grid::Grid_Location::up(loc);
};

auto move_down_left = [](const auto &loc) -> aoc::grid::Grid_Location {
  return aoc::grid::Grid_Location::down(aoc::grid::Grid_Location::left(loc));
};

auto move_down_right = [](const auto &loc) -> aoc::grid::Grid_Location {
  return aoc::grid::Grid_Location::down(aoc::grid::Grid_Location::right(loc));
};

auto move_up_left = [](const auto &loc) -> aoc::grid::Grid_Location {
  return aoc::grid::Grid_Location::up(aoc::grid::Grid_Location::left(loc));
};

auto move_up_right = [](const auto &loc) -> aoc::grid::Grid_Location {
  return aoc::grid::Grid_Location::up(aoc::grid::Grid_Location::right(loc));
};

} // namespace

namespace aoc::season2024 {

Day04Part1::ResultType Day04Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day04Part1::solve()");

  // Look for XMAS in order. if its backwards it will be found from another
  // location
  const std::string XMAS = "XMAS";
  std::string walking_string;
  int xmas_count = 0;

  auto handler_noop = [](const aoc::grid::Grid_Location &location, char value) {
    (void)(location);
    (void)(value);
  };

  auto builder_pred = [&](const aoc::grid::Grid_Location &location,
                          char value) {
    (void)(location);
    walking_string += value;
    if (walking_string == XMAS) {
      ++xmas_count;
      return true;
    } else if (XMAS.compare(0, walking_string.size(), walking_string) != 0) {
      return true;
    }

    return false;
  };

  auto cell_processor = [&](const aoc::grid::Grid_Location &location,
                            char value) {
    if (value == 'X') {
      walking_string.clear();
      aoc::grid::walk_until(input, location, move_right, handler_noop,
                            builder_pred);

      walking_string.clear();
      aoc::grid::walk_until(input, location, move_down, handler_noop,
                            builder_pred);

      walking_string.clear();
      aoc::grid::walk_until(input, location, move_left, handler_noop,
                            builder_pred);

      walking_string.clear();
      aoc::grid::walk_until(input, location, move_up, handler_noop,
                            builder_pred);

      walking_string.clear();
      aoc::grid::walk_until(input, location, move_down_right, handler_noop,
                            builder_pred);

      walking_string.clear();
      aoc::grid::walk_until(input, location, move_down_left, handler_noop,
                            builder_pred);

      walking_string.clear();
      aoc::grid::walk_until(input, location, move_up_left, handler_noop,
                            builder_pred);

      walking_string.clear();
      aoc::grid::walk_until(input, location, move_up_right, handler_noop,
                            builder_pred);
    }
  };

  input.for_each_cell(cell_processor);

  return xmas_count;
}

} // namespace aoc::season2024
