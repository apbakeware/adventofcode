#include <algorithm>
#include <iterator>
#include <list>

#include "season2024/types/disk.h"
#include "spdlog/fmt/bundled/core.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day09part2.h"

namespace aoc::season2024 {

template <typename Blocks_Coll_T>
long computeDiskChecksum(const Blocks_Coll_T &disk) {

  long position = 0;
  long checksum = 0;
  for (const auto &block : disk) {
    if (isEmpty(block)) {
      position += block.count;
    } else {
      for (int cnt = 0; cnt < block.count; ++cnt) {
        checksum += position * block.id;
        ++position;
      }
    }
  }
  return checksum;
}

struct SpaceFinder {
  explicit SpaceFinder(int space_required) : m_space_required(space_required) {}

  bool operator()(const DiskBlock &block) {

    const bool found =
        isEmptyWithSpace(block) && block.count >= m_space_required;
    return found;
  }

  int m_space_required = 0;
};

Day09Part2::ResultType Day09Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day09Part2::solve()");

  // Use a list to keep iterators valid during moves
  std::list<aoc::season2024::DiskBlock> file_blocks(input.blocks.begin(),
                                                    input.blocks.end());

  // TODO: Optimization: keep list of iterators to open spaces so
  // searching is easier...non delete (we arent doing) for
  // lists do not invalidate iterators

  auto block_to_move = file_blocks.rbegin();
  auto open_space_start = file_blocks.begin();

  while (block_to_move != file_blocks.rend()) {
    if (isEmpty(*block_to_move) || block_to_move->has_been_moved) {
      ++block_to_move;
      continue;
    }

    SpaceFinder finder(block_to_move->count);
    const auto search_range_end = block_to_move.base();
    auto block_to_move_to =
        std::find_if(open_space_start, search_range_end, finder);

    if (block_to_move_to != search_range_end) {
      int extra_space = block_to_move_to->count - block_to_move->count;
      auto block_to_insert = *block_to_move;
      block_to_insert.has_been_moved = true;
      block_to_move_to->count = extra_space;
      block_to_move->id = DiskBlock::EMPTY;

      file_blocks.insert(block_to_move_to, std::move(block_to_insert));
    }

    ++block_to_move;
  }

  return computeDiskChecksum(file_blocks);
}

} // namespace aoc::season2024
