#include <sstream>

#include "spdlog/spdlog.h"

#include "season2024/days/day07part2.h"

namespace {

uint64_t operator_concat(uint64_t left, uint64_t right) {
  uint64_t result = 0;
  std::stringstream ostr;
  ostr << left << right;
  ostr >> result;

  return result;
}

template <typename Iter_T>
bool can_be_generated(Iter_T begin, Iter_T end, uint64_t current,
                      uint64_t target) {

  if (begin == end) {
    return (current == target) ? true : false;
  } else if (current > target) {
    return false;
  }

  uint64_t value = *begin;
  ++begin;

  return can_be_generated(begin, end, current + value, target) ||
         can_be_generated(begin, end, current * value, target) ||
         can_be_generated(begin, end, operator_concat(current, value), target);
}

} // namespace

namespace aoc::season2024 {

Day07Part2::ResultType Day07Part2::solve(InputType &input) {
  SPDLOG_TRACE("Day07Part2::solve()");
  ResultType result = 0;
  for (const auto &eqn : input.equations) {
    auto begin = eqn.operands.begin();
    auto end = eqn.operands.end();
    const bool is_possible =
        can_be_generated(++begin, end, eqn.operands.front(), eqn.answer);

    if (is_possible) {
      result += eqn.answer;
    }
  }

  return result;
}

} // namespace aoc::season2024
