#include <array>
#include <numeric>

#include "spdlog/fmt/bundled/core.h"
#include "spdlog/spdlog.h"

#include "season2024/days/day14part1.h"

namespace aoc::season2024 {

Day14Part1::ResultType Day14Part1::solve(InputType &input) {
  SPDLOG_TRACE("Day14Part1::solve()");

  const int frames = 100;
  const int width = 101;
  const int height = 103;

  const int quad_width = width / 2;
  const int quad_height = height / 2;

  std::array<int, 4> quadrant_counts;
  quadrant_counts.fill(0);

  SPDLOG_WARN(
      "Day14.1 Cannot solve sample and real, requires hard coded values");

  for (const auto &robot : input.values()) {
    const auto &final_location = applyMotion(robot, frames, width, height);

    if (final_location.first != quad_width &&
        final_location.second != quad_height) {
      if (final_location.first <= quad_width) {
        if (final_location.second <= quad_height) {
          quadrant_counts[0] += 1;
        } else {
          quadrant_counts[2] += 1;
        }
      } else {
        if (final_location.second <= quad_height) {
          quadrant_counts[1] += 1;
        } else {
          quadrant_counts[3] += 1;
        }
      }
    }
  }

  return std::accumulate(quadrant_counts.begin(), quadrant_counts.end(), 1,
                         std::multiplies<int>());
}

} // namespace aoc::season2024
