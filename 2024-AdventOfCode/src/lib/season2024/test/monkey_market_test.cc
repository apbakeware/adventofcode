#include <catch2/catch_test_macros.hpp>
#include <cstdint>

#include "spdlog/fmt/bundled/core.h"

#include "season2024/types/monkey_market.h"

namespace aoc::season2024::test {

TEST_CASE("Monkey market test") {
  SECTION("Test mix") { CHECK(monkey_market::doMix(42, 15) == 37); }
  SECTION("Test prune") {
    CHECK(monkey_market::doPrune(100000000) == 16113920);
    CHECK(monkey_market::doPrune(16777216) == 0);
    CHECK(monkey_market::doPrune(16777217) == 1);
  }
  SECTION("Test doStepOne") { CHECK(monkey_market::doStepOne(123) == 7867); }
  SECTION("Test doStepTwo") { CHECK(monkey_market::doStepTwo(7867) == 7758); }
  SECTION("Test doStepThree") {
    CHECK(monkey_market::doStepThree(7758) == 15887950);
  }
  SECTION("Test computeNextSecret") {
    CHECK(monkey_market::computeNextSecret(123) == 15887950);
    CHECK(monkey_market::computeNextSecret(15887950) == 16495136);
    CHECK(monkey_market::computeNextSecret(16495136) == 527345);
  }
}

} // namespace aoc::season2024::test
