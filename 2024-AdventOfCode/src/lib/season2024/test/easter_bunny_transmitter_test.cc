#include <catch2/catch_test_macros.hpp>

#include "aoc/grid/grid_location.h"
#include "season2024/types/easter_bunny_transmitter.h"

namespace aoc::season2024::test {

TEST_CASE("EasterBunnyTransmitters  Test") {

  SECTION("findAntiNodeLocations positive slope") {

    const aoc::grid::Grid_Location exp1 = {1, 3};
    const aoc::grid::Grid_Location exp2 = {7, 6};

    const aoc::grid::Grid_Location l1 = {3, 4};
    const aoc::grid::Grid_Location l2 = {5, 5};

    const auto &anti_nodes = findAntiNodeLocations(l1, l2);

    CHECK(anti_nodes.first == exp1);
    CHECK(anti_nodes.second == exp2);
  }

  SECTION("findAntiNodeLocations negative slope slope") {
    const aoc::grid::Grid_Location exp1 = {1, 5};
    const aoc::grid::Grid_Location exp2 = {7, 2};

    const aoc::grid::Grid_Location l1 = {3, 4};
    const aoc::grid::Grid_Location l2 = {5, 3};

    const auto &anti_nodes = findAntiNodeLocations(l1, l2);

    CHECK(anti_nodes.first == exp1);
    CHECK(anti_nodes.second == exp2);
  }
}

} // namespace aoc::season2024::test