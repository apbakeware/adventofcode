#include <catch2/catch_test_macros.hpp>
#include <cstdint>

#include "spdlog/fmt/bundled/core.h"

#include "season2024/types/pluto_stones.h"

namespace aoc::season2024::types::test {

TEST_CASE("Pluto stones test") {
  aoc::season2024::types::StoneCollection::ValuesType sut = {125, 17};
  SECTION("blinkStoneValue test") {
    CHECK(blinkStoneValue(0) == std::make_tuple(1, 1, 0));
    CHECK(blinkStoneValue(1) == std::make_tuple(1, 2024, 0));

    // Sample 1 blink
    CHECK(blinkStoneValue(125) == std::make_tuple(1, 253000, 0));
    CHECK(blinkStoneValue(17) == std::make_tuple(2, 1, 7));

    // Sample 2 blink
    CHECK(blinkStoneValue(253000) == std::make_tuple(2, 253, 0));
    CHECK(blinkStoneValue(1) == std::make_tuple(1, 2024, 0));
    CHECK(blinkStoneValue(7) == std::make_tuple(1, 14168, 0));

    // Sample 3 blink
    CHECK(blinkStoneValue(253) == std::make_tuple(1, 512072, 0));
    CHECK(blinkStoneValue(0) == std::make_tuple(1, 1, 0));
    CHECK(blinkStoneValue(2024) == std::make_tuple(2, 20, 24));
    CHECK(blinkStoneValue(14168) == std::make_tuple(1, 28676032, 0));

    // Sample 4 blink
    CHECK(blinkStoneValue(512072) == std::make_tuple(2, 512, 72));
    CHECK(blinkStoneValue(1) == std::make_tuple(1, 2024, 0));
    CHECK(blinkStoneValue(20) == std::make_tuple(2, 2, 0));
    CHECK(blinkStoneValue(24) == std::make_tuple(2, 2, 4));
    CHECK(blinkStoneValue(28676032) == std::make_tuple(2, 2867, 6032));

    // Sample 5 blink
    CHECK(blinkStoneValue(512) == std::make_tuple(1, 1036288, 0));
    CHECK(blinkStoneValue(72) == std::make_tuple(2, 7, 2));
    CHECK(blinkStoneValue(2024) == std::make_tuple(2, 20, 24));
    CHECK(blinkStoneValue(2) == std::make_tuple(1, 4048, 0));
    CHECK(blinkStoneValue(0) == std::make_tuple(1, 1, 0));
    CHECK(blinkStoneValue(2) == std::make_tuple(1, 4048, 0));
    CHECK(blinkStoneValue(4) == std::make_tuple(1, 8096, 0));
    CHECK(blinkStoneValue(2867) == std::make_tuple(2, 28, 67));
    CHECK(blinkStoneValue(6032) == std::make_tuple(2, 60, 32));

    // Sample 5 blink
    CHECK(blinkStoneValue(1036288) == std::make_tuple(1, 2097446912, 0));
    CHECK(blinkStoneValue(7) == std::make_tuple(1, 14168, 0));
    CHECK(blinkStoneValue(2) == std::make_tuple(1, 4048, 0));
    CHECK(blinkStoneValue(20) == std::make_tuple(2, 2, 0));
    CHECK(blinkStoneValue(24) == std::make_tuple(2, 2, 4));
    CHECK(blinkStoneValue(4048) == std::make_tuple(2, 40, 48));
    CHECK(blinkStoneValue(1) == std::make_tuple(1, 2024, 0));
    CHECK(blinkStoneValue(4048) == std::make_tuple(2, 40, 48));
    CHECK(blinkStoneValue(8096) == std::make_tuple(2, 80, 96));
    CHECK(blinkStoneValue(28) == std::make_tuple(2, 2, 8));
    CHECK(blinkStoneValue(67) == std::make_tuple(2, 6, 7));
    CHECK(blinkStoneValue(60) == std::make_tuple(2, 6, 0));
    CHECK(blinkStoneValue(32) == std::make_tuple(2, 3, 2));
  }

  /*
  Initial arrangement:
  125 17

  After 1 blink:
  253000 1 7

  After 2 blinks:
  253 0 2024 14168

  After 3 blinks:
  512072 1 20 24 28676032

  After 4 blinks:
  512 72 2024 2 0 2 4 2867 6032

  After 5 blinks:
  1036288 7 2 20 24 4048 1 4048 8096 28 67 60 32

  After 6 blinks:
  2097446912 14168 4048 2 0 2 4 40 48 2024 40 48 80 96 2 8 6 7 6 0 3 2
  */
  SECTION("Sample arrangement 1 blink") { CHECK(doBlinksFast(sut, 1) == 3); }
  SECTION("Sample arrangement 2 blink") { CHECK(doBlinksFast(sut, 2) == 4); }
  SECTION("Sample arrangement 3 blink") { CHECK(doBlinksFast(sut, 3) == 5); }
  SECTION("Sample arrangement 4 blink") { CHECK(doBlinksFast(sut, 4) == 9); }
  SECTION("Sample arrangement 5 blink") { CHECK(doBlinksFast(sut, 5) == 13); }
  SECTION("Sample arrangement 6 blink") { CHECK(doBlinksFast(sut, 6) == 22); }
  SECTION("Sample arrangement 25 blink") {
    CHECK(doBlinksFast(sut, 25) == 55312);
  }
}

} // namespace aoc::season2024::types::test