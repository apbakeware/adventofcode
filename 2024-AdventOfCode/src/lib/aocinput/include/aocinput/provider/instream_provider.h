#ifndef __INSTREAM_PROVIDER_H__
#define __INSTREAM_PROVIDER_H__

#include <fstream>

#include "spdlog/spdlog.h"

namespace aoc::input::provider {

/**
 * The InstreamProvider class uses an input stream with operator>>
 * to populate the contents of a data record.
 */
template <class Populated_T> class InstreamProvider {
public:
  using PopulatedType = Populated_T;

  explicit InstreamProvider(std::istream &in_str) : m_in_str(in_str) {}

  /**
   * Update the contents of the recording using operator>> with the record type.
   *
   * @return False if the input stream is not valid, otherwise true.
   */
  bool populate(PopulatedType &record) {
    SPDLOG_TRACE("InstreamProvider::populate()");

    if (!m_in_str.good()) {
      SPDLOG_ERROR("Input stream is not in a good state");
      return false;
    }

    m_in_str >> record;
    return true;
  }

private:
  std::istream &m_in_str;
};

} // namespace aoc::input::provider

#endif /* __INSTREAM_PROVIDER_H__ */