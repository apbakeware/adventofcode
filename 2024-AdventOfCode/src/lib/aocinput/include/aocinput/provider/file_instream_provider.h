#ifndef __FILE_INSTREAM_PROVIDER_H__
#define __FILE_INSTREAM_PROVIDER_H__

#include <fstream>
#include <string>

#include "spdlog/spdlog.h"

#include "instream_provider.h"

namespace aoc::input::provider {

/**
 * The FileInstreamProvider class populates the contents of a record using
 * an input file stream. The stream uses operator>>
 * to populate the contents of a data record.
 */
template <class Populated_T> class FileInstreamProvider {
public:
  using PopulatedType = Populated_T;
  FileInstreamProvider(const std::string &file_path) : m_file_path(file_path) {}

  /**
   * Open the file an populate the record contents using an InstreamProvider.
   */
  bool populate(PopulatedType &record) {
    SPDLOG_TRACE("FileInstreamProvider::populate()");

    std::ifstream in_file_stream(m_file_path);
    InstreamProvider<PopulatedType> provider(in_file_stream);
    return provider.populate(record);
  }

private:
  std::string m_file_path;
};

} // namespace aoc::input::provider

#endif /* __FILE_INSTREAM_PROVIDER_H__ */