#ifndef __LINE_COLLECTION_H__
#define __LINE_COLLECTION_H__

#include <string>
#include <vector>

namespace aoc::input {

/**
 * Input record type which is a simple
 * collection of text lines.
 */
class LineCollection {
public:
  using Line = std::string;
  using Lines = std::vector<Line>;

  bool append(const Line &line);

  Lines &lines();

  // todo: for_each taking a line

private:
  Lines m_lines;
};

} // namespace aoc::input

#endif /* __LINE_COLLECTION_H__ */