#ifndef __VALUE_COLLECTION_H__
#define __VALUE_COLLECTION_H__

#include <iostream>
#include <vector>

namespace aoc::input {

/**
 * The ValuesCollection provides a simple collect of values. The
 * class implements operator>> to read values until the end of a stream.
 */
template <typename Value_T> class ValueCollection {
public:
  using ValueType = Value_T;
  using ValuesType = std::vector<ValueType>;

  /**
   * Get a reference to the values collection.
   */
  ValuesType &values() { return m_values; }

  /**
   * Read all the values until the end of the stream. Values are
   * added to the values collection.
   */
  friend std::istream &operator>>(std::istream &instr,
                                  ValueCollection<ValueType> &obj) {
    ValueType value;
    while (instr >> value) {
      obj.m_values.push_back(value);
    }
    return instr;
  }

private:
  ValuesType m_values;
};

} // namespace aoc::input

#endif /* __VALUE_COLLECTION_H__ */