#include "aocinput/line_collection.h"

namespace aoc::input {

bool LineCollection::append(const Line &line) {
  m_lines.push_back(line);
  return true;
}

LineCollection::Lines &LineCollection::lines() { return m_lines; }

} // namespace aoc::input