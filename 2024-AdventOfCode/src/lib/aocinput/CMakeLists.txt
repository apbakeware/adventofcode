project(aocinput)

set(SOURCE_FILES 
  src/line_collection.cc
)

add_library(${PROJECT_NAME} ${SOURCE_FILES})

add_library(libs::${PROJECT_NAME} ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
	PUBLIC
		$<INSTALL_INTERFACE:include>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
	PRIVATE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/>
)


target_link_libraries(${PROJECT_NAME} spdlog)

##################
# Testing
if(BUILD_TESTS)
	add_subdirectory(test)
endif()
