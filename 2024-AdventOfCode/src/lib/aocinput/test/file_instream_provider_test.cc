#include <algorithm>
#include <fstream>
#include <iosfwd>
#include <iterator>
#include <string>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

#include "aocinput/provider/file_instream_provider.h"
#include "aocinput/value_collection.h"

namespace aoc::input::test {

using aoc::input::provider::FileInstreamProvider;

TEST_CASE("FileInstreamProvider value test") {

  SECTION("Test single value") {
    const int test_value = 19;
    std::string data_file_name("./test_case_file.txt");

    {
      std::ofstream writer(data_file_name);
      writer << test_value;
    }

    FileInstreamProvider<int> sut(data_file_name);
    FileInstreamProvider<int>::PopulatedType populated = 0;

    CHECK(sut.populate(populated));
    REQUIRE(populated == test_value);
  }

  SECTION("Test value collection") {

    using CollectionType = ValueCollection<int>;

    const CollectionType::ValuesType test_values = {7, -19, 1264};
    std::string data_file_name("./test_case_file.txt");
    {
      std::ofstream writer(data_file_name);
      std::copy(test_values.begin(), test_values.end(),
                std::ostream_iterator<int>(writer, " "));
    }

    FileInstreamProvider<CollectionType> sut(data_file_name);
    FileInstreamProvider<CollectionType>::PopulatedType populated;

    CHECK(sut.populate(populated));
    REQUIRE_THAT(populated.values(), Catch::Matchers::Equals(test_values));
  }

  SECTION("Test value collection with bad file path") {
    using CollectionType = ValueCollection<int>;

    FileInstreamProvider<CollectionType> sut("bad_file_path.txt.dat");
    FileInstreamProvider<CollectionType>::PopulatedType populated;

    CHECK_FALSE(sut.populate(populated));
    CHECK(populated.values().empty());
  }
}

} // namespace aoc::input::test