#include <algorithm>
#include <iterator>
#include <string>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>

#include "aocinput/line_collection.h"
#include "aocinput/provider/file_get_line_provider.h"

namespace aoc::input::test {

using aoc::input::provider::FileGetLineProvider;

TEST_CASE("FileGetLineProvider test") {

  SECTION("Test parsing from multi-line file") {
    const std::string data_file_name("./test_case_file.mulitline_text.txt");
    const LineCollection::Lines test_values = {"this is a bunch of text with",
                                               "space and different",
                                               "lines and stuff.  ya know?"};

    {
      std::ofstream writer(data_file_name);
      std::copy(test_values.begin(), test_values.end(),
                std::ostream_iterator<LineCollection::Line>(writer, "\n"));
    }

    FileGetLineProvider<LineCollection> sut(data_file_name);
    FileGetLineProvider<LineCollection>::PopulatedType populated;

    CHECK(sut.populate(populated));
    REQUIRE_THAT(populated.lines(), Catch::Matchers::Equals(test_values));
  }

  SECTION("Test value collection with bad file path") {

    const std::string data_file_name("./_____BAD_____.txt");
    FileGetLineProvider<LineCollection> sut(data_file_name);
    FileGetLineProvider<LineCollection>::PopulatedType populated;

    CHECK_FALSE(sut.populate(populated));
    CHECK(populated.lines().empty());
  }
}

} // namespace aoc::input::test