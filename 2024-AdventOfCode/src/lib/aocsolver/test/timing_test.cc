#include <catch2/catch_test_macros.hpp>

#include <algorithm>
#include <chrono>

#include "aocsolver/timing.h"

namespace aoc::solver::test {

using Ms = std::chrono::microseconds;

std::chrono::time_point<std::chrono::system_clock> createTimestamp(Ms micros) {
  return std::chrono::time_point<std::chrono::system_clock>(micros);
}

TEST_CASE("Timing_Test") {
  SECTION("Test computeDuration") {

    const PipelineTimestampRecord timestamps{
        createTimestamp(Ms(0)),  createTimestamp(Ms(5)),
        createTimestamp(Ms(10)), createTimestamp(Ms(15)),
        createTimestamp(Ms(25)), createTimestamp(Ms(30)),
    };

    const PipelineDurationRecord sut = computeDuration(timestamps);

    // REQUIRE(Day09_Sensor_Predictor::predict_next_reading(input) == 18);
    CHECK(sut.parse_duration == Ms(5));
    CHECK(sut.solve_duration == Ms(10));
    CHECK(sut.total_pipeline_duration == Ms(30));
  }
}

} // namespace aoc::solver::test