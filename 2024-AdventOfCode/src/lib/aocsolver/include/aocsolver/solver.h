#ifndef __SOLVER_H__
#define __SOLVER_H__

namespace aoc::solver {

/**
 * The Solver class is the CRTP
 * base class for solution solvers.
 * It defines the input type,
 * result type, and solver interface
 * to be implemented.
 */
template <class Derived_T, class Input_T, class Result_T> class Solver {
public:
  using SolverType = Derived_T;
  using InputType = Input_T;
  using ResultType = Result_T;

  Result_T solve(Input_T &input) {
    return static_cast<SolverType *>(this)->solve(input);
  }
};

} // namespace aoc::solver

#endif /* __SOLVER_H__ */