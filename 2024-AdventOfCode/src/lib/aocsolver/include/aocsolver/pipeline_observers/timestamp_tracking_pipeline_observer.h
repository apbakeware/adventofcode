#ifndef __TIMESTAMP_TRACKING_PIPELINE_OBSERVER_H__
#define __TIMESTAMP_TRACKING_PIPELINE_OBSERVER_H__

#include <string>
#include <tuple>
#include <vector>

#include "aocsolver/isolver_pipeline_observer.h"
#include "aocsolver/timing.h"

namespace aoc::solver {

// Store multiple solver run times
class TimestampTrackingPipelineObserver : public ISolverPipelineObserver {
public:
  virtual ~TimestampTrackingPipelineObserver();

  virtual void onSolverPipelineStart(const std::string &solver_name,
                                     const Timestamp &timestamp);

  virtual void onSolverPipelineComplete(const Timestamp &timestamp);

  virtual void onSolverParseInputStart(const Timestamp &timestamp);

  virtual void onSolverParseInputComplete(const Timestamp &timestamp);

  virtual void onSolverRunSolverStart(const Timestamp &timestamp);

  virtual void onSolverRunSolverComplete(const Timestamp &timestamp,
                                         const std::string &solution);

  template <typename Operation> void for_each_result(Operation op) {
    for (auto &record : m_duration_records) {
      op(std::get<0>(record), std::get<1>(record), std::get<2>(record));
    }
  }

private:
  using SolverTimestampRecord =
      std::tuple<std::string, PipelineTimestampRecord, std::string>;

  SolverTimestampRecord m_current_tracking;
  std::vector<SolverTimestampRecord> m_duration_records;
};

} // namespace aoc::solver

#endif /* __TIMESTAMP_TRACKING_PIPELINE_OBSERVER_H__ */