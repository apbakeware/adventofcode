#ifndef __STDOUT_LOGGING_PIPELINE_OBSERVER_H__
#define __STDOUT_LOGGING_PIPELINE_OBSERVER_H__

#include <string>

#include "../isolver_pipeline_observer.h"

namespace aoc::solver {

class StdOutLoggingPipelineObserver : public ISolverPipelineObserver {
public:
  virtual ~StdOutLoggingPipelineObserver();

  virtual void onSolverPipelineStart(const std::string &solver_name,
                                     const Timestamp &timestamp);

  virtual void onSolverPipelineComplete(const Timestamp &timestamp);

  virtual void onSolverParseInputStart(const Timestamp &timestamp);

  virtual void onSolverParseInputComplete(const Timestamp &timestamp);

  virtual void onSolverRunSolverStart(const Timestamp &timestamp);

  virtual void onSolverRunSolverComplete(const Timestamp &timestamp,
                                         const std::string &solution);

private:
  std::string m_solver_name;
};

} // namespace aoc::solver

#endif /* __STDOUT_LOGGING_PIPELINE_OBSERVER_H__ */