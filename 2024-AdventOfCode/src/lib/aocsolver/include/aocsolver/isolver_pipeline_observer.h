#ifndef __ISOLVER_PIPELINE_OBSERVER_H__
#define __ISOLVER_PIPELINE_OBSERVER_H__

#include <string>

#include "timing.h"

namespace aoc::solver {

class ISolverPipelineObserver {
public:
  virtual ~ISolverPipelineObserver() {}

  virtual void onSolverPipelineStart(const std::string &solver_name,
                                     const Timestamp &timestamp) = 0;

  virtual void onSolverPipelineComplete(const Timestamp &timestamp) = 0;

  virtual void onSolverParseInputStart(const Timestamp &timestamp) = 0;

  virtual void onSolverParseInputComplete(const Timestamp &timestamp) = 0;

  virtual void onSolverRunSolverStart(const Timestamp &timestamp) = 0;

  virtual void onSolverRunSolverComplete(const Timestamp &timestamp,
                                         const std::string &solution) = 0;

  // interface to report answer
};

} // namespace aoc::solver

#endif /* __ISOLVER_PIPELINE_OBSERVER_H__ */