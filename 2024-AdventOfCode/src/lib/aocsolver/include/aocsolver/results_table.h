#ifndef __RESULTS_TABLE_H__
#define __RESULTS_TABLE_H__

#include <string>
#include <vector>

#include "aocsolver/pipeline_observers/expected_solution_pipeline_observer.h"
#include "aocsolver/pipeline_observers/timestamp_tracking_pipeline_observer.h"

namespace aoc::solver {

void renderResultsTable(
    aoc::solver::TimestampTrackingPipelineObserver &results);
void renderResultsTable(aoc::solver::TimestampTrackingPipelineObserver &results,
                        const std::vector<std::string> &solver_filter);

void renderResultsTable(aoc::solver::ExpectedSolutionPipelineObserver &results);
void renderResultsTable(aoc::solver::ExpectedSolutionPipelineObserver &results,
                        const std::vector<std::string> &solver_filter);

} // namespace aoc::solver

#endif /* __RESULTS_TABLE_H__ */