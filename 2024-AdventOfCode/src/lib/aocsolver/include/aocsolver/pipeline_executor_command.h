#ifndef __PIPELINE_EXECUTOR_COMMAND_H__
#define __PIPELINE_EXECUTOR_COMMAND_H__

#include "aoc/util/i_command.h"
#include "aocsolver/pipeline.h"

namespace aoc::solver {

/**
 * The PipelineExecutorCommand implements the ICommand
 * interface to execute a pipeline solver. The template
 * parameters define the type of the solver (Solver_T) and the
 * type of the input provider (Provider_T). The provider
 * must be able to provide the Solver_T::InputType to the
 * solver.
 */
template <class Solver_T, class Provider_T>
class PipelineExecutorCommand : public aoc::util::ICommand {
public:
  using SolverType = Solver_T;
  using InputProviderType = Provider_T;

  PipelineExecutorCommand(aoc::solver::Pipeline &pipeline,
                          Provider_T &input_provider,
                          const std::string &solver_name)
      : m_solver_name(solver_name), m_pipeline(pipeline),
        m_input_provider(std::move(input_provider)) {}

  PipelineExecutorCommand(aoc::solver::Pipeline &pipeline,
                          Provider_T &&input_provider,
                          const std::string &solver_name)
      : m_solver_name(solver_name), m_pipeline(pipeline),
        m_input_provider(std::move(input_provider)) {}

  virtual ~PipelineExecutorCommand() = default;

  virtual bool execute() {
    SolverType solver;
    m_pipeline.runSolverPipeline(m_input_provider, solver, m_solver_name);

    return true;
  }

private:
  std::string m_solver_name;
  aoc::solver::Pipeline &m_pipeline;
  InputProviderType m_input_provider;
};

} // namespace aoc::solver

#endif /* __PIPELINE_EXECUTOR_COMMAND_H__ */