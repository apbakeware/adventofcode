#ifndef __SOLVER_TYPELIST_H__
#define __SOLVER_TYPELIST_H__

#include <string>
#include <tuple>
#include <type_traits>
#include <vector>

#include "spdlog/spdlog.h"

#include "aoc/traits/constructor_traits.h"
#include "aoc/util/null_command.h"
#include "aocsolver/pipeline.h"
#include "aocsolver/pipeline_executor_command.h"

namespace aoc::solver {

// Try and build the provider. If the provider has a default constructor,
// it is prefered, otherwise get teh aruments from the spec.
//
// TODO: This should prefer parameterized constructors, not default.
template <typename Provider, typename Spec> Provider construct_provider() {
  if constexpr (aoc::traits::has_default_constructor<Provider>::value) {
    SPDLOG_DEBUG("Specification has not provider arguments, default "
                 "constructor provider");
    return Provider();
  } else {

    SPDLOG_DEBUG("Specification has provider arguments, constructing provider "
                 "with arguments");
    Provider data_provider = std::apply(
        [](auto &&...args) {
          return Provider(std::forward<decltype(args)>(args)...);
        },
        Spec::providerArgs());
    return data_provider;
  }
}

/**
 * The SolverTypeList is defined with a variable set of solver
 * specifications and provides a suite of operations to perform
 * on solver types.
 *
 * The templates are a variable number of Solver_Spec_T types defining
 * and naming what commands can be built.
 *
 * The Solver_Spec_T requires:
 *    Solver_Spec_T::SolverType type satisifying the Solver<> interface
 *    Solver_Spec_T::InputProviderType type providing the input type for the
 * solver.
 *    Solver_Spec_T::name() unique name to key the spec type to build by name.
 *    Solver_Spec_T::providerArgs() required to support whatever arguments
 *       are needed by Solver_Spec_T::InputProviderType as a tuple
 */
template <class Solver_Spec_T, class... Solvers> struct SolverTypeList {

  using SolverType = typename Solver_Spec_T::SolverType;
  using InputProviderType = typename Solver_Spec_T::InputProviderType;

  /**
   * Recurvsivly check the SolverTypeList for a solver with the specified name.
   */
  static bool hasSolver(const std::string &name) {
    if (Solver_Spec_T::name() == name) {
      return true;
    }

    return SolverTypeList<Solvers...>::hasSolver(name);
  }

  /**
   * Append the name of each solver to the list of names.
   */
  static void getSolverNames(std::vector<std::string> &out_names) {
    out_names.push_back(Solver_Spec_T::name());
    SolverTypeList<Solvers...>::getSolverNames(out_names);
  }

  /**
   * Add an Pipeline command for each solver type in the list and
   * add it to the command list.
   */
  static void
  addExecutorCommand(std::vector<aoc::util::ICommand *> &out_commands,
                     Pipeline &pipeline) {
    SPDLOG_TRACE("addExecutorCommand()");
    SolverTypeList<Solver_Spec_T>::addExecutorCommand(out_commands, pipeline);
    SolverTypeList<Solvers...>::addExecutorCommand(out_commands, pipeline);
  }

  /**
   * Look for the solver with the given name. If it is found add it. If
   * it is not found in the list of solvers, a NullCommand is added.
   */
  static aoc::util::ICommand *
  executorCommandBuilder(const std::string &solver_name, Pipeline &pipeline) {
    SPDLOG_TRACE("executorCommandBuilder( solver_name={}, pipline={})",
                 solver_name, (void *)(&pipeline));

    SPDLOG_DEBUG(
        "executorCommandBuilder() Solver_T::name() = {}, solver_name={}",
        Solver_Spec_T::name(), solver_name);

    if (Solver_Spec_T::name() == solver_name) {
      SPDLOG_DEBUG("Found solver by name, building new command");
      return SolverTypeList<Solver_Spec_T>::buildExecutorCommand(pipeline);
    }

    return SolverTypeList<Solvers...>::executorCommandBuilder(solver_name,
                                                              pipeline);
  }

  /**
   * Call the operation with the name and expected
   * result of the solver. Call the remaining solver types.
   */
  template <typename Operation>
  static void registerExpectedResults(Operation op) {
    op(Solver_Spec_T::name(), Solver_Spec_T::expected());
    SolverTypeList<Solvers...>::registerExpectedResults(op);
  }
};

/**
 * Single value template specialization of the variadic
 * SolverTypeList method.
 */
template <class Solver_Spec_T> struct SolverTypeList<Solver_Spec_T> {

  using SolverType = typename Solver_Spec_T::SolverType;
  using InputProviderType = typename Solver_Spec_T::InputProviderType;

  /**
   * Build a pipeline command for the Solver.
   */
  static aoc::util::ICommand *buildExecutorCommand(Pipeline &pipeline) {
    SPDLOG_TRACE("executorCommandBuilder( pipline, args...)");

    InputProviderType data_provider =
        construct_provider<InputProviderType, Solver_Spec_T>();

    return new PipelineExecutorCommand<SolverType, InputProviderType>(
        pipeline, std::move(data_provider), Solver_Spec_T::name());
  }

  /**
   * Recurvsivly check the SolverTypeList for a solver with the specified name.
   */
  static bool hasSolver(const std::string &name) {
    return Solver_Spec_T::name() == name;
  }

  /**
   * Add the name of the solver to the list.
   */
  static void getSolverNames(std::vector<std::string> &out_names) {
    out_names.push_back(Solver_Spec_T::name());
  }

  /**
   * Build an executor command for teh solver and add it to the list of
   * commands.
   */
  static void
  addExecutorCommand(std::vector<aoc::util::ICommand *> &out_commands,
                     Pipeline &pipeline) {
    SPDLOG_TRACE("addExecutorCommand()");
    out_commands.push_back(buildExecutorCommand(pipeline));
  }

  /**
   * Build a pipeline command for the Solver if the name matches,
   * otherwise a NullCommand is built.
   */
  static aoc::util::ICommand *
  executorCommandBuilder(const std::string &solver_name, Pipeline &pipeline) {
    SPDLOG_TRACE("executorCommandBuilder( solver_name={}, pipline={})",
                 solver_name, (void *)(&pipeline));

    SPDLOG_DEBUG(
        "executorCommandBuilder() Solver_T::name() = {}, solver_name={}",
        Solver_Spec_T::name(), solver_name);

    if (Solver_Spec_T::name() != solver_name) {
      SPDLOG_WARN("Failed to find solver named: '{}', using NullCommand",
                  solver_name);
      return new aoc::util::NullCommand();
    }

    SPDLOG_DEBUG("Found solver by name, building new command");
    return buildExecutorCommand(pipeline);
  }

  /**
   * Call the operation with the name and expected
   * result of the solver.
   */
  template <typename Operation>
  static void registerExpectedResults(Operation op) {
    op(Solver_Spec_T::name(), Solver_Spec_T::expected());
  }
};

} // namespace aoc::solver

#endif /* __SOLVER_TYPELIST_H__ */