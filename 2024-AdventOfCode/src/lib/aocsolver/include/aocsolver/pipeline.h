#ifndef __PIPELINE_H__
#define __PIPELINE_H__

#include <vector>

#include "spdlog/spdlog.h"

#include "aoc/util/as_string.hpp"
#include "aoc/util/demangle_type_name.h"
#include "aoc/util/observable.h"
#include "aocsolver/isolver_pipeline_observer.h"

namespace aoc::solver {

// make an observer implementation to do the resgister observer...
class Pipeline : public util::Observable<ISolverPipelineObserver> {
public:
  // Solver_T :: must have getName(), InputType;
  // Parser_T implements bool parse(Solver_T::InputType & out_input)
  template <class Input_Provider_T, class Solver_T>
  void runSolverPipeline(Input_Provider_T &input_provider, Solver_T &solver,
                         const std::string &solver_name) {

    SPDLOG_TRACE("Pipeline::runSolverPipeline<{}, {}>()",
                 demangled_type_name(input_provider),
                 demangled_type_name(solver));

    typename Solver_T::InputType input;
    typename Solver_T::ResultType solution;
    bool parse_successful = false;

    SPDLOG_INFO("Running solver: {}", solver_name);
    notifyObservers(std::bind(&ISolverPipelineObserver::onSolverPipelineStart,
                              std::placeholders::_1, solver_name,
                              timestampNow()));

    SPDLOG_DEBUG("starting solution input parsing");
    notifyObservers(std::bind(&ISolverPipelineObserver::onSolverParseInputStart,
                              std::placeholders::_1, timestampNow()));
    parse_successful = input_provider.populate(input);
    notifyObservers(
        std::bind(&ISolverPipelineObserver::onSolverParseInputComplete,
                  std::placeholders::_1, timestampNow()));
    SPDLOG_DEBUG("completed solution input parsing, was successful? {}",
                 parse_successful);

    SPDLOG_DEBUG("starting solution solving");
    notifyObservers(std::bind(&ISolverPipelineObserver::onSolverRunSolverStart,
                              std::placeholders::_1, timestampNow()));
    if (parse_successful) {
      solution = solver.solve(input);
    } else {
      SPDLOG_ERROR("Input parsing was not successful, solving skipped");
    }

    notifyObservers(std::bind(
        &ISolverPipelineObserver::onSolverRunSolverComplete,
        std::placeholders::_1, timestampNow(), aoc::util::as_string(solution)));
    SPDLOG_DEBUG("completed solution solving");

    notifyObservers(
        std::bind(&ISolverPipelineObserver::onSolverPipelineComplete,
                  std::placeholders::_1, timestampNow()));

    SPDLOG_INFO("Completed solver: {}", solver_name);
  }
};

} // namespace aoc::solver

#endif /* __PIPELINE_H__ */