#include "aocsolver/pipeline_observers/timestamp_tracking_pipeline_observer.h"
#include <tuple>

namespace aoc::solver {

const PipelineTimestampRecord DEFAULT_TIMESTAMP_RECORD = {
    Timestamp::min(), Timestamp::min(), Timestamp::min(),
    Timestamp::min(), Timestamp::min(), Timestamp::min()};

TimestampTrackingPipelineObserver::~TimestampTrackingPipelineObserver() {}

void TimestampTrackingPipelineObserver::onSolverPipelineStart(
    const std::string &solver_name, const Timestamp &timestamp) {

  m_current_tracking =
      std::make_tuple(solver_name, DEFAULT_TIMESTAMP_RECORD, "tbd");
  std::get<1>(m_current_tracking).pipeline_start = timestamp;
}

void TimestampTrackingPipelineObserver::onSolverPipelineComplete(
    const Timestamp &timestamp) {

  std::get<1>(m_current_tracking).pipeline_complete = timestamp;
  m_duration_records.push_back(m_current_tracking);

  // TODO: reset the object?
}

void TimestampTrackingPipelineObserver::onSolverParseInputStart(
    const Timestamp &timestamp) {
  std::get<1>(m_current_tracking).parse_start = timestamp;
}

void TimestampTrackingPipelineObserver::onSolverParseInputComplete(
    const Timestamp &timestamp) {
  std::get<1>(m_current_tracking).parse_complete = timestamp;
}

void TimestampTrackingPipelineObserver::onSolverRunSolverStart(
    const Timestamp &timestamp) {
  std::get<1>(m_current_tracking).solve_start = timestamp;
}

void TimestampTrackingPipelineObserver::onSolverRunSolverComplete(
    const Timestamp &timestamp, const std::string &solution) {
  std::get<1>(m_current_tracking).solve_complete = timestamp;
  std::get<2>(m_current_tracking) = solution;
}

} // namespace aoc::solver