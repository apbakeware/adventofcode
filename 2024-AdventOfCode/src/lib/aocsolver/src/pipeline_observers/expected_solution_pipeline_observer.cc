#include <tuple>

#include "spdlog/spdlog.h"

#include "aocsolver/pipeline_observers/expected_solution_pipeline_observer.h"

namespace aoc::solver {

ExpectedSolutionPipelineObserver::~ExpectedSolutionPipelineObserver() {}

void ExpectedSolutionPipelineObserver::onSolverPipelineStart(
    const std::string &solver_name, const Timestamp &timestamp) {
  (void)(timestamp);

  SPDLOG_DEBUG("Setting current solver name: {}", solver_name);
  m_current_solver = solver_name;
}

void ExpectedSolutionPipelineObserver::onSolverPipelineComplete(
    const Timestamp &timestamp) {
  (void)(timestamp);

  SPDLOG_DEBUG("Clearing current solver name");
  m_current_solver.clear();
}

void ExpectedSolutionPipelineObserver::onSolverParseInputStart(
    const Timestamp &timestamp) {
  (void)(timestamp);
}

void ExpectedSolutionPipelineObserver::onSolverParseInputComplete(
    const Timestamp &timestamp) {
  (void)(timestamp);
}

void ExpectedSolutionPipelineObserver::onSolverRunSolverStart(
    const Timestamp &timestamp) {
  (void)(timestamp);
}

void ExpectedSolutionPipelineObserver::onSolverRunSolverComplete(
    const Timestamp &timestamp, const std::string &solution) {

  (void)(timestamp);
  ResultTable::iterator iter = m_result_records.find(m_current_solver);
  if (iter != m_result_records.end()) {
    iter->second.actual = solution;
  } else {
    SPDLOG_DEBUG("Did not find solver: {}, skipping", m_current_solver);
  }
}

ExpectedSolutionPipelineObserver &
ExpectedSolutionPipelineObserver::addExpectedResult(
    const std::string &solver_name, const std::string &expected) {

  m_result_records[solver_name] = {expected, ""};
  return *this;
}

} // namespace aoc::solver