#include <iostream>

#include "aocsolver/pipeline_observers/stdout_logging_pipeline_observer.h"

namespace aoc::solver {

StdOutLoggingPipelineObserver::~StdOutLoggingPipelineObserver() {}

void StdOutLoggingPipelineObserver::onSolverPipelineStart(
    const std::string &solver_name, const Timestamp &timestamp) {

  const WallClockTimestampWriter wc_timestamp = {timestamp};
  std::cout << "StdOutLoggingPipelineObserver::onSolverPipelineStart( "
            << solver_name << " ) @ " << wc_timestamp << "\n";
  m_solver_name = solver_name;
}

void StdOutLoggingPipelineObserver::onSolverPipelineComplete(
    const Timestamp &timestamp) {

  const WallClockTimestampWriter wc_timestamp = {timestamp};
  std::cout << "StdOutLoggingPipelineObserver::onSolverPipelineComplete() @ "
            << wc_timestamp << "\n";
}

void StdOutLoggingPipelineObserver::onSolverParseInputStart(
    const Timestamp &timestamp) {

  const WallClockTimestampWriter wc_timestamp = {timestamp};
  std::cout << "StdOutLoggingPipelineObserver::onSolverParseInputStart() @ "
            << wc_timestamp << "\n";
}

void StdOutLoggingPipelineObserver::onSolverParseInputComplete(
    const Timestamp &timestamp) {

  const WallClockTimestampWriter wc_timestamp = {timestamp};
  std::cout << "StdOutLoggingPipelineObserver::onSolverParseInputComplete() @ "
            << wc_timestamp << "\n";
}

void StdOutLoggingPipelineObserver::onSolverRunSolverStart(
    const Timestamp &timestamp) {

  const WallClockTimestampWriter wc_timestamp = {timestamp};
  std::cout << "StdOutLoggingPipelineObserver::onSolverRunSolverStart() @ "
            << wc_timestamp << "\n";
}

void StdOutLoggingPipelineObserver::onSolverRunSolverComplete(
    const Timestamp &timestamp, const std::string &solution) {

  const WallClockTimestampWriter wc_timestamp = {timestamp};
  std::cout << "StdOutLoggingPipelineObserver::onSolverRunSolverComplete() @ "
            << wc_timestamp << "\n";
}

} // namespace aoc::solver