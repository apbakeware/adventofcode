#include <chrono>
#include <iostream>

#include "spdlog/fmt/bundled/color.h"

#include "aocsolver/results_table.h"
#include "aocsolver/timing.h"

namespace aoc::solver {

// clang-format off
const std::string TABLE_OUTLINE = "====================================================================\n";
const std::string TABLE_ROW_SEP = "--------------------------------------------------------------------\n";
// clang-format on

void renderTableBorder() { fmt::print("{:=<70}\n", ""); }

void renderTableSeparator() { fmt::print("{:-<70}\n", ""); }

void renderTableHeader(const std::string &title) {
  renderTableBorder();
  fmt::print("{:^70}\n", title);
}

std::string format_microsecond_duration(Duration duration) {
  static const std::string FIELD = "{:>10}";
  if (duration > std::chrono::milliseconds(500)) {
    return fmt::format(fmt::fg(fmt::color::red), FIELD, duration.count());

  } else if (duration > std::chrono::milliseconds(100)) {
    return fmt::format(fmt::fg(fmt::color::yellow), FIELD, duration.count());
  } else if (duration < std::chrono::milliseconds(5)) {
    return fmt::format(fmt::fg(fmt::color::green), FIELD, duration.count());
  }

  return fmt::format(fmt::fg(fmt::color::blue), FIELD, duration.count());
}

void renderResultsTable(
    aoc::solver::TimestampTrackingPipelineObserver &results) {

  static const std::string OUTPUT_FORMAT_STRING(
      "{:<10} {:>10}  {:>10}  {:>10}   {:>22}\n");

  auto print_result_row = [](const std::string &name,
                             const aoc::solver::PipelineTimestampRecord &record,
                             const std::string &result) {
    const aoc::solver::PipelineDurationRecord &duration =
        aoc::solver::computeDuration(record);

    fmt::print(OUTPUT_FORMAT_STRING, name,
               format_microsecond_duration(duration.parse_duration),
               format_microsecond_duration(duration.solve_duration),
               format_microsecond_duration(duration.total_pipeline_duration),
               result);
  };

  renderTableHeader("RESULTS");
  fmt::print(OUTPUT_FORMAT_STRING, "", "Parse", "Solve", "Total", "");
  fmt::print(OUTPUT_FORMAT_STRING, "Solver", "(us)", "(us)", "(us)",
             "Solution");
  renderTableSeparator();
  results.for_each_result(print_result_row);
  renderTableBorder();
}

void renderResultsTable(aoc::solver::TimestampTrackingPipelineObserver &results,
                        const std::vector<std::string> &solver_filter) {
  static const std::string OUTPUT_FORMAT_STRING(
      "{:<10} {:>10}  {:>10}  {:>10}   {}\n");

  auto print_result_row =
      [&solver_filter](const std::string &name,
                       const aoc::solver::PipelineTimestampRecord &record,
                       const std::string &result) {
        if (std::find(solver_filter.begin(), solver_filter.end(), name) !=
            solver_filter.end()) {
          const aoc::solver::PipelineDurationRecord &duration =
              aoc::solver::computeDuration(record);

          fmt::print(OUTPUT_FORMAT_STRING, name,
                     duration.parse_duration.count(),
                     duration.solve_duration.count(),
                     duration.total_pipeline_duration.count(), result);
        }
      };

  renderTableHeader("RESULTS");
  fmt::print(OUTPUT_FORMAT_STRING, "", "Parse", "Solve", "Total", "");
  fmt::print(OUTPUT_FORMAT_STRING, "Solver", "(us)", "(us)", "(us)",
             "Solution");
  renderTableSeparator();
  results.for_each_result(print_result_row);
  renderTableBorder();
}

void renderResultsTable(
    aoc::solver::ExpectedSolutionPipelineObserver &results) {
  static const std::string OUTPUT_FORMAT_STRING("{:<10} {:1}  {:<20}  {}\n");

  auto row_renderer = [](const std::string &solver_name,
                         const std::string &expected, const std::string &actual,
                         bool is_correct) {
    auto formatted_correct = is_correct
                                 ? fmt::format(fmt::fg(fmt::color::green),
                                               "\u2714") // unicode checkmark
                                 : fmt::format(fmt::fg(fmt::color::yellow),
                                               "\u2716"); // unicode x mark

    fmt::print(OUTPUT_FORMAT_STRING, solver_name, formatted_correct, expected,
               actual);
  };

  renderTableHeader("EXPECTATIONS");
  fmt::print(OUTPUT_FORMAT_STRING, "Solver", " ", "Expected", "Actual");
  renderTableSeparator();
  results.for_each_result(row_renderer);
  renderTableBorder();
}

void renderResultsTable(aoc::solver::ExpectedSolutionPipelineObserver &results,
                        const std::vector<std::string> &solver_filter) {
  static const std::string OUTPUT_FORMAT_STRING("{:<10} {:1}  {:<20}  {}\n");

  auto row_renderer = [&solver_filter](const std::string &solver_name,
                                       const std::string &expected,
                                       const std::string &actual,
                                       bool is_correct) {
    if (std::find(solver_filter.begin(), solver_filter.end(), solver_name) !=
        solver_filter.end()) {
      auto formatted_correct = is_correct
                                   ? fmt::format(fmt::fg(fmt::color::green),
                                                 "\u2714") // unicode checkmark
                                   : fmt::format(fmt::fg(fmt::color::yellow),
                                                 "\u2716"); // unicode x mark

      fmt::print(OUTPUT_FORMAT_STRING, solver_name, formatted_correct, expected,
                 actual);
    }
  };

  renderTableHeader("EXPECTATIONS");
  fmt::print(OUTPUT_FORMAT_STRING, "Solver", " ", "Expected", "Actual");
  renderTableSeparator();
  results.for_each_result(row_renderer);
  renderTableBorder();
}

} // namespace aoc::solver