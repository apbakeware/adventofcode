#include <iostream>

#include "aoc/util/null_type.h"

namespace aoc::util {

std::string NullType::str() const { return "NullType"; }

std::ostream &operator<<(std::ostream &ostr, NullType &obj) {
  ostr << obj.str();
  return ostr;
}

std::istream &operator>>(std::istream &instr, NullType &obj) {
  (void)(obj);
  return instr;
}

} // namespace aoc::util
