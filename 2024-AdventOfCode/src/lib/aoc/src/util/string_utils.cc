#include "aoc/util/string_utils.hpp"

namespace aoc::util {

size_t find_last_occurance(const std::string &src, const std::string &target) {
  size_t return_pos = std::string::npos;
  size_t search_result = std::string::npos;
  size_t search_pos = 0;

  while ((search_result = src.find(target, search_pos + 1)) !=
         std::string::npos) {
    search_pos = search_result;
    return_pos = search_result;
  }
  return return_pos;
}

// TODO: structure to dtermine if is valid?
std::pair<size_t, size_t>
find_first_and_last_occurance(const std::string &str, const std::string &srch) {
  std::pair<size_t, size_t> result{std::string::npos, std::string::npos};

  result.first = str.find(srch);
  if (result.first != std::string::npos) {
    result.second = result.first;
    size_t position = result.first;
    while ((position = str.find(srch, position + 1)) != std::string::npos) {
      result.second = position;
    }
  }

  return result;
}

std::vector<ssize_t> find_fold_reflection_points(const std::string &str) {
  std::vector<ssize_t> reflection_points;
  if (str.length() < 1) {
    return reflection_points;
  }

  std::string reflection;
  std::string::const_iterator str_pop_iter = str.begin();
  const std::string::const_iterator pop_end_iter = str.end();

  reflection.push_back(*str_pop_iter++);

  while (str_pop_iter != pop_end_iter) {
    const auto dist_from_start = std::distance(str.begin(), str_pop_iter);
    const auto until_end = std::distance(str_pop_iter, pop_end_iter);

    bool is_reflextion_point =
        (dist_from_start < until_end)
            ? std::equal(reflection.rbegin(), reflection.rend(), str_pop_iter)
            : std::equal(str_pop_iter, str.end(), reflection.rbegin());

    if (is_reflextion_point) {
      reflection_points.push_back(dist_from_start);
    }

    reflection.push_back(*str_pop_iter++);
  }

  return reflection_points;
}

std::vector<std::string> split(const std::string &str, char delim) {
  std::vector<std::string> splits;

  size_t start = 0;
  size_t pos = std::string::npos;
  while ((pos = str.find(delim, start)) != std::string::npos) {
    size_t dist = pos - start;
    splits.push_back(str.substr(start, dist));
    start = pos + 1;
  }
  splits.push_back(str.substr(start));
  return splits;
}

} // namespace aoc::util