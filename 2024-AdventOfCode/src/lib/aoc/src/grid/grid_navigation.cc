#include "grid/grid_navigation.h"
#include "grid/grid_location.h"

namespace aoc::grid {

Orientation4 rotate_cw(Orientation4 orientation) {
  Orientation4 cw = Orientation4::UP;
  switch (orientation) {
  case Orientation4::UP:
    cw = Orientation4::RIGHT;
    break;
  case Orientation4::RIGHT:
    cw = Orientation4::DOWN;
    break;
  case Orientation4::DOWN:
    cw = Orientation4::LEFT;
    break;
  case Orientation4::LEFT:
    cw = Orientation4::UP;
    break;
  }

  return cw;
}

Orientation4 rotate_ccw(Orientation4 orientation) {
  Orientation4 cw = Orientation4::UP;
  switch (orientation) {
  case Orientation4::UP:
    cw = Orientation4::LEFT;
    break;
  case Orientation4::RIGHT:
    cw = Orientation4::UP;
    break;
  case Orientation4::DOWN:
    cw = Orientation4::RIGHT;
    break;
  case Orientation4::LEFT:
    cw = Orientation4::DOWN;
    break;
  }

  return cw;
}

Grid_Location ahead(Orientation4 orientation, const Grid_Location &location) {
  Grid_Location dest(location);
  switch (orientation) {
  case Orientation4::UP:
    dest.row++;
    break;
  case Orientation4::RIGHT:
    dest.col++;
    break;
  case Orientation4::DOWN:
    dest.row--;
    break;
  case Orientation4::LEFT:
    dest.col--;
    break;
  }

  return dest;
}

Grid_Location behind(Orientation4 orientation, const Grid_Location &location) {
  Grid_Location dest(location);
  switch (orientation) {
  case Orientation4::UP:
    dest.row--;
    break;
  case Orientation4::RIGHT:
    dest.col--;
    break;
  case Orientation4::DOWN:
    dest.row++;
    break;
  case Orientation4::LEFT:
    dest.col++;
    break;
  }

  return dest;
}

std::string as_string(Orientation4 orient) {
  std::string repr;
  switch (orient) {
  case Orientation4::UP:
    repr = "UP";
    break;
  case Orientation4::RIGHT:
    repr = "RIGHT";
    break;
  case Orientation4::DOWN:
    repr = "DOWN";
    break;
  case Orientation4::LEFT:
    repr = "LEFT";
    break;
  }

  return repr;
}
} // namespace aoc::grid