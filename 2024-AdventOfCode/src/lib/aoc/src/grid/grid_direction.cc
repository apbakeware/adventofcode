#include "spdlog/spdlog.h"

#include "aoc/grid/grid_direction.h"

namespace aoc::grid {

namespace Orthogonal_Direction {

Grid_Location next_location(const Grid_Location &loc, Direction direction) {
  Grid_Location destination{0, 0};
  switch (direction) {
  case Orthogonal_Direction::UP:
    destination = Grid_Location::up(loc);
    break;
  case Orthogonal_Direction::RIGHT:
    destination = Grid_Location::right(loc);
    break;
  case Orthogonal_Direction::DOWN:
    destination = Grid_Location::down(loc);
    break;
  case Orthogonal_Direction::LEFT:
    destination = Grid_Location::left(loc);
    break;
  case Orthogonal_Direction::NONE:
    SPDLOG_WARN("Value in direciton provided no movement");
    break;
  default:
    SPDLOG_ERROR("Unknown Orthogonal_Direction direction, return NO_MOVEMENT");
  }

  return destination;
}

} // namespace Orthogonal_Direction

} // namespace aoc::grid