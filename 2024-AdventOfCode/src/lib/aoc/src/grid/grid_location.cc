#include <iostream>
#include <tuple>

#include "spdlog/fmt/fmt.h"

#include "aoc/grid/grid_location.h"

namespace aoc::grid {

Grid_Location Grid_Location::stay(const Grid_Location &loc) { return loc; }

Grid_Location Grid_Location::up(const Grid_Location &loc, ssize_t dist) {
  return {loc.row + dist, loc.col};
}

Grid_Location Grid_Location::right(const Grid_Location &loc, ssize_t dist) {
  return {loc.row, loc.col + dist};
}

Grid_Location Grid_Location::down(const Grid_Location &loc, ssize_t dist) {
  return {loc.row - dist, loc.col};
}

Grid_Location Grid_Location::left(const Grid_Location &loc, ssize_t dist) {
  return {loc.row, loc.col - dist};
}

bool operator<(const Grid_Location &lhs, const Grid_Location &rhs) {
  return std::tie(lhs.row, lhs.col) < std::tie(rhs.row, rhs.col);
}

bool operator<=(const Grid_Location &lhs, const Grid_Location &rhs) {
  return std::tie(lhs.row, lhs.col) <= std::tie(rhs.row, rhs.col);
}

bool operator>(const Grid_Location &lhs, const Grid_Location &rhs) {
  return std::tie(lhs.row, lhs.col) > std::tie(rhs.row, rhs.col);
}

bool operator>=(const Grid_Location &lhs, const Grid_Location &rhs) {
  return std::tie(lhs.row, lhs.col) >= std::tie(rhs.row, rhs.col);
}

bool operator==(const Grid_Location &lhs, const Grid_Location &rhs) {
  return std::tie(lhs.row, lhs.col) == std::tie(rhs.row, rhs.col);
}

bool operator!=(const Grid_Location &lhs, const Grid_Location &rhs) {
  return std::tie(lhs.row, lhs.col) != std::tie(rhs.row, rhs.col);
}

Grid_Location operator+(const Grid_Location &lhs, const Grid_Location &rhs) {
  return {lhs.row + rhs.row, lhs.col + rhs.col};
}

Grid_Location operator-(const Grid_Location &lhs, const Grid_Location &rhs) {
  return {lhs.row - rhs.row, lhs.col - rhs.col};
}

bool is_within_bound(const grid::Grid_Location &location,
                     const grid::Grid_Location &low_bound,
                     const grid::Grid_Location &high_bound) {

  return location.row >= low_bound.row && location.row <= high_bound.row &&
         location.col >= low_bound.col && location.col <= high_bound.col;
}

std::istream &operator>>(std::istream &istr, Grid_Location record) {
  istr >> record.row >> record.col;
  return istr;
}

} // namespace aoc::grid