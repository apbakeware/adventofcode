#ifndef __I_COMMAND_H__
#define __I_COMMAND_H__

namespace aoc::util {

/**
 * The ICommand is the base interface for the Command
 * pattern.
 *
 * Command reference: https://refactoring.guru/design-patterns/command
 */
class ICommand {
public:
  virtual ~ICommand() = default;

  /**
   * Execute the encapuslaed command logic.
   */
  virtual bool execute() = 0;
};

} // namespace aoc::util

#endif /* __I_COMMAND_H__ */