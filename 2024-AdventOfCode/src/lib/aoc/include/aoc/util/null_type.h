#ifndef __NULL_TYPE_H__
#define __NULL_TYPE_H__

#include <iosfwd>
#include <string>

namespace aoc::util {

struct NullType {
  std::string str() const;
};

std::ostream &operator<<(std::ostream &ostr, NullType &obj);

std::istream &operator>>(std::istream &instr, NullType &obj);

} // namespace aoc::util

#endif /* __NULL_TYPE_H__ */