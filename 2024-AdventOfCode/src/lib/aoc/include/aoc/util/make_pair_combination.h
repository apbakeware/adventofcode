#ifndef __MAKE_PAIR_COMBINATION_H__
#define __MAKE_PAIR_COMBINATION_H__

#include <iterator>
#include <vector>

namespace aoc::util {

/**
 * Create a vector of pairs for each pair combination in the
 * provided range.
 */
template <typename Iter_T>
std::vector<std::pair<typename std::iterator_traits<Iter_T>::value_type,
                      typename std::iterator_traits<Iter_T>::value_type>>
makePairCombination(Iter_T begin, Iter_T end) {
  std::vector<std::pair<typename std::iterator_traits<Iter_T>::value_type,
                        typename std::iterator_traits<Iter_T>::value_type>>
      pairs;

  for (Iter_T iter = begin; iter != end; ++iter) {
    Iter_T inner = iter;
    for (std::advance(inner, 1); inner != end; ++inner) {
      pairs.emplace_back(std::make_pair(*iter, *inner));
    }
  }

  return pairs;
}

} // namespace aoc::util

#endif /* __MAKE_PAIR_COMBINATION_H__ */