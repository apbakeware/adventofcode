#ifndef __DEMANGLE_TYPE_NAME_H__
#define __DEMANGLE_TYPE_NAME_H__

#include <cxxabi.h>
#include <memory>
#include <string>

template <typename T> std::string demangled_type_name(const T &variable) {
  int status;
  std::unique_ptr<char, void (*)(void *)> res{
      abi::__cxa_demangle(typeid(variable).name(), nullptr, nullptr, &status),
      std::free};
  return (status == 0) ? res.get() : typeid(variable).name();
}

#endif /* __DEMANGLE_TYPE_NAME_H__ */