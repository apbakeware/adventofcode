#ifndef __OBSERVABLE_H__
#define __OBSERVABLE_H__

#include <algorithm>
#include <functional>
#include <vector>

#include "spdlog/spdlog.h"

namespace aoc::util {

/**
 * The Obserable class implements the "publisher" portion of the
 * Observer pattern. Classes which can be observed implement this
 * interface with a concrete observer type which handles the notifications
 * of the observed type. Once defined, clients register observer instances
 * with the Observable. Each of the registered observers are notified
 * when by the Observable when the notifyObservers() is called.
 *

 */
template <class Observer_T> class Observable {
public:
  using ObserverType = Observer_T;

  virtual ~Observable() {}

  bool registerObserver(ObserverType *observer) {
    SPDLOG_TRACE("Observable<Observer_T>::registerObserver( {} )",
                 (void *)observer);
    if (observer == nullptr) {
      SPDLOG_WARN("prevented null observer registration");
      return false;
    }

    m_observers.push_back(observer);
    return true;
  }

  void unregisterObserver(ObserverType *observer) {
    SPDLOG_TRACE("Observable<Observer_T>::unregisterObserver( {} )",
                 (void *)observer);
    if (observer == nullptr) {
      return;
    }

    m_observers.erase(
        std::remove(m_observers.begin(), m_observers.end(), observer),
        m_observers.end());
  }

  // clang-format off
  /**
   * Notify each of the registered observers by invoking the provided function
   * on each object.
   *
   * @param notify: Function which takes a reference to the observer.
   *
   * Example:
   * How to notify an ObserverType type with a member function with
   * values bound to the name and id parameters.
   * @verbatim
   *   void doStuff(std::string name, int id)
   * @endverbatim
   *
   * Using std::bind to pass a member function:
   * @verbatim
   *   auto notifier = std::bind(
   *      &ObserverType::doStuff,
   *      std::placeholders::_1,  // This is the implicit "this" pointer for member function 
   *      "myName",
   *      17
   *   );
   * 
   *   observable.notifyObservers(notify);
   * @endverbatim
   *
   * Using a lambda for a member function:
   * @verbatim
   *   auto notify = [](ObserverType &inf) { 
   *     inf.doStuff( "myName", 17 ); 
   *   };
   * 
   *   observable.notifyObservers(notify);
   * @endverbatim
   *
   */
  // clang-format on
  void notifyObservers(std::function<void(ObserverType &)> notify) {
    SPDLOG_TRACE("Observable<Observer_T>::notifyObservers()");
    for (auto observer : m_observers) {
      notify(*observer);
    }
  }

private:
  using Observers = std::vector<ObserverType *>;
  Observers m_observers;
};

} // namespace aoc::util

#endif /* __OBSERVABLE_H__ */