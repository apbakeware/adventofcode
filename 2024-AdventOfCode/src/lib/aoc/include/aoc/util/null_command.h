#ifndef __NULL_COMMAND_H__
#define __NULL_COMMAND_H__

#include "spdlog/spdlog.h"

#include "aoc/util/i_command.h"

namespace aoc::util {

/**
 * The NullCommand implements the
 * null-object pattern for the ICommand
 * interface.
 */
class NullCommand : public ICommand {
public:
  virtual ~NullCommand() = default;

  virtual bool execute() {
    SPDLOG_TRACE("NullCommand::execute()");
    return true;
  }
};

} // namespace aoc::util

#endif /* __NULL_COMMAND_H__ */