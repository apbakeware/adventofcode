#ifndef __GRID_H__
#define __GRID_H__

#include <array>
#include <iostream>
#include <iterator>
#include <vector>

#include "spdlog/fmt/bundled/core.h"
#include "spdlog/fmt/bundled/format.h"
#include "spdlog/fmt/ostr.h"
#include "spdlog/spdlog.h"

#include "aoc/grid/grid_location.h"

namespace aoc::grid {

// Set on initialization
template <class Data_T> class Fixed_Sized_Grid {
public:
  typedef Data_T value_type;

  Fixed_Sized_Grid(int num_rows, int num_cols, Data_T fill_val = Data_T())
      : m_num_rows(num_rows), m_num_cols(num_cols) {
    grid.resize(num_rows);
    for (auto &row : grid) {
      row.resize(num_cols, fill_val);
    }
  }

  ssize_t number_of_cols() const { return static_cast<ssize_t>(m_num_cols); }

  ssize_t number_of_rows() const { return static_cast<ssize_t>(m_num_rows); }

  Data_T at(const Grid_Location &location) const {
    return grid.at(location.row).at(location.col);
  }

  Data_T &at_ref(const Grid_Location &location) const {
    return grid.at(location.row).at(location.col);
  }

  void set(const Grid_Location &location, const Data_T &data) {
    grid.at(location.row).at(location.col) = data;
  }

  void set_all(const Data_T &data) {
    for (auto &row : grid) {
      std::fill(row.begin(), row.end(), data);
    }
  }

  // todo row() col() to return vector of Data_T (see char specialization)

  /**
   * Call op with the location and value of each cell on the grid.
   *
   * @param op Callable with the signature (Grid_Location, value_type).
   *
   * @return The operation.
   */
  template <class Op> Op for_each_cell(Op op) {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location));
      }
    }
    return op;
  }

  /**
   * Call op with the location and value of each cell on the grid.
   *
   * @param op Callable with the signature (Grid_Location, value_type).
   *
   * @return The operation.
   */
  template <class Op> Op for_each_cell(Op op) const {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location));
      }
    }
    return op;
  }

  /**
   * Call op with the location and value of each cell on the grid. Additionally
   * a reference to the grid is given to the operation.
   *
   * @param op Callable with the signature (Grid_Location, value_type, Grid).
   *
   * @return The operation.
   */
  template <class Op> Op for_each_cell_with_grid(Op op) {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location), *this);
      }
    }
    return op;
  }

  /**
   * Call op with the location and value of each cell on the grid. Additionally
   * a reference to the grid is given to the operation.
   *
   * @param op Callable with the signature (Grid_Location, value_type, Grid).
   *
   * @return The operation.
   */
  template <class Op> Op for_each_cell_with_grid(Op op) const {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location), *this);
      }
    }
    return op;
  }

  /**
   * Write the contents of the grid to the output stream. This
   * is done in reverse row order. In this manner, the highest
   * indexed row is the first inserted line. This replicates
   * how it would be read in a file with a (0,0) in the
   * lower left corner.
   */
  template <typename OStream>
  friend OStream &operator<<(OStream &os,
                             const Fixed_Sized_Grid<Data_T> &grid) {
    for (auto row = grid.grid.rbegin(); row != grid.grid.rend(); ++row) {
      std::copy(row->begin(), row->end(),
                std::ostream_iterator<value_type>(os, ""));
      os << "\n";
    }

    return os;
  }

private:
  using Row = std::vector<Data_T>;
  using Grid = std::vector<Row>;

  ssize_t m_num_rows;
  ssize_t m_num_cols;

  Grid grid;
};

template <> class Fixed_Sized_Grid<char> {
public:
  typedef char value_type;

  Fixed_Sized_Grid(int num_rows, int num_cols,
                   value_type fill_val = value_type())
      : m_num_rows(num_rows), m_num_cols(num_cols) {
    grid.resize(num_rows);
    for (auto &row : grid) {
      row.resize(num_cols, fill_val);
    }
  }

  Fixed_Sized_Grid() : m_num_rows(0), m_num_cols(0) {}

  ssize_t number_of_cols() const { return static_cast<ssize_t>(m_num_cols); }

  ssize_t number_of_rows() const { return static_cast<ssize_t>(m_num_rows); }

  value_type at(const Grid_Location &location) const {
    return grid.at(location.row).at(location.col);
  }

  void set(const Grid_Location &location, const value_type &data) {
    grid.at(location.row).at(location.col) = data;
  }

  void set_all(const value_type &data) {
    for (auto &row : grid) {
      std::fill(row.begin(), row.end(), data);
    }
  }

  /**
   * Return the characters in the specified row from col 0 to col X.
   */
  std::string row(ssize_t num) const {
    return std::string(grid.at(num).begin(), grid.at(num).end());
  }

  /**
   * Return the characters in the column from row 0 to row X.
   */
  std::string col(ssize_t num) const {
    std::string col;
    for (const auto &row : grid) {
      col += row.at(num);
    }
    return col;
  }

  /**
   * Call op with the location and value of each cell on the grid.
   *
   * @param op Callable with the signature (Grid_Location, value_type).
   *
   * @return The operation.
   */
  template <class Op> Op for_each_cell(Op op) {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location));
      }
    }
    return op;
  }

  /**
   * Call op with the location and value of each cell on the grid.
   *
   * @param op Callable with the signature (Grid_Location, value_type).
   *
   * @return The operation.
   */
  template <class Op> Op for_each_cell(Op op) const {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location));
      }
    }
    return op;
  }

  /**
   * Call op with the location and value of each cell on the grid. Additionally
   * a reference to the grid is given to the operation.
   *
   * @param op Callable with the signature (Grid_Location, value_type, Grid).
   *
   * @return The operation.
   */
  template <class Op> Op for_each_cell_with_grid(Op op) {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location), *this);
      }
    }
    return op;
  }

  /**
   * Call op with the location and value of each cell on the grid. Additionally
   * a reference to the grid is given to the operation.
   *
   * @param op Callable with the signature (Grid_Location, value_type, Grid).
   *
   * @return The operation.
   */
  template <class Op> Op for_each_cell_with_grid(Op op) const {
    for (auto row = 0; row < number_of_rows(); ++row) {
      for (auto col = 0; col < number_of_cols(); ++col) {
        const Grid_Location location = {row, col};
        op(location, at(location), *this);
      }
    }
    return op;
  }

  /**
   * Write the contents of the grid to the output stream. This
   * is done in reverse row order. In this manner, the highest
   * indexed row is the first inserted line. This replicates
   * how it would be read in a file with a (0,0) in the
   * lower left corner.
   */
  // template <typename OStream>
  // friend OStream &operator<<(OStream &os,
  //                            const Fixed_Sized_Grid<value_type> &grid) {
  //   for (auto row = grid.grid.rbegin(); row != grid.grid.rend(); ++row) {
  //     std::copy(row->begin(), row->end(),
  //               std::ostream_iterator<value_type>(os, ""));
  //     os << "\n";
  //   }

  //   return os;
  // }

  // Assumed to be rows of same column lenght...throw???
  friend std::istream &operator>>(std::istream &instr, Fixed_Sized_Grid &grid) {
    std::string line;
    while (std::getline(instr, line)) {
      Row row(line.begin(), line.end());
      grid.grid.emplace_back(row);
    }

    grid.m_num_rows = grid.grid.size();
    grid.m_num_cols = grid.m_num_rows == 0 ? 0 : grid.grid.front().size();

    return instr;
  }

  friend struct fmt::formatter<Fixed_Sized_Grid<char>>;

private:
  using Row = std::vector<value_type>;
  using Grid = std::vector<Row>;

  ssize_t m_num_rows;
  ssize_t m_num_cols;

  Grid grid;
};

} // namespace aoc::grid

template <>
struct fmt::formatter<aoc::grid::Fixed_Sized_Grid<char>>
    : fmt::formatter<std::string_view> {

  bool print_headers;

  size_t countDigits(int number) const {
    if (number == 0)
      return 1; // Special case for 0

    size_t count = 0;
    while (number != 0) {
      number /= 10;
      ++count;
    }
    return count;
  }

  /// Parsing the format specification (not needed for this simple case)
  constexpr auto parse(format_parse_context &ctx) -> decltype(ctx.begin()) {
    auto it = ctx.begin();
    while (it != ctx.end() && *it != '}') {
      if (*it == 'h')
        print_headers = true;
      ++it;
    }
    return it;
  }

  // Formatting the aoc::grid::Fixed_Sized_Grid<char> instance
  template <typename FormatContext>
  auto format(const aoc::grid::Fixed_Sized_Grid<char> &obj,
              FormatContext &ctx) const -> decltype(ctx.out()) {
    // Use format_to to format the MyClass instance to the output iterator
    auto out = ctx.out();

    const size_t col_hdr_height = countDigits(obj.number_of_cols());
    const size_t row_hdr_width = countDigits(obj.number_of_rows());

    if (print_headers) {
      int last = -1;
      for (size_t col = col_hdr_height; col > 1; --col) {
        fmt::format_to(out, "{} ", std::string(row_hdr_width + 1, ' '));
        for (size_t cidx = 0; cidx < obj.number_of_cols(); ++cidx) {
          int cur = ((cidx >> (col - 1)) / 10);
          if (cur != last) {
            fmt::format_to(out, "{}", cur);
            last = cur;
          } else {
            fmt::format_to(out, " ");
          }
        }
        fmt::format_to(out, "\n");
      }
      fmt::format_to(out, "{} ", std::string(row_hdr_width + 1, ' '));
      for (size_t cidx = 0; cidx < obj.number_of_cols(); ++cidx) {
        fmt::format_to(out, "{}", cidx % 10);
      }
      fmt::format_to(out, "\n");
      fmt::format_to(out, "{} {}\n", std::string(row_hdr_width, ' '),
                     std::string(obj.number_of_cols() + 1, '-'));
    }

    for (auto row = obj.number_of_rows(); row > 0; --row) {
      int ridx = row - 1;
      if (print_headers) {
        fmt::format_to(out, "{:>{}}| ", ridx, row_hdr_width); // Row header
      }
      fmt::format_to(
          out, "{}\n",
          fmt::join(obj.grid[ridx].begin(), obj.grid[ridx].end(), ""));
    }
    return out;
  }
};

#endif // __GRID_H__