#ifndef __GRID_NAVIGATION_H__
#define __GRID_NAVIGATION_H__

#include <string>

#include "spdlog/spdlog.h"

#include "grid_location.h"

namespace aoc::grid {

enum class Orientation4 { UP, RIGHT, DOWN, LEFT };

Orientation4 rotate_cw(Orientation4 orientation);
Orientation4 rotate_ccw(Orientation4 orientation);

Grid_Location ahead(Orientation4 orientation, const Grid_Location &location);
Grid_Location behind(Orientation4 orientation, const Grid_Location &location);

std::string as_string(Orientation4 orient);

} // namespace aoc::grid

template <> struct fmt::formatter<aoc::grid::Orientation4> {

  constexpr auto parse(format_parse_context &ctx)
      -> format_parse_context::iterator {
    return ctx.begin();
  }

  auto format(const aoc::grid::Orientation4 &orient, format_context &ctx) const
      -> format_context::iterator {
    return fmt::format_to(ctx.out(), "{}", aoc::grid::as_string(orient));
  }
};

#endif /* __GRID_NAVIGATION_H__ */