#ifndef __GRID_FLOOD_FILL_H__
#define __GRID_FLOOD_FILL_H__

#include <bitset>
#include <limits>
#include <queue>

#include "aoc/grid/grid_utils.h"

namespace aoc::grid {

/**
 * This method implements a flood fill algorithm. The flood fill starts at an
 * origination location in the grid. If the value is equal to the open value
 * it is marked with the flood value. Then the surrounding cells are used
 * to repeat the process until there are not more open cells.
 *
 * The number of cells flooded is returned.
 */
template <class Grid_T>
int grid_flood_fill(Grid_T &grid, const Grid_Location &location,
                    typename Grid_T::value_type open_value,
                    typename Grid_T::value_type flood_value) {
  if (!is_on_grid(location, grid))
    return 0;
  int flood_count = 0;
  std::queue<Grid_Location> location_q;
  location_q.push(location);

  while (!location_q.empty()) {
    const Grid_Location current = location_q.front();
    location_q.pop();
    if (is_on_grid(current, grid)) {
      if (grid.at(current) == open_value) {
        ++flood_count;
        grid.set(current, flood_value);
        location_q.push(Grid_Location::up(current));
        location_q.push(Grid_Location::right(current));
        location_q.push(Grid_Location::down(current));
        location_q.push(Grid_Location::left(current));
      }
    }
  }
  return flood_count;
}

/**
 * This method implements a flood fill algorithm. The flood fill starts at an
 * origination location in the grid. If the value is not in the collection
 * of occupied cell it is marked with the flood value. Then the surrounding
 * cells are used to repeat the process until there are not more open cells.
 *
 * @tparam Occupied_Iter_T Iterator to collection of values inidicating
 * a cell is occupied and cannot be flooded.
 *
 * The number of cells flooded is returned.
 */
// template <class Grid_T, class Occupied_Iter_T>
// int grid_flood_fill(Grid_T& grid, const Grid_Location& location,
//                     const Occupied_Iter_T occupied_values_start,
//                     const Occupied_Iter_T occupied_values_end,
//                     typename Grid_T::value_type flood_value) {
//   if (!is_on_grid(location, grid)) return 0;

//   const auto cell_value = grid.at(location);
//   if (cell_value == flood_value ||
//       std::find(occupied_values_start, occupied_values_end, cell_value) !=
//           occupied_values_end) {
//     return 0;
//   }

//   grid.set(location, flood_value);

//   const auto up = Grid_Location::up(location);
//   const auto right = Grid_Location::right(location);
//   const auto down = Grid_Location::down(location);
//   const auto left = Grid_Location::left(location);
//   return 1 +
//          grid_flood_fill(grid, up, occupied_values_start,
//          occupied_values_end,
//                          flood_value) +
//          grid_flood_fill(grid, right, occupied_values_start,
//                          occupied_values_end, flood_value) +
//          grid_flood_fill(grid, down, occupied_values_start,
//          occupied_values_end,
//                          flood_value) +
//          grid_flood_fill(grid, left, occupied_values_start,
//          occupied_values_end,
//                          flood_value);
// }

// TODO: restrict to CHAR grid types?

/**
 * This structure implements a flood-fill algorithm
 * which can specify a set of values which block
 * / cannot be flooded. Any other value is
 * flooded.
 */
template <class Grid_T> class Grid_Flood_Filler {
public:
  using Value_Type = typename Grid_T::value_type;

  Grid_Flood_Filler<Grid_T> &unfloodable(Value_Type value) {
    _unfloodable_values.set(value);
    return *this;
  }

  int do_flood_fill(Grid_T &grid, const Grid_Location &location,
                    Value_Type flood_value) {
    if (!is_on_grid(location, grid))
      return 0;

    const auto cell_value = grid.at(location);
    if (_unfloodable_values.test(cell_value))
      return 0;

    grid.set(location, flood_value);
    SPDLOG_DEBUG("Flooded cell: {}  value now: {}", location,
                 grid.at(location));

    const auto up = Grid_Location::up(location);
    const auto right = Grid_Location::right(location);
    const auto down = Grid_Location::down(location);
    const auto left = Grid_Location::left(location);

    return 1 + do_flood_fill(grid, up, flood_value) +
           do_flood_fill(grid, right, flood_value) +
           do_flood_fill(grid, down, flood_value) +
           do_flood_fill(grid, left, flood_value);
  }

private:
  static constexpr ssize_t _lookup_size() {
    return 1 + std::numeric_limits<Value_Type>::max();
  }

  using Lookup_Type = std::bitset<_lookup_size()>;

  Lookup_Type _unfloodable_values;
};

} // namespace aoc::grid

#endif /* __GRID_FLOOD_FILL_H__ */