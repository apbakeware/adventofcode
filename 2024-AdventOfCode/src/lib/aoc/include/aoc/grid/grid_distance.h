#ifndef __GRID_DISTANCE_H__
#define __GRID_DISTANCE_H__

#include "spdlog/spdlog.h"

#include "aoc/grid/grid.h"
#include "aoc/grid/grid_utils.h"

namespace aoc::grid {

/**
 * Compute the manhatten distance between two cell locations. Otherwise
 * known as the "city block" distance, this measures the distance
 * of the two legs of a triangle formted between a location, not
 * the hypotenuese.
 *
 * https://en.wikipedia.org/wiki/Taxicab_geometry
 *
 * @param orig Destination location.
 * @param dest Destination location
 *
 * @return The manhatten distance between locations.
 */
inline int manhattan_distance(const Grid_Location &orig,
                              const Grid_Location &dest) {
  return std::abs(dest.row - orig.row) + std::abs(dest.col - orig.col);
}

/**
 * Search the grid from the proved location looking for
 * a cell where the predicate is true. The movement
 * in the grid is provided as a function. The original
 * cell location is not tested for the value.
 *
 * @tparam Predicate_T callable taking Grid_T::value_type.
 *
 * @return Number of movement operations from the starting location to the
 * case where predicate is true. -1 if the predicate is not
 * true before the movement function runs off the grid.
 */
template <class Grid_T, class Predicate_T>
int distance_to(Grid_T &grid, Grid_Location location,
                std::function<Grid_Location(const Grid_Location &)> movement,
                Predicate_T stop_pred) {
  if (!is_on_grid(location, grid))
    return -1;

  int distance = 0;
  bool found = false;

  while (!found) {
    location = movement(location);

    if (!is_on_grid(location, grid)) {
      found = true;
      distance = -1;
    } else {
      ++distance;
      found = stop_pred(grid.at(location));
    }
  }

  return distance;
}

/**
 * Search 'up' the grid for a value satisfying the predicate.
 *
 * @see distance_to()
 */
template <class Grid_T, class Predicate_T>
int distance_up_to(Grid_T &grid, const Grid_Location &location,
                   Predicate_T stop_pred) {
  auto movement = [](const auto &loc) -> Grid_Location {
    return Grid_Location::up(loc);
  };

  return distance_to(grid, location, movement, stop_pred);
}

/**
 * Search 'right' the grid for a value satisfying the predicate.
 *
 * @see distance_to()
 */
template <class Grid_T, class Predicate_T>
int distance_right_to(Grid_T &grid, const Grid_Location &location,
                      Predicate_T stop_pred) {
  auto movement = [](const auto &loc) -> Grid_Location {
    return Grid_Location::right(loc);
  };

  return distance_to(grid, location, movement, stop_pred);
}

/**
 * Search 'doen' the grid for a value satisfying the predicate.
 *
 * @see distance_to()
 */
template <class Grid_T, class Predicate_T>
int distance_down_to(Grid_T &grid, const Grid_Location &location,
                     Predicate_T stop_pred) {
  auto movement = [](const auto &loc) -> Grid_Location {
    return Grid_Location::down(loc);
  };

  return distance_to(grid, location, movement, stop_pred);
}

/**
 * Search 'left' the grid for a value satisfying the predicate.
 *
 * @see distance_to()
 */
template <class Grid_T, class Predicate_T>
int distance_left_to(Grid_T &grid, const Grid_Location &location,
                     Predicate_T stop_pred) {
  auto movement = [](const auto &loc) -> Grid_Location {
    return Grid_Location::left(loc);
  };

  return distance_to(grid, location, movement, stop_pred);
}

} // namespace aoc::grid

#endif /* __GRID_DISTANCE_H__ */