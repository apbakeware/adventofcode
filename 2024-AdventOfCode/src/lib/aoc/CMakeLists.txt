project(aoc)

set(SOURCE_FILES 
  src/graph/graph.cc
  src/grid/grid_direction.cc
  src/grid/grid_location.cc
  src/grid/grid_navigation.cc
  src/util/int_builder.cc
  src/util/null_type.cc
  src/util/string_utils.cc
)

add_library(${PROJECT_NAME}	${SOURCE_FILES} )

add_library(libs::${PROJECT_NAME} ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
	PUBLIC
		$<INSTALL_INTERFACE:include>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
	PRIVATE
		# ${CMAKE_CURRENT_SOURCE_DIR}
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/${PROJECT_NAME}>
)


target_link_libraries(${PROJECT_NAME} spdlog)

##################
# Testing
if(BUILD_TESTS)
	add_subdirectory(test)
endif()
