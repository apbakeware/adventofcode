project(libaoc-test)

set(TEST_SOURCE_FILES 
  libaoc_test_main.cc
  graph/named_route_graph_test.cc
  grid/grid_coordinate_system_test.cc
  grid/grid_distance_test.cc
  grid/grid_flood_fill_test.cc
  grid/grid_navigation_test.cc
  grid/grid_traversal_test.cc
  util/apply_adjacent_operation_test.cc
  util/as_string_test.cc
  util/observable_test.cc
  util/string_utils_test.cc
)

add_executable(${PROJECT_NAME} ${TEST_SOURCE_FILES}) 
target_link_libraries(${PROJECT_NAME}
  PRIVATE 
  spdlog::spdlog
  Catch2::Catch2
  aoc
)

add_test(NAME ${PROJECT_NAME} COMMAND ${PROJECT_NAME})
  
