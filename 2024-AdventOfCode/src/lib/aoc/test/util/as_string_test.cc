#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <functional>
#include <iostream>

#include "spdlog/spdlog.h"

#include "aoc/util/as_string.hpp"

namespace {

struct StrDefined {
  std::string str() const { return "StrDefined"; }
};

struct OutstreamDefined {};

std::ostream &operator<<(std::ostream &ostr, const OutstreamDefined &obj) {
  (void)(obj);
  ostr << "OutstreamDefined";
  return ostr;
}

} // namespace

namespace aoc::util::test {

TEST_CASE("Test as_string()") {

  SECTION("Test check for operator<<") {
    CHECK_FALSE(aoc::util::has_ostream_operator<StrDefined>::value);
    CHECK(aoc::util::has_ostream_operator<OutstreamDefined>::value);
  }

  SECTION("Test plain data type") {
    const int sut = 1930;
    REQUIRE(as_string(sut) == "1930");
  }

  SECTION("Test ostream operator defined") {
    OutstreamDefined sut;
    REQUIRE(as_string(sut) == "OutstreamDefined");
  }

  SECTION("Test str() method defined") {
    StrDefined sut;
    REQUIRE(as_string(sut) == "StrDefined");
  }
}
} // namespace aoc::util::test