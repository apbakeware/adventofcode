#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <functional>
#include <iostream>

#include "spdlog/spdlog.h"

#include "aoc/util/apply_adjacent_operation.h"
#include "aoc/util/string_utils.hpp"

namespace aoc::util::test {

TEST_CASE("Test String_Utils.find_fold_reflection_points", "[String_Utils]") {
  SECTION("test day13 sample 1 row 1") {
    const std::string SUT = "#.##..##.";
    const std::vector<ssize_t> EXP{5, 7};

    const auto &actual = find_fold_reflection_points(SUT);

    REQUIRE(EXP == actual);
  }

  SECTION("test day13 sample 1 column 1") {
    // Top of grid is row 1
    const std::string SUT = "##.##.#";
    const std::vector<ssize_t> EXP{1, 4};

    const auto &actual = find_fold_reflection_points(SUT);

    REQUIRE(EXP == actual);
  }

  SECTION("test string '..##..#'") {
    const std::string SUT = "..##..#";
    const std::vector<ssize_t> EXP{1, 3, 5};

    const auto &actual = find_fold_reflection_points(SUT);

    REQUIRE(EXP == actual);
  }
}

} // namespace aoc::util::test