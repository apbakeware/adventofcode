#include <catch2/catch_test_macros.hpp>

#include <algorithm>
#include <chrono>

#include "aoc/util/observable.h"

namespace aoc::util::test {

struct TestObserverInterface {

  unsigned int onNotifyCalls = 0;

  void onNotify() { ++onNotifyCalls; }
};

class TestObservable : public Observable<TestObserverInterface> {
public:
  void doNotifications() {
    auto notify = [](TestObserverInterface &inf) { inf.onNotify(); };

    notifyObservers(notify);
  }
};

TEST_CASE("Observable_Test") {

  TestObservable sut;

  SECTION("Test registered observer is notified") {
    TestObserverInterface observer;

    sut.registerObserver(&observer);
    sut.doNotifications();

    REQUIRE(observer.onNotifyCalls == 1);
  }

  SECTION("Test registered observer multiple notifications") {
    TestObserverInterface observer;

    sut.registerObserver(&observer);
    sut.doNotifications();
    sut.doNotifications();
    sut.doNotifications();
    sut.doNotifications();

    REQUIRE(observer.onNotifyCalls == 4);
  }

  SECTION("Test duplicate registration") {
    TestObserverInterface observer;

    sut.registerObserver(&observer);
    sut.registerObserver(&observer);
    sut.doNotifications();

    REQUIRE(observer.onNotifyCalls == 2);
  }

  SECTION("Test unregistered observer is notified") {
    TestObserverInterface observer;

    sut.registerObserver(&observer);
    sut.doNotifications();
    sut.unregisterObserver(&observer);
    sut.doNotifications();

    REQUIRE(observer.onNotifyCalls == 1);
  }

  SECTION("Test null observer registered") {
    TestObserverInterface *null_observer = nullptr;

    sut.registerObserver(null_observer);

    // call and make sure there are no issues.
    sut.doNotifications();
  }
}

} // namespace aoc::util::test