#include <iostream>
#include <iterator>
#include <string>
#include <vector>

#include <cxxopts/cxxopts.hpp>

#include "spdlog/sinks/basic_file_sink.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"

#include "aoc/util/i_command.h"

#include "aocsolver/pipeline.h"
#include "aocsolver/pipeline_observers/expected_solution_pipeline_observer.h"
#include "aocsolver/pipeline_observers/stdout_logging_pipeline_observer.h"
#include "aocsolver/pipeline_observers/timestamp_tracking_pipeline_observer.h"
#include "aocsolver/results_table.h"
#include "aocsolver/timing.h"

#include "season2024/season2024_solvers.h"

using namespace aoc::solver;

namespace {

bool _initialize_logger() {
  auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
  console_sink->set_level(spdlog::level::warn);

  auto file_sink =
      std::make_shared<spdlog::sinks::basic_file_sink_mt>("logs/aoc.log", true);
  file_sink->set_level(spdlog::level::trace);

  std::vector<spdlog::sink_ptr> sinks{console_sink, file_sink};

  auto logger =
      std::make_shared<spdlog::logger>("aoc", sinks.begin(), sinks.end());
  logger->set_level(spdlog::level::trace);
  logger->set_pattern("[%H:%M:%S.%e] [%n:%-8l] %v  [%s:%#]");
  spdlog::set_default_logger(logger);
  return true;
}

const bool logger_was_initialized = _initialize_logger();

} // namespace

template <typename Solver_Type_List, typename Name_Iter>
static std::vector<aoc::util::ICommand *>
buildSolverCommands(Pipeline &pipeline, Name_Iter begin, Name_Iter end) {
  SPDLOG_INFO("Building command for named solvers");
  std::vector<aoc::util::ICommand *> commands;
  for (auto iter = begin; iter != end; ++iter) {
    SPDLOG_DEBUG("Looking for command line solver: {}", *iter);

    if (Solver_Type_List::hasSolver(*iter)) {
      commands.push_back(
          Solver_Type_List::executorCommandBuilder(*iter, pipeline));
    } else {
      SPDLOG_ERROR("Unable to find solver: '{}', skipping", *iter);
    }
  }

  return commands;
}

int main(int argc, char **argv) {

  cxxopts::Options options("Advent of Code", "Santa Saver's Unite!");

  // clang-format off
  options.add_options()
    ("h,help", "Display help and exit", cxxopts::value<bool>())
    ("l,list", "List the solvers and exit", cxxopts::value<bool>())
    ("s,solvers", "Run the named solvers. Use '-l' to get list.",cxxopts::value<std::vector<std::string>>())
    ("m,mode", "Execution mode of the solver [solve, sample, regression]",cxxopts::value<std::string>()->default_value("solve"))
  ;
  // clang-format on

  int exit_code = 1;

  // TODO: Dynamicall select a season?
  using Solvers = aoc::season2024::SeasonSolvers;
  using ActualSolvers = typename Solvers::Actual;
  using SampleSolvers = typename Solvers::Sample;

  try {
    auto opts = options.parse(argc, argv);

    // Do the "query and exit" commands first, then any solving
    if (opts.count("help")) {
      std::cout << options.help({"", "Group"}) << std::endl;
      exit_code = 0;

    } else if (opts.count("list")) {
      std::vector<std::string> solver_names;
      ActualSolvers::getSolverNames(solver_names);
      std::cout << "Solver Names:\n";
      std::copy(solver_names.begin(), solver_names.end(),
                std::ostream_iterator<std::string>(std::cout));
      std::cout << std::endl;
      exit_code = 0;

    } else {

      // TODO: results json observer?
      // StdOutLoggingPipelineObserver stdout_observer;
      TimestampTrackingPipelineObserver timestamp_observer;
      ExpectedSolutionPipelineObserver expected_observer;
      Pipeline pipeline;

      // Need to delete when done
      std::vector<aoc::util::ICommand *> commands;

      auto registerExpectedOp = std::bind(
          &ExpectedSolutionPipelineObserver::addExpectedResult,
          &expected_observer, std::placeholders::_1, std::placeholders::_2);

      const bool do_sample_solvers = opts["mode"].as<std::string>() == "sample";
      const bool do_regression = opts["mode"].as<std::string>() == "regression";
      bool render_expected_actual = false;

      std::vector<std::string> solvers_to_run;

      if (opts.count("solvers")) {
        solvers_to_run = opts["solvers"].as<std::vector<std::string>>();
      }

      if (do_sample_solvers) {
        if (solvers_to_run.empty()) {
          SPDLOG_INFO("Running all sample solvers");
          SampleSolvers::addExecutorCommand(commands, pipeline);
        } else {
          SPDLOG_INFO("Running named subset sample solvers");
          commands = buildSolverCommands<SampleSolvers>(
              pipeline, solvers_to_run.begin(), solvers_to_run.end());
        }

        SPDLOG_INFO("Adding sample expectations");
        SampleSolvers::registerExpectedResults(registerExpectedOp);
        render_expected_actual = true;

      } else {
        if (solvers_to_run.empty()) {
          ActualSolvers::addExecutorCommand(commands, pipeline);
        } else {
          SPDLOG_INFO("Running named subset solvers");
          commands = buildSolverCommands<ActualSolvers>(
              pipeline, solvers_to_run.begin(), solvers_to_run.end());
        }

        if (do_regression) {
          SPDLOG_INFO("Adding regression expectations");
          ActualSolvers::registerExpectedResults(registerExpectedOp);
          render_expected_actual = true;
        }
      }

      // pipeline.registerObserver(&stdout_observer);
      pipeline.registerObserver(&timestamp_observer);
      pipeline.registerObserver(&expected_observer);

      for (auto cmd : commands) {
        cmd->execute();
        delete cmd;
      }
      commands.clear();

      if (solvers_to_run.empty()) {
        renderResultsTable(timestamp_observer);
      } else {
        renderResultsTable(timestamp_observer, solvers_to_run);
      }

      if (render_expected_actual) {
        if (solvers_to_run.empty()) {
          renderResultsTable(expected_observer);
        } else {
          renderResultsTable(expected_observer, solvers_to_run);
        }
      }

      exit_code = 0;
    }

  } catch (std::exception &e) {
    SPDLOG_CRITICAL("Caught generic exection at the top level: {}", e.what());
    exit_code = 2;
  } catch (...) {
    SPDLOG_CRITICAL("Caught '...' at the top level");
    exit_code = 2;
  }

  return exit_code;
}
