#!/bin/bash

# NOTE: Run within the seasonXXXX directory

SEASON="season2024"

INC="src/lib/${SEASON}/include/${SEASON}/days"
SRC="src/lib/${SEASON}/src/days"


if [ ! -d ${INC} ] ; then
  mkdir ${INC}
fi

if [ ! -d ${SRC} ] ; then
  mkdir ${SRC}
fi

CLASSES="Day01Part1 Day01Part2 Day02Part1 Day02Part2 Day03Part1 Day03Part2 Day04Part1 Day04Part2 Day05Part1 Day05Part2 Day06Part1 Day06Part2 Day07Part1 Day07Part2 Day08Part1 Day08Part2 Day09Part1 Day09Part2 Day10Part1 Day10Part2 Day11Part1 Day11Part2 Day12Part1 Day12Part2 Day13Part1 Day13Part2 Day14Part1 Day14Part2 Day15Part1 Day15Part2 Day16Part1 Day16Part2 Day17Part1 Day17Part2 Day18Part1 Day18Part2 Day19Part1 Day19Part2 Day20Part1 Day20Part2 Day21Part1 Day21Part2 Day22Part1 Day22Part2 Day23Part1 Day23Part2 Day24Part1 Day24Part2 Day25Part1 Day25Part2"

for CLASSNAME in $CLASSES; do

UCLASSNAME="${CLASSNAME^^}"
FNAME_H="${INC}/${CLASSNAME,,}.h"
FNAME_CC="${SRC}/${CLASSNAME,,}.cc"

echo "Writing: ${FNAME_H}"
cat << EOF > ${FNAME_H}
#ifndef __${UCLASSNAME}_H__
#define __${UCLASSNAME}_H__

#include "aoc/util/null_type.h"
#include "aocsolver/solver.h"

namespace aoc::${SEASON} {

class ${CLASSNAME} 
  : public aoc::solver::Solver<${CLASSNAME}, aoc::util::NullType, aoc::util::NullType> {
public:

  ResultType solve(InputType & input);
};

} // namespace aoc::${SEASON}

#endif /* __${UCLASSNAME}_H__ */
EOF

echo "Writing: ${FNAME_CC}"
cat << EOF > ${FNAME_CC}
#include "spdlog/spdlog.h"

#include "${SEASON}/days/${CLASSNAME,,}.h"

namespace aoc::${SEASON} {

${CLASSNAME}::ResultType ${CLASSNAME}::solve(InputType & input) {
  SPDLOG_TRACE("${CLASSNAME}::solve()");
  (void)(input);
  return ResultType{};
}

} // namespace aoc::${SEASON}

EOF



done




