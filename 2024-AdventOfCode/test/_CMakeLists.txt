# set(DOCTEST_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/lib/doctest/ CACHE INTERNAL "Path to include folder for doctest")
# add_library(doctest INTERFACE)
# target_include_directories(doctest INTERFACE ${DOCTEST_INCLUDE_DIR})
# add_executable(aoc-test aoc_test.cc)
# target_link_libraries(aoc-test doctest)

add_executable(aoc_tests 
  aoc_test.cc
  utils/apply_adjacent_operation_test.cc
  utils/string_utils_test.cc
  utils/graph/named_route_graph_test.cc
  utils/grid/grid_coordinate_system_test.cc
  utils/grid/grid_distance_test.cc
  utils/grid/grid_flood_fill_test.cc
  utils/grid/grid_traversal_test.cc
)


include_directories(aoc_tests ${PROJECT_SOURCE_DIR}/src)
target_link_libraries(aoc_tests PRIVATE spdlog::spdlog)
target_link_libraries(aoc_tests PRIVATE aoc_days)
#target_link_libraries(aoc_tests PRIVATE Catch2::Catch2WithMain)
target_link_libraries(aoc_tests PRIVATE Catch2::Catch2)

get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
foreach(dir ${dirs})
  message(STATUS "dir='${dir}'")
endforeach()