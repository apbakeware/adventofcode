#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <memory>
#include <sstream>
#include "../Solutions/DataLoader.h"
#include "../Solutions/ISolution.h"
#include "../Solutions/ProblemFactory.h"

struct Problem_To_Solve {
   const std::string problem_name;
   const std::string data_file_name;
};

struct Solved_Problem {

   Solved_Problem() : duration(0) {

   }

   std::string name;
   std::chrono::microseconds duration;
   std::shared_ptr<ISolution> solution;

   static void write_header(std::ostream& str) {
      str << "\n\n"
         << " Duration (ms)  | Name                  | Result\n"
         << "----------------+-----------------------+--------------------\n";
   }

   friend std::ostream& operator<<(std::ostream& outs, const Solved_Problem& obj) {
      outs
         << std::setprecision(3) << std::fixed << std::setw(15) << std::setfill(' ')
         << obj.duration.count() * 0.001 << "  "
         << std::setw(20) << std::setfill(' ') << std::left << obj.name << "   ";
      obj.solution->write_result(outs);
      outs << std::resetiosflags(std::ios::adjustfield);
      return outs;
   }
};

const std::vector<Problem_To_Solve> PROBLEMS_TO_SOLVE = {
   /*{ "Day1Prob1", "day01.txt"},
   { "Day1Prob2", "day01.txt"},
   { "Day2Prob1", "day02.txt"},
   { "Day2Prob2", "day02.txt"},
   { "Day3Prob1", "day03.txt"},
   { "Day3Prob2", "day03.txt"},
   { "Day4Prob1", "day04.txt"},
   { "Day4Prob2", "day04.txt"},
   { "Day5Prob1", "day05.txt"},
   { "Day5Prob2", "day05.txt"},
   { "Day6Prob1", "day06.txt"},
   { "Day6Prob2", "day06.txt"},
   { "Day7Prob1", "day07.txt"},
   { "Day7Prob2", "day07.txt"},
   { "Day8Prob1", "day08.txt"},
   { "Day8Prob2", "day08.txt"},
   { "Day9Prob1", "day09.txt"},
   { "Day9Prob2", "day09.txt"},
   { "Day10Prob1", "day10.txt"},
   { "Day10Prob2", "day10.txt"},
   { "Day11Prob1", "day11.txt"},
   { "Day11Prob2", "day11.txt"},
   { "Day12Prob1", "day12.txt"},
   { "Day12Prob2", "day12.txt"},
   { "Day13Prob1", "day13.txt"},
   { "Day13Prob2", "day13.txt"},
   { "Day14Prob1", "day14.txt"},
   { "Day14Prob2", "day14.txt"},
   { "Day15Prob1", "day15.txt"},
   { "Day15Prob2", "day15.txt"},*/
   { "Day16Prob1", "day16.txt"},
   { "Day16Prob2", "day16.txt"}
};

std::vector<Solved_Problem> solved_problems;

void solve_problem(const std::string& name, std::istream& input_stream) {

   std::unique_ptr<IProblem> problem(ProblemFactory::factory().build(name));
   if(problem) {
      std::cout << "Finding solution for " << problem->name() << "\n";

      Solved_Problem solved;
      solved.name = problem->name();
      auto start_t = std::chrono::high_resolution_clock::now();
      solved.solution.reset(problem->execute(input_stream));
      auto end_t = std::chrono::high_resolution_clock::now();
      solved.duration = std::chrono::duration_cast<std::chrono::microseconds>
         (end_t - start_t);

      solved_problems.push_back(solved);
   } else {
      std::cout << "Failed to allocate problem" << std::endl;
   }
}

int main() {

   for(auto const& problem : PROBLEMS_TO_SOLVE) {
      std::ifstream input_data_stream(problem.data_file_name);
      if(input_data_stream.is_open()) {
         solve_problem(problem.problem_name, input_data_stream);
      } else {
         std::cout << "Failed to open data file '" << problem.data_file_name
            << "', can't solve problem '" << problem.data_file_name << "'"
            << "\n";
      }
   }

   Solved_Problem::write_header(std::cout);
   std::ostream_iterator<Solved_Problem> out_it(std::cout, "\n");
   std::copy(solved_problems.begin(), solved_problems.end(), out_it);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
