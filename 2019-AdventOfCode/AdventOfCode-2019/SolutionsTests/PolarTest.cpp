#include "gtest/gtest.h"
#include "../Solutions/Polar.h"
#include "../Solutions/Cartesian.h"

// 2 | 1
// - + -
// 3 | 4

TEST(PolarTest, ConvertFromCartesianQuad_01) {
   const cartesian::Point2 base = {0,0};
   const cartesian::Point2 target = {0,1};

   const polar::Point2 sut = polar::convert_from(base, target);
   ASSERT_NEAR(sut.magnitude, 1.0, 0.001);
   ASSERT_NEAR(sut.angle, 90.0, 0.001);
}

TEST(PolarTest, ConvertFromCartesianQuad_10) {
   const cartesian::Point2 base = {0,0};
   const cartesian::Point2 target = {1,0};

   const polar::Point2 sut = polar::convert_from(base, target);
   ASSERT_NEAR(sut.magnitude, 1.0, 0.001);
   ASSERT_NEAR(sut.angle, 0.0, 0.001);
}

TEST(PolarTest, ConvertFromCartesianQuad_0_1) {
   const cartesian::Point2 base = {0,0};
   const cartesian::Point2 target = {0,-1};

   const polar::Point2 sut = polar::convert_from(base, target);
   ASSERT_NEAR(sut.magnitude, 1.0, 0.001);
   ASSERT_NEAR(sut.angle, 270.0, 0.001);
}

TEST(PolarTest, ConvertFromCartesianQuad__10) {
   const cartesian::Point2 base = {0,0};
   const cartesian::Point2 target = {-1,0};

   const polar::Point2 sut = polar::convert_from(base, target);
   ASSERT_NEAR(sut.magnitude, 1.0, 0.001);
   ASSERT_NEAR(sut.angle, 180.0, 0.001);
}

TEST(PolarTest, TestRotate) {
   const polar::Point2 sut = {1.4, 90.0};
   const polar::Point2 rotated = polar::rotate(sut, -90.0);

   ASSERT_DOUBLE_EQ(rotated.angle, 0.0);
}