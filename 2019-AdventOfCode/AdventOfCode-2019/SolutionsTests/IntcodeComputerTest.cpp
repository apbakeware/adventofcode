#include "gtest/gtest.h"
#include "../Solutions/IntcodeComputer.h"
#include "../Solutions/StringBuilderIntecodeComputerOutput.h"
#include "../Solutions/IntcodeComputerInputsList.h"

//TEST(OpcodeTest, CreateOpcodeParameterAsPosition) {
//   IntcodeProgram program = {
//      1002, // p2 : immediate   p1 : position  Inst: Multiply
//      4, // [4]
//      77,
//      0,
//      5 // expected as position 1 argument
//   };
//
//   Opcode sut(program.front(), program);
//
//   EXPECT_EQ(sut.param1(4), program[4]);
//}
//
//TEST(OpcodeTest, CreateOpcodeParameterAsImmediate) {
//   IntcodeProgram program = {
//      1002, // p2 : immediate   p1 : position  Inst: Multiply
//      4, // [4]
//      77,
//      0,
//      0,
//      5 // expected as position 1 argument
//   };
//
//   Opcode sut(program.front(), program);
//
//   EXPECT_EQ(sut.param2(73), 73);
//}
//
//TEST(OpcodeTest, CreateAddInstruction) {
//   IntcodeProgram program = {1001};
//
//   Opcode sut(program.front(), program);
//
//   EXPECT_EQ(sut.get_instruction(), Instruction::ADD);
//   EXPECT_TRUE(bool(sut));
//}
//
//TEST(OpcodeTest, CreateMultiplyInstruction) {
//   IntcodeProgram program = {1002};
//
//   Opcode sut(program.front(), program);
//
//   EXPECT_EQ(sut.get_instruction(), Instruction::MULTIPLY);
//   EXPECT_TRUE(bool(sut));
//}
//
//TEST(OpcodeTest, CreateInputInstruction) {
//   IntcodeProgram program = {1003};
//
//   Opcode sut(program.front(), program);
//
//   EXPECT_EQ(sut.get_instruction(), Instruction::INPUT);
//   EXPECT_TRUE(bool(sut));
//}
//
//TEST(OpcodeTest, CreateOutputInstruction) {
//   IntcodeProgram program = {1004};
//
//   Opcode sut(program.front(), program);
//
//   EXPECT_EQ(sut.get_instruction(), Instruction::OUTPUT);
//   EXPECT_TRUE(bool(sut));
//}
//
//TEST(OpcodeTest, CreateTerminateInstruction) {
//   IntcodeProgram program = {99};
//
//   Opcode sut(program.front(), program);
//
//   EXPECT_EQ(sut.get_instruction(), Instruction::TERMINATE);
//   EXPECT_FALSE(bool(sut));
//}

/*TEST(IntComputerTest, Day5P2SampleProgramInputLT8) {
   const std::string test_data = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99";
   std::istringstream str(test_data);
   std::vector<int> program;
   std::string token;
   while(std::getline(str, token, ',')) {
      program.push_back(atoi(token.c_str()));
   }

   IntcodeComputer sut(program);
   ASSERT_EQ(sut.execute(), 999);
}*/

TEST(IntcodeComputerTest, Day09TestCase1) {
   const std::string program_str = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99";
   std::istringstream str(program_str);
   std::vector<IntcodeType> program(110,0);
   auto piter = program.begin();
   std::string token;
   while(std::getline(str, token, ',')) {
      *piter = atoi(token.c_str());
      ++piter;
   }

   StringBuilderIntecodeComputerOutput output_builder;
   IntcodeComputerInputsList null_input({});
   IntcodeComputer sut(program, null_input, output_builder);
   
   sut.execute();
   
   ASSERT_EQ(program_str + ",", output_builder.get_output());
}

TEST(IntcodeComputerTest, Day09TestCase2) {
   const std::string program_str = "1102,34915192,34915192,7,4,7,99,0";
   std::istringstream str(program_str);
   std::vector<IntcodeType> program(100000, 0);
   auto piter = program.begin();
   std::string token;
   while(std::getline(str, token, ',')) {
      *piter = atoi(token.c_str());
      ++piter;
   }

   StringBuilderIntecodeComputerOutput output_builder;
   IntcodeComputerInputsList null_input({});
   IntcodeComputer sut(program, null_input, output_builder);

   sut.execute();
   auto output = output_builder.get_output();
   output.pop_back();

   ASSERT_EQ(16, output.size());
}

TEST(IntcodeComputerTest, Day09TestCase3) {
   const std::string program_str = "104,1125899906842624,99";
   std::istringstream str(program_str);
   std::vector<IntcodeType> program(10, 0);
   auto piter = program.begin();
   std::string token;
   while(std::getline(str, token, ',')) {
      std::istringstream istr(token);
      istr >> *piter;
      ++piter;
   }

   StringBuilderIntecodeComputerOutput output_builder;
   IntcodeComputerInputsList null_input({});
   IntcodeComputer sut(program, null_input, output_builder);
   sut.execute();
   
   ASSERT_EQ("1125899906842624,", output_builder.get_output());
}
