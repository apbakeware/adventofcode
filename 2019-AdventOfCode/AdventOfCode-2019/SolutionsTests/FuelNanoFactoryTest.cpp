#include <vector>
#include <string>
#include "gtest/gtest.h"
#include "../Solutions/FuelNanoFactory.h"
#include "../Solutions/ReactionEquation.h"



TEST(FuelNanoFactoryTest, SingleCompoundSingleIngredientNumberOfCompoundsTest) {

   const std::string INPUT = "9 ORE => 2 A";
   FuelNanoFactory sut;
   sut.extract_reaction_question(INPUT);

   ASSERT_EQ(2, sut.get_number_of_compounds());
}

TEST(FuelNanoFactoryTest, SingleCompoundSingleIngredientTestProduce) {

   const std::string INPUT = "9 ORE => 2 A";
   FuelNanoFactory sut;
   sut.extract_reaction_question(INPUT);

   auto compound = sut.get_compound("A");
   auto ore = sut.get_compound("ORE");
   compound->produce_to_satisfy(3);
   
   ASSERT_EQ(18, ore->get_total_units_produced());
}

TEST(FuelNanoFactoryTest, SingleCompoundDualIngredientNumberOfCompoundsTest) {

   const std::string INPUT = "9 ORE, 2 APB => 2 A";
   FuelNanoFactory sut;
   sut.extract_reaction_question(INPUT);

   ASSERT_EQ(3, sut.get_number_of_compounds());
}

TEST(FuelNanoFactoryTest, SingleCompoundDualIngredientTestProduce) {

   const std::string INPUT = "9 ORE, 2 APB => 2 A";
   FuelNanoFactory sut;
   sut.extract_reaction_question(INPUT);

   auto compound = sut.get_compound("A");
   auto ore = sut.get_compound("ORE");
   auto apb = sut.get_compound("APB");
   compound->produce_to_satisfy(5);

   ASSERT_EQ(6,  compound->get_total_units_produced());
   ASSERT_EQ(27, ore->get_total_units_produced());
   ASSERT_EQ(6,  apb->get_total_units_produced());
}

TEST(FuelNanoFactoryTest, MultilineInputTestDay14Test1) {

   const std::vector<std::string> INPUT = {
      "10 ORE => 10 A",
      "1 ORE => 1 B",
      "7 A, 1 B => 1 C",
      "7 A, 1 C => 1 D",
      "7 A, 1 D => 1 E",
      "7 A, 1 E => 1 FUEL"
   };

   FuelNanoFactory sut;

   for(const auto& input : INPUT) {
      sut.extract_reaction_question(input);
   }

   auto fuel = sut.get_compound("FUEL");
   auto ore = sut.get_compound("ORE");
   fuel->produce_to_satisfy(1);
   
   ASSERT_EQ(31, ore->get_total_units_produced());
}


TEST(FuelNanoFactoryTest, MultilineInputTestDay14Test2) {

   const std::vector<std::string> INPUT = {
      "9 ORE => 2 A",
      "8 ORE => 3 B",
      "7 ORE => 5 C",
      "3 A, 4 B => 1 AB",
      "5 B, 7 C => 1 BC",
      "4 C, 1 A => 1 CA",
      "2 AB, 3 BC, 4 CA => 1 FUEL"
   };

   FuelNanoFactory sut;

   for(const auto& input : INPUT) {
      sut.extract_reaction_question(input);
   }

   auto fuel = sut.get_compound("FUEL");
   fuel->produce_to_satisfy(1);

   EXPECT_EQ(10, sut.get_compound("A")->get_total_units_produced());
   EXPECT_EQ(24, sut.get_compound("B")->get_total_units_produced());
   EXPECT_EQ(40, sut.get_compound("C")->get_total_units_produced());
   EXPECT_EQ(2,  sut.get_compound("AB")->get_total_units_produced());
   EXPECT_EQ(3,  sut.get_compound("BC")->get_total_units_produced());
   EXPECT_EQ(4,  sut.get_compound("CA")->get_total_units_produced());
   ASSERT_EQ(165, sut.get_compound("ORE")->get_total_units_produced());
}
