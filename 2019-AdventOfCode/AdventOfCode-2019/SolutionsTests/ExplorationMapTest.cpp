#include "gtest/gtest.h"
#include <stack>
#include "../Solutions/Cartesian.h"
#include "../Solutions/ExplorationMap.h"

namespace {

enum class RepairBotSensing {
   WALL,
   EXPLORED,
   FOUND_GOAL, 
   UNEXPLORED
};

typedef QuadDirectionExlorationRecord<RepairBotSensing, RepairBotSensing::UNEXPLORED> QuadExporationTest;

}



TEST(QuadDirectionExlorationRecordTest, IsInitiallyUnexplored) {
   QuadExporationTest sut;

   ASSERT_FALSE(sut.has_been_explored());
}


TEST(QuadDirectionExlorationRecordTest, IsUnexploredAfterUpdatingExporationResult) {
   QuadExporationTest sut;

   sut.discover(RepairBotSensing::WALL);

   ASSERT_TRUE(sut.has_been_explored());
}

TEST(QuadDirectionExlorationRecordTest, ExplorationCompletedIsIntiallyFalse) {
   QuadExporationTest sut;

   ASSERT_FALSE(sut.exploration_completed());
}

TEST(QuadDirectionExlorationRecordTest, ExplorationCompletedExplorationSequence) {
   QuadExporationTest sut;

   sut.next_exploration();
   ASSERT_FALSE(sut.exploration_completed());

   sut.next_exploration();
   ASSERT_FALSE(sut.exploration_completed());

   sut.next_exploration();
   ASSERT_FALSE(sut.exploration_completed());

   sut.next_exploration();
   ASSERT_TRUE(sut.exploration_completed());
}

TEST(QuadDirectionExlorationRecordTest, NextExplorationSequence) {
   
   const std::vector<cartesian::Point2> exp = { {1,0}, {0,1}, {-1,0}, {0,-1} };
   std::vector<cartesian::Point2> sequence;

   QuadExporationTest sut;
   sequence.push_back(sut.next_exploration());
   sequence.push_back(sut.next_exploration());
   sequence.push_back(sut.next_exploration());
   sequence.push_back(sut.next_exploration());

   ASSERT_EQ(exp, sequence);
}

TEST(QuadDirectionExlorationRecordTest, ResetExplorationDoesNotResetDiscovery) {

   QuadExporationTest sut;

   sut.discover(RepairBotSensing::EXPLORED);
   sut.next_exploration();
   sut.next_exploration();
   sut.next_exploration();
   sut.next_exploration();

   ASSERT_TRUE(sut.has_been_explored());
   ASSERT_NE(RepairBotSensing::UNEXPLORED, sut.discovered());

   sut.reset_keeping_discovery();

   ASSERT_FALSE(sut.has_been_explored());
   ASSERT_FALSE(sut.exploration_completed());
}