#include "../Solutions/ReactionEquation.h"
#include "gtest/gtest.h"

namespace {

const std::string COMPOUND_NAME = "ASDFJKL";
const int PRODUCED_QTY = 5;


}



class AtomicCompountReactionEquationTest : public ::testing::Test {
protected:

   void SetUp() override {
      sut.reset(new ReactionEquation(COMPOUND_NAME, PRODUCED_QTY));
   }

   std::unique_ptr<ReactionEquation> sut;

};
TEST_F(AtomicCompountReactionEquationTest, ParameterizedConstrutorAndAccessors) {

   ASSERT_EQ(COMPOUND_NAME, sut->get_name());
   ASSERT_EQ(PRODUCED_QTY, sut->get_unit_quantity());
   ASSERT_EQ(0, sut->get_units_available());
   ASSERT_EQ(0, sut->get_total_units_produced());
   ASSERT_EQ(0, sut->get_units_available());
   ASSERT_FALSE(sut->can_satisfy_request(1));
}
TEST_F(AtomicCompountReactionEquationTest, TestProduceUnitQuantity) {

   const int REQUESTED_QTY = PRODUCED_QTY;

   sut->produce_to_satisfy(REQUESTED_QTY);

   ASSERT_EQ(PRODUCED_QTY, sut->get_units_available());
}

TEST_F(AtomicCompountReactionEquationTest, TestProduceLessThanUnitQuantity) {

   const int REQUESTED_QTY = PRODUCED_QTY - 1;

   sut->produce_to_satisfy(REQUESTED_QTY);

   ASSERT_EQ(PRODUCED_QTY, sut->get_units_available());
}

TEST_F(AtomicCompountReactionEquationTest, TestProduceMoreThanUnitQuantity) {

   const int REQUESTED_QTY = PRODUCED_QTY + 1;

   sut->produce_to_satisfy(REQUESTED_QTY);

   ASSERT_EQ(2 * PRODUCED_QTY, sut->get_units_available());
}

TEST_F(AtomicCompountReactionEquationTest, TestProduceConsume) {

   const int REQUESTED_QTY = PRODUCED_QTY;
   const int CONSUME_REQUEST = 2;

   sut->produce_to_satisfy(REQUESTED_QTY);

   ASSERT_TRUE(sut->consume_units(CONSUME_REQUEST));
   ASSERT_EQ(REQUESTED_QTY - CONSUME_REQUEST, sut->get_units_available());
}

TEST_F(AtomicCompountReactionEquationTest, TestFailedConsumeDoesntConsumeAny) {

   const int CONSUME_REQUEST = 7;

   sut->produce_to_satisfy(PRODUCED_QTY);

   ASSERT_FALSE(sut->consume_units(CONSUME_REQUEST));
   ASSERT_EQ(PRODUCED_QTY, sut->get_units_available());
}


class BinaryCompountReactionEquationTest : public ::testing::Test {
protected:

   void SetUp() override {

      ingredient_1.reset(new ReactionEquation("ING-1", 1));
      ingredient_2.reset(new ReactionEquation("ING-2", 2));

      sut.reset(new ReactionEquation(COMPOUND_NAME, PRODUCED_QTY));

      sut->add_ingredient(*ingredient_1, 3);
      sut->add_ingredient(*ingredient_2, 3);
   }

   std::unique_ptr<ReactionEquation> ingredient_1;
   std::unique_ptr<ReactionEquation> ingredient_2;
   std::unique_ptr<ReactionEquation> sut;

};

TEST_F(BinaryCompountReactionEquationTest, ProduceRequestProducesIngredients) {

   sut->produce_to_satisfy(1);

   ASSERT_EQ(3, ingredient_1->get_total_units_produced());
   ASSERT_EQ(0, ingredient_1->get_units_available());
   ASSERT_EQ(4, ingredient_2->get_total_units_produced());
   ASSERT_EQ(1, ingredient_2->get_units_available());
}

TEST(CompountReactionTest, AOCDay14TestCase1) {

   ReactionEquation fuel("FUEL", 1);
   ReactionEquation ore("ORE", 1);
   ReactionEquation chem_a("A", 10);
   ReactionEquation chem_b("B", 1);
   ReactionEquation chem_c("C", 1);
   ReactionEquation chem_d("D", 1);
   ReactionEquation chem_e("E", 1);

   fuel.add_ingredient(chem_a, 7);
   fuel.add_ingredient(chem_e, 1);

   chem_e.add_ingredient(chem_a, 7);
   chem_e.add_ingredient(chem_d, 1);

   chem_d.add_ingredient(chem_a, 7);
   chem_d.add_ingredient(chem_c, 1);

   chem_c.add_ingredient(chem_a, 7);
   chem_c.add_ingredient(chem_b, 1);

   chem_b.add_ingredient(ore, 1);
   chem_a.add_ingredient(ore, 10);

   fuel.produce_to_satisfy(1);

   ASSERT_EQ(31, ore.get_total_units_produced());
}
