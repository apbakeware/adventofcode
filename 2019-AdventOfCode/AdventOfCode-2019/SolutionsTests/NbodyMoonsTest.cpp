#include "gtest/gtest.h"
#include "../Solutions/NbodyMoons.h"
#include "../Solutions/Cartesian.h"

namespace {

const std::vector<cartesian::Point3> DAY12_TEST_POINTS = {
      {-1, 0, 2},
      {2, -10, -7},
      {4, -8, 8},
      {3, 5, -1}
};

}

TEST(NbodyMoonsTest, GravityTestLowerBase) {
   const cartesian::Point3 base = {3, 3, 3};
   const cartesian::Point3 other = {5, 5, 5};

   const cartesian::Point3 gravity = compute_gravity(base, other);
   const cartesian::Point3 exp = {1, 1, 1};

   EXPECT_EQ(exp, gravity);
}

TEST(NbodyMoonsTest, GravityTestHigherBase) {
   const cartesian::Point3 base = {5, 5, 5};
   const cartesian::Point3 other = {3, 3, 3};

   const cartesian::Point3 gravity = compute_gravity(base, other);
   const cartesian::Point3 exp = {-1, -1, -1};

   EXPECT_EQ(exp, gravity);
}

TEST(NbodyMoonsTest, GravityTestEqual) {
   const cartesian::Point3 base = {3, 3, 3};
   const cartesian::Point3 other = {3, 3, 3};

   const cartesian::Point3 gravity = compute_gravity(base, other);
   const cartesian::Point3 exp = {0, 0, 0};

   EXPECT_EQ(exp, gravity);
}

TEST(NbodyMoonsTest, TestComputeVelocityDay12Sample_Base1) {


   const cartesian::Point3 exp = {3, -1, -1};
   const cartesian::Point3 sut = {-1, 0, 2};
   const auto velocity = compute_velocity
   (sut, DAY12_TEST_POINTS.begin(), DAY12_TEST_POINTS.end());

   EXPECT_EQ(exp, velocity);
}

TEST(NbodyMoonsTest, TestComputeVelocityDay12Sample_Base2) {

   const cartesian::Point3 exp = {1, 3, 3};
   const cartesian::Point3 sut = {2, -10, -7};
   const auto velocity = compute_velocity
   (sut, DAY12_TEST_POINTS.begin(), DAY12_TEST_POINTS.end());

   EXPECT_EQ(exp, velocity);
}

TEST(NbodyMoonsTest, TestSimulateMoonMovement_Step1Moon1) {

   const cartesian::Point3 exp_pos = {2, -1, 1};
   const cartesian::Point3 exp_vel = {3, -1, -1};
   
   const auto sut = simulate_moon_movement(DAY12_TEST_POINTS, 1);

   EXPECT_EQ(sut[0].position, exp_pos);
   EXPECT_EQ(sut[0].velocity, exp_vel);
}

TEST(NbodyMoonsTest, TestSimulateMoonMovement_Step1Moon2) {
   const cartesian::Point3 exp_pos = {3, -7, -4};
   const cartesian::Point3 exp_vel = {1, 3, 3};

   const auto sut = simulate_moon_movement(DAY12_TEST_POINTS, 1);

   EXPECT_EQ(sut[1].position, exp_pos);
   EXPECT_EQ(sut[1].velocity, exp_vel);
}

TEST(NbodyMoonsTest, TestSimulateMoonMovement_Step2Moon1) {
   const cartesian::Point3 exp_pos = {5, -3, -1};
   const cartesian::Point3 exp_vel = {3, -2, -2};

   const auto sut = simulate_moon_movement(DAY12_TEST_POINTS, 2);

   EXPECT_EQ(sut[0].position, exp_pos);
   EXPECT_EQ(sut[0].velocity, exp_vel);
}


TEST(NbodyMoonsTest, TestSimulateMoonMovement_Step10Moon3) {
   const cartesian::Point3 exp_pos = {3, -6, 1};
   const cartesian::Point3 exp_vel = {3, 2, -3};

   const auto sut = simulate_moon_movement(DAY12_TEST_POINTS, 10);

   EXPECT_EQ(sut[2].position, exp_pos);
   EXPECT_EQ(sut[2].velocity, exp_vel);
}


TEST(NbodyMoonsTest, TestComputeTotalEnergy) {
   const auto sut = simulate_moon_movement(DAY12_TEST_POINTS, 10);

   ASSERT_EQ(179, compute_total_energy(sut) );
}

TEST(NbodyMoonsTest, TestComputeTotalEnergy_TestCase2) {
   const std::vector<cartesian::Point3> TEST_CASE_2 = {
      {-8, -10, 0},
      {5, 5, 10},
      {2, -7, 3},
      {9, -8, -3}
   };

   const auto sut = simulate_moon_movement(TEST_CASE_2, 100);

   ASSERT_EQ(1940, compute_total_energy(sut));
}