#pragma once
#include <deque>
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include "IntcodeTypes.h"
#include "IParameterProxy.h"
#include "IIntcodeComputerInput.h"
#include "IIntcodeComputerOutput.h"

/**
 * Run the intcode program ad initially described in 
 * day 02's problem.
 * 
 * @param program Integer sequence comprising the program to run.
 * @param noun Initial noun value to set program[1]
 * @param verb Initial verb value to set program[2]
 *
 * @return Result of the program (stored in program[0]).
 */
int run_intcode_program(const std::vector<int>& program, int noun, int verb);




class IntcodeComputer;

class Opcode {
public:

   explicit Opcode(IntcodeComputer & computer);

   Opcode& operator=(const Opcode& lhs) = default;

   Instruction get_instruction() const {
      return instruction;
   }

   IParameterProxy* param1();
   IParameterProxy* param2();
   IParameterProxy* param3();
 
   operator bool() const {
      return instruction != Instruction::TERMINATE;
   }

   friend std::ostream& operator<<(std::ostream& str, const Opcode& opcode);

private:
   IntcodeProgram& program;
   IntcodeType opcode_value;
   Instruction instruction;
   
   OpcodeModes p1_mode;
   OpcodeModes p2_mode;
   OpcodeModes p3_mode;

   std::unique_ptr<IParameterProxy> param_1;
   std::unique_ptr<IParameterProxy> param_2;
   std::unique_ptr<IParameterProxy> param_3;
};



class IntcodeComputer {
public:

   IntcodeComputer(
      const IntcodeProgram& program, 
      IIntcodeComputerInput& input_provider, 
      IIntcodeComputerOutput& output_handler
   );

   IntcodeComputer(const IntcodeComputer& orig);

   IntcodeComputer& operator=(const IntcodeComputer& rhs) = delete;

   bool execute();
   std::vector<IntcodeType> get_outputs() const;

   friend class Opcode;

   void log_program(std::ostream& str);

private:
   
   IntcodeProgram::iterator execute_add(Opcode& opcode, IntcodeProgram::iterator instr);
   IntcodeProgram::iterator execute_multiply(Opcode& opcode, IntcodeProgram::iterator instr);
   IntcodeProgram::iterator execute_input(Opcode& opcode, IntcodeProgram::iterator instr, IntcodeType value);
   IntcodeProgram::iterator execute_output(Opcode& opcode, IntcodeProgram::iterator instr);
   IntcodeProgram::iterator execute_jit(Opcode& opcode, IntcodeProgram::iterator instr);
   IntcodeProgram::iterator execute_jif(Opcode& opcode, IntcodeProgram::iterator instr);
   IntcodeProgram::iterator execute_lt(Opcode& opcode, IntcodeProgram::iterator instr);
   IntcodeProgram::iterator execute_eq(Opcode& opcode, IntcodeProgram::iterator instr);
   IntcodeProgram::iterator execute_rel_base(Opcode& opcode, IntcodeProgram::iterator instr);

   IntcodeProgram program;
   IntcodeProgram::iterator piter;
   std::vector<IntcodeType> outputs;
   IIntcodeComputerInput& input_provider;
   IIntcodeComputerOutput& output_handler;
   IntcodeType rel_base;
   
};