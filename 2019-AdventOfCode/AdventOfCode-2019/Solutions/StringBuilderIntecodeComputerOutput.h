#pragma once
#include <vector>
#include "IntcodeTypes.h"
#include "IIntcodeComputerOutput.h"

class StringBuilderIntecodeComputerOutput : public IIntcodeComputerOutput {
public:

   virtual ~StringBuilderIntecodeComputerOutput() = default;

   virtual void handle_output(IntcodeType output);

   std::string get_output() const;

private:
   std::vector<IntcodeType> outputs;
};
