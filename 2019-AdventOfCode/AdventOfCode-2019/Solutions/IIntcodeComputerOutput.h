#pragma once

#include "IntcodeTypes.h"

class IIntcodeComputerOutput {
public:

   virtual ~IIntcodeComputerOutput() = default;

   virtual void handle_output(IntcodeType output) = 0;
};
