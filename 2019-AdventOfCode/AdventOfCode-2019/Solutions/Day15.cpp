#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "IntcodeComputer.h"
#include "O2RepairDroid.h"

namespace {

class Day15Prob1 : public IProblem {
public:

   virtual ~Day15Prob1() = default;

   virtual std::string name() const override {
      return "Day 15 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day15Prob2 : public IProblem {
public:

   virtual ~Day15Prob2() = default;

   virtual std::string name() const override {
      return "Day 15 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

ProblemFactoryRegisterer<Day15Prob1> _problem1("Day15Prob1");
ProblemFactoryRegisterer<Day15Prob2> _problem2("Day15Prob2");


ISolution* Day15Prob1::execute(std::istream& data_set) {

   std::vector<IntcodeType> input_data(100000, 0);
   auto piter = input_data.begin();
   std::string token;
   while(std::getline(data_set, token, ',')) {
      std::istringstream istr(token);
      istr >> *piter;
      ++piter;
   }

   O2RepairDroid repair_droid;
   IntcodeComputer computer(input_data, repair_droid, repair_droid);
   computer.execute();
   repair_droid.render(std::cout);
   return new SingleValueSolution<int>(repair_droid.get_min_steps_to_repair_site());
}


ISolution* Day15Prob2::execute(std::istream& data_set) {

   std::vector<IntcodeType> input_data(100000, 0);
   auto piter = input_data.begin();
   std::string token;
   while(std::getline(data_set, token, ',')) {
      std::istringstream istr(token);
      istr >> *piter;
      ++piter;
   }

   O2RepairDroid repair_droid;
   IntcodeComputer computer(input_data, repair_droid, repair_droid);
   computer.execute();
   repair_droid.render(std::cout);
   return new SingleValueSolution<int>(repair_droid.get_steps_to_fill_room_with_02());
}

}
