#pragma once

#include <string>
#include <unordered_map>
#include <vector>

class IProblem;

/**
 * Function pointer returning a IProblem pointer taking no arguments.
 */
typedef IProblem* (*Concrete_Builder)();

class ProblemFactory {
public:

   

   static ProblemFactory& factory();

   bool register_builder(const std::string& name, Concrete_Builder builder);

   IProblem* build(const std::string& name);

   std::vector<std::string> all_keys() const;

private:

   ProblemFactory();
   ProblemFactory(const ProblemFactory& orig) = delete;
   ProblemFactory& operator=(const ProblemFactory& orig) = delete;

   std::unordered_map<std::string, Concrete_Builder> builders;
};

template<class Concrete_T>
class ProblemFactoryRegisterer {
public:

   explicit ProblemFactoryRegisterer(const std::string& key) {
      ProblemFactory::factory().register_builder
      (key, []() { return static_cast<IProblem*>(new Concrete_T()); });
   }
};
