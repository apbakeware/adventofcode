#include <iostream>
#include "SubstringLineResult.h"

SubstringLineResult::SubstringLineResult(const std::string& str, size_t substr_len)
   : str(str), substr_len(substr_len) {

}

void SubstringLineResult::write_result(std::ostream& outs) {

   for(size_t start = 0; start < str.size(); start += substr_len) {
      outs << str.substr(start, substr_len) << "\n                                        ";
   }
   outs << "\n";
}
