#include <algorithm>
#include <numeric>
#include <string>
#include <sstream>
#include <iostream>
#include <queue>
#include <vector>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "SubstringLineResult.h"
#include "DataLoader.h"
#include "IntcodeComputer.h"
#include "PaintBotIO.h"

namespace {

class Day11Prob1 : public IProblem {
public:

   virtual ~Day11Prob1() = default;

   virtual std::string name() const override {
      return "Day 11 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day11Prob2 : public IProblem {
public:

   virtual ~Day11Prob2() = default;

   virtual std::string name() const override {
      return "Day 11 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

ProblemFactoryRegisterer<Day11Prob1> _problem1("Day11Prob1");
ProblemFactoryRegisterer<Day11Prob2> _problem2("Day11Prob2");


ISolution* Day11Prob1::execute(std::istream& data_set) {

   std::vector<IntcodeType> input_data(100000, 0);
   auto piter = input_data.begin();
   std::string token;
   while(std::getline(data_set, token, ',')) {
      std::istringstream istr(token);
      istr >> *piter;
      ++piter;
   }

   PaintBotIO paintbot(PaintColor::BLACK);
   IntcodeComputer computer(input_data, paintbot, paintbot);
   computer.execute();
   return new SingleValueSolution<int>(paintbot.number_of_painted_squares());
}


ISolution* Day11Prob2::execute(std::istream& data_set) {

   std::vector<IntcodeType> input_data(100000, 0);
   auto piter = input_data.begin();
   std::string token;
   while(std::getline(data_set, token, ',')) {
      std::istringstream istr(token);
      istr >> *piter;
      ++piter;
   }

   PaintBotIO paintbot(PaintColor::WHITE);
   IntcodeComputer computer(input_data, paintbot, paintbot);
   computer.execute();

   auto rendering = paintbot.render_painting();

   return new SubstringLineResult(rendering.first, rendering.second);
}

}
