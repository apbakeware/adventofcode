#include <algorithm>
#include <numeric>
#include <string>
#include <sstream>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "IntcodeComputer.h"

namespace {

class Day2Prob1 : public IProblem {
public:

   virtual ~Day2Prob1() = default;

   virtual std::string name() const override {
      return "Day 02 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day2Prob2 : public IProblem {
public:

   virtual ~Day2Prob2() = default;

   virtual std::string name() const override {
      return "Day 02 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};


ProblemFactoryRegisterer<Day2Prob1> _problem1("Day2Prob1");
ProblemFactoryRegisterer<Day2Prob2> _problem2("Day2Prob2");


ISolution* Day2Prob1::execute(std::istream& data_set) {
   std::vector<int> input_data;
   std::string token;

   while(std::getline(data_set, token, ',')) {
      input_data.push_back(atoi(token.c_str()));
   }
   
   int result = run_intcode_program(input_data, 12, 2);
   return new SingleValueSolution<int>(result);
}


ISolution* Day2Prob2::execute(std::istream& data_set) {
   
   std::vector<int> input_data;
   std::string token;

   while(std::getline(data_set, token, ',')) {
      input_data.push_back(atoi(token.c_str()));
   }

   const int REQUIED = 19690720;

   size_t noun = 0;
   size_t verb = 0;


   // max index that can be read
   for(noun = 1; noun < input_data.size(); ++noun) {
      for(verb = 1; verb < input_data.size(); ++verb) {
         if(run_intcode_program(input_data, noun, verb) == REQUIED) {
            return new SingleValueSolution<int>(100 * noun + verb);
         }
      }
   }

   return new SingleValueSolution<int>(-1);
}
}
