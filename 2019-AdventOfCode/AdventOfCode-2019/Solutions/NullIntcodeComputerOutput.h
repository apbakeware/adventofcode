#pragma once

#include "IntcodeTypes.h"
#include "IIntcodeComputerOutput.h"

class NullIntcodeComputerOutput : public IIntcodeComputerOutput {
public:

   virtual ~NullIntcodeComputerOutput() = default;

   virtual void handle_output(IntcodeType output);
};
