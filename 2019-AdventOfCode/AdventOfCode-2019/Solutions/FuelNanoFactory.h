#pragma once
#include <unordered_map>
#include <string>

class ReactionEquation;

class FuelNanoFactory {
public:

   ReactionEquation* get_compound(const std::string& name);

   size_t get_number_of_compounds() const;

   void extract_reaction_question(const std::string& str);

private:

   ReactionEquation* process_resulting_compound_input(const std::string& resulting_compound_text);

   void process_compound_ingredient_input
   (ReactionEquation& produced, const std::string& ingredients_text);

   ReactionEquation* get_or_create(const std::string& name);

   typedef std::unordered_map <std::string, std::shared_ptr<ReactionEquation> > CompoundTable;

   CompoundTable reactions;

};
