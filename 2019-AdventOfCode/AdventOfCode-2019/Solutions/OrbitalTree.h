#pragma once

#include <algorithm>
#include <iosfwd>
#include <memory>
#include <unordered_map>
#include <string>
#include <sstream>

const char ORBIT_SEP = ')';

struct OrbitInputSpec {
   std::string base;
   std::string orbiter;
};

struct TreeNode;
typedef std::shared_ptr<TreeNode> TreeNodePtr;

struct TreeNode {
   std::string value;
   TreeNode* parent;
   std::vector<TreeNodePtr> children;
};

typedef std::pair<std::string, int> NodeLevel;
typedef std::vector<NodeLevel> NodeLevels;

typedef std::vector<OrbitInputSpec> OrbitalSpecs;
typedef std::unordered_map<std::string, TreeNodePtr> OrbitalNodes;

std::istream& operator>>(std::istream& str, OrbitInputSpec& spec);

std::ostream& operator<<(std::ostream& str, const OrbitInputSpec& spec);

OrbitalNodes create_orbital_tree(std::istream& str);

void dfs_generate_node_levels(const TreeNode& node, NodeLevels& node_levels, int level = 0);

std::vector<TreeNode*> get_orbital_path(const OrbitalNodes& orbits, const std::string& starting_node);


// Return iterators to each collection of the last common element in sequence.
// Collection 2 must be same length or more
template<class Iter_T1, class Iter_T2>
std::pair<Iter_T1, Iter_T2> find_first_non_common(Iter_T1 first1, Iter_T1 last1, Iter_T2 first2) {

   while(first1 != last1 && *first1 == *first2) {
      ++first1;
      ++first2;
   }

   return std::make_pair(first1, first2);
}