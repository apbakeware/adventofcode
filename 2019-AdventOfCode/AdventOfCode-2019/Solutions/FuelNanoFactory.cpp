#include <iostream>
#include <stdexcept>
#include <sstream>
#include "FuelNanoFactory.h"
#include "ReactionEquation.h"

namespace {

struct ReactionQtyNameHelper {
   int qty;
   std::string name;
};

std::istream& operator>>(std::istream& str, ReactionQtyNameHelper& obj) {
   str >> obj.qty >> obj.name;
   return str;
}

}

ReactionEquation* FuelNanoFactory::get_compound(const std::string& name) {
   auto iter = reactions.find(name);
   if(iter == reactions.end()) {
      return nullptr; // or null object?
   }

   return (iter->second).get();
}

size_t FuelNanoFactory::get_number_of_compounds() const {
   return reactions.size();
}

void FuelNanoFactory::extract_reaction_question(const std::string& str) {

   const std::string RESULTANT_SPLIT = "=>";

   const auto resultant_idx = str.find(RESULTANT_SPLIT);
   if(resultant_idx == std::string::npos) {
      throw std::runtime_error("Failed to find produced compound delimiter");
   }

   const auto ingredient_compound_str = str.substr(0, resultant_idx);
   const auto resultant_compound_str = str.substr(resultant_idx + RESULTANT_SPLIT.size());

   //std::cout << "Found " << RESULTANT_SPLIT << " at position: " << resultant_idx 
   //   << "\n  Ingredient str: " << ingredient_compound_str 
   //   << "  Resultant str: " << resultant_compound_str << "\n";

   ReactionEquation* produced = process_resulting_compound_input(resultant_compound_str);
   process_compound_ingredient_input(*produced, ingredient_compound_str);
}

ReactionEquation* FuelNanoFactory::process_resulting_compound_input(const std::string& resulting_compound_text) {

   std::istringstream istr(resulting_compound_text);
   ReactionQtyNameHelper extraction;

   istr >> extraction;

   ReactionEquation* produced = get_or_create(extraction.name);
   produced->update_unit_quantity(extraction.qty);
   return produced;
}

void FuelNanoFactory::process_compound_ingredient_input(ReactionEquation& produced, const std::string& ingredients_text) {

   const char INGREDIENT_DELIM = ',';
   std::istringstream istr(ingredients_text);
   ReactionQtyNameHelper extraction;
   std::string ingredient_token;

   while(std::getline(istr, ingredient_token, INGREDIENT_DELIM)) {
      std::istringstream token_str(ingredient_token);
      token_str >> extraction;

      ReactionEquation* ingredient = get_or_create(extraction.name);
      produced.add_ingredient(*ingredient, extraction.qty);
   }
}

ReactionEquation* FuelNanoFactory::get_or_create(const std::string& name) {

   auto iter = reactions.find(name);
   if(iter == reactions.end()) {
      CompoundTable::value_type produced_compound = std::make_pair(
         name, std::make_shared<ReactionEquation>(name)
      );

      auto inserted = reactions.insert(produced_compound);
      iter = inserted.first;
   }

   return (iter->second).get();
}
