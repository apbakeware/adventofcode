#pragma once

#include <deque>
#include <vector>

#include "IntcodeTypes.h"
#include "IIntcodeComputerInput.h"

class IntcodeComputerInputsList : public IIntcodeComputerInput {
public:

   explicit IntcodeComputerInputsList(const std::vector<IntcodeType>& inputs);

   virtual ~IntcodeComputerInputsList() = default;

   virtual bool input_available() const;

   virtual IntcodeType get_intput();

private:
   std::deque<IntcodeType> inputs;

};