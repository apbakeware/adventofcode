#pragma once
#include <iosfwd>
#include "Cartesian.h"

namespace polar {

struct Point2 {
   double magnitude;
   double angle; // degrees
};

std::ostream& operator<<(std::ostream& str, const Point2& rec);

Point2 convert_from(const cartesian::Point2 & base, const cartesian::Point2& target);
Point2 convert_from(const cartesian::Point2& base, const cartesian::Point2& target, double rotation_degrees);

Point2 rotate(const Point2& base, double degrees);

}