#include <algorithm>
#include <numeric>
#include <string>
#include <sstream>
#include <iostream>
#include <queue>
#include <vector>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "IntcodeComputer.h"

namespace {

class Day7Prob1 : public IProblem {
public:

   virtual ~Day7Prob1() = default;

   virtual std::string name() const override {
      return "Day 07 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day7Prob2 : public IProblem {
public:

   virtual ~Day7Prob2() = default;

   virtual std::string name() const override {
      return "Day 07 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

// Chains output from an IntcodeCOmputer itner [Phase, Input]
class AmplifierInput : public IIntcodeComputerOutput, public IIntcodeComputerInput {
public:

   explicit AmplifierInput(IntcodeType phase) {
      inputs.push(phase);
   }

   virtual ~AmplifierInput() = default;

   void queue_input(IntcodeType input) {
      inputs.push(input);
   }

   virtual bool input_available() const override {
      return !inputs.empty();
   }

   // Must have value
   virtual IntcodeType get_intput() override {
      auto val = inputs.front();
      inputs.pop();
      return val;
   }

   virtual void handle_output(IntcodeType output) override {
      inputs.push(output);
   }

private:
   std::queue<IntcodeType> inputs;
};

ProblemFactoryRegisterer<Day7Prob1> _problem1("Day7Prob1");
ProblemFactoryRegisterer<Day7Prob2> _problem2("Day7Prob2");



ISolution* Day7Prob1::execute(std::istream& data_set) {

   std::vector<IntcodeType> input_data;
   std::string token;
   while(std::getline(data_set, token, ',')) {
      input_data.push_back(atoi(token.c_str()));
   }

   IntcodeType max_thruster_output = std::numeric_limits<IntcodeType>::min();
   std::vector<IntcodeType> phase_sequence = {0, 1, 2, 3, 4};
   
   do {
      AmplifierInput amp_a_input(phase_sequence[0]);
      AmplifierInput amp_b_input(phase_sequence[1]);
      AmplifierInput amp_c_input(phase_sequence[2]);
      AmplifierInput amp_d_input(phase_sequence[3]);
      AmplifierInput amp_e_input(phase_sequence[4]);

      IntcodeComputer amp_a(input_data, amp_a_input, amp_b_input);
      IntcodeComputer amp_b(input_data, amp_b_input, amp_c_input);
      IntcodeComputer amp_c(input_data, amp_c_input, amp_d_input);
      IntcodeComputer amp_d(input_data, amp_d_input, amp_e_input);
      IntcodeComputer amp_e(input_data, amp_e_input, amp_a_input);

      amp_a_input.queue_input(0);
      amp_a.execute();
      amp_b.execute();
      amp_c.execute();
      amp_d.execute();
      amp_e.execute();

      max_thruster_output = std::max(max_thruster_output, amp_e.get_outputs().front());
      
   } while(std::next_permutation(phase_sequence.begin(), phase_sequence.end()));

   return new SingleValueSolution<IntcodeType>(max_thruster_output);
}


ISolution* Day7Prob2::execute(std::istream& data_set) {

   std::vector<IntcodeType> input_data;
   std::string token;
   while(std::getline(data_set, token, ',')) {
      input_data.push_back(atoi(token.c_str()));
   }

   // Because parameter 3 params may exceed capacity of vector....how to handle in intcode.
   // Changed on final intcode
   input_data.push_back(0);
   input_data.push_back(0);
   input_data.push_back(0);

   IntcodeType max_thruster_output = std::numeric_limits<IntcodeType>::min();
   std::vector<IntcodeType> phase_sequence = {5, 6, 7, 8, 9};
   do {

      AmplifierInput amp_ea_loop(phase_sequence[0]);
      AmplifierInput amp_ab_loop(phase_sequence[1]);
      AmplifierInput amp_bc_loop(phase_sequence[2]);
      AmplifierInput amp_cd_loop(phase_sequence[3]);
      AmplifierInput amp_de_loop(phase_sequence[4]);

      std::vector<IntcodeComputer> amplifiers = {
         {input_data, amp_ea_loop, amp_ab_loop},
         {input_data, amp_ab_loop, amp_bc_loop},
         {input_data, amp_bc_loop, amp_cd_loop},
         {input_data, amp_cd_loop, amp_de_loop},
         {input_data, amp_de_loop, amp_ea_loop},
      };

      amp_ea_loop.queue_input(0);
      auto complete = false;
      do {

         for (auto& amp : amplifiers) {
            complete = amp.execute();
         }

      } while(!complete);

      max_thruster_output = std::max(max_thruster_output, amplifiers.back().get_outputs().front());

   } while(std::next_permutation(phase_sequence.begin(), phase_sequence.end()));

   return new SingleValueSolution<IntcodeType>(max_thruster_output);
}

}
