#pragma once
#include <string>
#include <vector>

typedef long long IntcodeType;

typedef std::vector<IntcodeType> IntcodeProgram;

typedef std::vector<IntcodeType>::iterator ProgramPtr;

enum class Instruction {
   ADD = 1,
   MULTIPLY = 2,
   INPUT = 3,
   OUTPUT = 4,
   JUMP_IF_TRUE = 5,
   JUMP_IF_FALSE = 6,
   LESS_THAN = 7,
   EQUAL = 8,
   REL_BASE = 9,
   TERMINATE = 99
};

std::string to_string(Instruction instruction);

enum class OpcodeModes {
   POSITION = 0,
   IMMEDIATE = 1,
   RELATIVE = 2,
   UNKNOWN = 99
};

std::string to_string(OpcodeModes mode);