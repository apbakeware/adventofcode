#include <cassert>
#include <cmath>
#include "FFT.h"

namespace {

const std::vector<int> SEQUENCE = {0, 1, 0, -1};


int compute_fft_value(
   std::vector<int>::const_iterator input_start, 
   std::vector<int>::const_iterator input_end, 
   std::vector<int>::const_iterator pattern_start) {

   int result = 0;
   while(input_start != input_end) {
      result += *input_start * *pattern_start;
      ++input_start;
      ++pattern_start;
   }
   return std::abs(result) % 10;
}

std::vector<int> compute_fft_pattern(int element, int length) {
   std::vector<int> pattern(length);

   int seq_idx = 0;
   auto res_iter = pattern.begin();

   while(res_iter != pattern.end()) {
      for(int cnt = 0; cnt < element; ++cnt) {
         *res_iter = SEQUENCE[seq_idx];
         if(++res_iter == pattern.end()) {
            break;
         }
      }
      seq_idx = (seq_idx + 1) % SEQUENCE.size();
   }

   return pattern;
}

}

FFT::FFT(int sequence_length)
   : fft_len(sequence_length),
     patterns_to_apply(fft_len + 1) {

   for(size_t idx = 1; idx < patterns_to_apply.size(); ++idx) {
      patterns_to_apply[idx] = compute_fft_pattern(idx, fft_len + 1);
   }

}

std::vector<int> FFT::apply_fft(const std::vector<int>& input, int num_phases) {
   assert(num_phases > 0);

   std::vector<int> phase_result = compute_phase(input);

   while(--num_phases > 0) {
      phase_result = compute_phase(phase_result);
   }

   return phase_result;
}

std::vector<int> FFT::compute_phase(const std::vector<int>& input) {
   assert(input.size() == fft_len);

   int element = 1;
   std::vector<int> result(input.size(), 0);

   for(auto iter = result.begin(); iter != result.end(); ++iter) {
      const auto & pattern = get_element_pattern(element);

      *iter = compute_fft_value(input.begin(), input.end(), pattern.first);
      ++element;
   }

   return result;
}

std::pair<std::vector<int>::const_iterator, std::vector<int>::const_iterator> FFT::get_element_pattern
(int element) {

   const auto& pattern = patterns_to_apply.at(element);
   return std::make_pair(pattern.begin() + 1, pattern.end());
}

