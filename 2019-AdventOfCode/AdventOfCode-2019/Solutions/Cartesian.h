#pragma once
#include <iosfwd>
#include <string>
#include <utility>
#include <vector>

namespace cartesian {

struct Point2 {
   int x;
   int y;
};

struct cantor_pairing {
   size_t operator()(const cartesian::Point2& point) const;
};

Point2 rotate(const Point2 & point, double angle);

bool operator==(const Point2& lhs, const Point2& rhs);

bool operator<(const Point2& lhs, const Point2& rhs);

Point2 operator-(const Point2& lhs, const Point2& rhs);

Point2 operator+(const Point2& lhs, const Point2& rhs);

std::ostream& operator<<(std::ostream& str, const Point2& pt);


struct Slope2 {
   double dx;
   double dy;
   double slope;
};

bool operator==(const Slope2& lhs, const Slope2 & rhs);


struct Point3 {
   int x;
   int y;
   int z;
};



Point3 operator+(const Point3& lhs, const Point3& rhs);

bool operator==(const Point3& lhs, const Point3& rhs);

std::ostream& operator<<(std::ostream& str, const Point3& pt);



typedef std::tuple<Point2, double> DistanceToPoint;

double delta_x(const Point2& p1, const Point2& p2);

double delta_y(const Point2& p1, const Point2& p2);

double distance(const Point2& p1, const Point2& p2);

Slope2 slope(const Point2& p1, const Point2& p2);

bool are_collinear(const Point2& p1, const Point2& p2, const Point2& p3);

std::vector<DistanceToPoint> points_sorted_by_distance_from
(const Point2& ref, const std::vector<Point2>& points);

std::vector<Point2> get_los_points
(const Point2& ref, const std::vector<DistanceToPoint>& points);

std::tuple<Point2, size_t> find_point_with_greatest_los_count
(const std::vector<Point2>& points);

std::vector<Point2> create_points_from_grid
(const std::vector<std::string>& lines, const char marker);

}