#include "IntcodeComputerInputsList.h"

IntcodeComputerInputsList::IntcodeComputerInputsList(const std::vector<IntcodeType>& inputs)
   : inputs(inputs.begin(), inputs.end()) {
}

bool IntcodeComputerInputsList::input_available() const {
   return !inputs.empty();
}

IntcodeType IntcodeComputerInputsList::get_intput() {
   auto val = inputs.front();
   inputs.pop_front();
   return val;
}

