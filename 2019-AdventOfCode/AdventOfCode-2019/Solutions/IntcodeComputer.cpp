#include <cassert>
#include <iterator>
#include <iostream>
#include <sstream>
#include "IntcodeComputer.h"

int run_intcode_program(const std::vector<int>& program, int noun, int verb) {
   auto executable(program);
   executable[1] = noun;
   executable[2] = verb;

   for(size_t idx = 0; idx < executable.size(); idx += 4) {
      auto op = executable[idx];
      if(op == 99) {
         break;
      }

      auto arg1_idx = executable[idx + 1];
      auto arg2_idx = executable[idx + 2];
      auto dest = executable[idx + 3];

      auto arg1 = executable[arg1_idx];
      auto arg2 = executable[arg2_idx];
      auto val = 0;


      if(op == 1) {
         val = arg1 + arg2;

      } else if(op == 2) {
         val = arg1 * arg2;

      }

      executable[dest] = val;
   }

   return executable.front();
}


std::ostream& operator<<(std::ostream& str, const Opcode& opcode) {
   str << "Opcode { value: " << opcode.opcode_value
      << "  instruction: " << to_string(opcode.instruction)
      << "  p1-mode: " << to_string(opcode.p1_mode)
      << "  p2-mode: " << to_string(opcode.p2_mode)
      << "  p3-mode: " << to_string(opcode.p3_mode)
      << "}"
      ;
   return str;
}



Opcode::Opcode(IntcodeComputer& computer)
   : program(computer.program), 
     opcode_value(*computer.piter),
     instruction{opcode_value % 100},
     p1_mode{(opcode_value / 100 % 10)},
     p2_mode{(opcode_value / 1000 % 10)},
     p3_mode{(opcode_value / 10000 % 10)},
     param_1(create_parameter(p1_mode, computer.program, computer.piter+1, computer.rel_base)),
     param_2(create_parameter(p2_mode, computer.program, computer.piter+2, computer.rel_base)),
     param_3(create_parameter(p3_mode, computer.program, computer.piter+3, computer.rel_base)) {

}

IParameterProxy* Opcode::param1() {
   return param_1.get();
}

IParameterProxy* Opcode::param2() {
   return param_2.get();
}

IParameterProxy* Opcode::param3() {
   return param_3.get();
}



IntcodeComputer::IntcodeComputer
(const IntcodeProgram& program, IIntcodeComputerInput& input_provider, IIntcodeComputerOutput& output_handler)
   : program(program), 
     piter(this->program.begin()),
     input_provider(input_provider),
     output_handler(output_handler),
     rel_base(0) {
}

IntcodeComputer::IntcodeComputer(const IntcodeComputer& orig)
   : program(orig.program), input_provider(orig.input_provider), output_handler(orig.output_handler), rel_base(orig.rel_base) {

   piter = program.begin();
}

bool IntcodeComputer::execute() {
   outputs.clear();
   if(program.empty()) {
      std::cout << "Program is empty, returning\n";
      return true;
   }

   bool program_complete = false;
   IntcodeType output = -1;

   while(piter != program.end()) {
      //log_program(std::cout);
      Opcode opcode(*this);
      if(!opcode) {
         program_complete = true;
         break;
      }

      //std::cout << "Executing opcode: " << opcode << "\n";

      switch(opcode.get_instruction()) {
      case Instruction::ADD:
         piter = execute_add(opcode, piter);
         break;
      case Instruction::MULTIPLY:
         piter = execute_multiply(opcode, piter);
         break;
      case Instruction::INPUT:
         if(!input_provider.input_available()) {
            //std::cout << "Input not available, breaking execution with incomplete program\n";
            return false;
         }
         piter = execute_input(opcode, piter, input_provider.get_intput());
         break;
      case Instruction::OUTPUT:
         piter = execute_output(opcode, piter);
         break;
      case Instruction::JUMP_IF_TRUE:
         piter = execute_jit(opcode, piter);
         break;
      case Instruction::JUMP_IF_FALSE:
         piter = execute_jif(opcode, piter);
         break;
      case Instruction::LESS_THAN:
         piter = execute_lt(opcode, piter);
         break;
      case Instruction::EQUAL:
         piter = execute_eq(opcode, piter);
         break;
      case Instruction::REL_BASE:
         piter = execute_rel_base(opcode, piter);
         break;
      default:
         throw std::runtime_error("Unknown opcode instrution");
      }
   }

   return program_complete;
}

std::vector<IntcodeType> IntcodeComputer::get_outputs() const {
   return outputs;
}

void IntcodeComputer::log_program(std::ostream& str) {
   str << "--------------\nPiter: " << std::distance(program.begin(), piter)
      << "  RelBase: " << rel_base
      << "\n";
   str << "Program: [";
   std::copy(
      program.begin(),
      program.end(),
      std::ostream_iterator<IntcodeType>(str, " ")
   );
   str << "]\n";
   
}


IntcodeProgram::iterator IntcodeComputer::execute_add
( Opcode& opcode, IntcodeProgram::iterator instr) {
   auto p1 = opcode.param1()->get();
   auto p2 = opcode.param2()->get();
   opcode.param3()->set(p1 + p2);

   return instr + 4;
}

IntcodeProgram::iterator IntcodeComputer::execute_multiply
(Opcode& opcode, IntcodeProgram::iterator instr) {
   auto p1 = opcode.param1()->get();
   auto p2 = opcode.param2()->get();
   opcode.param3()->set(p1 * p2);

   return instr + 4;
}

IntcodeProgram::iterator IntcodeComputer::execute_input
(Opcode& opcode, IntcodeProgram::iterator instr, IntcodeType value) {
   opcode.param1()->set(value);
   return instr + 2;
}

IntcodeProgram::iterator IntcodeComputer::execute_output
(Opcode& opcode, IntcodeProgram::iterator instr) {
   //std::cout << "IntcodeComputer::execute_output(): " << * (instr + 1) << "\n";
   IntcodeType val = opcode.param1()->get();
   //std::cout << "\tOutput: " << val << "\n";
   outputs.push_back(val);
   output_handler.handle_output(val);
   return instr + 2;
}

IntcodeProgram::iterator IntcodeComputer::execute_jit
(Opcode& opcode, IntcodeProgram::iterator instr) {
   auto jump = 3;
   if(opcode.param1() -> get() != 0) {
      instr = program.begin() + static_cast<int>(opcode.param2()->get());
      jump = 0;
   }
   return instr + jump;
}

IntcodeProgram::iterator IntcodeComputer::execute_jif
(Opcode& opcode, IntcodeProgram::iterator instr) {
   auto jump = 3;
   if(opcode.param1()->get() == 0) {
      instr = program.begin() + static_cast<int>(opcode.param2()->get());
      jump = 0;
   }

   return instr + jump;
}

IntcodeProgram::iterator IntcodeComputer::execute_lt
(Opcode& opcode, IntcodeProgram::iterator instr) {
   auto p1 = opcode.param1()->get();
   auto p2 = opcode.param2()->get();
   IntcodeType val = p1 < p2 ? 1 : 0;
   opcode.param3()->set(val);
   return instr + 4;
}

IntcodeProgram::iterator IntcodeComputer::execute_eq
(Opcode& opcode, IntcodeProgram::iterator instr) {
   auto p1 = opcode.param1()->get();
   auto p2 = opcode.param2()->get();
   IntcodeType val = p1 == p2 ? 1 : 0;
   opcode.param3()->set(val);
   return instr + 4;
}

IntcodeProgram::iterator IntcodeComputer::execute_rel_base(Opcode& opcode, IntcodeProgram::iterator instr) {
   //std::cout << "IntcodeComputer::execute_rel_base()\n";
   rel_base += opcode.param1()->get();
   //std::cout << "  RelBase is now: " << rel_base << "\n";
   return instr + 2;
}


