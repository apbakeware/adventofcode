#include <algorithm>
#include <iostream>
#include "ArcadeGameIO.h"

namespace {

const cartesian::Point2 SCORE_INDICATOR = {-1,0};

// https://stackoverflow.com/questions/919612/mapping-two-integers-to-one-in-a-unique-and-deterministic-way
// Cantor pairing function:
// (a + b)* (a + b + 1) / 2 + a



std::string get_rendering(GameTileTypes val) {
   std::string rendering = "";
   switch(val) {
   case GameTileTypes::EMPTY:
      rendering = " ";
      break;

   case GameTileTypes::WALL:
      rendering = "X";
      break;

   case GameTileTypes::BLOCK:
      rendering = "=";
      break;

   case GameTileTypes::PADDLE:
      rendering = "T";
      break;

   case GameTileTypes::BALL:
      rendering = "O";
      break;

   default:
      throw std::runtime_error("Unknown GameTileType");
   }
   return rendering;
}

}

ArcadeGameIO::ArcadeGameIO() :
   max_x(0),
   max_y(0),
   score(0),
   ball_x(0),
   paddle_x(0) {

   output_handler = std::bind(
      &ArcadeGameIO::handle_x_tile_position_output,
      this,
      std::placeholders::_1
   );
}

bool ArcadeGameIO::input_available() const {
   return true;
}

IntcodeType ArcadeGameIO::get_intput() {
   IntcodeType result = 0;
   if(ball_x < paddle_x) {
      result = -1;
   } else if(ball_x > paddle_x) {
      result = 1;
   }
   
   return result;
}

void ArcadeGameIO::handle_output(IntcodeType output) {
   output_handler(output);
}

int ArcadeGameIO::get_number_of_blocks() const {
   return std::count_if(
      game_tiles.begin(),
      game_tiles.end(),
      [](const auto& val) {
         return val.second == GameTileTypes::BLOCK;
      }

   );
}

int ArcadeGameIO::get_score() const {
   return score;
}

void ArcadeGameIO::render(std::ostream& str) const {
   for(int ry = 0; ry <= max_y; ++ry) {
      for(int rx = 0; rx <= max_x; ++rx) {
         const cartesian::Point2 point = {rx,ry};
         const auto iter = game_tiles.find(point);
         const std::string to_render = iter == game_tiles.end() ? " " : get_rendering(iter->second);
         str << to_render;
      }
      str << "\n";
   }
}

void ArcadeGameIO::handle_x_tile_position_output(IntcodeType val) {
   
   current_pos.x = static_cast<int>(val);
   max_x = std::max(max_x, current_pos.x);
   
   output_handler = std::bind(
      &ArcadeGameIO::handle_y_tile_position_output,
      this,
      std::placeholders::_1
   );
}

void ArcadeGameIO::handle_y_tile_position_output(IntcodeType val) {
   current_pos.y = static_cast<int>(val);
   max_y = std::max(max_y, current_pos.y);

   output_handler = std::bind(
      &ArcadeGameIO::handle_tile_or_score_output,
      this,
      std::placeholders::_1
   );
}

void ArcadeGameIO::handle_tile_or_score_output(IntcodeType val) {

   if(current_pos == SCORE_INDICATOR) {
      score = static_cast<int>(val);

   } else {
      
      const GameTileTypes tile_type = static_cast<GameTileTypes>(val);
      game_tiles[current_pos] = tile_type;
      
      if(tile_type == GameTileTypes::PADDLE) {
         paddle_x = current_pos.x;
      } else if(tile_type == GameTileTypes::BALL) {
         ball_x = current_pos.x;
      }
   }

 
   output_handler = std::bind(
      &ArcadeGameIO::handle_x_tile_position_output,
      this,
      std::placeholders::_1
   );
}
