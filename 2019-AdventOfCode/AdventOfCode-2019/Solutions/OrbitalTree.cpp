#include <sstream>

#include "OrbitalTree.h"

std::istream& operator>>(std::istream& str, OrbitInputSpec& spec) {
   std::string tmp;
   str >> tmp;

   auto sep_pos = tmp.find(ORBIT_SEP);
   spec.base = tmp.substr(0, sep_pos);
   spec.orbiter = tmp.substr(sep_pos + 1);
   return str;
}

std::ostream& operator<<(std::ostream& str, const OrbitInputSpec& spec) {
   str << "OrbitSpec { base: " << spec.base << "  orbiter: " << spec.orbiter << "}\n";
   return str;
}

OrbitalNodes create_orbital_tree(std::istream& str) {

   OrbitalNodes orbital_tree;
   OrbitInputSpec spec;
   while(str >> spec) {
      auto base_iter = orbital_tree.find(spec.base);
      if(base_iter == orbital_tree.end()) {
         auto node = std::make_shared<TreeNode>(TreeNode{spec.base, nullptr});
         orbital_tree.insert({spec.base, node});
         base_iter = orbital_tree.find(spec.base); // todo: look at storing insert result.
      }

      auto orbit_iter = orbital_tree.find(spec.orbiter);
      if(orbit_iter == orbital_tree.end()) {
         auto node = std::make_shared<TreeNode>(TreeNode{spec.orbiter, nullptr});
         orbital_tree.insert({spec.orbiter, node});
         orbit_iter = orbital_tree.find(spec.orbiter); // TODO look at insert result;
      }

      (base_iter->second)->children.push_back((orbit_iter->second));
      (orbit_iter->second)->parent = (base_iter->second).get();
   }

   return orbital_tree;
}

void dfs_generate_node_levels(const TreeNode& node, NodeLevels& node_levels, int level) {

   for(const auto kid : node.children) {
      dfs_generate_node_levels(*kid, node_levels, level + 1);
   }

   node_levels.push_back({node.value, level});
}

// DOnt include the starting node as thats "you" not your orbital
std::vector<TreeNode*> get_orbital_path(const OrbitalNodes& orbits, const std::string& starting_node) {
   std::vector<TreeNode*> path;
   auto node_iter = orbits.find(starting_node);
   if(node_iter == orbits.end()) {
      path;
   }

   TreeNode* node = node_iter->second.get();
   while(node->parent != nullptr) {
      node = node->parent;
      //path = node->value + path;
      path.push_back(node);
   }

   std::reverse(path.begin(), path.end());
   return path;
}
