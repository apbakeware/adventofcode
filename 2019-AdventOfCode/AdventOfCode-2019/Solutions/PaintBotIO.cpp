#include <algorithm>
#include <iostream>
#include <numeric>
#include "PaintBotIO.h"

namespace {

const std::vector<cartesian::Point2> ORIENTATION_OFFSETS = {
   {0,1},  // up
   {1,0},  // right
   {0,-1}, // down
   {-1,0}  // left
};

}


PaintBotIO::PaintBotIO(PaintColor starting_color) :
   starting_color(starting_color),
   orientation_index(0),
   bot_location({0,0}) {

   current_output_handler = std::bind(
      &PaintBotIO::paint_output_handler, 
      this,
      std::placeholders::_1
   );
}

bool PaintBotIO::input_available() const {
   return true;
}

IntcodeType PaintBotIO::get_intput() {
   const PaintColor color = 
      painted_sections.empty() ? starting_color : get_paint_color_under_bot();
   
   return color == PaintColor::WHITE ? 1 : 0;
}

void PaintBotIO::handle_output(IntcodeType output) {
   current_output_handler(output);
}

int PaintBotIO::number_of_painted_squares() const {
   return painted_sections.size();
}

std::pair<std::string, int> PaintBotIO::render_painting() const {

   cartesian::Point2 lbound = {
      std::numeric_limits<int>::max(),
      std::numeric_limits<int>::max()
   };
   cartesian::Point2 ubound = {
      std::numeric_limits<int>::min(),
      std::numeric_limits<int>::min()
   };

   for(auto section : painted_sections) {
      const cartesian::Point2 &pt = section.first;
      
      lbound.x = std::min(lbound.x, pt.x);
      lbound.y = std::min(lbound.y, pt.y);

      ubound.x = std::max(ubound.x, pt.x);
      ubound.y = std::max(ubound.y, pt.y);
   }

   std::string line;
   for(int y = ubound.y; y >= lbound.y; --y) {
      for(int x = ubound.x; x >= lbound.x; --x) {
         const cartesian::Point2 pt = {x,y};
         const auto iter = painted_sections.find(pt);
         char paint = ' ';
         if(iter != painted_sections.end() && iter->second == PaintColor::WHITE) {
            paint = '#';
         }
         line.append(1,paint);
      }
   }

   const auto result = std::make_pair(line, ubound.x - lbound.x + 1);
   return result;
}

void PaintBotIO::paint_output_handler(IntcodeType output) {

   const PaintColor paint = output == 1 ? PaintColor::WHITE : PaintColor::BLACK;

   painted_sections[bot_location] = paint;

   current_output_handler = std::bind(
      &PaintBotIO::orient_output_handler,
      this,
      std::placeholders::_1
   );
}

void PaintBotIO::orient_output_handler(IntcodeType output) {
   
   if(output == 1) {
      rotate_left_and_advance();
   } else if(output == 0) {
      rotate_right_and_advance();
   } else {
      throw std::runtime_error("Unknown orientation command");
   }

   current_output_handler = std::bind(
      &PaintBotIO::paint_output_handler,
      this,
      std::placeholders::_1
   );
}

void PaintBotIO::rotate_right_and_advance() {
   ++orientation_index;
   if(orientation_index == ORIENTATION_OFFSETS.size()) {
      orientation_index = 0;
   }

   bot_location = bot_location + ORIENTATION_OFFSETS[orientation_index];
}

void PaintBotIO::rotate_left_and_advance() {
   if(orientation_index == 0 ) {
      orientation_index = ORIENTATION_OFFSETS.size();
   }
   --orientation_index;

   bot_location = bot_location + ORIENTATION_OFFSETS[orientation_index];
}

PaintColor PaintBotIO::get_paint_color_under_bot() const {

   const auto iter = painted_sections.find(bot_location);
   if(iter == painted_sections.end()) {
      return PaintColor::BLACK;
   }

   return iter->second;
}
