#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <limits>
#include <numeric>
#include "O2RepairDroid.h" 

#include <chrono>
#include <thread>

namespace {

typedef std::unordered_map<cartesian::Point2, int, cartesian::cantor_pairing> CountByLocation;

std::unordered_map<RepairBotSensing, std::string> SENSING_RESULT_CHAR = {
   {RepairBotSensing::WALL, "#"},
   {RepairBotSensing::EXPLORED, "."},
   {RepairBotSensing::FOUND_GOAL, "X"},
   {RepairBotSensing::MAP_START, "*"},
   {RepairBotSensing::UNEXPLORED, " "}
};

std::unordered_map<IntcodeType, std::string> MOVEMENT_DIRECTION_NAME = {
   {1, "NORTH"}, {2, "SOUTH"}, {3, "WEST"}, {4, "EAST"}
};

std::unordered_map<cartesian::Point2, IntcodeType, cartesian::cantor_pairing> CARTESIAN_TO_INTCODE_INPUT = {
   {{0,1},  1},
   {{0,-1}, 2},
   {{-1,0}, 3},
   {{1,0},  4},
};

std::vector<RepairBotSensing> OUTPUT_TO_SENSING = {
   RepairBotSensing::WALL,
   RepairBotSensing::EXPLORED,
   RepairBotSensing::FOUND_GOAL
};

void compute_min_distant_to_recurse(
   ExplorationMap& exploration_map,
   CountByLocation& count_map,
   const cartesian::Point2 location,
   int steps) {

   auto& exploration = exploration_map.at(location);

   if(exploration.discovered() == RepairBotSensing::WALL) {
      return;
   }

   int nsteps = steps + 1;
   count_map.at(location) = steps;
   while(!exploration.exploration_completed()) {
      const auto nloc = location + exploration.next_exploration();
      if(count_map.at(nloc) > nsteps) {
         compute_min_distant_to_recurse(
            exploration_map,
            count_map,
            nloc,
            nsteps
         );
      }
   }
}

CountByLocation compute_all_min_distances(
   ExplorationMap& exploration_map, 
   const cartesian::Point2& location) {
   
   CountByLocation min_moves_to_location;
   for(auto& exp_record : exploration_map) {
      exp_record.second.reset_keeping_discovery();
      min_moves_to_location[exp_record.first] = 10000; // std::numeric_limits<int>::max();
   }

   compute_min_distant_to_recurse(
      exploration_map,
      min_moves_to_location,
      location,
      1
   );

   return min_moves_to_location;
}



void render_bfs(std::ostream& str, ExplorationMap& graph, int min_x, int max_x, int min_y, int max_y, int cnt) {

   system("cls");

   str << "Exploration Count: " << cnt
      << "\n---------------------------------------------------------------\n";
   for(int y = max_y; y >= min_y; --y) {
      for(int x = min_x; x <= max_x; ++x) {
         const cartesian::Point2 coord = {x,y};

         const auto iter = graph.find(coord);
         if(iter == graph.end()) {
            str << " ";
         } else {
            if(is_occupiable(iter->second.discovered())) {
               if(iter->second.has_been_explored()) {
                  str << "O";
               } else {
                  str << " ";
               }
            } else {
               str << "#";
            }
         }

      }
      str << "\n";
   }

   str << std::endl;
   std::this_thread::sleep_for(std::chrono::milliseconds(200));
}


int visit_all_bfs(
   CountByLocation& location_count,
   ExplorationMap& exploration_map,
   std::vector<cartesian::Point2>& locations,
   int count) {

   if(locations.empty()) {
      return count - 1;
   }

   std::vector<cartesian::Point2> next_locations;
   for(const auto& location : locations) {
      auto& exploration_node = exploration_map.at(location);
      
      if(exploration_node.has_been_explored()) {
         continue;
      }

      if(!is_occupiable(exploration_node.discovered())) {
         continue;
      }

      exploration_node.mark_explored();
      location_count.at(location) = count;

      for(const auto& move : QUAD_ORIDINAL_DIRECTIONS) {
         next_locations.push_back(location + move);
      }
    }

   return visit_all_bfs(location_count, exploration_map, next_locations, count + 1);
   
}

// breadth first



}

bool bot_moved_locations(RepairBotSensing sensing_result) {
   return sensing_result != RepairBotSensing::WALL;
}

bool is_occupiable(RepairBotSensing value) {
   return value != RepairBotSensing::WALL;
}

std::ostream& operator<<(std::ostream& str, const RepairBotSensing val) {
   str << SENSING_RESULT_CHAR[val];
   return str;
}


O2RepairDroid::O2RepairDroid() 
   : bot_location({0,0}), o2_location({-1,-1}), movement_dir({0,0}),
     min_x(std::numeric_limits<int>::max()),
     max_x(std::numeric_limits<int>::min()),
     min_y(std::numeric_limits<int>::max()),
     max_y(std::numeric_limits<int>::min()),
     repair_point_found(false) {

   QuadExplorationRecord origin_location;
   origin_location.discover(RepairBotSensing::MAP_START);
   exploration_map[bot_location] = origin_location;

}

bool O2RepairDroid::input_available() const {

   if(walkback_stack.empty() && exploration_map.at(bot_location).exploration_completed()) {
      return false;
   }

   const auto& current_exploration_record = exploration_map.at(bot_location);
   return true;
}

// Not sure this exploration will get to all the rooms....may 
// have to "move" across a bunch of unexplored areas.

IntcodeType O2RepairDroid::get_intput() {
   IntcodeType input = -1;
   auto& current_exploration_record = exploration_map.at(bot_location);
   was_walkback = false;
   cartesian::Point2 next_loc;

   bool coordinate_found = false;
   while(!current_exploration_record.exploration_completed()) {
      movement_dir = current_exploration_record.next_exploration();
      next_loc = bot_location + movement_dir;
      if(!exploration_map[next_loc].has_been_explored()) {
         coordinate_found = true;
         break;
      }
   }

   if(!coordinate_found && !walkback_stack.empty()) {
      was_walkback = true;
      movement_dir = walkback_stack.top();
      walkback_stack.pop();
   }

   exploring_location = bot_location + movement_dir;
   input = CARTESIAN_TO_INTCODE_INPUT.at(movement_dir);

   return input;
}

void O2RepairDroid::handle_output(IntcodeType output) {
   static int render_ctn = 1000;
   const RepairBotSensing sensing_result = OUTPUT_TO_SENSING.at(static_cast<size_t>(output));
   auto & exploration_record = exploration_map[exploring_location];

   min_x = std::min(min_x, exploring_location.x);
   max_x = std::max(max_x, exploring_location.x);
   min_y = std::min(min_y, exploring_location.y);
   max_y = std::max(max_y, exploring_location.y);

   if(output == 2) {
      std::cout << "GOAL\n";
      repair_point_found = true;
      o2_location = bot_location;
   }
   
   exploration_record.discover(sensing_result);
   if(bot_moved_locations(sensing_result)) {
      bot_location = exploring_location;

      if(!was_walkback) {
         const auto walkback_dir = cartesian::Point2() - movement_dir;
         walkback_stack.push(walkback_dir);
      }
   }
}

int O2RepairDroid::get_min_steps_to_repair_site() {

   if(!repair_point_found) {
      throw std::runtime_error("Repair point not found yet");
   }

   const cartesian::Point2 location = {0,0};
   const CountByLocation& min_moves_to_location = compute_all_min_distances(exploration_map, location);
   return min_moves_to_location.at(o2_location);
}

int O2RepairDroid::get_steps_to_fill_room_with_02() {

   CountByLocation min_moves_to_location;
   std::vector<cartesian::Point2> reachable = {o2_location};

   for(auto& exp_record : exploration_map) {
      exp_record.second.reset_keeping_discovery();
      min_moves_to_location[exp_record.first] = std::numeric_limits<int>::max();
   }

   return visit_all_bfs(
      min_moves_to_location,
      exploration_map,
      reachable,
      0
   );
}

void O2RepairDroid::render(std::ostream& str) const {

   str << "Walkback Stack Size: " << walkback_stack.size()
      << "\n---------------------------------------------------------------\n";
   for(int y = max_y; y >= min_y; --y) {
      for(int x = min_x; x <= max_x; ++x) {
         const cartesian::Point2 coord = {x,y};

         if(coord == bot_location) {
            str << "@";
         } else if(coord.x == 0 && coord.y == 0) {
            str << "O";
         } else {

            const auto iter = exploration_map.find(coord);
            const auto exp = iter != exploration_map.end() ?
               (iter->second).discovered() : RepairBotSensing::UNEXPLORED;

            str << exp;
         }
      }
      str << "\n";
   }

   str << "\n\n";
}
