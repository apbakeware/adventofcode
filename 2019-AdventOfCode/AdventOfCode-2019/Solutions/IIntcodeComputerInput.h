#pragma once

#include "IntcodeTypes.h"

class IIntcodeComputerInput {
public:

   virtual ~IIntcodeComputerInput() = default;

   virtual bool input_available() const = 0;

   virtual IntcodeType get_intput() = 0;

};