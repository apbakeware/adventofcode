#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <string>
#include <sstream>
#include <vector>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "AstroidField.h"
#include "Cartesian.h"
#include "DataLoader.h"

namespace {

class Day10Prob1 : public IProblem {
public:

   virtual ~Day10Prob1() = default;

   virtual std::string name() const override {
      return "Day 10 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day10Prob2 : public IProblem {
public:

   virtual ~Day10Prob2() = default;

   virtual std::string name() const override {
      return "Day 10 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

ProblemFactoryRegisterer<Day10Prob1> _problem1("Day10Prob1");
ProblemFactoryRegisterer<Day10Prob2> _problem2("Day10Prob2");

ISolution* Day10Prob1::execute(std::istream& data_set) {

   const auto lines = load_data<std::string>(data_set);
   const auto points = cartesian::create_points_from_grid(lines, '#');
   auto best_los = cartesian::find_point_with_greatest_los_count(points);

   std::cout << "Coordinate of best point: " << std::get<0>(best_los) << "\n";

   return new SingleValueSolution<int>(std::get<1>(best_los));
}


ISolution* Day10Prob2::execute(std::istream& data_set) {
   const cartesian::Point2 LASER = {22,28};
   const auto lines = load_data<std::string>(data_set);
   const auto points = cartesian::create_points_from_grid(lines, '#');
   const auto& laze_seq = astroid_field::perform_lasing_operations
      (points, LASER, 200);
   const auto& last_laz = laze_seq.back();
   
   return new SingleValueSolution<int>(last_laz.x * 100 + last_laz.y);
}

}
