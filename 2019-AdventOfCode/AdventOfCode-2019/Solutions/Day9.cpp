#include <iostream>
#include <string>
#include <sstream>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "IntcodeComputer.h"
#include "NullIntcodeComputerOutput.h"
#include "IntcodeComputerInputsList.h"

namespace {

class Day9Prob1 : public IProblem {
public:

   virtual ~Day9Prob1() = default;

   virtual std::string name() const override {
      return "Day 09 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day9Prob2 : public IProblem {
public:

   virtual ~Day9Prob2() = default;

   virtual std::string name() const override {
      return "Day 09 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

ProblemFactoryRegisterer<Day9Prob1> _problem1("Day9Prob1");
ProblemFactoryRegisterer<Day9Prob2> _problem2("Day9Prob2");

ISolution* Day9Prob1::execute(std::istream& data_set) {

   std::vector<IntcodeType> input_data(100000, 0);
   auto piter = input_data.begin();
   std::string token;
   while(std::getline(data_set, token, ',')) {
      std::istringstream istr(token);
      istr >> *piter;
      ++piter;
   }

   NullIntcodeComputerOutput output_handler;
   IntcodeComputerInputsList input_provider({1});

   IntcodeComputer computer(input_data, input_provider, output_handler);
   computer.execute();
   return new SingleValueSolution<IntcodeType>(computer.get_outputs().front());
}


ISolution* Day9Prob2::execute(std::istream& data_set) {
   std::vector<IntcodeType> input_data(100000, 0);
   auto piter = input_data.begin();
   std::string token;
   while(std::getline(data_set, token, ',')) {
      std::istringstream istr(token);
      istr >> *piter;
      ++piter;
   }

   NullIntcodeComputerOutput output_handler;
   IntcodeComputerInputsList input_provider({2});

   IntcodeComputer computer(input_data, input_provider, output_handler);
   computer.execute();
   return new SingleValueSolution<IntcodeType>(computer.get_outputs().front());
}

}