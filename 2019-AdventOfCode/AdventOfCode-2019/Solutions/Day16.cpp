#include <algorithm>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "FFT.h"

namespace {

class Day16Prob1 : public IProblem {
public:

   virtual ~Day16Prob1() = default;

   virtual std::string name() const override {
      return "Day 16 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day16Prob2 : public IProblem {
public:

   virtual ~Day16Prob2() = default;

   virtual std::string name() const override {
      return "Day 16 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

ProblemFactoryRegisterer<Day16Prob1> _problem1("Day16Prob1");
ProblemFactoryRegisterer<Day16Prob2> _problem2("Day16Prob2");


ISolution* Day16Prob1::execute(std::istream& data_set) {

   std::string input;
   data_set >> input;

   std::vector<int> fft_input_values(input.size());
   FFT fft(input.size());
   auto iter = fft_input_values.begin();
   
   for(size_t idx = 0; idx < input.size(); ++idx) {
      const std::string & sstr = input.substr(idx, 1);
      *iter = std::stoi(sstr);
      ++iter;
   }

   const auto& fft_result = fft.apply_fft(fft_input_values, 100);

   std::string first_8_digits;
   for(size_t idx = 0; idx < 8; ++idx) {
      first_8_digits.append(std::to_string(fft_result[idx]));
   }

   return new SingleValueSolution<std::string>(first_8_digits);
}


ISolution* Day16Prob2::execute(std::istream& data_set) {

   return new SingleValueSolution<std::string>("?");
}

}
