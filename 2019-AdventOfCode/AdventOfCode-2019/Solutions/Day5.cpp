#include <algorithm>
#include <numeric>
#include <string>
#include <sstream>
#include <unordered_set>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "VectorSolution.h"
#include "DataLoader.h"
#include "IntcodeComputer.h"
#include "NullIntcodeComputerOutput.h"
#include "IntcodeComputerInputsList.h"


namespace {

class Day5Prob1 : public IProblem {
public:

   virtual ~Day5Prob1() = default;

   virtual std::string name() const override {
      return "Day 05 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day5Prob2 : public IProblem {
public:

   virtual ~Day5Prob2() = default;

   virtual std::string name() const override {
      return "Day 05 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};


ProblemFactoryRegisterer<Day5Prob1> _problem1("Day5Prob1");
ProblemFactoryRegisterer<Day5Prob2> _problem2("Day5Prob2");


ISolution* Day5Prob1::execute(std::istream& data_set) {

   std::vector<IntcodeType> input_data;
   std::string token;
   while(std::getline(data_set, token, ',')) {
      input_data.push_back(atoi(token.c_str()));
   }
   
   NullIntcodeComputerOutput output_handler;
   IntcodeComputerInputsList input_provider({1});
   
   IntcodeComputer computer(input_data, input_provider, output_handler);
   computer.execute();
   return new VectorSolution<IntcodeType>(computer.get_outputs());
}


ISolution* Day5Prob2::execute(std::istream& data_set) {
   std::vector<IntcodeType> input_data;
   std::string token;
   while(std::getline(data_set, token, ',')) {
      input_data.push_back(atoi(token.c_str()));
   }

   // Because parameter 3 params may exceed capacity of vector....how to handle in intcode.
   // Changed on final intcode
   input_data.push_back(0);
   input_data.push_back(0);
   input_data.push_back(0);

   NullIntcodeComputerOutput output_handler;
   IntcodeComputerInputsList input_provider({5});
   IntcodeComputer computer(input_data, input_provider, output_handler);
   computer.execute();
   return new VectorSolution<IntcodeType>(computer.get_outputs());
}

}
