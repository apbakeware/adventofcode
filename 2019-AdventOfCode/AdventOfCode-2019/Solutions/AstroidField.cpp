#include <algorithm>
#include <functional>
#include <iterator>
#include <iostream>
#include "AstroidField.h"
#include "Polar.h"


namespace astroid_field {

struct Lazing_Record {
   cartesian::Point2 cpt;
   polar::Point2 ppt;
   bool lazed;
};

std::ostream& operator<<(std::ostream& str, const Lazing_Record& rec) {
   str << "Lazing_Record {\n\tcpt: " << rec.cpt
      << "\n\tppt: " << rec.ppt
      << "\n\tlazed: " << rec.lazed 
      << "\n}";

   return str;
}

std::vector<Lazing_Record> create_lazing_records
(const std::vector<cartesian::Point2>& astroid_field) {

   const cartesian::Point2 ORIGIN = {0,0};
   std::vector<Lazing_Record> ppoints(astroid_field.size());
   std::transform(
      astroid_field.begin(),
      astroid_field.end(),
      ppoints.begin(),
      [=](const auto& point) {
         // In the xy system, let the point P have polar coordinates{\displaystyle(r,\alpha)} (r, \alpha).Then, in the x'y' system, P will have polar coordinates{\displaystyle(r,\alpha - \theta)} (r, \alpha - \theta) .

         const auto polar_pt = polar::convert_from(ORIGIN, point);
         return Lazing_Record{
            point,
            polar::convert_from(ORIGIN, point, -90.0),
            false
         };
      }
   );

   return ppoints;
}


std::vector<cartesian::Point2> perform_lasing_operations
(const std::vector<cartesian::Point2>& astroid_field,
 const cartesian::Point2& laser_pos,
 int number_of_lases) {

   std::vector<cartesian::Point2> laze_sequence;
   std::vector<cartesian::Point2> translated_astroid_field(astroid_field.size());

   // Translate coordinates so the laser is at 0,0
   std::transform(
      astroid_field.begin(),
      astroid_field.end(),
      translated_astroid_field.begin(),
      std::bind(std::minus<cartesian::Point2>(), std::placeholders::_1, laser_pos)
   );

  
   // Remove the astroid with the laser.
   const cartesian::Point2 ORIGIN = {0,0};
   translated_astroid_field.erase(
      std::remove(translated_astroid_field.begin(), 
                  translated_astroid_field.end(), 
                  ORIGIN),
      translated_astroid_field.end()
   );

   // Create polar coordinates
   // Rotate them to make 'up' angle 0 keep it positive angles
   std::vector<Lazing_Record> ppoints = create_lazing_records
      (translated_astroid_field);

   std::sort(
      ppoints.begin(),
      ppoints.end(),
      [](const auto& p1, const auto& p2) {
         return std::tie(p1.ppt.angle, p1.ppt.magnitude) < std::tie(p2.ppt.angle, p2.ppt.magnitude);
      }
   );

   //std::copy(ppoints.begin(),
   //   ppoints.end(),
   //   std::ostream_iterator<Lazing_Record>(std::cout, "\n")
   //);

   auto iter = ppoints.begin();
   double lazed_angle = 0;
   for(int count = number_of_lases; count > 0; --count) {
      while(iter->ppt.angle == lazed_angle || iter->lazed) {
         ++iter;
      }
      const cartesian::Point2 lased_astroid_position_raw = (iter->cpt) + laser_pos;
      laze_sequence.push_back(lased_astroid_position_raw);

//      std::cout << "Lazed: " << lased_astroid_position_raw << " @ angle: " << iter->ppt.angle << "\n";
      iter->lazed = true;
      lazed_angle = iter->ppt.angle;
   }
   return laze_sequence;
}

}