#include "IntcodeTypes.h"

std::string to_string(Instruction instruction) {
   std::string result;
   switch(instruction) {
   case Instruction::ADD:
      result = "ADD";
      break;
   case Instruction::MULTIPLY:
      result = "MULTIPLY";
      break;
   case Instruction::INPUT:
      result = "INPUT";
      break;
   case Instruction::OUTPUT:
      result = "OUTPUT";
      break;
   case Instruction::JUMP_IF_TRUE:
      result = "JIT";
      break;
   case Instruction::JUMP_IF_FALSE:
      result = "JIF";
      break;
   case Instruction::LESS_THAN:
      result = "LESS-THAN";
      break;
   case Instruction::EQUAL:
      result = "EQUAL";
      break;
   case Instruction::REL_BASE:
      result = "REL_BASE";
      break;
   case Instruction::TERMINATE:
      result = "TERM";
      break;
   }
   return result;
}

std::string to_string(OpcodeModes mode) {
   std::string result = "UNKNOWN";
   switch(mode) {
   case OpcodeModes::POSITION:
      result = "POSITION";
      break;
   case OpcodeModes::IMMEDIATE:
      result = "IMMEDIATE";
      break;
   case OpcodeModes::RELATIVE:
      result = "RELATIVE";
      break;
   }

   return result;
}
