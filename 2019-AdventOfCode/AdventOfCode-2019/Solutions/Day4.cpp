#include <algorithm>
#include <numeric>
#include <string>
#include <sstream>
#include <tuple>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"


namespace {

class Day4Prob1 : public IProblem {
public:

   virtual ~Day4Prob1() = default;

   virtual std::string name() const override {
      return "Day 04 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day4Prob2 : public IProblem {
public:

   virtual ~Day4Prob2() = default;

   virtual std::string name() const override {
      return "Day 04 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};


ProblemFactoryRegisterer<Day4Prob1> _problem1("Day4Prob1");
ProblemFactoryRegisterer<Day4Prob2> _problem2("Day4Prob2");

typedef std::tuple<int, int, int, int, int, int> Digits;

Digits make_digits(int value) {
   return std::make_tuple(
      value / 100000 % 10,
      value / 10000 % 10,
      value / 1000 % 10,
      value / 100 % 10,
      value / 10 % 10,
      value % 10
   );
}

bool digits_non_decreasing(const Digits& digits) {
   return
      std::get<0>(digits) <= std::get<1>(digits) &&
      std::get<1>(digits) <= std::get<2>(digits) &&
      std::get<2>(digits) <= std::get<3>(digits) &&
      std::get<3>(digits) <= std::get<4>(digits) &&
      std::get<4>(digits) <= std::get<5>(digits);
}

bool has_sequential_digit(const Digits& digits) {
   return
      std::get<0>(digits) == std::get<1>(digits) ||
      std::get<1>(digits) == std::get<2>(digits) ||
      std::get<2>(digits) == std::get<3>(digits) ||
      std::get<3>(digits) == std::get<4>(digits) ||
      std::get<4>(digits) == std::get<5>(digits);
}

bool repeat_digit_not_part_of_larger_group(int value) {
   std::ostringstream str;
   str << value;
   const auto linear = str.str();
   auto iter = linear.begin();
   while(iter != linear.end()) {
      int span = 1;
      auto prev = *iter;
      while(++iter != linear.end()) {
         if(*iter == prev) {
            ++span;
         } else {
            break;
         }
      }

      if(span == 2) {
         return true;
      }
   }

   return false;
}


ISolution* Day4Prob1::execute(std::istream& data_set) {

   const int min = 165432;
   const int max = 707912;

   int count = 0;
   for(int val = min; val <= max; ++val) {
      const auto digits = make_digits(val);
      if( digits_non_decreasing(digits) && has_sequential_digit(digits)) {
         ++count;
      }
   }

   return new SingleValueSolution<int>(count);
}


ISolution* Day4Prob2::execute(std::istream& data_set) {

   const int min = 165432;
   const int max = 707912;

   int count = 0;
   for(int val = min; val <= max; ++val) {
      const auto digits = make_digits(val);
      if(digits_non_decreasing(digits) && repeat_digit_not_part_of_larger_group(val)) {
         ++count;
      }
   }

   return new SingleValueSolution<int>(count);
}

}
