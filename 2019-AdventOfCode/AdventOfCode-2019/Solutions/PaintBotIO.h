#pragma once

#include <functional>
#include <map>
#include <iosfwd>
#include <string>
#include <utility>
#include "IIntcodeComputerInput.h"
#include "IIntcodeComputerOutput.h"
#include "Cartesian.h"

enum class PaintColor {
   BLACK,
   WHITE
};

// Paint bot operations in support of Day 11 to run the Intcode Computer
// Input / Output
class PaintBotIO : public IIntcodeComputerInput, public IIntcodeComputerOutput {
public:

   explicit PaintBotIO(PaintColor starting_color);

   virtual ~PaintBotIO() = default;

   virtual bool input_available() const;

   virtual IntcodeType get_intput();

   virtual void handle_output(IntcodeType output);

   int number_of_painted_squares() const;

   // whole string, number of chars per line
   std::pair<std::string, int> render_painting() const;

private:

   void paint_output_handler(IntcodeType output);
   void orient_output_handler(IntcodeType output);

   void rotate_right_and_advance();
   void rotate_left_and_advance();

   PaintColor get_paint_color_under_bot() const;

   const PaintColor starting_color;
   size_t orientation_index;
   cartesian::Point2 bot_location;

   std::function<void(IntcodeType)> current_output_handler;

   // want has, but need to write a hash function for Pair
   std::map<cartesian::Point2, PaintColor> painted_sections;
   
};

