#pragma once
#include <iosfwd>
#include <string>
#include <tuple>
#include <unordered_map>

class ReactionEquation {
public:

   ReactionEquation() = default;

   // defaults to 1 unit
   explicit ReactionEquation(const std::string& name);
   
   ReactionEquation(const std::string& name, long long unit_quantity);

   void update_unit_quantity(long long quantity);

   std::string get_name() const;

   long long get_unit_quantity() const;

   long long get_total_units_produced() const;

   long long get_units_available() const;

   void add_ingredient(ReactionEquation& ingredient, long long quantity_required);

   bool can_satisfy_request(long long units_requested) const;

   // TODO need to produce children
   void produce_to_satisfy(long long units_required);

   // all or nothing consumed
   bool consume_units(long long quantity);

   friend std::ostream& operator<<(std::ostream& str, const ReactionEquation& obj);

private:

   std::string name;
   long long unit_quantity; // Number of units produced by 1 set of ingredients
   long long total_units_produced;
   long long units_available;

   typedef std::tuple<ReactionEquation*, long long> Ingredient_Record;

   std::unordered_map<std::string, Ingredient_Record> ingredients;

};

