#include <stdexcept>
#include "IParameterProxy.h"


class PositionModeParameter : public IParameterProxy {
public:

   PositionModeParameter(IntcodeProgram& program, IntcodeProgram::iterator piter)
      : program(program), piter(piter) {

   }

   virtual ~PositionModeParameter() = default;

   virtual IntcodeType get() const {
      const auto idx = static_cast<IntcodeProgram::size_type>(*piter);
      return program.at(idx);
   }

   virtual void set(IntcodeType val) {
      const auto idx = static_cast<IntcodeProgram::size_type>(*piter);
      program.at(idx) = val;
   }

private:
   IntcodeProgram& program;
   IntcodeProgram::iterator piter;
};

class ImmediateModeParameter : public IParameterProxy {
public:

   ImmediateModeParameter(IntcodeProgram& program, IntcodeProgram::iterator piter)
      : program(program), piter(piter) {

   }

   virtual ~ImmediateModeParameter() = default;

   virtual IntcodeType get() const {
      return *piter;
   }

   virtual void set(IntcodeType val) {
      throw std::runtime_error("Cannot set immediate mode parameter");
   }

private:
   IntcodeProgram& program;
   IntcodeProgram::iterator piter;
};

class RelativeModeParameter : public IParameterProxy {
public:

   RelativeModeParameter(IntcodeProgram& program, IntcodeProgram::iterator piter, IntcodeType rel_base)
      : program(program), piter(piter), rel_base(rel_base) {

   }

   virtual ~RelativeModeParameter() = default;

   virtual IntcodeType get() const {
      const auto idx = static_cast<IntcodeProgram::size_type>(rel_base + *piter);
      return program.at(idx);
   }

   virtual void set(IntcodeType val) {
      const auto idx = static_cast<IntcodeProgram::size_type>(rel_base + *piter);
      program.at(idx) = val;
   }

private:
   IntcodeProgram& program;
   IntcodeProgram::iterator piter;
   IntcodeType rel_base;
};



IParameterProxy* create_parameter
(OpcodeModes mode, IntcodeProgram& program, ProgramPtr pptr, IntcodeType rel_base) {

   switch(mode) {
   case OpcodeModes::POSITION:
      return new PositionModeParameter(program, pptr);
      break;

   case OpcodeModes::IMMEDIATE:
      return new ImmediateModeParameter(program, pptr);
      break;

   case OpcodeModes::RELATIVE:
      return new RelativeModeParameter(program, pptr, rel_base);
      break;
   }

   return nullptr;
}