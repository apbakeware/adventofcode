#include "Cartesian.h"
#include <algorithm>
#include <cmath>
#include <iostream>



namespace cartesian {

size_t cantor_pairing::operator()(const cartesian::Point2& point) const {
   const int a = point.x;
   const int b = point.y;
   return (a + b) * (a + b + 1) / 2 + a;
}

Point2 rotate(const Point2& point, double angle) {
   const double sina = std::sin(angle);
   const double cosa = std::cos(angle);
   const Point2 rotated = { 
      static_cast<int>(point.x * cosa + point.y * sina),
      static_cast<int>(-1 * point.y * sina + point.x * cosa)
   };
   return rotated;
}
bool operator==(const Point2& lhs, const Point2& rhs) {
   return std::tie(lhs.x, lhs.y) == std::tie(rhs.x, rhs.y);
}

bool operator<(const Point2& lhs, const Point2& rhs) {
   return std::tie(lhs.x, lhs.y) < std::tie(rhs.x, rhs.y);
}

Point2 operator-(const Point2& lhs, const Point2& rhs) {
   return {lhs.x - rhs.x, lhs.y - rhs.y};
}

Point2 operator+(const Point2& lhs, const Point2& rhs) {
   return {lhs.x + rhs.x, lhs.y + rhs.y};
}

std::ostream& operator<<(std::ostream& str, const Point2& pt) {
   str << "{POINT -- x: " << pt.x << "  y: " << pt.y << "}";
   return str;
}

bool operator==(const Slope2& lhs, const Slope2& rhs)
{
   return std::tie(lhs.dx, lhs.dy, lhs.slope) == std::tie(rhs.dx, rhs.dy, rhs.slope);
}

Point3 operator+(const Point3& lhs, const Point3& rhs) {
   const Point3 result = {
      lhs.x + rhs.x,
      lhs.y + rhs.y,
      lhs.z + rhs.z
   };

   return result;
}

bool operator==(const Point3& lhs, const Point3& rhs) {
   return std::tie(lhs.x, lhs.y, lhs.z) == std::tie(rhs.x, rhs.y, rhs.z);
}

std::ostream& operator<<(std::ostream& str, const Point3& pt) {
   str << "{POINT -- x: " << pt.x << "  y: " << pt.y << "  z: " << pt.z << "}";
   return str;
}

double delta_x(const Point2& p1, const Point2& p2) {
   return static_cast<double>(p2.x) - static_cast<double>(p1.x);
}

double delta_y(const Point2& p1, const Point2& p2) {
   return static_cast<double>(p2.y) - static_cast<double>(p1.y);
}

double distance(const Point2& p1, const Point2& p2) {
   const double dx = delta_x(p1, p2);
   const double dy = delta_y(p1, p2);
   return sqrt(dx * dx + dy * dy);
}

Slope2 slope(const Point2& p1, const Point2& p2) {
   const double dx = delta_x(p1, p2);
   const double dy = delta_y(p1, p2);
   return { dx, dy, dy / dx };
}

bool are_collinear(const Point2& p1, const Point2& p2, const Point2& p3) {
   const Slope2 s1 = slope(p1, p2);
   const Slope2 s2 = slope(p2, p3);
   return s1.slope == s2.slope;
}

std::vector<DistanceToPoint> points_sorted_by_distance_from(const Point2& ref, const std::vector<Point2>& points) {

   std::vector<DistanceToPoint> points_by_distance(points.size());

   std::transform(
      points.begin(),
      points.end(),
      points_by_distance.begin(),
      [=](const auto& p1) {
         return std::make_tuple(p1, distance(ref, p1));
      }
   );

   std::sort(
      points_by_distance.begin(),
      points_by_distance.end(),
      [](const auto& a, const auto& b) {
         return std::get<1>(a) < std::get<1>(b);
      }
   );

   return points_by_distance;
}

std::vector<Point2> get_los_points(const Point2& ref, const std::vector<DistanceToPoint>& points) {
   std::vector<Point2> los_points;
   const auto ME_IN_LIST = points.rend() - 1;

   // sorted ascending distance so go in reverse
   for(auto far_iter = points.rbegin(); far_iter != ME_IN_LIST; ++far_iter) {
      const auto far = std::get<0>(*far_iter);
      
      if(ref == far) {
         continue;
      }

      const Slope2 slope_far = slope(ref, far);
      bool has_los = true;
      for(auto near_iter = far_iter + 1; near_iter != ME_IN_LIST; ++near_iter) {
         const auto near = std::get<0>(*near_iter);
         
         if (ref == near) {
            continue;
         }

         const Slope2 slope_near = slope(ref, near);
         if (slope_far.slope == slope_near.slope) {

            const bool near_is_between_ref_and_far =
               std::signbit(slope_far.dx) == std::signbit(slope_near.dx) &&
               std::signbit(slope_far.dy) == std::signbit(slope_near.dy);

            if(near_is_between_ref_and_far) {
               has_los = false;
               break;
            }   
         }
      }

      if(has_los) {
         los_points.push_back(far);
      }
   }

   return los_points;
}

std::tuple<Point2, size_t> find_point_with_greatest_los_count(const std::vector<Point2>& points) {

   std::tuple<Point2, size_t> result = {{0,0}, std::numeric_limits<size_t>::min()};
   for(const auto& pt : points) {
      const auto ptd = points_sorted_by_distance_from(pt, points);
      const auto los_points = get_los_points(pt, ptd);

      if(los_points.size() > std::get<1>(result)) {
         result = std::make_pair(pt, los_points.size());
      }
   }

   return result;
}

std::vector<Point2> create_points_from_grid
(const std::vector<std::string>& lines, const char marker) {
   
   std::vector<Point2> points;
   int x = 0;
   int y = 0;
   for(const auto & line : lines) {
      for(const auto cell : line) {
         if(cell == marker) {
            points.push_back({x,y});
         }
         ++x;
      }
      ++y;
      x = 0;
   }

   return points;
}

}