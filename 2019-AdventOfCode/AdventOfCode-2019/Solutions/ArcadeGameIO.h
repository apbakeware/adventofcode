#pragma once

#include <functional>
#include <iosfwd>
#include <unordered_map>
#include "IIntcodeComputerInput.h"
#include "IIntcodeComputerOutput.h"
#include "Cartesian.h"

enum class GameTileTypes {
   EMPTY = 0,
   WALL = 1,
   BLOCK = 2,
   PADDLE = 3,
   BALL = 4
};

class ArcadeGameIO : public IIntcodeComputerInput, public IIntcodeComputerOutput {
public:

   ArcadeGameIO();

   virtual ~ArcadeGameIO() = default;

   virtual bool input_available() const;

   virtual IntcodeType get_intput();

   virtual void handle_output(IntcodeType output);

   int get_number_of_blocks() const;

   int get_score() const;

   void render(std::ostream& str) const;

private:

   void handle_x_tile_position_output(IntcodeType val);
   void handle_y_tile_position_output(IntcodeType val);
   void handle_tile_or_score_output(IntcodeType val);


   int max_x;
   int max_y;

   int score;

   int ball_x;
   int paddle_x;

   cartesian::Point2 current_pos;

   std::function<void(IntcodeType)> output_handler;

   std::unordered_map<cartesian::Point2, GameTileTypes, cartesian::cantor_pairing> game_tiles;
};

