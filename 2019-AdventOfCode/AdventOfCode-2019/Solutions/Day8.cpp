#include <algorithm>
#include <cstdlib>
#include <iterator>
#include <iostream>
#include <numeric>
#include <sstream>
#include <utility>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "SubstringLineResult.h"
#include "DataLoader.h"

namespace {

class Day8Prob1 : public IProblem {
public:

   virtual ~Day8Prob1() = default;

   virtual std::string name() const override {
      return "Day 08 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day8Prob2 : public IProblem {
public:

   virtual ~Day8Prob2() = default;

   virtual std::string name() const override {
      return "Day 08 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};


ProblemFactoryRegisterer<Day8Prob1> _problem1("Day8Prob1");
ProblemFactoryRegisterer<Day8Prob2> _problem2("Day8Prob2");

ISolution* Day8Prob1::execute(std::istream& data_set) {

   // Dont need pixels, just read in layser as 25 * 6 (grid) * 3 values pixel
   const size_t ROWS = 25;
   const size_t COLS = 6;

   int pixel = 0;
   std::vector<int> most_zero_layer(10,std::numeric_limits<int>::max());
   
   std::istream_iterator<char> data_iter(data_set);
   while(data_iter != std::istream_iterator<char>()) {

      std::vector<int> current(10, 0);
      for(size_t cnt = ROWS * COLS; cnt > 0; --cnt) {
         pixel = *data_iter - '0';
         ++current[pixel];
         ++data_iter;
      }

      if(current[0] < most_zero_layer[0]) {
         most_zero_layer.swap(current);
      }
   }

   return new SingleValueSolution<int>(most_zero_layer[1] * most_zero_layer[2]);
}


ISolution* Day8Prob2::execute(std::istream& data_set) {

   const int TRANSPARENT = 2;
   const size_t ROWS = 6;
   const size_t COLS = 25;
   const size_t LAYER_COUNT = ROWS * COLS;

   int pixel = 0;
   std::vector<int> rendering(LAYER_COUNT, TRANSPARENT);
   std::istream_iterator<char> data_iter(data_set);
   while(data_iter != std::istream_iterator<char>()) {

      for(size_t idx = 0; idx < LAYER_COUNT; ++idx) {
         pixel = *data_iter - '0';
         rendering[idx] = rendering[idx] == TRANSPARENT ? pixel : rendering[idx];
         ++data_iter;
      }
   }

   std::string output;
   std::transform(
      rendering.begin(),
      rendering.end(),
      std::back_inserter(output),
      [](auto val) {
         return val == 1 ? '#' : ' ';
      }
   );



   return new SubstringLineResult(output, COLS);
}

}