#pragma once

#include "IntcodeTypes.h"

class IParameterProxy {
public:

   virtual ~IParameterProxy() = default;

   virtual IntcodeType get() const = 0;
   
   virtual void set(IntcodeType val) = 0;

};

IParameterProxy* create_parameter
(OpcodeModes mode, IntcodeProgram& program, ProgramPtr pptr, IntcodeType rel_base);