#include <string>
#include <sstream>
#include <vector>
#include "ProblemFactory.h"
#include "SingleValueSolution.h"
#include "DataLoader.h"
#include "Cartesian.h"
#include "NbodyMoons.h"

namespace {

class Day12Prob1 : public IProblem {
public:

   virtual ~Day12Prob1() = default;

   virtual std::string name() const override {
      return "Day 12 - Problem 1";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

class Day12Prob2 : public IProblem {
public:

   virtual ~Day12Prob2() = default;

   virtual std::string name() const override {
      return "Day 12 - Problem 2";
   }

   virtual ISolution* execute(std::istream& data_set) override;
};

ProblemFactoryRegisterer<Day12Prob1> _problem1("Day12Prob1");
ProblemFactoryRegisterer<Day12Prob2> _problem2("Day12Prob2");


ISolution* Day12Prob1::execute(std::istream& data_set) {

   // todo need to make data file parser...for now just hard code
   const std::vector<cartesian::Point3> MOONS = {
      {5,4,4},
      {-11,-11,-3},
      {0,7,0},
      {-13,2,10}
   };

   const auto & energy = simulate_moon_movement(MOONS, 1000);

   return new SingleValueSolution<int>(compute_total_energy(energy));
}


ISolution* Day12Prob2::execute(std::istream& data_set) {

   return new SingleValueSolution<int>(-1);
}

}
