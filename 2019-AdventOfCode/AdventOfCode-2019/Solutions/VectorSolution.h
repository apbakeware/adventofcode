#pragma once
#include <algorithm>
#include <iterator>
#include <iostream>
#include <vector>
#include "ISolution.h"

template<class Value_T>
class VectorSolution : public ISolution {
public:

   // Should this be a move ?
   explicit VectorSolution(const std::vector<Value_T> vals)
      : vals(vals) {
   }

   VectorSolution(const std::string & label, const std::vector<Value_T> vals)
      : label(label), vals(vals) {
   }

   virtual ~VectorSolution() = default;

   virtual void write_result(std::ostream& outs) override {
      outs << label << " ";
      std::copy(vals.begin(), vals.end(), std::ostream_iterator<Value_T>(outs, " "));
   }

private:
   const std::string label;
   const std::vector<Value_T> vals;
};

