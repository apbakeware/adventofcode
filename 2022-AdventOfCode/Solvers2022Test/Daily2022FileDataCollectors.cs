﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solvers2022Test
{

    // Dont use cache for testing!!!! The static caching causes issues becuase xUnit parallelizes test

    public class Daily2022SUnitTestingFileDataCollectorFactory : IDataCollectorFactory
    {
        public IDataCollector CreateDataCollector(SolverIdentifier solverIdentifier)
        {
            string fileName = $"Data\\{solverIdentifier.DayString()}.txt";
            return new FileDataCollector(fileName);
        }
    }

    public class Daily2022UnitTestingSampleFileDataCollectorFactory : IDataCollectorFactory
    {
        public IDataCollector CreateDataCollector(SolverIdentifier solverIdentifier)
        {
            string fileName = $"Data\\{solverIdentifier.DayString()}.test.txt";
            return new FileDataCollector(fileName);
        }
    }
}
