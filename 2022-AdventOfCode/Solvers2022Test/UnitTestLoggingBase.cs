﻿using Microsoft.VisualStudio.TestPlatform.Utilities;
using Serilog;
using Xunit.Abstractions;

namespace Solvers2022Test
{
    public class UnitTestLoggingBase
    {
        protected UnitTestLoggingBase(ITestOutputHelper output)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.TestOutput(output)
                .CreateLogger();
        }
    }
}
