﻿using Core;
using Solvers2022;
using Xunit.Abstractions;

namespace Solvers2022Test
{
    public class DaySolvedSampleValueTest : UnitTestLoggingBase
    {
        private readonly IDataCollectorFactory dataCollectorFactory;

        private readonly ISolverExecutorFactory executorFactory;

        public DaySolvedSampleValueTest(ITestOutputHelper output) : base(output)
        {
            dataCollectorFactory = new Daily2022UnitTestingSampleFileDataCollectorFactory();
            executorFactory = new Daily2022SolverExecutorFactory();
        }

        [Theory]
        //[InlineData(SolverIdentifier.Day01Part01, "24000")]
        //[InlineData(SolverIdentifier.Day01Part02, "45000")]
        [InlineData(SolverIdentifier.Day02Part01, "15")]
        [InlineData(SolverIdentifier.Day02Part02, "12")]
        [InlineData(SolverIdentifier.Day03Part01, "157")]
        [InlineData(SolverIdentifier.Day03Part02, "70")]
        [InlineData(SolverIdentifier.Day04Part01, "2")]
        [InlineData(SolverIdentifier.Day04Part02, "4")]
        [InlineData(SolverIdentifier.Day05Part01, "CMZ")]
        [InlineData(SolverIdentifier.Day05Part02, "MCD")]
        [InlineData(SolverIdentifier.Day06Part01, "7")]
        [InlineData(SolverIdentifier.Day06Part02, "19")]
        [InlineData(SolverIdentifier.Day07Part01, "95437")]
        [InlineData(SolverIdentifier.Day07Part02, "24933642")]
        [InlineData(SolverIdentifier.Day08Part01, "21")]
        [InlineData(SolverIdentifier.Day08Part02, "8")]
        [InlineData(SolverIdentifier.Day09Part01, "13")]
        [InlineData(SolverIdentifier.Day09Part02, "1")]
        //[InlineData(SolverIdentifier.Day10Part01, "13140")]
        //[InlineData(SolverIdentifier.Day10Part02, "NullSolution")]
        [InlineData(SolverIdentifier.Day11Part01, "10605")]
        //[InlineData(SolverIdentifier.Day11Part02, "2713310158")]
        [InlineData(SolverIdentifier.Day12Part01, "31")]
        [InlineData(SolverIdentifier.Day12Part02, "29")]
        //[InlineData(SolverIdentifier.Day13Part01, "13")]
        //[InlineData(SolverIdentifier.Day13Part02, "NullSolution")]
        [InlineData(SolverIdentifier.Day14Part01, "24")]
        [InlineData(SolverIdentifier.Day14Part02, "93")]
        [InlineData(SolverIdentifier.Day15Part01, "26")]
        [InlineData(SolverIdentifier.Day15Part02, "56000011")]
        //[InlineData(SolverIdentifier.Day16Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day16Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day17Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day17Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day18Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day18Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day19Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day19Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day20Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day20Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day21Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day21Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day22Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day22Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day23Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day23Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day24Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day24Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day25Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day25Part02, "NullSolution")]
        public void TestSampleValues(SolverIdentifier solverIdentifier, string expResult)
        {
            var sut = executorFactory.CreateSolver(dataCollectorFactory, solverIdentifier);
            var solution = sut.Execute();

            solution.Solution.Solution().Should().Be(expResult);
        }
    }
}
