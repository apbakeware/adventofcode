﻿using Core;
using Core.Exceptions;
using Solvers2022;
using Xunit.Abstractions;

namespace Solvers2022Test
{

    public class Daily2022SolverExecutorFactoryTest : UnitTestLoggingBase
    {
        private IDataCollectorFactory dataCollectorFactory;
        private Daily2022SolverExecutorFactory sut;

        public Daily2022SolverExecutorFactoryTest(ITestOutputHelper output) : base(output)
        {
            dataCollectorFactory = new NullDataCollectorFactory();
            sut = new Daily2022SolverExecutorFactory();
        }

        //[Fact]
        //public void TestNullDataCollectorFactoryThrowsException()
        //{
        //    sut.Invoking(obj => obj.CreateSolver(null, SolverIdentifier.UNKNOWN))
        //        .Should().Throw<FactoryException>();
        //}

        [Fact]
        public void TestUnknownIdentiferThrowsException()
        {
            sut.Invoking(obj => obj.CreateSolver(dataCollectorFactory, SolverIdentifier.UNKNOWN))
                .Should().Throw<FactoryException>();
        }

        [Fact]
        public void TestCreateSolverWithValidIdentifier()
        {
            var solver = sut.CreateSolver(dataCollectorFactory, SolverIdentifier.Day01Part01);

            solver.Should().NotBeNull();
        }
    }
}
