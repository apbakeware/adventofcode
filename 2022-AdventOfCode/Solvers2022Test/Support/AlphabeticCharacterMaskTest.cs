﻿using Solvers2022.Support;
using Xunit.Abstractions;

namespace Solvers2022Test.Support
{
    public class AlphabeticCharacterMaskTest : UnitTestLoggingBase
    {
        public AlphabeticCharacterMaskTest(ITestOutputHelper output) : base(output)
        {
        }

        [Theory]
        [InlineData('a', 0)]
        [InlineData('z', 25)]
        [InlineData('A', 26)]
        [InlineData('Z', 51)]
        public void TestIndexFor(Char sut, int expIndex)
        {
            AlphabeticCharacterMask.IndexFor(sut).Should().Be(expIndex);
        }

        [Theory]
        [InlineData(0, 'a')]
        [InlineData(25, 'z')]
        [InlineData(26, 'A')]
        [InlineData(51, 'Z')]
        public void TestAtIndex(int sut, Char expChar)
        {
            AlphabeticCharacterMask.AtIndex(sut).Should().Be(expChar);
        }

        [Fact]
        public void TestAtIndexThrowsWithNegativeIndex()
        {
            int index = AlphabeticCharacterMask.IndexFor('a') - 1;
            Action act = () => AlphabeticCharacterMask.AtIndex(index);

            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void TestAtIndexThrowsAfterMaxIndex()
        {
            int index = AlphabeticCharacterMask.IndexFor('Z') + 1;
            Action act = () => AlphabeticCharacterMask.AtIndex(index);

            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void TestCharacterInStringIsSet()
        {
            AlphabeticCharacterMask sut = new("asBd");
            sut.IsSet('a').Should().BeTrue();
        }

        [Fact]
        public void TestCharacterNotInStringIsNotSet()
        {
            AlphabeticCharacterMask sut = new("asBd");
            sut.IsSet('q').Should().BeFalse();
        }

        [Fact]
        public void TestCharacterCapitalizationMatters()
        {
            AlphabeticCharacterMask sut = new("e");
            
            sut.IsSet('e').Should().BeTrue();
            sut.IsSet('E').Should().BeFalse();
        }

        [Fact]
        public void TestCommonCharactersNothingInCommon()
        {
            AlphabeticCharacterMask first = new("abcd");
            AlphabeticCharacterMask second = new("ABCD");

            List<char> common = AlphabeticCharacterMask.CommonCharacters(
                first, second);

            common.Should().BeEmpty();
        }

        [Fact]
        public void TestCommonCharactersWithOneCommon()
        {
            AlphabeticCharacterMask first = new("abqcd");
            AlphabeticCharacterMask second = new("ABqCD");

            List<char> common = AlphabeticCharacterMask.CommonCharacters(
                first, second);

            common.Should().Equal(new List<char>() { 'q' });
        }

        [Fact]
        public void TestCommonCharactersWithMulitpleCommon()
        {
            AlphabeticCharacterMask first = new("abqcdW");
            AlphabeticCharacterMask second = new("ABqCDW");

            List<char> common = AlphabeticCharacterMask.CommonCharacters(
                first, second);

            common.Should().Equal(new List<char>() { 'q', 'W' });
        }
    }
}
