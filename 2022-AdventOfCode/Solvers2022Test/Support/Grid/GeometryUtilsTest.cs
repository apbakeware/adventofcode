﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;
using FluentAssertions;
using Solvers2022.Support.Grid;

namespace Solvers2022Test.Support.Grid
{
    public class GeometryUtilsTest
    {
        public class PointLineTestTest : UnitTestLoggingBase
        {
            public PointLineTestTest(ITestOutputHelper output) : base(output)
            {
            }

            [Theory]
            [InlineData(5,3,-1)]
            [InlineData(5, 2, 0)]
            [InlineData(5,1,1)]
            public void TestHorizontalLine(int pointX, int pointY, int exp)
            {
                Coord start = new(3, 2);
                Coord end = new(7, 2);

                Coord sut = new(pointX, pointY);

                GeometryUtils.PointLineTest(start, end, sut).Should().Be(exp);
            }

            [Theory]
            [InlineData(1, 2, -1)]
            [InlineData(3, 2, 0)]
            [InlineData(35, 2, 1)]
            public void TestVerticalLine(int pointX, int pointY, int exp)
            {
                Coord start = new(3, 1);
                Coord end = new(3, 5);

                Coord sut = new(pointX, pointY);

                GeometryUtils.PointLineTest(start, end, sut).Should().Be(exp);
            }

            [Theory]
            [InlineData(2, 3, -1)]
            [InlineData(3, 3, 0)]
            [InlineData(2, 1, 1)]
            public void TestDiagnonalLine(int pointX, int pointY, int exp)
            {
                Coord start = new(1, 1);
                Coord end = new(5, 5);

                Coord sut = new(pointX, pointY);

                GeometryUtils.PointLineTest(start, end, sut).Should().Be(exp);
            }
        }

        public class PointEnclosedByConvexPolygonTest : UnitTestLoggingBase
        {
            private List<Coord> verticies;

            public PointEnclosedByConvexPolygonTest(ITestOutputHelper output) : base(output)
            {
                verticies = new List<Coord>
                {
                    new Coord(0, 0),
                    new Coord(2, 2),
                    new Coord(4, 0),
                };
            }

            [Fact]
            public void InMiddleReturnsTrue()
            {
                Coord point = new(1, 1);

                GeometryUtils.PointEnclosedByConvexPolygon(verticies, point).Should().Be(true);
            }

            [Fact]
            public void OnEdgeReturnsTrue()
            {
                Coord point = new(2, 1);

                GeometryUtils.PointEnclosedByConvexPolygon(verticies, point).Should().Be(true);
            }

            [Fact]
            public void OnOutsideReturnsFalse()
            {
                Coord point = new(1, 4);

                GeometryUtils.PointEnclosedByConvexPolygon(verticies, point).Should().Be(false);
            }

        }
    }
}
