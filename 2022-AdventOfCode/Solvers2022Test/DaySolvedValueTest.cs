﻿using Core;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Utilities;
using Solvers2022;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Solvers2022Test
{
    public class DaySolvedValueTest : UnitTestLoggingBase
    {
        private readonly IDataCollectorFactory dataCollectorFactory;

        private readonly ISolverExecutorFactory executorFactory;

        public DaySolvedValueTest(ITestOutputHelper output) : base(output) 
        {
            dataCollectorFactory = new Daily2022SUnitTestingFileDataCollectorFactory();
            executorFactory = new Daily2022SolverExecutorFactory();
        }

        [Theory]
        [InlineData(SolverIdentifier.Day01Part01, "70369")]
        [InlineData(SolverIdentifier.Day01Part02, "203002")]
        [InlineData(SolverIdentifier.Day02Part01, "12740")]
        [InlineData(SolverIdentifier.Day02Part02, "11980")]
        [InlineData(SolverIdentifier.Day03Part01, "8240")]
        [InlineData(SolverIdentifier.Day03Part02, "2587")]
        [InlineData(SolverIdentifier.Day04Part01, "588")]
        [InlineData(SolverIdentifier.Day04Part02, "911")]
        [InlineData(SolverIdentifier.Day05Part01, "WCZTHTMPS")]
        [InlineData(SolverIdentifier.Day05Part02, "BLSGJSDTS")]
        [InlineData(SolverIdentifier.Day06Part01, "1855")]
        [InlineData(SolverIdentifier.Day06Part02, "3256")]
        [InlineData(SolverIdentifier.Day07Part01, "1490523")]
        [InlineData(SolverIdentifier.Day07Part02, "12390492")]
        [InlineData(SolverIdentifier.Day08Part01, "1733")]
        [InlineData(SolverIdentifier.Day08Part02, "284648")]
        [InlineData(SolverIdentifier.Day09Part01, "6026")]
        [InlineData(SolverIdentifier.Day09Part02, "2273")]
        //[InlineData(SolverIdentifier.Day10Part01, "15260")]
        //[InlineData(SolverIdentifier.Day10Part02, "NullSolution")]
        [InlineData(SolverIdentifier.Day11Part01, "54036")]
        //[InlineData(SolverIdentifier.Day11Part02, "2713310158")]
        [InlineData(SolverIdentifier.Day12Part01, "423")]
        [InlineData(SolverIdentifier.Day12Part02, "416")]
        //[InlineData(SolverIdentifier.Day13Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day13Part02, "NullSolution")]
        [InlineData(SolverIdentifier.Day14Part01, "672")]
        [InlineData(SolverIdentifier.Day14Part02, "26831")]
        [InlineData(SolverIdentifier.Day15Part01, "5181556")]
        //[InlineData(SolverIdentifier.Day15Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day16Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day16Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day17Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day17Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day18Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day18Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day19Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day19Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day20Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day20Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day21Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day21Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day22Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day22Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day23Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day23Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day24Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day24Part02, "NullSolution")]
        //[InlineData(SolverIdentifier.Day25Part01, "NullSolution")]
        //[InlineData(SolverIdentifier.Day25Part02, "NullSolution")]
        public void TestDaySolvedValue(SolverIdentifier solverIdentifier, string expResult)
        {
            var sut = executorFactory.CreateSolver(dataCollectorFactory, solverIdentifier);
            var solution = sut.Execute();

            solution.Solution.Solution().Should().Be(expResult);
        }
    }
}
