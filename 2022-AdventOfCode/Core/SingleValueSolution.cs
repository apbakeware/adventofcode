﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class SingleValueSolution<T> : ISolution
    {
        public string ProblemName { get; }

        public T Value { get; }

        public SingleValueSolution(string problemName, T value)
        {
            ProblemName = problemName;
            Value = value;
        }

        public string Solution() => Value.ToString();
    }

}
