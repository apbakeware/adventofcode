﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// The ISolver interface defines the strategy pattern
    /// for solving a problem from a dataset.
    /// 
    /// The Ingest method is to be called prior to Solve
    /// to accept the input data set.
    /// </summary>
    public interface ISolver
    {
        /// <summary>
        /// Accept the input. The input is stored/managed
        /// for use by the Solve method.
        /// </summary>
        /// <param name="input">Collection of strings to be used by the solver</param>
        public void Ingest( List<string> input );

        /// <summary>
        /// Compute the solution and return it. The Ingest method
        /// should be called prior to solving.
        /// </summary>
        /// <returns>Solution.</returns>
        public ISolution Solve();
    }
}
