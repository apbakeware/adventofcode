﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// Solution to a Advent of Code problem.
    /// </summary>
    public interface ISolution
    {
        /// <summary>
        /// The name of the problem this solution is for.
        /// </summary>
        public string ProblemName { get; }

        /// <summary>
        /// The solution of the problem.
        /// </summary>
        /// <returns>String representation of the solution to the problem.</returns>
        public string Solution();
    }

}
