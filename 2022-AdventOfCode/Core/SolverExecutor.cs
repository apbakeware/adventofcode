﻿using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// The SolverExecutor encapsulates
    /// the problem solving alogrithm using
    /// the ISolver interfaces.
    /// </summary>
    public class SolverExecutor
    {
        private ILogger _logger;
        private IDataCollector _dataCollector;
        private ISolver _solver;

        public SolverExecutor(IDataCollector dataCollector, ISolver solver) 
        {
            _logger = Log.ForContext<SolverExecutor>();
            _dataCollector = dataCollector ?? throw new ArgumentNullException(nameof(dataCollector));
            _solver = solver ?? throw new ArgumentNullException(nameof(solver));
        }

        public SolutionTime Execute()
        {
            Stopwatch stopWatch = new ();

            var dataCollectionResult = PerformDataCollection(stopWatch);
            var injestionResult = PerformInputIngestion(stopWatch, dataCollectionResult.inputData);
            var solverResult = PeformSolver(stopWatch);

            _logger.Debug("SolverExecutor.Execute() Durations (s) -- {Name} -- Data Collection: {DcTime}  Injestion: {InjestTime}  Solve: {SolveTime}",
                solverResult.solution.ProblemName,
                dataCollectionResult.duration.TotalSeconds,
                injestionResult.TotalSeconds,
                solverResult.duration.TotalSeconds
            );

            return new SolutionTime(
                solverResult.solution,
                dataCollectionResult.duration,
                injestionResult,
                solverResult.duration
            );
        }

        private (TimeSpan duration, List<string> inputData) PerformDataCollection(Stopwatch stopwatch)
        {
            stopwatch.Restart();
            var data = _dataCollector.GetData();
            _logger.Information("Read {Num} lines of input", data.Count);
            stopwatch.Stop();
            return (stopwatch.Elapsed, data);
        }

        private TimeSpan PerformInputIngestion(Stopwatch stopwatch, List<string> solverInput)
        {
            stopwatch.Restart();
            _solver.Ingest(solverInput);
            stopwatch.Stop();
            return stopwatch.Elapsed;
        }

        private (TimeSpan duration, ISolution solution) PeformSolver(Stopwatch stopwatch)
        {
            stopwatch.Restart();
            var solution = _solver.Solve();
            stopwatch.Stop();
            return (stopwatch.Elapsed, solution);
        }
    }
}
