﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public class NullSolver : ISolver
    {
        public void Ingest(List<string> input)
        {
        }

        public ISolution Solve()
        {
            return new SingleValueSolution<string>("Null Problem", "NULL");
        }
    }
}
