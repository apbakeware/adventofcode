﻿using System;
using System.Collections.Generic;

namespace Core
{
    public class NullDataCollector : IDataCollector
    {
        public List<string> GetData() => new List<string>();
    }
}
