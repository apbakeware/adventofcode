﻿namespace Core
{
    public interface ISolutionRenderer
    {
        void Render(List<SolutionTime> solutions);
    }
}