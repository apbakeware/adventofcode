﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public record SolutionTime
    {
        
        public ISolution Solution { get; }
        public TimeSpan DataCollectionDuration { get; }
        public TimeSpan InputIngestDuration { get; }
        public TimeSpan SolverDuration { get; }
        public TimeSpan TotalDuration 
        { 
            get
            {
                return DataCollectionDuration + InputIngestDuration + SolverDuration;
            }
        }

        public SolutionTime(ISolution solution, TimeSpan dataCollectionDuration, TimeSpan inputIngestDuration, TimeSpan solverDuration)
        {
            Solution = solution;
            DataCollectionDuration = dataCollectionDuration;
            InputIngestDuration = inputIngestDuration;
            SolverDuration = solverDuration;
        }
    }
}
