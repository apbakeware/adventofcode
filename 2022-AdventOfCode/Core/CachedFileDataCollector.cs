﻿using Serilog;

namespace Core
{
    /// <summary>
    /// Implementation of the IDataCollection interface
    /// to read all lines from a text file.
    /// </summary>
    public class CachedFileDataCollector : IDataCollector
    {
        protected ILogger logger;
        private static Lazy<List<string>> _data;

        public CachedFileDataCollector(string fname)
        {
            logger = Log.ForContext<CachedFileDataCollector>();
            _data = new(() => CachedFileDataCollector.GetDataFromFile(fname));
        }

        public List<string> GetData()
        {
            return _data.Value;
        }

        private static List<string> GetDataFromFile(string fname)
        {
            Log.ForContext<CachedFileDataCollector>().Debug("Loading file: {Fname}", fname);
            return File.ReadAllLines(fname).ToList<string>();
        }
    }
}
