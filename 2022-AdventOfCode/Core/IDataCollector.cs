﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    /// <summary>
    /// Abstract interface for getting input data for a solver.
    /// </summary>
    public interface IDataCollector
    {
        /// <summary>
        /// Get a list of string inputs from the data source.
        /// </summary>
        /// <returns>Collection of strings from the data source.</returns>
        List<string> GetData();
    }
}
