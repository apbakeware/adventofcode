﻿Param
  (
     [parameter(Position=0, Mandatory=$true)]
     [String]
     $OutDir
  ) 

  function GenerateClassFileContent {
    param (
      [Parameter (Mandatory = $true)] [String]$Day
    )

    $dayClassName = "Day${Day}"
    $part1ClassName = "${dayClassName}Part01"
    $part2ClassName = "${dayClassName}Part02"
  
    return @"
  using System;
  using Serilog;
  using Serilog.Context;
  using Core;
  
  namespace Solvers2022
  {
  
      // Checklist:
      //  * Implement data variable type
      //  * Implement Injest
      //  * Implement Solve for each class
      //  * Extract classes into dedicated .cs files
  
      public abstract class ${dayClassName} : ISolver
      {
        // TODO: Update data collection type
        protected List<string> data;
  
        private ILogger logger;
  
        public ${dayClassName}()
        {
            logger = Log.ForContext<${dayClassName}>();
            data = new();
        }
  
        public void Ingest(List<string> input)
        {
            // TODO: Update Injest
            data = input.ConvertAll( x => x );
        }
  
        public abstract ISolution Solve();
      }
  
      public class ${part1ClassName} : ${dayClassName}
      {
          private ILogger logger;
  
          public ${part1ClassName}()
          {
            logger = Log.ForContext<${part1ClassName}>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("${aocDay}-01", "Unimplemented");
          }
      }
  
      public class ${part2ClassName} : ${dayClassName}
      {
          private ILogger logger;
  
          public ${part2ClassName}()
          {
            logger = Log.ForContext<${part2ClassName}>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("${aocDay}-02", "Unimplemented");
          }
      }
    }    
"@
}

#"01","02","03","04",

$aocDayNumbers = "05","06","07","08","09","10",`
  "11","12","13","14","15","16","17","18","19","20", `
  "21","22","23","24","25"



# Create directories
$solverDirectory = "${OutDir}\DaySolvers"
$dataDirectory = "${OutDir}\Data"
New-Item -ItemType directory $solverDirectory
New-Item -ItemType directory $dataDirectory

foreach ($aocDay in $aocDayNumbers)
{  

  $aocClassFileName = "${solverDirectory}\Day${aocDay}.cs"
  if( -not(Test-Path -Path $aocClassFileName -PathType Leaf) )
  {
    Write-Host "Generating: ${aocClassFileName}"
    $classTemplate = GenerateClassFileContent -Day $aocDay
      
    # Write-Host $classTemplate
    Set-Content -Path $aocClassFileName -Value $classTemplate 
  }
  else
  {
    Write-Host "Solver exists, skipping: ${aocClassName} ${aocClassFileName}"
  }

  New-Item -Name "${dataDirectory}\Day${aocDay}.txt" -ItemType File
  New-Item -Name "${dataDirectory}\Day${aocDay}.test.txt" -ItemType File

}

