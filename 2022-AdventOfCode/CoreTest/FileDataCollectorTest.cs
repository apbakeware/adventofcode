using Core;
using Xunit.Abstractions;

namespace CoreTest
{
    public class FileDataCollectorTest : UnitTestLoggingBase
    {
        public FileDataCollectorTest(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void TestWithFileDoesntExistThrows()
        {
            var sut = new FileDataCollector("Invalid_File_Name.dat");

            sut.Invoking(x => x.GetData() ).Should().Throw<FileNotFoundException>();
        }

        [Fact]
        public void TestWithKnownFileReturnsCorrectContent()
        {
            var sut = new FileDataCollector("KnownDataContent.txt");

            var content = sut.GetData();

            content.Should().HaveCount(4);
            content.Should().Equal(new List<string> { "This file", "should", "return ", "4 lines." });
        }
    }
}