﻿using Core;
using Xunit.Abstractions;

namespace CoreTest
{
    public class SolverExecutorTest : UnitTestLoggingBase
    {
        private Mock<IDataCollector> mockDataCollector;
        private Mock<ISolver> mockSolver;
        private Mock<ISolution> mockSolution;
        private SolverExecutor sut;

        public SolverExecutorTest(ITestOutputHelper output) : base(output)
        { 
            mockDataCollector = new ();
            mockSolver = new ();
            mockSolution = new Mock<ISolution> ();
            sut = new(mockDataCollector.Object, mockSolver.Object);

            mockSolver.Setup(x => x.Solve())
                .Returns(mockSolution.Object);
        }

        [Fact]
        public void TestConstructorThrowsOnNullParameters()
        {
            mockDataCollector.Object.Should().NotBeNull();
            mockSolver.Object.Should().NotBeNull();

            var ctorNullDataCollector = () => new SolverExecutor(null, mockSolver.Object);
            var ctorNullSolver = () => new SolverExecutor(mockDataCollector.Object, null);

            ctorNullDataCollector.Should().Throw<ArgumentNullException>();
            ctorNullSolver.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void TestDataCollectorDataIsInjestedByExecutor()
        {
            List<string> collectorInput = new() { "A", "B", "C" };

            mockDataCollector.Setup(x => x.GetData()).Returns(collectorInput);
            
            sut.Execute();

            mockSolver.Verify(x => x.Ingest(collectorInput));
        }

        [Fact]
        public void TestDataCollectorExecuteSequence()
        {
            List<string> collectorInput = new() { "A", "B", "C" };

            mockDataCollector.Setup(x => x.GetData()).Returns(collectorInput);

            sut.Execute();

            mockSolver.Verify(x => x.Ingest(collectorInput));
            mockSolver.Verify(x => x.Solve());
        }
    }
}
