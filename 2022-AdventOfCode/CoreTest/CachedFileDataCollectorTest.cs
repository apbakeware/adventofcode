﻿using Core;
using Serilog;
using Xunit.Abstractions;

namespace CoreTest
{
    public class CachedFileDataCollectorTest : UnitTestLoggingBase
    {
        public CachedFileDataCollectorTest(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void TestWithFileDoesntExistThrows()
        {
            var sut = new CachedFileDataCollector("Invalid_File_Name.dat");

            sut.Invoking(x => x.GetData()).Should().Throw<FileNotFoundException>();
        }

        [Fact]
        public void TestWithKnownFileReturnsCorrectContent()
        {
            var sut = new CachedFileDataCollector("KnownDataContent.txt");

            var content = sut.GetData();

            content.Should().HaveCount(4);
            content.Should().Equal(new List<string> { "This file", "should", "return ", "4 lines." });

        }

        [Fact]
        public void TestWithKnownFileReturnsSameContent()
        {
            var sut = new CachedFileDataCollector("KnownDataContent.txt");

            var content = sut.GetData();
            var content2 = sut.GetData();

            // TODO: verify doesnt read file again?

            Assert.Same(content, content2);
        }
    }
}