﻿// See https://aka.ms/new-console-template for more information
using Microsoft.Extensions.Configuration;
using Serilog;

using Core;
using Solvers2022;
using AOC;
using TerminalGuilRunner;
using Serilog.Core;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Hello, World!");

        var builder = new ConfigurationBuilder();
        var config = Configuration.BuildAppConfig(builder);

        var levelSwitch = new LoggingLevelSwitch()
        {
            MinimumLevel = Serilog.Events.LogEventLevel.Information
        };

        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(config)
            .MinimumLevel.ControlledBy(levelSwitch)
            .CreateLogger();


        // TODO: IOC CONTAINER
        IDataCollectorFactory dataCollectorFactory =
            new Daily2022FileDataCollectorFactory();

        IDataCollectorFactory sampleDataCollectorFactory =
            new Daily2022SampleFileDataCollectorFactory();

        ISolverExecutorFactory executorFactory =
            new Daily2022SolverExecutorFactory();

        //ISolutionRenderer solutionRenderer = new SolutionTimeTable();

        //ISolutionPicker solutionPicker = new FixedSolutionPicker();

        //try
        //{
        //    List<SolutionTime> solutions = new();
        //    List<SolverIdentifier> solutionIds = solutionPicker.GetSolversList();
        //    foreach (var solutionIdentifier in solutionIds)
        //    {
        //        var executor = executorFactory.CreateSolver(dataCollectorFactory, solutionIdentifier);
        //        solutions.Add(executor.Execute());
        //    }

        //    solutionRenderer.Render(solutions);
        //}
        //catch (Exception ex)
        //{
        //    Console.WriteLine($"Failed to run!!!!");
        //    Console.WriteLine(ex.ToString());
        //}

        var gui = new TerminalGuilRunner.TerminalGuiRunner(
            dataCollectorFactory,
            sampleDataCollectorFactory,
            executorFactory,
            levelSwitch);

        gui.Run();
        Log.CloseAndFlush();
    }
}