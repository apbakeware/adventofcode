﻿using Core;
using Solvers2022;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOC
{
    public class FixedSolutionPicker : ISolutionPicker
    {
        public List<SolverIdentifier> GetSolversList()
        {
            return new List<SolverIdentifier>()
            {
                SolverIdentifier.Day01Part01,
                SolverIdentifier.Day01Part02 //,
                //SolverIdentifier.Day02Part01,
                //SolverIdentifier.Day02Part02,
                //SolverIdentifier.Day03Part01,
                //SolverIdentifier.Day03Part02,
                //SolverIdentifier.Day04Part01,
                //SolverIdentifier.Day04Part02,
                //SolverIdentifier.Day05Part01,
                //SolverIdentifier.Day05Part02,
                //SolverIdentifier.Day06Part01,
                //SolverIdentifier.Day06Part02,
                //SolverIdentifier.Day07Part01,
                //SolverIdentifier.Day07Part02,
                //SolverIdentifier.Day08Part01,
                //SolverIdentifier.Day08Part02,
                //SolverIdentifier.Day09Part01,
                //SolverIdentifier.Day09Part02,
                //SolverIdentifier.Day10Part01,
                //SolverIdentifier.Day10Part02,
                //SolverIdentifier.Day11Part01,
                //SolverIdentifier.Day11Part02,
                //SolverIdentifier.Day12Part01,
                //SolverIdentifier.Day12Part02,
                //SolverIdentifier.Day13Part01,
                //SolverIdentifier.Day13Part02,
                //SolverIdentifier.Day14Part01,
                //SolverIdentifier.Day14Part02,
                //SolverIdentifier.Day15Part01,
                //SolverIdentifier.Day15Part02,
                //SolverIdentifier.Day16Part01,
                //SolverIdentifier.Day16Part02,
                //SolverIdentifier.Day17Part01,
                //SolverIdentifier.Day17Part02,
                //SolverIdentifier.Day18Part01,
                //SolverIdentifier.Day18Part02,
                //SolverIdentifier.Day19Part01,
                //SolverIdentifier.Day19Part02,
                //SolverIdentifier.Day20Part01,
                //SolverIdentifier.Day20Part02,
                //SolverIdentifier.Day21Part01,
                //SolverIdentifier.Day21Part02,
                //SolverIdentifier.Day22Part01,
                //SolverIdentifier.Day22Part02,
                //SolverIdentifier.Day23Part01,
                //SolverIdentifier.Day23Part02,
                //SolverIdentifier.Day24Part01,
                //SolverIdentifier.Day24Part02,
                //SolverIdentifier.Day25Part01,
                //SolverIdentifier.Day25Part02
            };
        }
    }
}
