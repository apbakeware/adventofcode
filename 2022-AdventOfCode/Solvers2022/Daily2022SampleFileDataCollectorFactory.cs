﻿using Core;

namespace Solvers2022
{
    // Consider making this generic for the data collector type
    public class Daily2022SampleFileDataCollectorFactory : IDataCollectorFactory
    {
        public IDataCollector CreateDataCollector(SolverIdentifier solverIdentifier)
        {
            // TODO...make sure its value
            string fileName = $"Data\\{solverIdentifier.DayString()}.test.txt";
            return new CachedFileDataCollector(fileName);
        }
    }
}
