  using System;
  using Serilog;
  using Serilog.Context;
  using Core;
  
  namespace Solvers2022
  {
  
      // Checklist:
      //  * Implement data variable type
      //  * Implement Injest
      //  * Implement Solve for each class
      //  * Extract classes into dedicated .cs files
  
      public abstract class Day23 : ISolver
      {
        // TODO: Update data collection type
        protected List<string> data;
  
        private ILogger logger;
  
        public Day23()
        {
            logger = Log.ForContext<Day23>();
            data = new();
        }
  
        public void Ingest(List<string> input)
        {
            // TODO: Update Injest
            data = input.ConvertAll( x => x );
        }
  
        public abstract ISolution Solve();
      }
  
      public class Day23Part01 : Day23
      {
          private ILogger logger;
  
          public Day23Part01()
          {
            logger = Log.ForContext<Day23Part01>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("23-01", "Unimplemented");
          }
      }
  
      public class Day23Part02 : Day23
      {
          private ILogger logger;
  
          public Day23Part02()
          {
            logger = Log.ForContext<Day23Part02>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("23-02", "Unimplemented");
          }
      }
    }    
