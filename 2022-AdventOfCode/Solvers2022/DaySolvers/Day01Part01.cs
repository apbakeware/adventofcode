using System;
using Serilog;
using Serilog.Context;
using Core;

namespace Solvers2022
{
    public class Day01Part01 : ISolver
    {
        protected ILogger logger;
        private List<string> data;

        public Day01Part01()
        {
          logger = Log.ForContext<Day01Part01>();
        }

        public void Ingest(List<string> input)
        {
            logger.Debug("Ingest() -- Data: {Input}", input);
            data = input.ToList<string>();
        }
    
        public ISolution Solve()
        {
            int elfCount = 0;
            int maxCalorieCount = 0;

            foreach (var row in data)
            {
                if (row.Length == 0)
                {
                    logger.Debug("New elf!!!");
                    maxCalorieCount = Math.Max(elfCount, maxCalorieCount);
                    elfCount = 0;
                    logger.Debug("Max: {max}", maxCalorieCount);
                }
                else
                {
                    elfCount += int.Parse(row);
                    logger.Debug("Elf count now: {Count}", elfCount);
                }
            }

            return new SingleValueSolution<int>("01-01", maxCalorieCount);
        }
    }
  }    
