  using System;
  using Serilog;
  using Serilog.Context;
  using Core;
  
  namespace Solvers2022
  {
  
      // Checklist:
      //  * Implement data variable type
      //  * Implement Injest
      //  * Implement Solve for each class
      //  * Extract classes into dedicated .cs files
  
      public abstract class Day25 : ISolver
      {
        // TODO: Update data collection type
        protected List<string> data;
  
        private ILogger logger;
  
        public Day25()
        {
            logger = Log.ForContext<Day25>();
            data = new();
        }
  
        public void Ingest(List<string> input)
        {
            // TODO: Update Injest
            data = input.ConvertAll( x => x );
        }
  
        public abstract ISolution Solve();
      }
  
      public class Day25Part01 : Day25
      {
          private ILogger logger;
  
          public Day25Part01()
          {
            logger = Log.ForContext<Day25Part01>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("25-01", "Unimplemented");
          }
      }
  
      public class Day25Part02 : Day25
      {
          private ILogger logger;
  
          public Day25Part02()
          {
            logger = Log.ForContext<Day25Part02>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("25-02", "Unimplemented");
          }
      }
    }    
