using System;
using Serilog;
using Serilog.Context;
using Core;

namespace Solvers2022
{
    public class Day04Part02 : Day04
    {
        private ILogger logger;
        
        public Day04Part02()
        {
          logger = Log.ForContext<Day04Part02>();
        }

        public override ISolution Solve()
        {
            int count = 0;

            foreach (var elfPair in data)
            {
                bool overlap =
                    (elfPair.elfOneMin >= elfPair.elfTwoMin && elfPair.elfOneMin <= elfPair.elfTwoMax) ||
                    (elfPair.elfOneMax >= elfPair.elfTwoMin && elfPair.elfOneMax <= elfPair.elfTwoMax) ||
                    (elfPair.elfTwoMin >= elfPair.elfOneMin && elfPair.elfTwoMin <= elfPair.elfOneMax) ||
                    (elfPair.elfTwoMax >= elfPair.elfOneMin && elfPair.elfTwoMax <= elfPair.elfOneMax);
                if (overlap)
                {
                    count++;
                }
            }

            return new SingleValueSolution<int>("04-01", count);
        }
    }
  }    
