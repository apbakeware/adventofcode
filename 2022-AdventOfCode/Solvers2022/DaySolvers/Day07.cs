using System;
using Serilog;
using Serilog.Context;
using Core;
using Serilog.Core;

namespace Solvers2022
{
    public record FileListing
    {
        public int Size;
        public string Name;
    }

    // TODO: make generic

    public class FileSystemTreeNode
    {
        
        public FileSystemTreeNode? Parent { get; set; }
        public List<FileSystemTreeNode> Children { get; }

        public string Name { get; }

        public List<FileListing> Files { get; }

        public bool IsLeaf {  get => Children.Count == 0; }

        public bool IsRoot {  get => Parent == null; }


        public int ImmediateDiskSpace
        {
            get => Files.Sum(x => x.Size);
        }

        public int TotalDiskSpace
        {
            get 
            {
                var requiredSpace = childrenSize.Value;
                //logger.Verbose("Node: {Name}  Total disk space: {Size}", Name, requiredSpace);
                return childrenSize.Value; 
            }
        }

        private ILogger logger;
        private Lazy<int> childrenSize;

        public FileSystemTreeNode(string name)
        {
            logger = Log.ForContext<FileSystemTreeNode>();
            Children = new List<FileSystemTreeNode>();
            Name = name;
            Files = new ();
            childrenSize = new( () => Children.Sum(x => x.TotalDiskSpace) + ImmediateDiskSpace );
        }

        public FileSystemTreeNode(FileSystemTreeNode parent, string name)
        {
            logger = Log.ForContext<FileSystemTreeNode>();
            Parent = parent;
            Children = new List<FileSystemTreeNode>();
            Name = name;
            Files = new ();
            childrenSize = new(() => Children.Sum(x => x.TotalDiskSpace) + ImmediateDiskSpace);
        }

        public void AddChild(FileSystemTreeNode child )
        {
            logger.Debug("Adding child {CName} to {PName}", child.Name, Name);
            Children.Add( child );
            child.Parent = this;
        }

        // Null if no child
        public FileSystemTreeNode GetChild(string name)
        {
            return Children.Find(x => x.Name == name);
        }

        public void AddFile(FileListing fileListing)
        {
            logger.Debug("Adding file {CName} size {PName}", fileListing.Name, fileListing.Size);
            Files.Add(fileListing);
        }

    }

    // TODO: non static, hashset to prevent revisit
    public static class DfsVisitor
    {
        public delegate void Visit(FileSystemTreeNode node);

        public static void DfsVisit(FileSystemTreeNode node, Action<FileSystemTreeNode> visitor)
        {
            foreach(var child in node.Children)
            {
                DfsVisit(child, visitor);
            }

            visitor(node);
        }
    }

    // Walks a node then its children invokding the deletgate at each node
    public static class FileSystemWalker
    {
        public delegate void Visit(FileSystemTreeNode node, int level);

        static public void Walk(FileSystemTreeNode node, Visit visitor)
        {
            Walk(node, visitor, 0, new HashSet<string>());
        }

        static private void Walk(FileSystemTreeNode node, Visit visitor, int level, HashSet<string> visited)
        {
            if(visited.Contains(node.Name))
            {
                return;
            }

            visited.Add(node.Name);
            visitor(node, level);
            foreach(var child in node.Children)
            {
                Walk(child, visitor, level + 1, visited);
            }
        }
    }





    // Checklist:
    //  * Implement data variable type
    //  * Implement Injest
    //  * Implement Solve for each class
    //  * Extract classes into dedicated .cs files

    public abstract class Day07 : ISolver
    {
        protected FileSystemTreeNode fileSystem;

        private ILogger logger;

        public Day07()
        {
            logger = Log.ForContext<Day07>();
        }

        public void Ingest(List<string> input)
        {
            fileSystem = new("/");
            FileSystemTreeNode currentNode = fileSystem;

            foreach (var line in input)
            {
                var fragments = line.Split(' ');
                if (fragments[0] == "$")
                {

                    // TODO: better detection of command
                    if (fragments[1] == "cd")
                    {
                        if (fragments[2] == "/")
                        {
                            logger.Debug("Returning to root node");
                            currentNode = fileSystem;
                        }
                        else if (fragments[2] == "..")
                        {
                            currentNode = currentNode.Parent;
                            logger.Debug("Moved up a directory, current directory now: {Dir}", currentNode.Name);
                        }
                        else
                        {
                            // Null guard
                            currentNode = currentNode.GetChild(fragments[2]);
                            logger.Debug("Moved down, current directory now: {Dir}", currentNode.Name);
                        }
                    }
                    else if (fragments[1] == "ls")
                    {
                        // Dont need to do anything
                    }
                    else
                    {
                        // TODO: throw
                    }
                }
                else if (fragments[0] == "dir")
                {

                    // TODO: 
                    // Check if child exists in current node...if not add it
                    if(currentNode.GetChild(fragments[1]) == null)
                    {
                        currentNode.AddChild(new FileSystemTreeNode (fragments[1]));
                    }
                }
                else
                {
                    FileListing fileListing = new()
                    {
                        Size = int.Parse(fragments[0]),
                        Name = fragments[1]
                    };
                    
                    currentNode.AddFile(fileListing);
                }
            }
        }

        public abstract ISolution Solve();
    }

    public class Day07Part01 : Day07
    {
        private ILogger logger;

        private int file_size_sum;

        public Day07Part01()
        {
            logger = Log.ForContext<Day07Part01>();
        }

        public override ISolution Solve()
        {
            file_size_sum = 0;
            DfsVisitor.DfsVisit(fileSystem, this.NodeVisitor);
            return new SingleValueSolution<int>("07-01", file_size_sum);
        }

        public void NodeVisitor(FileSystemTreeNode node)
        {
            if( node.TotalDiskSpace <= 100000)
            {
                logger.Debug("Node: {Name} has total size less than 100000 ({Size})",
                    node.Name, node.TotalDiskSpace);
                file_size_sum += node.TotalDiskSpace;
            }
        }
    }

    public class Day07Part02 : Day07
    {
        private ILogger logger;

        private const int DRIVE_SIZE = 70000000;
        private const int REQ_DISK_SPACE = 30000000;
        private int space_required_to_free;
        private int smallest_suitable_size;

        public Day07Part02()
        {
            logger = Log.ForContext<Day07Part02>();
        }

        public override ISolution Solve()
        {
            int currentFreeSpace = DRIVE_SIZE - fileSystem.TotalDiskSpace;
            space_required_to_free = REQ_DISK_SPACE - currentFreeSpace;
            smallest_suitable_size = int.MaxValue;

            FileSystemWalker.Walk(fileSystem, this.FileSystemLogger);
            DfsVisitor.DfsVisit(fileSystem, this.NodeVisitor);

            return new SingleValueSolution<int>("07-02", smallest_suitable_size);
        }

        public void NodeVisitor(FileSystemTreeNode node)
        {
            if (node.TotalDiskSpace >= space_required_to_free)
            {
                logger.Debug("Node: {Name} has total size >= {Req} ({Size})",
                    node.Name, space_required_to_free, node.TotalDiskSpace);
                smallest_suitable_size = Math.Min(smallest_suitable_size, node.TotalDiskSpace);
            }
        }

        public void FileSystemLogger(FileSystemTreeNode node, int level )
        {
            string prefix = new(' ', level*2);
            logger.Debug("{Prefix}{Name}/  ({ImmSize}/{TSize})", prefix, node.Name, node.ImmediateDiskSpace, node.TotalDiskSpace);
            foreach(var f in node.Files)
            {
                logger.Debug("{Prefix}  {Fname} ({Size})", prefix, f.Name, f.Size);
            }
        }
    }
}
