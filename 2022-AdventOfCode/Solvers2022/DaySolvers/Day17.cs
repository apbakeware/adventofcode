  using System;
  using Serilog;
  using Serilog.Context;
  using Core;
  
  namespace Solvers2022
  {
  
      // Checklist:
      //  * Implement data variable type
      //  * Implement Injest
      //  * Implement Solve for each class
      //  * Extract classes into dedicated .cs files
  
      public abstract class Day17 : ISolver
      {
        // TODO: Update data collection type
        protected List<string> data;
  
        private ILogger logger;
  
        public Day17()
        {
            logger = Log.ForContext<Day17>();
            data = new();
        }
  
        public void Ingest(List<string> input)
        {
            // TODO: Update Injest
            data = input.ConvertAll( x => x );
        }
  
        public abstract ISolution Solve();
      }
  
      public class Day17Part01 : Day17
      {
          private ILogger logger;
  
          public Day17Part01()
          {
            logger = Log.ForContext<Day17Part01>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("17-01", "Unimplemented");
          }
      }
  
      public class Day17Part02 : Day17
      {
          private ILogger logger;
  
          public Day17Part02()
          {
            logger = Log.ForContext<Day17Part02>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("17-02", "Unimplemented");
          }
      }
    }    
