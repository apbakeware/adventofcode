using System;
using System.Collections.Generic;
using Serilog;
using Serilog.Context;
using Core;
using Solvers2022.Support.Grid;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.CompilerServices;

namespace Solvers2022
{
    public abstract class Day09 : ISolver
    {
        protected List<Func<Coord, Coord>> movementCommands;

        private ILogger logger;

        public Day09()
        {
            logger = Log.ForContext<Day09>();
        }

        public void Ingest(List<string> input)
        {
            movementCommands = new();
            foreach(var row in input)
            {
                var fields = row.Split(" ");
                string direction = fields[0];
                int numMoves = int.Parse(fields[1]);
                for(int cnt = 0; cnt < numMoves; cnt++ )
                {
                    Func<Coord, Coord> cmd = direction switch
                    {
                        "U" => (x) => CoordinateTranslation.Down(x, 1),
                        "R" => (x) => CoordinateTranslation.Right(x, 1),
                        "D" => (x) => CoordinateTranslation.Up(x, 1),
                        "L" => (x) => CoordinateTranslation.Left(x, 1),
                        _ => throw new Core.Exceptions.SolverException("Unknown field value")
                    };

                    movementCommands.Add(cmd);
                }
            }
        }

        public abstract ISolution Solve();

        public Coord UpdateTailLocation(Coord headLocation, Coord tailLocation)
        {
            (int x_dist, int y_dist) offset = (headLocation.X - tailLocation.X, headLocation.Y - tailLocation.Y);
            Coord updatedTailLocation = offset switch
            {
                (0, 2) => CoordinateTranslation.Down(tailLocation),
                (2, 0) => CoordinateTranslation.Right(tailLocation),
                (0, -2) => CoordinateTranslation.Up(tailLocation),
                (-2, 0) => CoordinateTranslation.Left(tailLocation),
                (1, 2) => CoordinateTranslation.Translate(tailLocation, 1, 1),
                (1, -2) => CoordinateTranslation.Translate(tailLocation, 1, -1),
                (-1, 2) => CoordinateTranslation.Translate(tailLocation, -1, 1),
                (-1, -2) => CoordinateTranslation.Translate(tailLocation, -1, -1),
                (2, 1) => CoordinateTranslation.Translate(tailLocation, 1, 1),
                (2, -1) => CoordinateTranslation.Translate(tailLocation, 1, -1),
                (-2, 1) => CoordinateTranslation.Translate(tailLocation, -1, 1),
                (-2, -1) => CoordinateTranslation.Translate(tailLocation, -1, -1),
                (2,2) => CoordinateTranslation.Translate(tailLocation, 1, 1),
                (2, -2) => CoordinateTranslation.Translate(tailLocation, 1, -1),
                (-2, 2) => CoordinateTranslation.Translate(tailLocation, -1, 1),
                (-2, -2) => CoordinateTranslation.Translate(tailLocation, -1, -1),
                _ => tailLocation
            };
            return updatedTailLocation;
        }
    }

    public class Day09Part01 : Day09
    {
        private ILogger logger;

        public Day09Part01()
        {
            logger = Log.ForContext<Day09Part01>();
        }

        public override ISolution Solve()
        {
            HashSet<Coord> tailLocations = new();
            Coord headLocation = new(0, 0);
            Coord tailLocation = new(0, 0);
            foreach(var cmd in movementCommands)
            {
                headLocation = cmd(headLocation);
                tailLocation = UpdateTailLocation(headLocation, tailLocation);
                tailLocations.Add(tailLocation);
            }

            return new SingleValueSolution<int>("09-01", tailLocations.Count);
        }
    }

    public class Day09Part02 : Day09
    {
        private ILogger logger;

        public Day09Part02()
        {
            logger = Log.ForContext<Day09Part02>();
        }

        public override ISolution Solve()
        {
            HashSet<Coord> tailLocations = new();
            Coord[] knotLocations = new Coord[10];
            for(int idx = 0; idx < knotLocations.Length; idx++)
            {
                knotLocations[idx] = new();
            }

            logger.Debug("Starting Array: {Ary}", knotLocations);
            foreach (var cmd in movementCommands)
            {
                knotLocations[0] = cmd(knotLocations[0]);
                for(int idx = 1; idx < knotLocations.Length; idx++)
                {
                    knotLocations[idx] = UpdateTailLocation(knotLocations[idx - 1], knotLocations[idx]);
                }

                logger.Debug("Array: {Ary}", knotLocations);
                tailLocations.Add(knotLocations[^1]);
            }
            return new SingleValueSolution<int>("09-02", tailLocations.Count);
        }
    }
}
