﻿using Serilog;
using Core;

namespace Solvers2022
{
    public abstract class Day04 : ISolver
    {
        protected List<(int elfOneMin, int elfOneMax, int elfTwoMin, int elfTwoMax)> data;
        private ILogger logger;

        public Day04()
        {
            logger = Log.ForContext<Day04Part01>();
            data = new();
        }

        public void Ingest(List<string> input)
        {
            foreach (var row in input)
            {
                var pair = row.Split(',');

                var elfOne = pair[0].Split('-');
                var elfTwo = pair[1].Split('-');

                (int elfOneMin, int elfOneMax, int elfTwoMin, int elfTwoMax) elfPairRange = (
                    int.Parse(elfOne[0]),
                    int.Parse(elfOne[1]),
                    int.Parse(elfTwo[0]),
                    int.Parse(elfTwo[1])
                );

                data.Add(elfPairRange);

            }
        }

        public abstract ISolution Solve();
    }
  }    
