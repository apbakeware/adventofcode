  using System;
  using Serilog;
  using Serilog.Context;
  using Core;
  
  namespace Solvers2022
  {
  
      // Checklist:
      //  * Implement data variable type
      //  * Implement Injest
      //  * Implement Solve for each class
      //  * Extract classes into dedicated .cs files
  
      public abstract class Day16 : ISolver
      {
        // TODO: Update data collection type
        protected List<string> data;
  
        private ILogger logger;
  
        public Day16()
        {
            logger = Log.ForContext<Day16>();
            data = new();
        }
  
        public void Ingest(List<string> input)
        {
            // TODO: Update Injest
            data = input.ConvertAll( x => x );
        }
  
        public abstract ISolution Solve();
      }
  
      public class Day16Part01 : Day16
      {
          private ILogger logger;
  
          public Day16Part01()
          {
            logger = Log.ForContext<Day16Part01>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("16-01", "Unimplemented");
          }
      }
  
      public class Day16Part02 : Day16
      {
          private ILogger logger;
  
          public Day16Part02()
          {
            logger = Log.ForContext<Day16Part02>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("16-02", "Unimplemented");
          }
      }
    }    
