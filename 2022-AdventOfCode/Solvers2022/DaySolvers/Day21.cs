  using System;
  using Serilog;
  using Serilog.Context;
  using Core;
  
  namespace Solvers2022
  {
  
      // Checklist:
      //  * Implement data variable type
      //  * Implement Injest
      //  * Implement Solve for each class
      //  * Extract classes into dedicated .cs files
  
      public abstract class Day21 : ISolver
      {
        // TODO: Update data collection type
        protected List<string> data;
  
        private ILogger logger;
  
        public Day21()
        {
            logger = Log.ForContext<Day21>();
            data = new();
        }
  
        public void Ingest(List<string> input)
        {
            // TODO: Update Injest
            data = input.ConvertAll( x => x );
        }
  
        public abstract ISolution Solve();
      }
  
      public class Day21Part01 : Day21
      {
          private ILogger logger;
  
          public Day21Part01()
          {
            logger = Log.ForContext<Day21Part01>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("21-01", "Unimplemented");
          }
      }
  
      public class Day21Part02 : Day21
      {
          private ILogger logger;
  
          public Day21Part02()
          {
            logger = Log.ForContext<Day21Part02>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("21-02", "Unimplemented");
          }
      }
    }    
