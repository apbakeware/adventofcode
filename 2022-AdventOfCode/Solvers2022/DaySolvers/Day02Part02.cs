using System;
using Serilog;
using Serilog.Context;
using Core;
using Solvers2022.Support;

namespace Solvers2022
{
    public class Day02Part02 : ISolver
    {
        protected ILogger logger;
        private List<(RockPaperScissors, char)> data;

        public Day02Part02()
        {
          logger = Log.ForContext<Day02Part02>();
          data = new();
        }

        public void Ingest(List<string> input)
        {
            foreach (var row in input)
            {
                var split = row.Split(' ');
                data.Add((RockPaperScissorsOps.FromChar(split[0][0]), split[1][0]));
            }
        }
    
        public ISolution Solve()
        {
            int totalScore = 0;
            foreach(var row in data)
            {
                RockPaperScissors myMove;
                if (NeedToLose(row.Item2))
                {
                    logger.Debug("I need to lose");
                    myMove = RockPaperScissorsOps.ToLoseTo(row.Item1);
                }
                else if(NeedToWin(row.Item2))
                {
                    logger.Debug("I need to win");
                    myMove = RockPaperScissorsOps.ToBeat(row.Item1);
                }
                else
                {
                    logger.Debug("I need to tie");
                    myMove = row.Item1;
                }

                int roundScore = RockPaperScissorsOps.ScoreRound(row.Item1, myMove);
                logger.Debug("Thier Move: {Them}  Result Code: {Result}  My Move: {Me}",
                    row.Item1, row.Item2, myMove);
                logger.Debug("Round Score: {Score}", roundScore);
                
                totalScore += roundScore;
            }
            return new SingleValueSolution<int>("02-02", totalScore);
        }

        public static bool NeedToWin(char inChar)
        {
            return inChar == 'Z';
        }

        public static bool NeedToLose(char inChar)
        {
            return inChar == 'X';
        }
    }
  }    
