using System;
using Serilog;
using Serilog.Context;
using Core;
using FluentAssertions.Data;

namespace Solvers2022
{

    public class Day04Part01 : Day04
    {
        private ILogger logger;
        
        public Day04Part01()
        {
          logger = Log.ForContext<Day04Part01>();
        }
    
        public override ISolution Solve()
        {
            int count = 0;

            foreach(var elfPair in data)
            {
                bool elfTwoContainsOne = elfPair.elfOneMin >= elfPair.elfTwoMin && elfPair.elfOneMax <= elfPair.elfTwoMax;
                bool elfOneContainsTwo = elfPair.elfTwoMin >= elfPair.elfOneMin && elfPair.elfTwoMax <= elfPair.elfOneMax;

                if( elfOneContainsTwo || elfTwoContainsOne )
                {
                    count++;
                }
            }

           return new SingleValueSolution<int>("04-01", count);
        }
    }
  }    
