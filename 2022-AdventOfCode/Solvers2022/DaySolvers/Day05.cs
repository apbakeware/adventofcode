using System;
using Serilog;
using Serilog.Context;
using Core;

namespace Solvers2022
{

    public class CrateStack
    {
        // Size of number of stacks + 1. Stack[0] is not used, but makes indexing simplier
        public Stack<char>[] Stacks;

        public Stack<char> this[int index]
        {
            get => Stacks[index];
        }

        public CrateStack(int numberOfStacks)
        {
            Stacks = new Stack<char>[numberOfStacks + 1];
            for(int idx = 0; idx < Stacks.Length; idx++)
            {
                Stacks[idx] = new();
            }
        }

        // inverts order of crates being moved
        public void MoveOneAtATime(int count, int fromStackIdx, int toStackIdx)
        {
            for (int cnt = 0; cnt < count; cnt++)
            {
                Stacks[toStackIdx].Push(Stacks[fromStackIdx].Pop());
            }
        }

        // maintains order of stack thats being move
        public void MoveStackAtOnce(int count, int fromStackIdx, int toStackIdx)
        {
            string cratesToMove = "";
            for (int cnt = 0; cnt < count; cnt++)
            {
                cratesToMove += Stacks[fromStackIdx].Pop();
            }

            foreach(var crate in cratesToMove.Reverse())
            {
                Stacks[toStackIdx].Push(crate);
            }
        }

        public string TopStackContents()
        {
            string content = "";
            for(int idx = 1; idx < Stacks.Length; idx++)
            {
                content += Stacks[idx].Peek();
            }
            return content;
        }
    }

    public abstract class Day05 : ISolver
    {
        protected CrateStack crates;
        protected List<string> moveLines;
        private ILogger logger;

        public Day05()
        {
            logger = Log.ForContext<Day05>();
        }

        public void Ingest(List<string> input)
        {
            List<string> crateStackLines = new();
            moveLines = new();
            
            foreach (var row in input)
            {
                if (row.Contains('['))
                {
                    crateStackLines.Add(row);
                }
                else if (row.StartsWith("move"))
                {
                    moveLines.Add(row);
                }
                else if (row.Length != 0)
                {
                    var split = row.Split(" ", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                    int numberOfStacks = int.Parse(split[^1]);
                    crates = new(numberOfStacks);
                }
            }

            // Reverse it because we need to insert from the bottom
            crateStackLines.Reverse();
            foreach (var stackLine in crateStackLines)
            {
                int stackIndex = 1;
                for(int idx = 1; idx < stackLine.Length; idx+=4)
                {
                    var crateContent = stackLine[idx];
                    if(char.IsLetter(crateContent))
                    {
                        crates[stackIndex].Push(crateContent);
                    }
                    stackIndex++;
                }
            }
        }

        public static void ExtractCratesToMove(string moveLine, out int numToMove, out int fromStackIdx, out int toStackIdx)
        {
            var split = moveLine.Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            numToMove = int.Parse(split[1]);
            fromStackIdx = int.Parse(split[3]);
            toStackIdx = int.Parse(split[5]);
        }

        public abstract ISolution Solve();
    }

    public class Day05Part01 : Day05
    {
        private ILogger logger;

        public Day05Part01()
        {
            logger = Log.ForContext<Day05Part01>();
        }

        public override ISolution Solve()
        {
            
            foreach(var moveLine in moveLines)
            {
                int numToMove, fromStackIdx, toStackIdx;
                ExtractCratesToMove(moveLine, out numToMove, out fromStackIdx, out toStackIdx);

                crates.MoveOneAtATime(numToMove, fromStackIdx, toStackIdx);
            }

            return new SingleValueSolution<string>("05-01", crates.TopStackContents());
        }
    }

    public class Day05Part02 : Day05
    {
        private ILogger logger;

        public Day05Part02()
        {
            logger = Log.ForContext<Day05Part02>();
        }

        public override ISolution Solve()
        {
            foreach (var moveLine in moveLines)
            {
                int numToMove, fromStackIdx, toStackIdx;
                ExtractCratesToMove(moveLine, out numToMove, out fromStackIdx, out toStackIdx);

                crates.MoveStackAtOnce(numToMove, fromStackIdx, toStackIdx);
            }

            return new SingleValueSolution<string>("05-02", crates.TopStackContents());
        }
    }
}
