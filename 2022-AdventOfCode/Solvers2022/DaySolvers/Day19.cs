  using System;
  using Serilog;
  using Serilog.Context;
  using Core;
  
  namespace Solvers2022
  {
  
      // Checklist:
      //  * Implement data variable type
      //  * Implement Injest
      //  * Implement Solve for each class
      //  * Extract classes into dedicated .cs files
  
      public abstract class Day19 : ISolver
      {
        // TODO: Update data collection type
        protected List<string> data;
  
        private ILogger logger;
  
        public Day19()
        {
            logger = Log.ForContext<Day19>();
            data = new();
        }
  
        public void Ingest(List<string> input)
        {
            // TODO: Update Injest
            data = input.ConvertAll( x => x );
        }
  
        public abstract ISolution Solve();
      }
  
      public class Day19Part01 : Day19
      {
          private ILogger logger;
  
          public Day19Part01()
          {
            logger = Log.ForContext<Day19Part01>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("19-01", "Unimplemented");
          }
      }
  
      public class Day19Part02 : Day19
      {
          private ILogger logger;
  
          public Day19Part02()
          {
            logger = Log.ForContext<Day19Part02>();
          }
      
          public override ISolution Solve()
          {
             // TODO: Implement Solve()
             return new SingleValueSolution<string>("19-02", "Unimplemented");
          }
      }
    }    
