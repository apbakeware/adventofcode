using System;
using System.Numerics;
using Serilog;
using Serilog.Context;
using Core;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Core.Exceptions;

namespace Solvers2022
{

    // Checklist:
    //  * Implement data variable type
    //  * Implement Injest
    //  * Implement Solve for each class
    //  * Extract classes uinto dedicated .cs files

    public class MischeviousMonkey
    {
        public int Id { get; protected init; }
        public Queue<uint> Items { get; protected init; }
        public Func<uint, uint>? WorryOperation { get; set; }
        public Func<uint, bool>? WorryPredicate { get; set; }
        public MischeviousMonkey? DestinationIfTrue { get; set; }
        public MischeviousMonkey? DestinationIfFalse { get; set; }

        public uint NumberOfInspections { get; private set; }

        private ILogger logger;

        public MischeviousMonkey(int id)
        {
            Id = id;
            Items = new();
            WorryOperation = null;
            WorryPredicate = null;
            DestinationIfTrue = null;
            DestinationIfFalse = null;
            NumberOfInspections = 0;

            logger = Log.ForContext<MischeviousMonkey>();
        }

        public void CauseMischief()
        {
            while(Items.Count > 0)
            {
                uint worryLevel = Items.Dequeue();
                uint worryLevelOp = WorryOperation(worryLevel);
                uint worryLevelPostInspect = worryLevelOp / 3;
                bool worryTest = WorryPredicate(worryLevelPostInspect);
                MischeviousMonkey throwTo = worryTest ? DestinationIfTrue : DestinationIfFalse;
                NumberOfInspections++;

                throwTo.Items.Enqueue(worryLevelPostInspect);
                
                logger.Debug("Monkey -- ID: {Id}  Worry:  {Worry}  WorryOp: {Op}  WorryPost: {Post}  Test:{Test}  Throwing To: {Tgt}",
                    Id, worryLevel, worryLevelOp, worryLevelPostInspect, worryTest, throwTo.Id);
            }
        }

        public void CauseMischief2()
        {
            while (Items.Count > 0)
            {
                uint worryLevelOp = WorryOperation(Items.Dequeue());
                bool worryTest = WorryPredicate(worryLevelOp);
                MischeviousMonkey throwTo = worryTest ? DestinationIfTrue : DestinationIfFalse;
                NumberOfInspections++;

                throwTo.Items.Enqueue(worryLevelOp);

                //logger.Debug("Monkey -- ID: {Id}  Worry:  {Worry}  WorryOp: {Op}  Test:{Test}  Throwing To: {Tgt}",
                //    Id, worryLevel, worryLevelOp, worryTest, throwTo.Id);
            }
        }
    }


    public abstract class Day11 : ISolver
    {
        public record struct MonkeyTossRecord
        {
            public int ThrowingMonkeyId;
            public int TargetMonkeyId;
        }

        protected List<MischeviousMonkey> data;

        private ILogger logger;
        private List<MonkeyTossRecord> tossTrueTargets;
        private List<MonkeyTossRecord> tossFalseTargets;
        private Func<string, MischeviousMonkey, MischeviousMonkey> lineParser;

        public Day11()
        {
            logger = Log.ForContext<Day11>();
            data = new();
            tossTrueTargets = new();
            tossFalseTargets = new();
            lineParser = ParseMonkeyLine;
        }

        public void Ingest(List<string> input)
        {
            MischeviousMonkey monkey = null;
            foreach (var row in input)
            {

                // build parser using sequential line parsers
                var trimmedRow = row.Trim();
                monkey = lineParser(trimmedRow, monkey);
            }

            // Can probably simplify the loop ...
            foreach(var tossRecord in tossTrueTargets)
            {
                // TODO: test rsult
                var thrower = data.Find((x) => x.Id == tossRecord.ThrowingMonkeyId);
                var receiver = data.Find((x) => x.Id == tossRecord.TargetMonkeyId);
                thrower.DestinationIfTrue = receiver;
            }

            foreach (var tossRecord in tossFalseTargets)
            {
                // TODO: test rsult
                var thrower = data.Find((x) => x.Id == tossRecord.ThrowingMonkeyId);
                var receiver = data.Find((x) => x.Id == tossRecord.TargetMonkeyId);
                thrower.DestinationIfFalse = receiver;
            }
        }

        public abstract ISolution Solve();

        private MischeviousMonkey ParseMonkeyLine(string line, MischeviousMonkey unused)
        {
            // Blank line is unused
            if (line.Length == 0)
            {
                return null;
            }

            // TODO validate Mokney line
            MischeviousMonkey newMonkey = null;
            Regex regex = new Regex(@"Monkey (\d+):");
            Match match = regex.Match(line);
            if (match.Success)
            {
                //Take matches from each capturing group here. match.Groups[n].Value;
                int monkeyId = int.Parse(match.Groups[1].Value);
                newMonkey = new(monkeyId);
                data.Add(newMonkey);
            }
            else
            {
                // todo throw
            }

            lineParser = ParseStartingItems;
            return newMonkey;
        }

        private MischeviousMonkey ParseStartingItems(string line, MischeviousMonkey monkey)
        {
            if (!line.StartsWith("Starting items:"))
            {
                // TODO: throw
            }

            var fragments = line.Split(':')[1].Split(", ");
            foreach (var fragment in fragments)
            {
                uint value = uint.Parse(fragment);
                monkey.Items.Enqueue(value);
            }

            lineParser = ParseOperation;
            return monkey;
        }

        private MischeviousMonkey ParseOperation(string line, MischeviousMonkey monkey)
        {
            if (!line.StartsWith("Operation:"))
            {
                logger.Error("ParseOperation() -- Unexpected start of line '{Line}'", line);
            }

            var fragments = line.Split(' ');
            var operation = fragments[4];
            var toApply = fragments[5];

            if (operation == "*")
            {
                if (toApply == "old")
                {
                    logger.Debug("Operation: (x) => x * x");
                    monkey.WorryOperation = (x) => x * x;
                }
                else
                {
                    uint valueToApply = uint.Parse(toApply);
                    logger.Debug("Operation: (x) => x * {Val}", valueToApply);
                    monkey.WorryOperation = (x) => x * valueToApply;
                }
            }
            else if (operation == "+")
            {
                if (toApply == "old")
                {
                    logger.Debug("Operation: (x) => x + x");
                    monkey.WorryOperation = (x) => x + x;
                }
                else
                {
                    uint valueToApply = uint.Parse(toApply);
                    logger.Debug("Operation: (x) => x + {Val}", valueToApply);
                    monkey.WorryOperation = (x) => x + valueToApply;
                }
            }
            else
            {
                logger.Error("ParseOperation() -- Unknown operation '{Op}'", operation);
                throw new SolverException("Unknown operation");
            }

            lineParser = ParseTestDivisible;
            return monkey;
        }

        private MischeviousMonkey ParseTestDivisible(string line, MischeviousMonkey monkey)
        {
            if (!line.StartsWith("Test: divisible by"))
            {
                logger.Error("ParseTestDivisible() -- Unexpected start of line '{Line}'", line);
            }

            var fragments = line.Split(' ');
            uint factor = uint.Parse(fragments[^1]);

            logger.Debug("Predicate: (x) => x % {Factor} == 0", factor);
            monkey.WorryPredicate = (x) => x % factor == 0;

            lineParser = ParseIfTrue;
            return monkey;
        }

        private MischeviousMonkey ParseIfTrue(string line, MischeviousMonkey monkey)
        {
            if (!line.StartsWith("If true:"))
            {
                logger.Error("ParseIfTrue() -- Unexpected start of line '{Line}'", line);
            }

            var fragments = line.Split(' ');
            int destMonkey = int.Parse(fragments[^1]);
            MonkeyTossRecord tossRecord = new MonkeyTossRecord
            {
                ThrowingMonkeyId = monkey.Id,
                TargetMonkeyId = destMonkey
            };
            tossTrueTargets.Add(tossRecord);
            lineParser = ParseIfFalse;
            return monkey;
        }

        private MischeviousMonkey ParseIfFalse(string line, MischeviousMonkey monkey)
        {
            if (!line.StartsWith("If false:"))
            {
                logger.Error("ParseIfFalse() -- Unexpected start of line '{Line}'", line);
            }

            var fragments = line.Split(' ');
            int destMonkey = int.Parse(fragments[^1]);
            MonkeyTossRecord tossRecord = new MonkeyTossRecord
            {
                ThrowingMonkeyId = monkey.Id,
                TargetMonkeyId = destMonkey
            };
            tossFalseTargets.Add(tossRecord);
            lineParser = ParseMonkeyLine;
            return monkey;
        }
    }

    public class Day11Part01 : Day11
    {
        private ILogger logger;

        public Day11Part01()
        {
            logger = Log.ForContext<Day11Part01>();
        }

        public override ISolution Solve()
        {
            logger.Debug(" ~~~~~~ CAUSING MISCHIEF ~~~~~~");
            for(int cnt = 0; cnt < 20; cnt++)
            {
                PerformRoundOfMischief();
            }

            data.Sort((x, y) => x.NumberOfInspections.CompareTo(y.NumberOfInspections));
            data.Reverse();

            return new SingleValueSolution<uint>("11-01", data[0].NumberOfInspections * data[1].NumberOfInspections);
        }

        protected void PerformRoundOfMischief()
        {
            foreach (var monkey in data)
            {
                monkey.CauseMischief();

                if (Log.IsEnabled(Serilog.Events.LogEventLevel.Debug))
                {
                    logger.Debug(" ~~~~~~ Monkey {Id} Complete ~~~~~~", monkey.Id);
                    foreach (var m2 in data)
                    {
                        logger.Debug("Monkey: {Id}  Items: {Items}", m2.Id, m2.Items);
                    }
                }
            }

            if (Log.IsEnabled(Serilog.Events.LogEventLevel.Debug))
            {
                logger.Debug(" ~~~~~~ ROUND COMPLETE ~~~~~~");
                foreach (var monkey in data)
                {
                    logger.Debug("Monkey: {Id}  Items: {Items}", monkey.Id, monkey.Items);
                }
            }
        }
    }

    public class Day11Part02 : Day11
    {
        private ILogger logger;

        public Day11Part02()
        {
            logger = Log.ForContext<Day11Part02>();
        }

        public override ISolution Solve()
        {
            logger.Debug(" ~~~~~~ CAUSING MISCHIEF ~~~~~~");
            //for (uint cnt = 1; cnt <= 2000; cnt++)
            //{
            //    PerformRoundOfMischief();

            //    if(cnt == 1 || cnt == 20 || cnt == 1000 || cnt == 2000)
            //    {
            //        logger.Information(" ~~~~~~ Round {Cnt} Complete ~~~~~~", cnt);
            //        foreach (var m2 in data)
            //        {
            //            logger.Information("Monkey: {Id}  Inspections: {I}", m2.Id, m2.NumberOfInspections);
            //        }
            //    }
            //}

            //data.Sort((x, y) => x.NumberOfInspections.CompareTo(y.NumberOfInspections));
            //data.Reverse();
            //logger.Information("Number of inspections {A} * {B} = {total}",
            //    data[0].NumberOfInspections, data[1].NumberOfInspections,
            //    data[0].NumberOfInspections * data[1].NumberOfInspections);

            return new SingleValueSolution<int>("11-02", -1);
        }

        protected void PerformRoundOfMischief()
        {
            foreach (var monkey in data)
            {
                monkey.CauseMischief2();

                if (Log.IsEnabled(Serilog.Events.LogEventLevel.Debug))
                {
                    logger.Debug(" ~~~~~~ Monkey {Id} Complete ~~~~~~", monkey.Id);
                    foreach (var m2 in data)
                    {
                        logger.Debug("Monkey: {Id}  Items: {Items}", m2.Id, m2.Items);
                    }
                }
            }

            if (Log.IsEnabled(Serilog.Events.LogEventLevel.Debug))
            {
                logger.Debug(" ~~~~~~ ROUND COMPLETE ~~~~~~");
                foreach (var monkey in data)
                {
                    logger.Debug("Monkey: {Id}  Items: {Items}", monkey.Id, monkey.Items);
                }
            }
        }
    }
}
