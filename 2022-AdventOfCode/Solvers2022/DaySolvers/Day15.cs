using System;
using System.Text.RegularExpressions;
using Serilog;
using Serilog.Context;
using Core;
using Solvers2022.Support.Grid;
using FluentAssertions.Equivalency.Steps;
using System.Collections;
using static Solvers2022.Day14;
using System.Text;
using static Solvers2022.Day15;

namespace Solvers2022
{

    public abstract class Day15 : ISolver
    {
        public struct SensorAndBeacon
        {
            public Coord Sensor;
            public Coord Beacon;
            public int ManhattenDistance
            {
                get => CoordinateUtils.ManhattenDistace(Sensor,Beacon);
            }

            public SensorAndBeacon()
            {
                Sensor = new Coord();
                Beacon = new Coord();
            }

            public SensorAndBeacon(Coord sensor, Coord beacon)
            {
                Sensor = sensor;
                Beacon = beacon;
            }
        }

        // TODO: Update data collection type
        protected List<SensorAndBeacon> sensorsAndBeacons;

        private ILogger logger;

        public Day15()
        {
            logger = Log.ForContext<Day15>();
            sensorsAndBeacons = new();
        }

        public void Ingest(List<string> input)
        {
            string pattern = @".*x=(?<SensorX>-?\d+), y=(?<SensorY>-?\d+): .*x=(?<BeaconX>-?\d+), y=(?<BeaconY>-?\d+)";

            foreach(var row in input)
            {
                Match match = Regex.Match(row, pattern, RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    SensorAndBeacon dataPoint = new SensorAndBeacon(
                        new Coord(int.Parse(match.Groups["SensorX"].Value), int.Parse(match.Groups["SensorY"].Value)),
                        new Coord(int.Parse(match.Groups["BeaconX"].Value), int.Parse(match.Groups["BeaconY"].Value))
                    );

                    sensorsAndBeacons.Add(dataPoint);
                }
                else
                {
                    // throw
                }
            }
        }

        public abstract ISolution Solve();
    }

    public class Day15Part01 : Day15
    {
        private ILogger logger;

        public Day15Part01()
        {
            logger = Log.ForContext<Day15Part01>();
        }

        public override ISolution Solve()
        {
            // int exclusion_row = 10;  // How to parameterize for test...
            int exclusion_row = 2000000;

            HashSet<Coord> exlusion_points = new();
            FindExclusionCells(exclusion_row, exlusion_points);

            foreach (var sensorAndBeacon in sensorsAndBeacons)
            {
                exlusion_points.Remove(sensorAndBeacon.Beacon);
            }

            return new SingleValueSolution<int>("15-01", exlusion_points.Count);
        }

        private void FindExclusionCells(int exclusion_row, HashSet<Coord> exlusion_points)
        {
            foreach (var sensorAndBeacon in sensorsAndBeacons)
            {
                //logger.Verbose("Sensor: {S}  Beacon: {B}  mDist: {D}",
                //    sensorAndBeacon.Sensor,
                //    sensorAndBeacon.Beacon,
                //    sensorAndBeacon.ManhattenDistance);

                int distance_to_exclusion_row = Math.Abs(exclusion_row - sensorAndBeacon.Sensor.Y);
                int span_in_exclusion_row = sensorAndBeacon.ManhattenDistance - distance_to_exclusion_row;
                //logger.Verbose("Span to mark in exclusion row: {Span}", span_in_exclusion_row);
                if (span_in_exclusion_row >= 0)
                {
                    Coord span_start = new(sensorAndBeacon.Sensor.X - span_in_exclusion_row, exclusion_row);
                    Coord span_end = new(sensorAndBeacon.Sensor.X + span_in_exclusion_row, exclusion_row);
                    foreach (var exclusion in CoordinateUtils.HorizontalSpan(span_start, span_end))
                    {
                        //logger.Verbose("Marking as exlusion: {Ex}", exclusion);
                        exlusion_points.Add(exclusion);
                    }
                }
            }
        }
    }

    public class Day15Part02 : Day15
    {
        public enum CellStatus
        {
            OPEN,
            SENSOR,
            BEACON,
            INHIBITED,
        }

        private ILogger logger;
        private Grid<CellStatus> possibleLocations;

        public Day15Part02()
        {
            logger = Log.ForContext<Day15Part02>();
        }

        public override ISolution Solve()
        {
            // TODO:
            //  Create polygons for each sensor region
            //   Test each grid point against the sensor region
            //   Optimize the sensors to choose from as most within a polygon will fail
            //   
            //   Optimize create a single polygon to minimize edges tested

            int result = -1;
            int coordinate_limit = 20;
            List<List<Coord>> polygons = new List<List<Coord>>();

            foreach(var record in sensorsAndBeacons)
            {
                // Create a polygon from extents
                List<Coord> polygon = new()
                {
                    CoordinateTranslation.Translate(record.Sensor, 0, record.ManhattenDistance),
                    CoordinateTranslation.Translate(record.Sensor, record.ManhattenDistance, 0),
                    CoordinateTranslation.Translate(record.Sensor, 0, -record.ManhattenDistance),
                    CoordinateTranslation.Translate(record.Sensor, -record.ManhattenDistance, 0)
                };

                polygons.Add(polygon);
            }

            for(int y = 0; y <= coordinate_limit; y++)
            {
                for(int x = 0; x <= coordinate_limit; x++)
                {
                    Coord testPoint = new(x, y);
                    bool isInsidePolygon = false;
                    foreach(var polygon in polygons)
                    {
                        isInsidePolygon = GeometryUtils.PointEnclosedByConvexPolygon(polygon, testPoint);
                        if(isInsidePolygon)
                        {
                            break; // the foreach
                        }
                    }

                    if(!isInsidePolygon)
                    {
                        // foundit
                        result = 4000000 * x + y;
                        logger.Debug("Logger found: x: {X}  y: {Y}", x, y);
                    }
                }
            }


            return new SingleValueSolution<int>("15-02", result);
        }

        public void RenderGrid()
        {
            foreach (var row in possibleLocations.GridRows)
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var cell in row)
                {
                    string str = cell switch
                    {
                        CellStatus.OPEN => ".",
                        CellStatus.BEACON => "B",
                        CellStatus.SENSOR => "S",
                        CellStatus.INHIBITED => "#",
                        _ => throw new Exception("Unahnded cell condition")
                    };
                    stringBuilder.Append(str);
                }
                logger.Information("Rows: [{R}]", stringBuilder.ToString());
            }
        }
    }

}
