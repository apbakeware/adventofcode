using System;
using Serilog;
using Serilog.Context;
using Core;
using Solvers2022.Support.Grid;
using System.Drawing;
using System.Text;

namespace Solvers2022
{
    public abstract class Day14 : ISolver
    {
        public enum SpaceType
        {
            OPEN,
            ROCK,
            SAND,
            ABYSS,
            FLOOR
        }

        protected Grid<SpaceType> grid;
        protected int maxScanHeight;

        private ILogger logger;

        public Day14()
        {
            logger = Log.ForContext<Day14>();
            grid = new(200,1000, SpaceType.OPEN);
            maxScanHeight = 0;
        }

        public void Ingest(List<string> input)
        {
            Queue<Coord> coordQueue = new();
            foreach (var row in input)
            {
                var coordSplits = row.Split("->");
                foreach (var coordStr in coordSplits)
                {
                    var individalCoord = coordStr.Split(',');
                    Coord coord = new(int.Parse(individalCoord[0]), int.Parse(individalCoord[1]));
                    coordQueue.Enqueue(coord);

                    maxScanHeight = Math.Max(maxScanHeight, coord.Y);
                }

                grid.ForEachInLine<SpaceType>(coordQueue, HandleBuildRockPosition);
            }
        }

        public abstract ISolution Solve();

        protected void HandleBuildRockPosition(Coord coord, SpaceType unused)
        {
            logger.Verbose("Setting coordinate {C} to rock", coord);
            MarkCell(coord, SpaceType.ROCK);
        }

        protected void HandleBuildAbyssPosition(Coord coord, SpaceType unused)
        {
            logger.Verbose("Setting coordinate {C} to abyss", coord);
            MarkCell(coord, SpaceType.ABYSS);
        }

        protected void HandleBuildFloorPosition(Coord coord, SpaceType unused)
        {
            logger.Verbose("Setting coordinate {C} to Floor", coord);
            MarkCell(coord, SpaceType.FLOOR);
        }

        protected void MarkCell(Coord coord, SpaceType value)
        {
            logger.Verbose("Setting coordinate {C} to {V}", coord, value);
            grid.Set(coord, value);
        }

        // result: true meands it came to rest
        protected (bool, Coord) DropSand(Coord loc)
        {
            if (GridIsAbyss(loc))
            {
                logger.Information("Fell off the bottom of the grid, assuming abyss");
                return (false, new());
            }

            Coord nextLoc = CoordinateTranslation.Translate(loc, 0, 1);
            if(SandFallsThroughCell(nextLoc))
            {
                return DropSand(nextLoc);
            }

            nextLoc = CoordinateTranslation.Translate(loc, -1, 1);
            if(SandFallsThroughCell(nextLoc))
            {
                return DropSand(nextLoc);
            }

            nextLoc = CoordinateTranslation.Translate(loc, 1, 1);
            if (SandFallsThroughCell(nextLoc))
            {
                return DropSand(nextLoc);
            }

            MarkCell(loc, SpaceType.SAND);
            logger.Debug("Sand came to rest at: {C}", loc);

            return (true, loc);
        }

        protected bool SandFallsThroughCell(Coord coord)
        {
            return grid.At(coord) switch
            {
                SpaceType.OPEN => true,
                SpaceType.ABYSS => true,
                SpaceType.SAND => false,
                SpaceType.FLOOR => false,
                SpaceType.ROCK => false,
                _ => throw new ArgumentException($"Unhandled SpaceType: {grid.At(coord).ToString()}")
            };
        }

        protected bool GridIsAbyss(Coord coord)
        {
            return grid.At(coord) == SpaceType.ABYSS;
        }

        public void RenderGrid()
        {
            foreach (var row in grid.GridRows)
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (var cell in row)
                {
                    string str = cell switch
                    {
                        SpaceType.ROCK => "#",
                        SpaceType.SAND => "O",
                        SpaceType.OPEN => ".",
                        SpaceType.ABYSS => "A",
                        SpaceType.FLOOR=> "F",
                        _ => throw new Exception("Unahnded cell condition")
                    };
                    stringBuilder.Append(str);
                }
                logger.Information("Rows: [{R}]", stringBuilder.ToString());
            }
        }
    }

    public class Day14Part01 : Day14
    {
        private ILogger logger;

        public Day14Part01()
        {
            logger = Log.ForContext<Day14Part01>();
        }

        public override ISolution Solve()
        {
            Coord sand_drop_loc = new(500, 0);
            int number_dropped = 0;

            grid.ForEachInLine<SpaceType>(
                new Coord(0, maxScanHeight + 1),
                new Coord(grid.ColumnCount - 1, maxScanHeight + 1),
                HandleBuildAbyssPosition );

            while (DropSand(sand_drop_loc).Item1)
            {
                number_dropped++;
            }
            logger.Information("Number of sand dropped: {Count}", number_dropped);
            return new SingleValueSolution<int>("14-01", number_dropped);
        }
    }

    public class Day14Part02 : Day14
    {
        private ILogger logger;

        public Day14Part02()
        {
            logger = Log.ForContext<Day14Part02>();
        }

        public override ISolution Solve()
        {
            Coord sand_drop_loc = new(500, 0);
            int number_dropped = 0;

            int floorHeight = maxScanHeight + 2;
            grid.ForEachInLine<SpaceType>(
                new Coord(0, floorHeight),
                new Coord(grid.ColumnCount - 1, floorHeight),
                HandleBuildFloorPosition);

            do
            {
                number_dropped++;
            }
            while (DropSand(sand_drop_loc).Item2 != sand_drop_loc) ;
            
            logger.Information("Number of sand dropped: {Count}", number_dropped);
            return new SingleValueSolution<int>("14-02", number_dropped);
        }
    }
}
