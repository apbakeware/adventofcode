using System;
using Serilog;
using Serilog.Context;
using Core;
using Solvers2022.Support.Grid;
using System.Security.AccessControl;
using System.Runtime.InteropServices;

namespace Solvers2022
{


    public abstract class Day12 : ISolver
    {
        protected Grid<char> heightMap;

        private ILogger logger;

        public Day12()
        {
            logger = Log.ForContext<Day12>();
            heightMap = new();
        }

        public bool CanTraverse(char current, char dest)
        {
            current = (current == 'S') ? 'a' : (current == 'E') ? 'z' : current;
            dest = (dest == 'S') ? 'a' : (dest == 'E') ? 'z' : dest;

            return (dest - current) <= 1;
        }

        public void Ingest(List<string> input)
        {
            foreach(var row in input)
            {
                heightMap.AddRow(row.ToArray());
            }

            logger.Debug("HeightMap: {Hm}", heightMap);
            foreach(var coord in heightMap.EachLocation())
            {
                logger.Information("Coord: {C}  Value: {V}",
                    coord, heightMap.At(coord));
            }
        }

        public abstract ISolution Solve();
    }

    public class Day12Part01 : Day12
    {
        private ILogger logger;
        int minPathLength;

        public Day12Part01()
        {
            logger = Log.ForContext<Day12Part01>();
            minPathLength = int.MaxValue;
        }

        public override ISolution Solve()
        {
            Coord startLoc, destLoc;
            bool foundStart = GridExtensions.TryFindOnGrid(heightMap, 'S', out startLoc);
            bool foundDest = GridExtensions.TryFindOnGrid(heightMap, 'E', out destLoc);

            logger.Information("Found start: {F} @ {Loc}", foundStart, startLoc);
            logger.Information("Found dest: {F} @ {Loc}", foundDest, destLoc);

            BreadthFirstMinPathFinder<char> pathFinder = new(heightMap);
            pathFinder.TraversalPermissiblePrediate = CanTraverse;

            int pathLength = pathFinder.FindShortestPath(startLoc, destLoc);

            logger.Information("min path: {L}", pathLength);

            // Subract 1 to remove the "starting point"
            return new SingleValueSolution<int>("12-01", pathLength );
        }
    }

    public class Day12Part02 : Day12
    {
        private ILogger logger;

        public Day12Part02()
        {
            logger = Log.ForContext<Day12Part02>();
        }

        public override ISolution Solve()
        {
            Coord destLoc;
            bool foundDest = GridExtensions.TryFindOnGrid(heightMap, 'E', out destLoc);
            int shortestPathLength = int.MaxValue;
            BreadthFirstMinPathFinder<char> pathFinder = new(heightMap);
            pathFinder.TraversalPermissiblePrediate = CanTraverse;

            foreach (var coord in heightMap.EachLocation())
            {
                if(heightMap.At(coord) == 'a')
                {
                    shortestPathLength = Math.Min(shortestPathLength,
                        pathFinder.FindShortestPath(coord, destLoc));
                }
            }

            return new SingleValueSolution<int>("12-02", shortestPathLength);
        }
    }
}
