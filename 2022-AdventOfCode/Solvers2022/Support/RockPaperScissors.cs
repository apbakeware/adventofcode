﻿namespace Solvers2022.Support
{
    public enum RockPaperScissors
    {
        ROCK,
        PAPER,
        SCISSORS
    };

    public static class RockPaperScissorsOps
    {
        public static int Score(this RockPaperScissors rps)
        {
            return rps switch
            {
                RockPaperScissors.ROCK => 1,
                RockPaperScissors.PAPER => 2,
                RockPaperScissors.SCISSORS => 3
            };
        }

        public static RockPaperScissors ToLoseTo(this RockPaperScissors rps)
        {
            return rps switch
            {
                RockPaperScissors.ROCK => RockPaperScissors.SCISSORS,
                RockPaperScissors.PAPER => RockPaperScissors.ROCK,
                RockPaperScissors.SCISSORS => RockPaperScissors.PAPER
            };
        }

        public static RockPaperScissors ToBeat(this RockPaperScissors rps)
        {
            return rps switch
            {
                RockPaperScissors.ROCK => RockPaperScissors.PAPER,
                RockPaperScissors.PAPER => RockPaperScissors.SCISSORS,
                RockPaperScissors.SCISSORS => RockPaperScissors.ROCK
            };
        }

        public static bool IWinRound(RockPaperScissors them, RockPaperScissors me)
        {
            return
                them == RockPaperScissors.ROCK && me == RockPaperScissors.PAPER ||
                them == RockPaperScissors.PAPER && me == RockPaperScissors.SCISSORS ||
                them == RockPaperScissors.SCISSORS && me == RockPaperScissors.ROCK;

        }

        public static bool RoundIsTie(RockPaperScissors them, RockPaperScissors me)
        {
            return them == me;
        }

        public static int RoundResultBonus(RockPaperScissors them, RockPaperScissors me)
        {
            if (RoundIsTie(them, me)) return 3;
            if (IWinRound(them, me)) return 6;
            return 0;
        }

        public static int ScoreRound(RockPaperScissors them, RockPaperScissors me)
        {
            return me.Score() + RoundResultBonus(them, me);
        }

        public static RockPaperScissors FromChar(char rpsChar)
        {
            return rpsChar switch
            {
                'A' => RockPaperScissors.ROCK,
                'X' => RockPaperScissors.ROCK,
                'B' => RockPaperScissors.PAPER,
                'Y' => RockPaperScissors.PAPER,
                'C' => RockPaperScissors.SCISSORS,
                'Z' => RockPaperScissors.SCISSORS,
            };
        }
    }
}

