﻿using Serilog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace Solvers2022.Support
{
    /// <summary>
    /// This class creats a mask of all characters which are in a string.
    /// The mask array is 0-indexed starting a 'a' and is case sensitive.
    /// </summary>
    public class AlphabeticCharacterMask
    {
        public const int Size = 26 * 2;

        ILogger _logger;
        private BitArray _charInStringMask;

        public AlphabeticCharacterMask()
        {
            _logger = Log.ForContext<AlphabeticCharacterMask>();
            _charInStringMask = new BitArray(Size);
        }

        public AlphabeticCharacterMask(string statement)
        {
            _logger = Log.ForContext<AlphabeticCharacterMask>();
            _charInStringMask = new BitArray(Size);
            ApplyMask(statement);
        }

        public void ApplyMask(string statement)
        {
            foreach(var c in statement)
            {
                _charInStringMask[IndexFor(c)] = true;
            }
        }

        public bool IsSet(char theChar)
        {
            return _charInStringMask[IndexFor(theChar)];
        }

        // Throws if not an alphabetic character
        static public int IndexFor(char theChar)
        {
            int index = 0;
            if( Char.IsLower(theChar) )
            {
                index = (int)theChar - (int)'a';
            }
            else if( Char.IsUpper(theChar))
            {
                index = (int)theChar - (int)'A' + 26;
            }
            else
            {
                throw new Core.Exceptions.SolverException($"Charcter is not alphabetic: '{theChar}'");
            }

            Log.Debug("Character: '{TheChar}' is index: {Idx}", theChar, index);

            return index;
        }

        static public char AtIndex(int index)
        {
            // TODO: Better "max" cap

            if (index < 0) throw new ArgumentOutOfRangeException("index out of range");
            if (index >= Size) throw new ArgumentOutOfRangeException("index out of range");

            return index < IndexFor('A') ? (char)('a' + index) : (char)('A' + index % 26);
        }

        static public List<char> CommonCharacters(params AlphabeticCharacterMask[] masks)
        {
            List<Char> commonChars = new();
            for (int idx = 0; idx < Size; idx++)
            {
                bool isCommon = true;
                for(int innerIdx = 0; innerIdx < masks.Length; innerIdx++)
                {
                    if (masks[innerIdx]._charInStringMask[idx] == false)
                    {
                        isCommon = false;
                        break;
                    }
                }

                if (isCommon)
                {
                    commonChars.Add(AtIndex(idx));
                }
            }
            return commonChars;
        }
    }
}
