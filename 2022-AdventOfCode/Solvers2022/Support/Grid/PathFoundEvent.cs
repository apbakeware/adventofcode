﻿namespace Solvers2022.Support.Grid
{
    // Checklist:
    //  * Implement data variable type
    //  * Implement Injest
    //  * Implement Solve for each class
    //  * Extract classes into dedicated .cs files

    public class PathFoundEvent : EventArgs
    {
        public int PathLength;

        public PathFoundEvent()
        {
            PathLength = -1;
        }

        public PathFoundEvent(int pathLength)
        {
            PathLength = pathLength;
        }
    }
}
