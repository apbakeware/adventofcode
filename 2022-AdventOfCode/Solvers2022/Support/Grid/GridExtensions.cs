﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Solvers2022.Support.Grid
{
    public static class GridExtensions
    {
        public static bool TryFindOnGrid<T>(this Grid<T> grid, T value, out Coord outCoord)
        {
            foreach(var coord in grid.EachLocation())
            {
                if(EqualityComparer<T>.Default.Equals(grid.At(coord), value))
                { 
                    outCoord = coord;
                    return true;
                }
            }

            outCoord = default;
            return false;
        }

        // inclusive
        public static void ForEachInLine<T>(this Grid<T> grid, Coord start, Coord end, Action<Coord, T> action)
        {
            Coord coord = start;
            int dX = end.X - start.X;
            int dY = end.Y - start.Y;

            // Todo: check coordinates are in a line

            dX = dX == 0 ? 0 : dX > 0 ? 1 : -1;
            dY = dY == 0 ? 0 : dY > 0 ? 1 : -1;

            do
            {
                action(coord, grid.At(coord));
                coord = CoordinateTranslation.Translate(coord, dX, dY);
            } while (coord != end);
            action(coord, grid.At(coord));
        }

        public static void ForEachInLine<T>(this Grid<T> grid, Queue<Coord> coordQueue, Action<Coord, T> action)
        {
            // TODO ensure queue is > 1

            Coord start = coordQueue.Dequeue();
            while(coordQueue.Count > 0)
            {
                ForEachInLine(grid, start, coordQueue.Peek(), action);
                start = coordQueue.Dequeue();
            }
        }
    }
}
