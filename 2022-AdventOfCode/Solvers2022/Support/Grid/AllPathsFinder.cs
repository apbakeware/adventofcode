﻿using Serilog;
using System.Collections;

namespace Solvers2022.Support.Grid
{
    // Instead of using the all path finder (poor performance)
    // breadth walk the nodes and assign a value to them all..

    // Find all paths from start to finish
    public class AllPathsFinder<T>
    {
        // Function used to check if a the path may be traversed from one cell value to another
        public Func<T, T, bool> TraversalPermissiblePrediate { get; set; }

        public event EventHandler<PathFoundEvent> PathFound;

        private ILogger logger;

        private Grid<T> grid;
        private BitArray visits;
        private Coord pathStart;
        private Coord pathEnd;

        private Stack<Coord> currentPath;
        private int traverseCallCount;

        public AllPathsFinder(Grid<T> grid)
        {
            logger = Log.ForContext<AllPathsFinder<T>>();
            this.grid = grid;
            visits = new(grid.RowCount * grid.ColumnCount, false);
        }

        public void FindPaths(Coord start, Coord end)
        {
            pathStart = start;
            pathEnd = end;
            currentPath = new();
            traverseCallCount = 0;
            Traverse(start);
        }

        public void Traverse(Coord currentLocation)
        {
            logger.Debug("Traversing location: {Loc}", currentLocation);
            traverseCallCount++;

            if (traverseCallCount % 1000 == 0)
            {
                logger.Information("Tarverse count: {C}  visited: {V}",
                    traverseCallCount, visits.ToString());
            }

            if (!grid.OnGrid(currentLocation))
            {
                logger.Debug("  not on grid");
                return;
            }

            if (HasBeenVisited(currentLocation))
            {
                logger.Debug("  has already been visited");
                return;
            }


            currentPath.Push(currentLocation);
            SetVisited(currentLocation, true);

            if (currentLocation != pathEnd)
            {
                var neighbors = new Coord[]
                {
                    CoordinateTranslation.Up(currentLocation),
                    CoordinateTranslation.Right(currentLocation),
                    CoordinateTranslation.Down(currentLocation),
                    CoordinateTranslation.Left(currentLocation)
                };

                foreach (var neighbor in neighbors)
                {
                    if (grid.OnGrid(neighbor) && TraversalPermissiblePrediate(grid.At(currentLocation), grid.At(neighbor)))
                    {
                        Traverse(neighbor);
                    }
                }
            }
            else
            {
                Log.Information(" !!!!  FOUND THE END !!!!  length: {L}", currentPath.Count);
                logger.Information("Path: {P}", currentPath);
                TriggerPathFoundEvent();
            }

            SetVisited(currentLocation, false);
            currentPath.Pop();
        }

        private bool HasBeenVisited(Coord testLoc)
        {
            return visits[VisitsIndexFor(testLoc)];
        }

        private void SetVisited(Coord testLoc, bool value)
        {
            visits[VisitsIndexFor(testLoc)] = value;
        }

        private int VisitsIndexFor(Coord coord)
        {
            return coord.Y * grid.ColumnCount + coord.X;
        }

        private void TriggerPathFoundEvent()
        {
            PathFoundEvent pathEvent = new PathFoundEvent(currentPath.Count);
            PathFound?.Invoke(this, pathEvent);
        }
    }
}
