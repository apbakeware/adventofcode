﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solvers2022.Support.Grid
{
    // Value type
    public record struct Coord
    {
        public int X { get; init; }
        public int Y { get; init; }

        public Coord()
        {
            X = 0;
            Y = 0;
        }

        public Coord(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }
}
