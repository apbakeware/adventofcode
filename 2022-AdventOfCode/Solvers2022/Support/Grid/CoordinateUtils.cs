﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Solvers2022.Support.Grid
{
    /// <summary>
    /// This class provides a suite of utilities for 
    /// the Coord class.
    /// </summary>
    public static class CoordinateUtils
    {
        public static IEnumerable<Coord> Neighbors4(Coord coord)
        {
            yield return CoordinateTranslation.Up(coord);
            yield return CoordinateTranslation.Right(coord);
            yield return CoordinateTranslation.Down(coord);
            yield return CoordinateTranslation.Left(coord);
        }

        public static IEnumerable<Coord> Neighbors8(Coord coord)
        {
            yield return CoordinateTranslation.Translate(coord, 0, -1);
            yield return CoordinateTranslation.Translate(coord, 1, -1);
            yield return CoordinateTranslation.Translate(coord, 1, 0);
            yield return CoordinateTranslation.Translate(coord, 1, 1);
            yield return CoordinateTranslation.Translate(coord, 0, 1);
            yield return CoordinateTranslation.Translate(coord, -1, 1);
            yield return CoordinateTranslation.Translate(coord, -1, 0);
            yield return CoordinateTranslation.Translate(coord, -1, -1);
        }

        // Return the neighborhood which is eveny coordinate within the specified
        // manhatten distance. Iterated from the "top" on a grid with 0,0 as upper left
        public static IEnumerable<Coord> EnumerateDiamondNeighbors(Coord coord, int manhattenDistance)
        {
            // assumes distancd is >= 0
            for(int y = -manhattenDistance; y <= manhattenDistance; y++)
            {
                int remainingDistance = manhattenDistance - Math.Abs(y);
                for(int x = -remainingDistance; x <= remainingDistance; x++)
                {
                    if (x != 0 || y != 0)
                    {
                        yield return CoordinateTranslation.Translate(coord, x, y);
                    }
                }
            }
        }

        public static List<Coord> GetDiamondNeighbors(Coord coord, int manhattenDistance)
        {
            List<Coord> result = new List<Coord>(EnumerateDiamondNeighbors(coord, manhattenDistance));
            return result;
        }


        // Horizontal line
        public static IEnumerable<Coord> HorizontalSpan(Coord start, Coord end)
        {
            // TODO: ensure in horizontal line
            Coord coord = start;
            int x_translation = (start.X < end.X) ? 1 : -1;
            while (coord != end)
            {
                yield return coord;
                coord = CoordinateTranslation.Translate(coord, x_translation, 0);
            }
            yield return coord;
        }

        public static int ManhattenDistace(Coord start, Coord end)
        {
            return Math.Abs(end.Y - start.Y) + Math.Abs(end.X - start.X);
        }
    }
}
