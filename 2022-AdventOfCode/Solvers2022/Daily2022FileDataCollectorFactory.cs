﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solvers2022
{
    public class Daily2022FileDataCollectorFactory : IDataCollectorFactory
    {
        public IDataCollector CreateDataCollector(SolverIdentifier solverIdentifier)
        {
            // TODO...make sure its value
            string fileName = $"Data\\{solverIdentifier.DayString()}.txt";
            return new CachedFileDataCollector(fileName);
        }
    }
}
