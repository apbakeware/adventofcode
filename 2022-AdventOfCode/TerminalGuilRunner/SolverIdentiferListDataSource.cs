﻿using Core;
using System.Collections;
using Terminal.Gui;
using NStack;

// Tasks:
//  Only enable execute button if there is more than 1 list item click
//  Render output
//  Execute selected solvers

namespace TerminalGuilRunner
{
    // Refer to example with Terminal.Gui:ListViewWithSelection.cs
    internal class SolverIdentiferListDataSource : IListDataSource
    {

        int _nameColumnWidth = 30;
        private List<SolverIdentifier>? solverIdentifiers;
        BitArray? marks;
        int count, len, numMarked;

        public int Count => solverIdentifiers != null ? solverIdentifiers.Count : 0;

        public int Length => len;

        public List<SolverIdentifier> SolverIdentifiers
        {
            get => solverIdentifiers;
            set
            {
                if (value != null)
                {
                    count = value.Count;
                    marks = new BitArray(count);
                    solverIdentifiers = value;
                    numMarked = 0;
                    //len = GetMaxLengthItem();
                }
            }
        }

        public IList ToList() => solverIdentifiers;

        public event EventHandler<MarksUpdatedEventArgs> MarksUpdated;

        public void Render(ListView container, ConsoleDriver driver, bool selected, int item, int col, int line, int width, int start = 0)
        {
            container.Move(col, line);
            // Equivalent to an interpolated string like $"{Scenarios[item].Name, -widtestname}"; if such a thing were possible
            var s = String.Format(String.Format("{{0,{0}}}", -_nameColumnWidth), solverIdentifiers[item].ToString());
            RenderUstr(driver, $"{s} ({solverIdentifiers[item].ToString()})", col, line, width, start);
        }

        public bool IsMarked(int item)
        {
            if (item >= 0 && item < count)
            {
                return marks[item];
            }

            return false;
        }

        public void SetMark(int item, bool value)
        {
            if(item >= 0 && item < count)
            {
                if (marks[item] != value)
                {
                    marks[item] = value;
                    numMarked += value ? 1 : -1;
                    MarksUpdatedEventArgs eArgs = new() { NumberOfRowsMarked = numMarked };
                    OnRaiseMarksUpdated(eArgs);
                }               
            }
        }

        private void RenderUstr(ConsoleDriver driver, ustring ustr, int col, int line, int width, int start = 0)
        {
            // see: https://github.com/gui-cs/Terminal.Gui/blob/fc1faba7452ccbdf49028ac49f0c9f0f42bbae91/Terminal.Gui/Views/ListView.cs#L433-L461
            int used = 0;
            int index = start;
            while (index < ustr.Length)
            {
                (var rune, var size) = Utf8.DecodeRune(ustr, index, index - ustr.Length);
                var count = Rune.ColumnWidth(rune);
                if (used + count >= width) break;
                driver.AddRune(rune);
                used += count;
                index += size;
            }

            while (used < width)
            {
                driver.AddRune(' ');
                used++;
            }
        }

        private void OnRaiseMarksUpdated(MarksUpdatedEventArgs e)
        {
            MarksUpdated(this, e);
        }
    }
}