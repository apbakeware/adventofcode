﻿using Core;
using Terminal.Gui;
using Serilog;
using System.Data;

// Tasks:
//  Only enable execute button if there is more than 1 list item click
//  Render output
//  Execute selected solvers

namespace TerminalGuilRunner
{
    internal class ResultsWindow
    {
        public Window? Win { get; set; }
        private ILogger logger;
        private TableView tableView;
        private DataTable dataTable;

        public ResultsWindow()
        {
            logger = Log.ForContext<ResultsWindow>();
        }

        public void Init(Pos WinX, Pos WinY, Dim WinWidth, Dim WinHeight)
        {
            Win = new Window("Solver Results")
            {
                X = WinX,
                Y = WinY,
                Width = WinWidth,
                Height = WinHeight
            };

            dataTable = new DataTable();

            dataTable.Columns.Add("Solver", typeof(string));
//            dataTable.Columns.Add("ReadTime", typeof(TimeSpan));
//            dataTable.Columns.Add("InjestTime", typeof(TimeSpan));
            dataTable.Columns.Add("SolveTime", typeof(TimeSpan));
            dataTable.Columns.Add("Result", typeof(string));

            tableView = new TableView()
            {
                X = 0,
                Y = 0,
                Width = Dim.Fill(),
                Height = Dim.Fill(1),
                Table = dataTable
            };

            tableView.Style.ShowHorizontalHeaderOverline = true;
            tableView.Style.ShowVerticalHeaderLines = true;
            tableView.Style.ShowHorizontalHeaderUnderline = true;
            tableView.Style.ShowVerticalCellLines = true;
            tableView.Style.ShowHorizontalScrollIndicators = true;

            Win.Add(tableView);
            tableView.Update();
        }

        public void RenderResult(SolutionTime solutionTime)
        {
            dataTable.Rows.Add(solutionTime.Solution.ProblemName,
                //solutionTime.DataCollectionDuration,
                //solutionTime.InputIngestDuration,
                solutionTime.SolverDuration,
                solutionTime.Solution.Solution()
            );
            tableView.Update();
        }
    
        public void ClearResults()
        {
            dataTable.Clear();
            Win.SetNeedsDisplay();
        }
    }
}