﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerminalGuilRunner
{
    internal class ExecutionRequestEventArgs : EventArgs
    {
        public List<SolverIdentifier>? SolverIdentifiers { get; set; }
        public bool UseSampleData { get; set; }
    }

    internal class MarksUpdatedEventArgs : EventArgs
    {
        public int NumberOfRowsMarked { get; set; }
    }
}
