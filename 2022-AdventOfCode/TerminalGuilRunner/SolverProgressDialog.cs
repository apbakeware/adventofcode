﻿using Terminal.Gui;

// Tasks:
//  Only enable execute button if there is more than 1 list item click
//  Render output
//  Execute selected solvers

namespace TerminalGuilRunner
{
    internal class SolverProgressDialog
    {
        public Dialog dialog;
        private ProgressBar progressBar;

        public SolverProgressDialog()
        {
            dialog = new Dialog("Solver progress");

            progressBar = new()
            {
                X = 1,
                Y = 1,
                Width = Dim.Fill(),
                Height = 1,
                Fraction = 0.25F,
            };

            dialog.Add(progressBar);
        }


    }
}