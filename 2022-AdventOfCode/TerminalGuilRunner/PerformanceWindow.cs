﻿using Core;
using Terminal.Gui;
using Serilog;
using Terminal.Gui.Graphs;

namespace TerminalGuilRunner
{
    internal class PerformanceWindow
    {
        public Window? Win { get; set; }
        private ILogger logger;
        private GraphView graphView;

        private BarSeries barSeries;

        public PerformanceWindow()
        {
            logger = Log.ForContext<PerformanceWindow>();
        }

        public void Init(Pos WinX, Pos WinY, Dim WinWidth, Dim WinHeight)
        {


            Win = new Window("Performance")
            {
                X = WinX,
                Y = WinY,
                Width = WinWidth,
                Height = WinHeight
            };

            var btnView = new View()
            {
                X = 0,
                Y = 1,
                Width = 10,
                Height = Dim.Fill()
            };

            var btnPageUp = new Button("UP")
            {
                X = 1,
                Y = 1,
                Width = 6,
                Height = 1
            };

            var btnZoomIn = new Button("Z+")
            {
                X = 1,
                Y = Pos.Bottom(btnPageUp),
                Width = 6,
                Height = 1
            };

            var btnZoomOut = new Button("Z-")
            {
                X = 1,
                Y = Pos.Bottom(btnZoomIn),
                Width = 6,
                Height = 1
            };

            var btnPageDown = new Button("DN")
            {
                X = 1,
                Y = Pos.Bottom(btnZoomOut),
                Width = 6,
                Height = 1
            };

            graphView = new GraphView()
            {
                X = Pos.Right(btnView),
                Y = 1,
                Width = Dim.Fill(),
                Height = Dim.Fill()
            };

            SetupGraphView();

            btnPageUp.Clicked += () => graphView.PageUp();
            btnPageDown.Clicked += () => graphView.PageDown();

            btnView.Add(btnPageUp, btnZoomIn, btnZoomOut, btnPageDown);
            Win.Add(btnView, graphView);
        }

        private void SetupGraphView()
        {
            barSeries = new BarSeries()
            {
                Bars = new List<BarSeries.Bar>()
                {
                }
            };

            graphView.Series.Add(barSeries);

            barSeries.Orientation = Orientation.Horizontal;

            // How much graph space each cell of the console depicts
            graphView.CellSize = new PointF(0.1f, 1f);
            // No axis marks since Bar will add it's own categorical marks
            graphView.AxisY.Increment = 0f;
            graphView.AxisY.ShowLabelsEvery = 1;
            graphView.AxisY.Text = "Solver";
            graphView.AxisY.Minimum = 0;

            graphView.AxisX.Increment = 0.1f;
            graphView.AxisX.ShowLabelsEvery = 10;
            //graphView.AxisX.LabelGetter = v => v.Value.ToString("N2");
            graphView.AxisX.Text = "ms";
            graphView.AxisX.Minimum = 0;

            // leave space for axis labels and title
            graphView.MarginBottom = 2;
            graphView.MarginLeft = 10u;

            graphView.SetNeedsDisplay();
        }

        public void RenderResult(SolutionTime solutionTime)
        {
            // Something is wrong here, its not rendering the content bar, but lable does

            var softStiple = new GraphCellToRender('\u2591');
            //var mediumStiple = new GraphCellToRender('\u2592');
            var mediumStiple = new GraphCellToRender('\u25a9');
            
            barSeries.Bars.Add(new BarSeries.Bar(solutionTime.Solution.ProblemName, mediumStiple, (float)solutionTime.SolverDuration.TotalMilliseconds));
            //barSeries.Bars.Add(new BarSeries.Bar(solutionTime.Solution.ProblemName, mediumStiple, (float)solutionTime.SolverDuration.TotalSeconds));
            //graphView.MarginLeft = (uint)barSeries.Bars.Max(b => b.Text.Length) + 2;
            graphView.SetNeedsDisplay();
        }

        public void ClearResults()
        {
            barSeries.Bars.Clear();
            Win.SetNeedsDisplay();
        }
    }
}