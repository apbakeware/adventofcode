﻿using Core;
using System.Collections;
using System.Threading.Tasks.Sources;
using Terminal.Gui;
using Serilog;
using System;
using System.Security.AccessControl;
using Serilog.Core;
using Serilog.Events;

// Tasks:
//  Only enable execute button if there is more than 1 list item click
//  Render output
//  Execute selected solvers

namespace TerminalGuilRunner
{
    public class TerminalGuiRunner
    {
        private ILogger logger;
        private IDataCollectorFactory dataCollectorFactory;
        private IDataCollectorFactory sampleDataCollectorFactory;
        private ISolverExecutorFactory solverExecutorFactory;

        private SelectionWindow selectionWindow;
        private ResultsWindow resultsWindow;
        private PerformanceWindow performanceWindow;
        private LoggingLevelSwitch loggingLevelSwitch;

        // TODO: Status bar at the bottom

        private ListView lstView;
        private ArrayList lstViewData;

        public TerminalGuiRunner(
            IDataCollectorFactory dataCollectorFactory,
            IDataCollectorFactory sampleDataCollectorFactory,
            ISolverExecutorFactory solverExecutorFactory,
            LoggingLevelSwitch loggingLevelSwitch)
        {
            logger = Log.ForContext<TerminalGuiRunner>();
            this.dataCollectorFactory = dataCollectorFactory ?? throw new ArgumentNullException(nameof(dataCollectorFactory));
            this.sampleDataCollectorFactory = sampleDataCollectorFactory ?? throw new ArgumentNullException(nameof(sampleDataCollectorFactory));
            this.solverExecutorFactory = solverExecutorFactory ?? throw new ArgumentNullException(nameof(solverExecutorFactory));
            this.loggingLevelSwitch = loggingLevelSwitch ?? throw new ArgumentNullException(nameof(loggingLevelSwitch));

            selectionWindow = new ();
            resultsWindow = new ();
            performanceWindow = new();
        }

        public void Run()
        {
            Application.Init();
            var menu = new MenuBar(new MenuBarItem[] {
                new MenuBarItem ("_File", new MenuItem [] {
                    new MenuItem ("_Quit", "", () => {
                        Application.RequestStop ();
                    })
                }),
                new MenuBarItem ("_Results", new MenuItem [] {
                    new MenuItem ("_Clear", "", () => {
                        ClearResults ();
                    })
                }),
                new MenuBarItem ("_Logging", new MenuItem [] {
                    new MenuItem ("Verbose", "", () => {
                        loggingLevelSwitch.MinimumLevel = LogEventLevel.Verbose;
                    }),
                    new MenuItem ("Debug", "", () => {
                        loggingLevelSwitch.MinimumLevel = LogEventLevel.Debug;
                    }),
                    new MenuItem ("Info", "", () => {
                        loggingLevelSwitch.MinimumLevel = LogEventLevel.Information;
                    }),
                    new MenuItem ("Warn", "", () => {
                        loggingLevelSwitch.MinimumLevel = LogEventLevel.Warning;
                    }),
                    new MenuItem ("Error", "", () => {
                        loggingLevelSwitch.MinimumLevel = LogEventLevel.Error;
                    }),
                    new MenuItem ("Fatal", "", () => {
                        loggingLevelSwitch.MinimumLevel = LogEventLevel.Fatal;
                    })
                }),
            });

            selectionWindow.Init(0, 0, Dim.Percent(20.0f), Dim.Fill());
            resultsWindow.Init(Pos.Right(selectionWindow.Win), 0, Dim.Fill(), Dim.Percent(60.0f));
            performanceWindow.Init(Pos.Right(selectionWindow.Win), Pos.Bottom(resultsWindow.Win), Dim.Fill(), Dim.Fill());

            selectionWindow.ExecuteSolvers += SelectionWindow_ExecuteSolvers;

            // Add both menu and win in a single call
            //Application.Top.Add(menu, CreateIdentifierWindow());
            Application.Top.Add(menu, selectionWindow.Win, resultsWindow.Win, performanceWindow.Win);
            Application.Run();
            Application.Shutdown();
        }

        private async void SelectionWindow_ExecuteSolvers(object? sender, ExecutionRequestEventArgs e)
        {
            logger.Debug("Handled ExecuteSolvers -- UseTestData? {TestData}", e.UseSampleData);
            // todo: async

            var dataCollectorFactoryToUse = e.UseSampleData ? sampleDataCollectorFactory : dataCollectorFactory;
            foreach (var solverIdentifer in e.SolverIdentifiers)
            {
                try
                {
                    // TODO: may ExecuteAsync
                    var executor = solverExecutorFactory.CreateSolver(dataCollectorFactoryToUse, solverIdentifer);
                    var solverResult = await Task.Run(() => executor.Execute());
                    if (solverResult != null)
                    {
                        logger.Information("Solver Complete -- Name: {Name}  Solution: {Solution}",
                            solverResult.Solution.ProblemName,
                            solverResult.Solution.Solution());
                        resultsWindow.RenderResult(solverResult);
                        performanceWindow.RenderResult(solverResult);
                    }
                }
                catch (Exception ex)
                {
                    // todo: handle message
                }
            }
        }

        private Window CreateResultsWindow()
        {
            var win = new Window();

            return win;
        }

        private void ClearResults()
        {
            resultsWindow.ClearResults();
            performanceWindow.ClearResults();
        }
    }
}